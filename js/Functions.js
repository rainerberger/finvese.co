
$(document).ready(function() {



var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
function isTouchDevice(){
    return typeof window.ontouchstart !== 'undefined';
}
//alert(!isTouchDevice());




//
//
//  ████████╗ ██████╗ ██████╗     ███╗   ███╗███████╗███╗   ██╗██╗   ██╗
//  ╚══██╔══╝██╔═══██╗██╔══██╗    ████╗ ████║██╔════╝████╗  ██║██║   ██║
//     ██║   ██║   ██║██████╔╝    ██╔████╔██║█████╗  ██╔██╗ ██║██║   ██║
//     ██║   ██║   ██║██╔═══╝     ██║╚██╔╝██║██╔══╝  ██║╚██╗██║██║   ██║
//     ██║   ╚██████╔╝██║         ██║ ╚═╝ ██║███████╗██║ ╚████║╚██████╔╝
//     ╚═╝    ╚═════╝ ╚═╝         ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝ ╚═════╝ 
//


	/* TOP MENU */
	$('#MainMenuTop1st > li:has(ul.MainMenuSub)').addClass('parent');
	$('ul.MainMenuSub > li:has(ul.MainMenuSub) > a').addClass('parent');

	$('#MainMenuTopToggle').click(function() {
		$('#MainMenuTop1st').slideToggle(300);
		$( 'nav.MainMenuTop' ).toggleClass( "fullscreen" );
		$( this ).toggleClass( "is-active" );
		return false;
	});

	$(window).resize(function() {
		if (!isTouchDevice() && $( "#MainMenuTopToggle" ).is( ".is-active" ) ) {
			$('#MainMenuTop1st').slideUp(300);
			$( 'nav.MainMenuTop' ).removeClass( "fullscreen" );
			$('#MainMenuTopToggle').removeClass('is-active');
		}		
		if ($(window).width() > 768) {
			$('#MainMenuTop1st').removeAttr('style');
		}
	});


	/* TOP MENU TOGGLER from Main Menu */
	$('#MainMenuTop1st > li:has(ul.MainMenuSub)').addClass('parent');
	$('ul.MainMenuSub > li:has(ul.MainMenuSub) > a').addClass('parent');

	$('#MainMenuTopToggle2').click(function() {
		$('#MainMenuTop1st').slideToggle(300);
		$( 'nav.MainMenuTop' ).toggleClass( "fullscreen" );
		$( this ).toggleClass( "is-active" );
		return false;
	});

	$(window).resize(function() {
		if (!isTouchDevice() && $( "#MainMenuTopToggle2" ).is( ".is-active" ) ) {
			$('#MainMenuTop1st').slideUp(300);
			$( 'nav.MainMenuTop' ).removeClass( "fullscreen" );
			$('#MainMenuTopToggle2').removeClass('is-active');
		}		
		if ($(window).width() > 768) {
			$('#MainMenuTop1st').removeAttr('style');
		}
	});










//
//
//  ███╗   ███╗ █████╗ ██╗███╗   ██╗    ███╗   ███╗███████╗███╗   ██╗██╗   ██╗
//  ████╗ ████║██╔══██╗██║████╗  ██║    ████╗ ████║██╔════╝████╗  ██║██║   ██║
//  ██╔████╔██║███████║██║██╔██╗ ██║    ██╔████╔██║█████╗  ██╔██╗ ██║██║   ██║
//  ██║╚██╔╝██║██╔══██║██║██║╚██╗██║    ██║╚██╔╝██║██╔══╝  ██║╚██╗██║██║   ██║
//  ██║ ╚═╝ ██║██║  ██║██║██║ ╚████║    ██║ ╚═╝ ██║███████╗██║ ╚████║╚██████╔╝
//  ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝    ╚═╝     ╚═╝╚══════╝╚═╝  ╚═══╝ ╚═════╝ 
//

	$('#MainMenu1st > li:has(ul.MainMenuSub)').addClass('parent');
	$('ul.MainMenuSub > li:has(ul.MainMenuSub) > a').addClass('parent');

	$('#MainMenuToggle').click(function() {
		$('#MainMenu1st').slideToggle(300);
		$( 'nav.MainMenu' ).toggleClass( "fullscreen" );
		$( this ).toggleClass( "is-active" );
		return false;
	});

	$(window).resize(function() {
		if (!isTouchDevice() && $( "#MainMenuToggle" ).is( ".is-active" ) ) {
			$('#MainMenu1st').slideUp(300);
			$( 'nav.MainMenu' ).removeClass( "fullscreen" );
			$('#MainMenuToggle').removeClass('is-active');
		}		

		if ($(window).width() > 768) {
			$('#MainMenu1st').removeAttr('style');
		}
	});





/*  F U L L S C R E E N   */

    $(".BtnFullscreen").click(function () {
        
        var $this = $(this);
    
        if ($this.hasClass('Max')){
            $this.removeClass('Max');
            $this.addClass('Min');
        }else if ($this.hasClass('Min')){
            $this.removeClass('Min');
            $this.addClass('Max');
        }
        $(this).closest('.TileContainer').toggleClass('Fullscreen');
    });

    $(".BtnFullscreen2").click(function () {
        
        var $this = $(this);
    	console.log($this);

		var chart = null;
		var chartWidth = 0;
		var chartHeight = 0;

		// Look for enclosing tile
		var t = $this[0].offsetParent;
		// Check if it contains a chart
		var qq = t.getElementsByClassName('hchart');
		if (qq.length > 0) {
			console.log("AA found chart");
			chart = qq[0];
			console.log(chart);
			chartWidth = $(chart).highcharts().chartWidth;
			chartHeight = $(chart).highcharts().chartHeight;
		}

		/*var z = document.querySelectorAll('.TileContainer');
		console.log(z);
		for (var i = 0; i < z.length; i++) {
    		var c = z[i].getElementsByClassName('hchart');
    		if (c.length > 0) {
    			console.log("found a chart");
    			chart = c[0];
    		}
    		console.log(c);
		}*/

        if ($this.hasClass('Max')){
            $this.removeClass('Max');
            $this.addClass('Min');
            if (chart) {
            	$(chart).highcharts().setSize(chartWidth*2,chartHeight*2);
            	//$('#assetBonds').highcharts().setSize(500,700);
            }
        }else if ($this.hasClass('Min')){
            $this.removeClass('Min');
            $this.addClass('Max');
            if (chart) {
            	$(chart).highcharts().setSize(chartWidth/2,chartHeight/2);
            	//$('#assetBonds').highcharts().setSize(250,350);
            }
        }
        $(this).closest('.TileContainer').toggleClass('Fullscreen');
    });

});

