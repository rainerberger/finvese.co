finvese.config(function($routeProvider, $locationProvider, $provide) {
  $routeProvider     
    .when('/signup', {
      templateUrl: 'templates/authentication/signup.html',
      controller: 'SignupController'
    }).when('/signup/:showMessage', {
      templateUrl: 'templates/authentication/signup.html',
      controller: 'SignupController'
    }).when('/verify', {
      templateUrl: 'templates/authentication/verification.html',
    }).when('/signout', {        
      templateUrl: 'templates/home.html',
      controller: 'SignoutController'
    }).when('/myprofile', {
      templateUrl: 'templates/authentication/myprofile.html',
      controller: 'MyProfileController'
    }).when('/verify_email', {
      templateUrl: 'templates/authentication/verify_email.html',
      controller: 'SignupVerifyEmailController'
    }).when('/users/password/edit', {
      templateUrl: 'templates/authentication/forget/change_password.html',
      controller: 'ForgetPasswordChangeController'
    }).when('/users/confirmation', {
      templateUrl: 'templates/authentication/confirm_email.html',
      controller: 'ConfirmEmailController'
    }).when('/forget', {
      templateUrl: 'templates/authentication/forget.html',
      controller: 'ForgetPasswordController'
    }).when('/forget_password/verify', {
      templateUrl: 'templates/authentication/forget/verify.html',
      controller: 'ForgetPasswordVerifyController'
    }).when('/change_password', {
      templateUrl: 'templates/authentication/forget/change_password.html',
      controller: 'ForgetPasswordChangeController'
    }).when('/password/updated', {
      templateUrl: 'templates/authentication/forget/password_success.html',
      controller: 'passwordUpdatedController'
    }).when('/signin', {
      templateUrl: 'templates/authentication/signin.html',
      controller: 'SigninNewController'
    }).when('/verified/:checkStatus', {
      templateUrl: 'templates/authentication/verification.html',
      controller: 'VerifiedController'
    })
})

finvese.controller('VerifiedController', function($http, $q, $scope, $location, $route, $rootScope,localStorageService){
  $rootScope.login = false;
  $scope.checkStatus = $route.current.params.checkStatus;

})


finvese.controller('SignoutController', function($http, $scope, $q, $location, $rootScope,localStorageService){
  //console.log("in SignoutController");
  $rootScope.login = false;
  $rootScope.chat_app = false;
  $rootScope.register = false;
  localStorageService.remove('current_user');
  localStorageService.remove('current_login');
  window.localStorage.removeItem('user_token_sso');
  localStorageService.remove('user_token');
  $rootScope.current_user = null;
  //console.log(localStorageService.get('current_user'));
  $scope.goToSiteUrl('/');
  $rootScope.signinAdvisor = false;

})

finvese.controller('SignupController', function($http, $q, $location, $route, $rootScope, $scope, $sce, $timeout, UserAuthenticate, Countries,localStorageService){
  $rootScope.login = false;
  $rootScope.register = true;
  $rootScope.home = false;
  $rootScope.resetLabels = function(){
          $('.form-block input').each(function(){
               $(this).siblings().addClass('active');
           });
          
        }
  $rootScope.dropdown = function(){ 
    $('.dropdown-button').dropdown();
  }
  $rootScope.countries = localStorageService.get('countries');
  // localStorageService.remove('countries');
  
  console.log($rootScope.countries);
  if(!$rootScope.countries){
    var myCountriesPromise = Countries.all();
    myCountriesPromise.then(function(result) {  // this is only run after $http completes
      
      console.log(result);
      //$rootScope.countries = result.data;
      $rootScope.countries = result;
      //localStorageService.add('countries', result.data);
      //result.data.unshift({'name': 'All Countries'});
      $rootScope.select_countries = $rootScope.countries;
      if($rootScope.select_countries[0] && $rootScope.select_countries[0]['name'] == 'All Countries')
        delete $rootScope.select_countries[0];
      
      $rootScope.selectCountries = _.compact($rootScope.select_countries);
    });

  }else{
    $rootScope.select_countries = $rootScope.countries;
    if($rootScope.select_countries[0] && $rootScope.select_countries[0]['name'] == 'All Countries')
      delete $rootScope.select_countries[0];
    
    $rootScope.selectCountries = _.compact($rootScope.select_countries);
  }

 
  $scope.clicked_dropdown = false;
  $rootScope.CloseCountryDropDown = function(clicked_dropdown){
    $timeout(function() {
      console.log(clicked_dropdown);
      if(!clicked_dropdown){
        el = angular.element(document.querySelector('#myCountry1.open'));
        console.log(el.length);
        if(el.length > 0){
          el.removeClass('open');
        }
      }else{
        el = angular.element(document.querySelector('#myCountry1.open'));

        if(el.length > 0){
          console.log(el);
          el.removeClass('open');
        }else{
          $timeout(function() {
            el_m = angular.element(document.querySelector('#myCountry1.open'));
            console.log(el_m);
            if(el_m.length == 0){
              el = angular.element(document.querySelector('#myCountry1'));
              // el[0].click();
            }
          }, 100);
        }
                
        
      }
      $scope.clicked_dropdown = false;
      
    }, 100);    
    // $rootScope.country_drop = true; 
  }

  $rootScope.selectUserCountry = function(country){
    $rootScope.selected_user_country_name = country.name;
    $rootScope.user.region = country;

  }


  $scope.no_proper_country_selected = true;

  if($route.current && $route.current.params && $route.current.params.showMessage)
    $scope.info_message=true;
    // $scope.info_message="You need to sign up to continue or You need to log in to continue"

  $scope.checkUniq = function(){
    
    if($rootScope.user.email){
      var myAuthenticatePromise = UserAuthenticate.uniqPhone({email: $rootScope.user.email});
      myAuthenticatePromise.then(function(result) {  // this is only run after $http completes
        $scope.already_taken = false;
      }, 
      function (error) {
        $scope.already_taken = true;
        console.log("$scope.user.phone$scope.user.phone");
        //console.log($rootScope.user);
        //console.log($rootScope.user.phone);
        //console.log($rootScope.user.phone.toString().length);
        //$scope.user_phone = $rootScope.user.phone.toString().length;
        $scope.success_message = null;
      });
    }else{
      console.log("empty message");
    };
  };

  $scope.checkCountry = function(){
    console.log('$rootScope.user.region');
    console.log($rootScope.user.region);
    $rootScope.regionSelected = $scope.user.region;
    if($rootScope.user.region===undefined){
      $scope.not_proper_country = true;
    }else{
      $scope.not_proper_country = false;
    };
  };

  $scope.changePasswordText = function(){
    if($scope.password_text == 'password' || $scope.password_text == undefined)
      $scope.password_text = 'text';
    else
      $scope.password_text = 'password';
  };
  
 $scope.signup_completed = true;

  $scope.update = function(user){
  $rootScope.dropdown = function(){
    $('.dropdown-button').dropdown();
  }

    $('.form-block input').each(function(){
         $(this).siblings().addClass('active');
    });
    // $rootScope.user.region = $rootScope.regionSelected;
    console.log('$scope.user.region111');
    console.log($rootScope.user);  
    // user.region = $scope.user.region;
    if((user.email === undefined && user.phone === undefined && user.region === undefined) || user.first_name === undefined ||  user.password === undefined){      
        return false;
      }
    if(user.region && user.region.phone_code && user.phone)
      $rootScope.phone = user.region.phone_code+user.phone;
    
    if(user.region)
      user.region = _.findWhere($rootScope.countries, {'iso':'US'});

    $scope.showLoader = true;
    // else
      // $rootScope.ema = user.email;
    console.log(user.region);
    //user_params = {contact_type: 'sms', first_name: user.first_name, country_id: user.region.id, phone: $rootScope.phone, password: user.password, 
    user_params = {contact_type: 'sms', first_name: user.first_name, country_id: 1, phone: $rootScope.phone, password: user.password, 
        profile_image_id: user.profile_image_id, preferred_messenger_name: user.preferred_messenger_name, email: user.email, last_name: user.last_name };
    
    console.log(user_params);
    $scope.signup_completed = false;
    var myAuthenticatePromise = UserAuthenticate.userSignup(user_params);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes      
      $scope.signup_completed = true;
      localStorageService.add('verify_current_user', result);
      // $rootScope.current_user = result;
      $scope.error_message = null;
      $scope.showLoader = false;
      $scope.success_message = 'Signed up successfully! please verify code';
      if(user.email && user.email.length < 0)
        $location.path('/verify');
      else
        //$location.path('/verify_email');
        $location.path('/signin');
      }, 
      function (error) {
        $scope.showLoader = false;
        $scope.signup_completed = true;
        localStorageService.remove('user_token');
        $scope.error_message = 'Invalid username and password';
        $scope.success_message = null;
      }

    )
  };

  $rootScope.countries = localStorageService.get('countries');
  user_details = localStorageService.get('verify_current_user');

  selected_country = null;

  if(user_details && user_details.country_id)
    selected_country = _.findWhere($rootScope.countries, {id: user_details.country_id});


  if((selected_country === null || !selected_country) && $rootScope.countries)
    selected_country = _.findWhere($rootScope.countries, {'iso':'US'});

  if(user_details){
    selected_country = _.findWhere($rootScope.countries, {id: user_details.country_id});  
    if(selected_country)
      phone_number = parseInt(user_details.phone.replace(selected_country.phone_code, ''));
    $rootScope.user = {region: selected_country}
    $rootScope.regionSelected = selected_country;
  }else{
    $rootScope.regionSelected = selected_country;
    $rootScope.user = {region: selected_country};
  }

  $scope.updateGoogle('Signup Page');

})

finvese.controller('SignupVerifyEmailController', function($http, $q, $location, $rootScope, $scope, localStorageService, UserAuthenticate){
  
 
})

finvese.controller('SignupVerifyController', function($http, $q, $location, $rootScope, $scope, localStorageService, UserAuthenticate){
  
  $rootScope.login = false;
  $rootScope.home_selected = false;
  user_obj = localStorageService.get('verify_current_user');
  $rootScope.user = {phone: user_obj.phone, user_id: user_obj.id};
  $rootScope.show_menu = false;

  $rootScope.store_page = false;
  $rootScope.event_page = false;    
  $rootScope.exclusive_event_page = false;
  $rootScope.signup_page = true;
  $rootScope.signin_page = false;
  $rootScope.suggest_events_page = false;
  $rootScope.luxepe_menu = false;
  $rootScope.luxepedia_menu = false;
  $rootScope.make_black = false;

  $scope.callNumber = function(user){
    var myCallPromise = UserAuthenticate.callNumberSignup(user);
    myCallPromise.then(function(result){

    });
  };

  $scope.resendMessage = function(user){
    var myCallPromise = UserAuthenticate.smsNumberSignup(user);
    myCallPromise.then(function(result){

    });
    $scope.resend = $scope.resend+1;
  }

  $scope.changeNumber = function(){
    $location.path('/signup');
  }
  $scope.verify_completed = true;
  $scope.submit = function(user){
    $scope.verify_completed = false;
    var myAuthenticatePromise = UserAuthenticate.verifySignup(user);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes            
      localStorageService.add('current_user', result.user);
      localStorageService.add('user_token', result.token);
      $scope.verify_completed = true;
      $rootScope.current_user = result;
      localStorageService.remove('verify_current_user');
      $location.path('/home');
    }, 
      function (error) {
        $scope.verify_completed = true;
        localStorageService.remove('user_token');
        $scope.error_message = true;
        $scope.success_message = null;
      });
  };
})



finvese.controller('ConfirmEmailController', function($scope, $rootScope, $route, $routeParams, $location, localStorageService, UserAuthenticate){
  $scope.reset_token = $route.current.params.confirmation_token;
  user = {};
  $scope.submit = function(){
    $scope.verify_completed = false;
    user.confirmation_token = $scope.reset_token;
    var myAuthenticatePromise = UserAuthenticate.verifySignupEmail(user);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes            
      $scope.verify_completed = true;
      localStorageService.add('current_user', result.user);
      localStorageService.add('user_token', result.token);

      $rootScope.current_user = result;
      localStorageService.remove('verify_current_user');
      $location.path('/verified/success');
    }, 
      function (error) {
        $scope.verify_completed = true;
        localStorageService.remove('user_token');
        $scope.error_message = true;
        $scope.success_message = null;
        $location.path('/verified/failure');
      });
  };

  $scope.submit();

})


finvese.controller('VerifyPasswordEmailController', function($scope, $rootScope, $location, localStorageService, ForgetPassword){

})


finvese.controller('ForgetPasswordEmailVerifyController', function($scope, $rootScope, $location, localStorageService, UserAuthenticate){
 
})

finvese.controller('ForgetPasswordVerifyController', function($scope, $rootScope, $location, localStorageService, UserAuthenticate){
  
  $rootScope.login = false;
  localStorageService.remove('forget_password_user_id');
  $scope.forgetpassword_verify_completed = true;
  $scope.verifyCode = function(user){  
    if(user.pin === undefined)
     {
        $scope.pin_errors = true;
        return false;
     }    
    $scope.pin_errors = false;
    $scope.forgetpassword_verify_completed = false;
    var myAuthenticatePromise = UserAuthenticate.verifySignup(user);
    myAuthenticatePromise.then(
      function(result) {  // this is only run after $http completes
        console.log(result);
        $scope.forgetpassword_verify_completed = true;
        localStorageService.add('forget_password_user_id', result.user);
        $scope.renderPage('/change_password');
      }, 
      function (error) {
        $scope.forgetpassword_verify_completed = true;
        $scope.error_message = "Invalid pin! please try again later.";
      }
    );
  };

})

finvese.controller('ForgetPasswordController', function($scope, $rootScope, $location, localStorageService, ForgetPassword){
  $rootScope.login = false;
  $rootScope.register = true;
  $rootScope.home = false;
  $rootScope.user = {};
  
  
  localStorageService.remove('forget_password_phone');
  $scope.forgetpassword_completed = true;
  $scope.sendCode = function(user){   
    
    $scope.forgetpassword_completed = false;
    data_query = {password: user.password, password_confirmation: user.password};
    console.log(user);
    var myForgetPasswordPromise = ForgetPassword.updateProfile(data_query);
    myForgetPasswordPromise.then(
      function(result) {  // this is only run after $http completes
        $scope.forgetpassword_change_completed = true;
        $scope.renderPage('/password/updated');
      }, 
      function (error) {
        $scope.forgetpassword_change_completed = true;
        $scope.error_message = "Invalid password! please try again later.";
      }
    );
  };

})

finvese.controller('ForgetPasswordChangeController', function($scope, $rootScope, $route, $routeParams, $location, localStorageService, ForgetPassword){
  
  $rootScope.login = false;
  $scope.phone = localStorageService.get('forget_password_phone');
  user_id = localStorageService.get('forget_password_user_id');
  $scope.user = {id: user_id, email: $scope.phone};
  $scope.password_confirmation_errors = false;
  $scope.checkConfirmPassword = function(user){
    // $scope.password_confirmation_errors = true;
    if(user.password !== user.password_confirmation){
      $scope.form1.$invalid = true;
      $scope.password_confirmation_errors = true;
      return true;
    }
    else{
      $scope.form1.$invalid = false;
      $scope.password_confirmation_errors = false;
      return false;
    }
      
  }


  $scope.user.reset_password_token = $route.current.params.reset_password_token;
  
  // if($scope.user.reset_password_token){
  //   console.log($scope.user);
  //   var myForgetPasswordPromise = ForgetPassword.verifyPasswordToken($scope.user);
  //   myForgetPasswordPromise.then(
  //     function(result) {  // this is only run after $http completes
  //       $scope.forgetpassword_change_completed = true;
  //       // $scope.renderPage('/signin');
  //     }, 
  //     function (error) {
  //       $scope.forgetpassword_change_completed = true;
  //       $scope.error_message = "Invalid token! please try again later.";
  //       $scope.renderPage('/signin');
  //     }
  //   );
  // }
  
  $scope.forgetpassword_change_completed = true;


  $scope.updatePassword = function(user){
    console.log("in updatePassword");
    console.log($rootScope.current_user);
    user.id = $rootScope.current_user.user.id;
    user.email = $rootScope.current_user.user.email;    
    $scope.forgetpassword_change_completed = false;
    console.log(user);
    var myForgetPasswordPromise = ForgetPassword.updatePassword(user);
    myForgetPasswordPromise.then(
      function(result) {  // this is only run after $http completes
        $scope.forgetpassword_change_completed = true;
        $scope.renderPage('/password/updated');
      }, 
      function (error) {
        $scope.forgetpassword_change_completed = true;
        $scope.error_message = "Invalid password! please try again later.";
      }
    );
  }

})

finvese.controller('passwordUpdatedController', function($scope, $rootScope, $route, $routeParams, $location, localStorageService, ForgetPassword){
 
})
finvese.controller('ShareController', function($scope, $rootScope, $location, localStorageService, Contactus){
  $rootScope.aboutus_page = false;
  $rootScope.carat_cart_page = false;
  $rootScope.contactus_page = false;
  $rootScope.suggest_events_page = false;
  $rootScope.signup_page = false;
  $rootScope.signin_page = false;
  $rootScope.description_page = false;
  $rootScope.search_page = false;
  $rootScope.profile_page = false;
  $rootScope.fourCs_page = true;
  $scope.success_message = null;
  $scope.error_message = null;
  key = localStorageService.get('language');

  if(!key)
    key = 'en';

  var myContactusPromise = Contactus.last();
  myContactusPromise.then(function(result) {  // this is only run after $http completes
      $rootScope.contactus = result.contactus;
      $rootScope.contactus = _.findWhere(result.contactus, {supported_language : key});
      console.log($rootScope.contactus);
    }, 
    function (error) {
      
    }
  );

})





finvese.controller('MyProfileController', function($scope, $rootScope, $location, localStorageService, UserAuthenticate){
  $rootScope.aboutus_page = false;
  $rootScope.carat_cart_page = false;
  $rootScope.contactus_page = false;
  $rootScope.suggest_events_page = false;
  $rootScope.signup_page = false;
  $rootScope.signin_page = false;
  $rootScope.description_page = false;
  $rootScope.search_page = false;
  $rootScope.profile_page = false;
  $rootScope.fourCs_page = true;
  $scope.success_message = null;
  $scope.error_message = null;
  $rootScope.show_menu = false;
  $rootScope.login = false;
  $scope.user = localStorageService.get('current_user');

  if(!$scope.user){
    return $scope.renderPage('/signin');
  }

  $scope.changePasswordText = function(){
    if($scope.password_text == 'password' || $scope.password_text == undefined)
      $scope.password_text = 'text';
    else
      $scope.password_text = 'password';
  };

  $scope.checkConfirmPassword = function(user){
    if(user.password !== user.password_confirmation)
      return true;
    else
      return false;
  }

   $scope.updatePassword = function(user){  
   if((user.exisiting_password === undefined) || (user.password === undefined) || (user.password_confirmation === undefined))
    {
      return false
    }  
    else if(user.password != user.password_confirmation)
    {
      return false
    }
    user_params = {exisiting_password: user.exisiting_password, password: user.password, password_confirmation: user.password_confirmation}
    var myAuthenticatePromise = UserAuthenticate.updatePassword(user_params);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes      
      // $rootScope.verifyUserResult = result;
      console.log(result);
      // $rootScope.current_user = result;
      $scope.password_error_message = null;
      $scope.password_success_message = 'Password updated successfully!';
      $rootScope.current_user = null;
      $scope.error_message = false;
      localStorageService.remove('current_user');
      localStorageService.remove('user_token');
      $location.path('/signin');
      }, 
      function (error) {
        // localStorageService.remove('user_token');
        $scope.password_error_message = 'Invalid existing password';
        $scope.password_success_message = null;
      }

    )
  };

  $scope.update = function(user){  
    user_params = {first_name: user.first_name, preferred_messenger_name: user.preferred_messenger_name, email: user.email}
    console.log(user_params);
    var myAuthenticatePromise = UserAuthenticate.updateProfile(user_params);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes      
      // $rootScope.verifyUserResult = result;
      console.log(result);
      // $rootScope.current_user = result;
      $scope.error_message = null;
      $scope.user = localStorageService.get('current_user');
      $scope.success_message = 'Profile updated successfully!';
      $scope.user.email = result.users[0].email;
      $scope.user.preferred_messenger_name = result.users[0].preferred_messenger_name;
      console.log($scope.user);
      localStorageService.add('current_user', $scope.user);
      // $location.path('/verify');
      }, 
      function (error) {
        // localStorageService.remove('user_token');
        $scope.error_message = 'Invalid name or email';
        $scope.success_message = null;
      }

    )
  };

})

convertDate = function(d) {
        var m = d.getMonth()+1;
        var ms = m.toString();
        if (m < 10) ms = "0" + ms;
        var s = d.getFullYear().toString() + ms + d.getDate().toString();
        return(s);
}

getToday = function() {
        var d = new Date();
        return(convertDate(d));
}


finvese.controller('SigninNewController', function($rootScope, $scope, $location, $route, $routeParams, $filter, $timeout, Countries, localStorageService, UserAuthenticate){
 
  $scope.errorSignin = false;  
  $scope.showLoader = false;
  $scope.signin_completed = true;
  $rootScope.login = true;
  $rootScope.home = false;
  console.log("in SigninNewController");
  $rootScope.nodashboard = true;



  $scope.update = function(user, forgot) {
    $rootScope.phone_errors = true;
    $rootScope.password_errors = true;
    console.log(user);
    user_phone = user.phone;
    // user.phone = user.region.phone_code+user.phone+"";      
    
    if((user.email === undefined ) || user.password === undefined){      
      return false;
    }
    console.log('user login');
    
    if(user.region && user.region.phone_code)
      data_query = {email: user.email+"", password: user.password}
    else
      data_query = {email: user.email, password: user.password}
    
    $scope.showLoader = true;
    $scope.signin_completed = false;
    var myAuthenticatePromise = UserAuthenticate.userAuthenticate(data_query);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes
        $scope.signin_completed = true;
        $scope.showLoader = false;
        $rootScope.signinAdvisor = true;

        localStorageService.add('current_user', result);


        var d = new Date();
        var m = d.getMonth()+1;
        var ms = m.toString();
        if (m < 10) ms = "0" + ms;
        var s = d.getFullYear().toString() + ms + d.getDate().toString();
        localStorageService.add('current_login', s);

        localStorageService.add('user_token', result.token);
        $rootScope.password_errors = false;
        if(forgot)
          $scope.renderPage('/forget');
        else
          $scope.renderPage('/overview');
          
        
		    
      }, 
      function (error) {
        $scope.showLoader = false;
        $scope.errorSignin = true;
        $scope.signin_completed = true;
        localStorageService.remove('user_token');
        user.phone = user_phone;          
        console.log(error);
        $scope.error_message = true;
        $scope.success_message = null;
      }
    );
  };

  $scope.reset = function() {
    $rootScope.user = angular.copy($scope.master);

  };

  $scope.isUnchanged = function(user) {
    return angular.equals(user, $scope.master);
  };

  $scope.reset();

  $scope.submit = function() {

  };

  selected_country = null;
  $scope.updateGoogle('Log in Page');
  // console.log('location_details');
  // console.log($rootScope.current_country);
  // console.log($rootScope.countries);
  // if($rootScope.current_country)
  //   selected_country = _.findWhere($rootScope.countries, {name: $rootScope.current_country});
  

  // if(selected_country === null || !selected_country)
  //   selected_country = _.findWhere($rootScope.all_countries_lng, {name: 'United Kingdom'});

  // $rootScope.user = {region: selected_country};
  
})


finvese.factory('UserAuthenticate', function($http, $q, $rootScope, localStorageService, myConfig) {
  var login = function(data_query) {
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/sessions", data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var logout = function(data_query) {
    var deferred = $q.defer();
    token = localStorageService.get('user_token');
    $http({method:"GET", url:myConfig.api+"/api/v1/sessions/destroy", data:data_query, headers:{"Authorization":token, "android":$rootScope.android, "ios":$rootScope.ios, "LuxeCorp": $rootScope.app_version}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var phoneUniq = function(data_query){
    var deferred = $q.defer();    
    //console.log("in signup");
    //console.log(myConfig.api+"/api/v1/users/uniq_phone?email");
    $http({method:"GET", url:myConfig.api+"/api/v1/users/uniq_phone?email="+data_query.email}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var signup = function(data_query) {
    var deferred = $q.defer();
    //console.log("in signup");
    //console.log(myConfig.api+"/users/1");
    //$http({method:"POST", url:myConfig.api+"/users.json", data:{user: data_query}}).success(function(result){
    $http({method:"POST", url:myConfig.api+"/api/v1/users/1", data:{user: data_query}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  var updateProfile = function(data_query) {
      token = localStorageService.get('user_token');
      var deferred = $q.defer();

      $http({method:"POST", url:myConfig.api+"/users/update_user", headers:{"Authorization":token}, data:{user: data_query}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });
      return deferred.promise;
    };

  var updatePassword = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/users/update_password", headers:{"Authorization":token}, data:{user: data_query}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  var verify = function(data_query) {
    var deferred = $q.defer();
    token = localStorageService.get('user_token');
    $http({method:"POST", url:myConfig.api+"/sms_verifications/verify.json", data:{user: data_query}, headers:{"Authorization":token}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var verifyEmail = function(data_query) {
    var deferred = $q.defer();
    token = localStorageService.get('user_token');
    $http({method:"GET", url:myConfig.api+"/users/confirmation", params:data_query, headers:{"Authorization":token}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var getUser = function(token) {
    var deferred = $q.defer();
    $http({method:"GET", url:myConfig.api+"/api/v1/sessions", params:{}, headers:{"Authorization":token}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };


  return { userAuthenticate: login,
  userSignup: signup,
  verifySignup: verify,
  verifySignupEmail: verifyEmail,
  uniqPhone: phoneUniq,
  logout: logout,
  updateProfile: updateProfile,
  updatePassword: updatePassword,
  getUser: getUser };
  
})

finvese.factory('Countries', function($http, $q, localStorageService, myConfig) {
  var getData = function() {
    var results = {};
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
      $http({method:"GET", url:myConfig.api+"/api/v1/countries", headers:{"Authorization":token}}).success(function(result){
        console.log("in factory");
        console.log(result);
        //results.data = result.countries;
        //deferred.resolve(results);
        deferred.resolve(result);
      });
      return deferred.promise;
    };
  return { all: getData };  
})

finvese.factory('ForgetPassword', function($http, $q, localStorageService, myConfig) {
  var sendCode = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
      $http({method:"POST", url:myConfig.api+"/api/v1/forget_passwords/send_code", data:data_query, headers:{"Authorization":token}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });;
      return deferred.promise;
    };

  var updatePassword = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
      $http({method:"POST", url:myConfig.api+"/api/v1/forget_passwords/update_password", data:data_query, headers:{"Authorization":token}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });
      return deferred.promise;
    };

  var verifyToken = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
    console.log(data_query)
      $http({method:"GET", url:myConfig.api+"/users/password/edit", params:data_query, headers:{"Authorization":token}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });
      return deferred.promise;
    };

  var updateProfile = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/users/update_user", headers:{"Authorization":token}, data:{user: data_query}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { sendCode: sendCode,
  updatePassword: updatePassword,
  verifyPasswordToken: verifyToken,
  updateProfile: updateProfile };  
})


