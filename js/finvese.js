window.finvese = angular.module('FinveseApp', ['ngMaterial','ngRoute',"mobile-angular-ui",'ngSanitize', 'LocalStorageModule']);

// finvese.controller('clientDashboardSlugController', function($rootScope, $scope, $routeParams, $route, $location){
//     $rootScope.clientSlug = $route.current.params.slug;
//     $rootScope.clientIndex = $route.current.params.clientIndex;
// })client


intToMonthName = function(num) {
  // Converts an integer (e.g. 1) into a month name (e.g., January)
  //var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  return months[num];
}

convertDateFormat = function(date_string) {
  // Convert a date in format yyyy-mm-dd to dd/mm/yyyy
  if (date_string == null) return null;

  var y = date_string.slice(0,4);
  var m = date_string.slice(5,7);
  var d = date_string.slice(8,10);
  return d + "/" + m + "/" + y;
}

getTodaysDate = function() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  if(dd<10){
    dd='0'+dd
  }
  if(mm<10){
    mm='0'+mm
  }
  var today = dd+'/'+mm+'/'+yyyy;
  return today;
}

roundOne = function(num) {
  return Math.floor(num*10.0)/10.0;
}

roundTwo = function(num) {
  return Math.floor(num*100.0)/100.0;
}

negZero = function(num) {
  if (num < 0)
    return 0.0;
  else
    return num;
}

max = function(num1, num2)
{
  if (num1 > num2)
    return num1;
  else
    return num2;
}

getDateLabels = function(end_month, end_year)
{
  // Compute the dates to be displayed in the x-axis of the charts
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  //var end_month = 6;
  //var end_year = 2016;
  var dc = [];
  var thisMonth = end_month;
  var thisYear = end_year - 5;
  for (var i=0; i<60; i++) {
    var s = months[thisMonth-1]+"-"+thisYear.toString().slice(2,4);
    dc.push(s);
    thisMonth++;
    if (thisMonth > 12) { thisMonth=1; thisYear++; }
  }
  return dc;
}

getSavingPlans = function(debug)
{
  // Look up Savings Plans
  var savingPlans = [];
  if (debug) 
  {
    //clearTable(table4);
    savingPlans = [];
    var tmp = {'saver_name': "Client", 'taxable': "Tax-Free", 'description': "Regular Savings", 
          'date_range_in': "Age", 'date_range_begin': 53, 'date_range_end': 65, 
          'measure': "$", 'amount': 20000, 'inflation': "Yes"};
          //'start': 51, 'finish': 64, 'valType': "$", 'value': 0, 'inflation': "Yes"};
    savingPlans.push(tmp);    

    tmp = {'saver_name': "Spouse", 'taxable': "Tax-deferred", 'description': "IRA", 
          'date_range_in': "Age", 'date_range_begin': 54, 'date_range_end': 55, 
          'measure': "%", 'amount': 50, 'inflation': "Yes"};
    savingPlans.push(tmp);    

    tmp = {'saver_name': "Client", 'taxable': "Tax-Free", 'description': "Roth IRA", 
          'date_range_in': "Years", 'date_range_begin': 2017, 'date_range_end': 2020, 
          'measure': "$", 'amount': 50000, 'inflation': "No"};
    savingPlans.push(tmp);    

    tmp = {'saver_name': "Spouse", 'taxable': "Tax-deferred", 'description': "403b", 
          'date_range_in': "Years", 'date_range_begin': 2021, 'date_range_end': 2027, 
          'measure': "$", 'amount': 20000, 'inflation': "Yes"};
    savingPlans.push(tmp);    

    tmp = {'saver_name': "Client", 'taxable': "Taxable", 'description': "Aggressive", 
          'date_range_in': "Age", 'date_range_begin': 53, 'date_range_end': 64, 
          'measure': "%", 'amount': 30, 'inflation': "No"};
    savingPlans.push(tmp);    

  }

  console.log("savingPlans "+ savingPlans);
  return(savingPlans);
}

getInvestmentAccounts = function(debug)
{
  // Look up Current Investment Account Balances
  //var table1 = document.getElementById('investAccountTable');
  //var rows = table1.rows.length;
  //console.log("found rows "+ rows);
  var investments = [];


  if (debug)
  {
    //clearTable(table1);
    investments = [];
    var tmp = {'account_owner': "Client", 'taxable': "Taxable", 'description': "Fidelity Taxable", 
            'account_value': 200000, 'cost_basis': 200000};
    investments.push(tmp);

    tmp = {'account_owner': "Spouse", 'taxable': "Tax-deferred", 'description': "Fidelity IRA", 
            'account_value': 10000, 'cost_basis': 0};
    investments.push(tmp);

    tmp = {'account_owner': "Client", 'taxable': "Tax-Free", 'description': "Vanguard Roth IRA", 
            'account_value': 10000, 'cost_basis': 0};
    investments.push(tmp);

    tmp = {'account_owner': "Spouse", 'taxable': "Tax-Free", 'description': "Vanguard Roth IRA", 
            'account_value': 100, 'cost_basis': 0};
    investments.push(tmp);
  }

  //console.log("total investment "+ totalInvestment);
  return(investments);
}

getIncomeNeeds = function(debug)
{
  // Look up Cash Needs
  var cashNeeds = [];

  if (debug)
  {
    cashNeeds = [];
    var tmp = {'description': "General Spending", 'date_range_in': "Age", 'expense_name': "Client", 
          'date_range_begin': 65, 'date_range_end': 85, 'annual_need': 150000, 'inflation': "Yes"};
    cashNeeds.push(tmp);

    tmp = {'description': "Payoff House", 'date_range_in': "Years", 'expense_name': "Client", 
          'date_range_begin': 2017, 'date_range_end': 2017, 'annual_need': 275000, 'inflation': "No"};
    cashNeeds.push(tmp);

    tmp = {'description': "Europe Trip", 'date_range_in': "Age", 'expense_name': "Spouse", 
          'date_range_begin': 58, 'date_range_end': 58, 'annual_need': 25000, 'inflation': "Yes"};
    cashNeeds.push(tmp);
  }

  console.log("cashNeeds "+ cashNeeds);
  return(cashNeeds);
}

getOtherIncome = function(debug)
{
  // Look up Other Income
  var otherIncome = [];
  if (debug)
  {
    //clearTable(table3);

    otherIncome = [];

    var tmp = {'description': "Social Security", 'date_range_in': "Age", 'income_name': "Client", 
          'date_range_begin': 67, 'date_range_end': 85, 'annual_flow': 20000, 'inflation': "Yes"};
    otherIncome.push(tmp);

    tmp = {'description': "Inheritance", 'date_range_in': "Year", 'income_name': "Client", 
          'date_range_begin': 2024, 'date_range_end': 2024, 'annual_flow': 1000000, 'inflation': "Yes"};
    otherIncome.push(tmp);
  }

  console.log("otherIncome "+ otherIncome);
  return(otherIncome);
}

function NormSInv(p) {
    // Normal Distribution Inverse Function
    var a1 = -39.6968302866538, a2 = 220.946098424521, a3 = -275.928510446969;
    var a4 = 138.357751867269, a5 = -30.6647980661472, a6 = 2.50662827745924;
    var b1 = -54.4760987982241, b2 = 161.585836858041, b3 = -155.698979859887;
    var b4 = 66.8013118877197, b5 = -13.2806815528857, c1 = -7.78489400243029E-03;
    var c2 = -0.322396458041136, c3 = -2.40075827716184, c4 = -2.54973253934373;
    var c5 = 4.37466414146497, c6 = 2.93816398269878, d1 = 7.78469570904146E-03;
    var d2 = 0.32246712907004, d3 = 2.445134137143, d4 = 3.75440866190742;
    var p_low = 0.02425, p_high = 1 - p_low;
    var q, r;
    var retVal;

    if ((p < 0) || (p > 1))
    {
        alert("NormSInv: Argument out of range.");
        retVal = 0;
    }
    else if (p < p_low)
    {
        q = Math.sqrt(-2 * Math.log(p));
        retVal = (((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) / ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
    }
    else if (p <= p_high)
    {
        q = p - 0.5;
        r = q * q;
        retVal = (((((a1 * r + a2) * r + a3) * r + a4) * r + a5) * r + a6) * q / (((((b1 * r + b2) * r + b3) * r + b4) * r + b5) * r + 1);
    }
    else
    {
        q = Math.sqrt(-2 * Math.log(1 - p));
        retVal = -(((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) / ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
    }

    return retVal;
}

function CDF(x) {
  // Normal Cummulative Distribution Function approximation
  s = x;
  value = x;
  for (i = 1; i <= 100; i++) {
      value = (value*x*x/(2*i+1));
      s = s + value;
  }
  result = 0.5+ 0.39894228*s*Math.exp(-(x*x)/2);
  return result;
}



getRanges = function(start_year, horizon, chosenRisk, chosenReturn, initial_invest) {
  var range1 = [];
  var range2 = [];
  var averages = [];
  //var currentYear = new Date().getFullYear();
  var currentYear = start_year;
  //var currentWealth = parseFloat(document.getElementById("intial_invest").value);
  var currentWealth = initial_invest;
  var currentSTD = 0.0;
  var tmp = [];
  var returnRate = chosenReturn/100.0;
  var riskRate = chosenRisk/100.0;
  console.log("getRanges "+returnRate+ " riskRate "+riskRate);

  averages.push([currentYear, currentWealth]);
  range1.push([currentYear, currentWealth, currentWealth]);
  range2.push([currentYear, currentWealth, currentWealth]);

  for (i = 1; i <= horizon; i++) {
      currentYear += 1;
      currentWealth = currentWealth * (1.0 + returnRate);
      currentSTD = riskRate*Math.sqrt(i);

      averages.push([currentYear, roundTwo(currentWealth)]);
      range1.push([currentYear, roundTwo(currentWealth*(1-currentSTD)), roundTwo(currentWealth*(1+currentSTD))]);
      range2.push([currentYear, roundTwo(currentWealth*(1-2*currentSTD)), roundTwo(currentWealth*(1+2*currentSTD))]);
  }
  return [range1, range2, averages];
}

getRange1 = function(horizon, chosenRisk, chosenReturn, initial_invest) {
  var range1 = [];
  var range2 = [];
  var averages = [];
  var currentYear = new Date().getFullYear();
  //var currentWealth = parseFloat(document.getElementById("intial_invest").value);
  var currentWealth = initial_invest;
  var currentSTD = 0.0;
  var tmp = [];
  var returnRate = chosenReturn/100.0;
  var riskRate = chosenRisk/100.0;

  averages.push([currentYear, currentWealth]);
  range1.push([currentYear, currentWealth, currentWealth]);
  range2.push([currentYear, currentWealth, currentWealth]);

  for (i = 1; i <= horizon; i++) {
      currentYear += 1;
      currentWealth = currentWealth * (1.0 + returnRate);
      currentSTD = riskRate*Math.sqrt(i);

      averages.push([currentYear, roundTwo(currentWealth)]);
      range1.push([currentYear, roundTwo(currentWealth*(1-currentSTD)), roundTwo(currentWealth*(1+currentSTD))]);
      range2.push([currentYear, roundTwo(currentWealth*(1-2*currentSTD)), roundTwo(currentWealth*(1+2*currentSTD))]);
  }
  return range1;
}

getRange2 = function(horizon, chosenRisk, chosenReturn, initial_invest) {
  var range1 = [];
  var range2 = [];
  var averages = [];
  var currentYear = new Date().getFullYear();
  //var currentWealth = parseFloat(document.getElementById("intial_invest").value);
  var currentWealth = initial_invest;
  var currentSTD = 0.0;
  var tmp = [];
  var returnRate = chosenReturn/100.0;
  var riskRate = chosenRisk/100.0;

  averages.push([currentYear, currentWealth]);
  range1.push([currentYear, currentWealth, currentWealth]);
  range2.push([currentYear, currentWealth, currentWealth]);

  for (i = 1; i <= horizon; i++) {
      currentYear += 1;
      currentWealth = currentWealth * (1 + returnRate);
      currentSTD = riskRate*Math.sqrt(i);

      averages.push([currentYear, roundTwo(currentWealth)]);
      range1.push([currentYear, roundTwo(currentWealth*(1-currentSTD)), roundTwo(currentWealth*(1+currentSTD))]);
      range2.push([currentYear, roundTwo(currentWealth*(1-2*currentSTD)), roundTwo(currentWealth*(1+2*currentSTD))]);
  }

  return range2;
}

getAverages = function(horizon, chosenRisk, chosenReturn, initial_invest) {
  var range1 = [];
  var range2 = [];
  var averages = [];
  var currentYear = new Date().getFullYear();
  //var currentWealth = parseFloat(document.getElementById("intial_invest").value);
  var currentWealth = initial_invest;
  var currentSTD = 0.0;
  var tmp = [];
  var returnRate = chosenReturn/100.0;
  var riskRate = chosenRisk/100.0;

  averages.push([currentYear, currentWealth]);
  range1.push([currentYear, currentWealth, currentWealth]);
  range2.push([currentYear, currentWealth, currentWealth]);

  for (i = 1; i <= horizon; i++) {
      currentYear += 1;
      currentWealth = currentWealth * (1 + returnRate);
      currentSTD = riskRate*Math.sqrt(i);

    //tmp = [currentYear, $scope.roundTwo(currentWealth)];
    //averages.push(tmp);
      averages.push([currentYear, roundTwo(currentWealth)]);
      range1.push([currentYear, roundTwo(currentWealth*(1-currentSTD)), roundTwo(currentWealth*(1+currentSTD))]);
      range2.push([currentYear, roundTwo(currentWealth*(1-2*currentSTD)), roundTwo(currentWealth*(1+2*currentSTD))]);
  }

  return averages;
}

//============================================================

setAdvisorMenuLabels = function($rootScope) {
  var label = document.getElementById('menuAdvisor');
  if (label) label.textContent = $rootScope.labels["L0003"];
  //label = document.getElementById('menuAdvisorDetails');
  //label.textContent = $rootScope.labels["L0004"];
  //label = document.getElementById('menuAdvisorDashboard');
  //label.textContent = $rootScope.labels["L0005"];
  //label = document.getElementById('menuAdvisorAllocation');
  //label.textContent = $rootScope.labels["L0006"];

  label = document.getElementById('dashboardLabel1');
  if (label) label.textContent = $rootScope.labels["L0001"];
  label = document.getElementById('dashboardLabel2');
  if (label) label.textContent = $rootScope.labels["L0001"];
  label = document.getElementById('clientsLabel1');
  if (label) label.textContent = $rootScope.labels["L0002"];
  label = document.getElementById('signoutLabel');
  if (label) label.textContent = $rootScope.labels["L0215"];  
  label = document.getElementById('signinLabel1');
  if (label) label.textContent = $rootScope.labels["L0214"];
  label = document.getElementById('signinLabel2');
  if (label) label.textContent = $rootScope.labels["L0214"];
}

setMenuLabels = function($rootScope) {

  var label = document.getElementById('dashboardLabel1');
  if (label) label.textContent = $rootScope.labels["L0001"];
  label = document.getElementById('dashboardLabel2');
  if (label) label.textContent = $rootScope.labels["L0001"];
  label = document.getElementById('clientsLabel1');
  if (label) label.textContent = $rootScope.labels["L0002"];
  label = document.getElementById('signoutLabel');
  if (label) label.textContent = $rootScope.labels["L0215"];  
  label = document.getElementById('signinLabel1');
  if (label) label.textContent = $rootScope.labels["L0214"];
  label = document.getElementById('signinLabel2');
  if (label) label.textContent = $rootScope.labels["L0214"];

  //label = document.getElementById('menuClient');
  //if ($rootScope.client_name) 
  //{
    //label.textContent = $rootScope.labels["L0032"] + " : " + $rootScope.client_name;
  //  label.textContent = $rootScope.client_name + " : " + $rootScope.labels["L0001"];
  //}
  //else
  //{  
    //label.textContent = $rootScope.labels["L0032"] + " : ";
    //label.textContent = $rootScope.labels["L0184"];
  //}

  //label = document.getElementById('menuClientDashboard');
  //label.textContent = $rootScope.labels["L0184"];
  //label = document.getElementById('menuClientList');
  //label.textContent = $rootScope.labels["L0021"];
  //label = document.getElementById('menuClientDetails');
  //label.textContent = $rootScope.labels["L0185"];
  label = document.getElementById('menuClientHistory');
  if (label) label.textContent = $rootScope.labels["L0156"];  // L0155 = Client History, L0156 = Reviews 
  label = document.getElementById('menuSavingsPlans');
  if (label) label.textContent = $rootScope.labels["L0157"];
  label = document.getElementById('menuInvestments');
  if (label) label.textContent = $rootScope.labels["L0058"];
  label = document.getElementById('menuOtherIncome');
  if (label) label.textContent = $rootScope.labels["L0171"];
  label = document.getElementById('menuExpenses');
  if (label) label.textContent = $rootScope.labels["L0066"];

  label = document.getElementById('menuRisk');
  if (label) label.textContent = $rootScope.labels["L0062"];
  label = document.getElementById('menuAllocation');
  if (label) label.textContent = $rootScope.labels["L0067"];
  label = document.getElementById('menuAssetClassSelection');
  if (label) label.textContent = $rootScope.labels["L0092"];
  label = document.getElementById('menuAssetAllocation');
  if (label) label.textContent = $rootScope.labels["L0113"];
  label = document.getElementById('menuProducts');
  if (label) label.textContent = $rootScope.labels["L0111"];
  label = document.getElementById('menuProductSelection');
  if (label) label.textContent = $rootScope.labels["L0112"];
  label = document.getElementById('menuTrades');
  if (label) label.textContent = $rootScope.labels["L0138"];
}

getClientList = function($rootScope, advisorId, Client) {
  console.log("getting clients for advisor "+advisorId);
  var client_list = [];
  var ClientPromise = Client.getAll(advisorId);
  ClientPromise.then(function(result){
    console.log("in client promise");
    console.log(result);

    var len = result.length;
    for (var i=0; i < len; i++) {
      //console.log(result[i].client_id + " " + result[i].review_date + " " + result[i].last_review);
      var s = { 'id': result[i].id,
              'clientId': result[i].client_id,
              'advisor_id': result[i].advisor_id,
              'name': result[i].first_name + " " + result[i].last_name,
              'firstName': result[i].first_name,
              'middleName': result[i].middle_name,
              'lastName': result[i].last_name,
              'title': result[i].title,
              'gender': result[i].gender,
              'dateOfBirth': result[i].date_of_birth,
              'expectedRetirementAge': result[i].retirement,
              'nationality': result[i].nationality,
              'countryResidence': result[i].residence,
              'maritalStatus': result[i].marital_status,
              'numberOfChildren': result[i].num_children,
              'emailAddress': result[i].email,
              'address1': result[i].address1,
              'address2': result[i].address2,
              'bestNumberToContact': result[i].phone,
              'additionalPhoneNumber': result[i].phone_alt,
              'city': result[i].city,
              'postCode': result[i].postcode,
              'country': result[i].country,
              'spouseName': result[i].spouse_name,
              'spouseDateBirth': result[i].spouse_dob,
              'spouseEmail': result[i].spouse_email,
              'spouseContactNumber': result[i].spouse_contact,
              'invest_amount': result[i].invest_amount,
              'networth': result[i].networth,
              'review_date': convertDateFormat(result[i].review_date),
              'last_review': convertDateFormat(result[i].last_review),
              'member_since': result[i].member_since,
              'sourceincome': result[i].sourceincome,
              'slug': result[i].slug
            };
      
      // Handle the case where either the first name or the last name is blank
      if (result[i].first_name == null)
        s.name = result[i].last_name;
      else if (result[i].last_name == null)
        s.name = result[i].first_name;

      console.log(s);
      client_list.push(s);
    }

    // Sort by name
    //client_list.sort(function(a,b) {return (a.name > b.name) - (a.name < b.name);});
    client_list.sort(function(a, b) {
      if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
        if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
        return 0;
    });

    console.log("client list");
    console.log(client_list);
    $rootScope.list = client_list;
  });
}

setAdvisor = function($rootScope) {
  $rootScope.current_advisor = 1;
  //$rootScope.default_language = "en";
  //$rootScope.current_uid = 1;
  if ($rootScope.current_user) {
    if ($rootScope.current_user.user) {
      var usr = $rootScope.current_user.user.id;
      console.log("USER "+usr);
      $rootScope.current_advisor = usr;
    }
  }
}

setCurrentClient = function($rootScope, client_id, Client) {
    var ClientPromise = Client.getOne($rootScope.current_advisor, client_id);
    ClientPromise.then(function(result){
      //console.log("Got client");
      //console.log(result);
      if (result) {
        $rootScope.current_client = result;
        $rootScope.client_name = result.first_name + " " + result.last_name;
        $rootScope.clientSlug = result.slug;
        $rootScope.clientCurrencyValue = $rootScope.current_client.invest_amount*1000000;
      }
    });
}

getSwapRate = function($rootScope, SwapRates) {
  //var hor = horizon;
  var SwapRatesPromise = SwapRates.getLatest();
  SwapRatesPromise.then(function(result) {
    if (result) {
      console.log("got swap rates");
      console.log(result[0]);
      $rootScope.swapRates = result[0];
    }
  })
}

convertCurrency = function(currency, amount, rates) {
  if (rates) {
    for (var i=0; i < rates.length; i++) {
      if (rates[i].currency == currency) {
        return amount/rates[i].rate;
      }
    }
  } else {
    console.log("ERR: Exchange rates not defined");
  }
  return amount;
}

convertCurrency2 = function(currency, amount, rates) {
  if (rates) {
    for (var i=0; i < rates.length; i++) {
      if (rates[i].currency == currency) {
        return amount*rates[i].rate;
      }
    }
  } else {
    console.log("ERR: Exchange rates not defined");
  }
  return amount;
}


//=====================================================

finvese.controller('advisorController', function($rootScope, $scope, Advisor) {
  console.log('in advisorController');
   //$rootScope.assets_allocation = true;
  // $rootScope.assets_class  = false;
  // $rootScope.investments = false;
  // $rootScope.savings = false;
  // $rootScope.income = false;
  // $rootScope.expenses = false;
  // $rootScope.asset_names = false;
  // $rootScope.client_id = 2;
  // $rootScope.home = false;
  // $scope.target_val = 'Return';
  // $scope.horizon = 10;
  // $scope.initial_invest = 5.0;
  // $rootScope.login = false;
  $rootScope.clientListMenu = false;
  $rootScope.clients_list = false;
})


finvese.controller('assetInputController', function($rootScope, $scope,$routeParams, $route, $location, CashHoldings, ClientHoldings, ExchangeRate, Product, CashBonds, ProductPrice, Client, Advisors, Translations) {
  $rootScope.login = false;
  $rootScope.home = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  setAdvisor($rootScope);
  //$rootScope.labels = {};
  //$rootScope.default_language = "en";
  console.log("in assetInputController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + " - Client Details";
      }
    })

  $scope.exchange_rates = [];
  $scope.product_list = [];
  $scope.cashbond_list = [];
  $scope.product_prices = [];

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();

      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);      
    
      var label = document.getElementById('cashLabel');
      if (label) label.textContent = $rootScope.labels["L0051"];

      //$scope.cashHeader = ['Currency','Amount'];
      $scope.cashHeader = [$rootScope.labels["L0053"], $rootScope.labels["L0054"]];

      //$scope.securitiesHeader = ['BBGID','Number of Units'];
      $scope.securitiesHeader = [$rootScope.labels["L0055"],$rootScope.labels["L0056"]];

      // + New Row
      label = document.getElementById('newRowLabel1');
      if (label) label.textContent = $rootScope.labels["L0057"];
      label = document.getElementById('newRowLabel2');
      if (label) label.textContent = $rootScope.labels["L0057"];

      // Securities
      label = document.getElementById('securitiesLabel');
      if (label) label.textContent = $rootScope.labels["L0052"];
      label = document.getElementById('submitLabel');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
  }


  //$scope.selectCurrency = {'currency':'USD'};
  //$scope.selectNewCurrency = {'newCurrency':'USD'};
          
  //$scope.currency = [{'name': 'USD', 'value': 'USD'}, {'name': 'AUD', 'value': 'AUD'}];
  //$scope.newCurrency = [{'name': 'USD', 'value': 'USD'}, {'name': 'AUD', 'value': 'AUD'}];


  //$scope.cashHeader = ['Currency','Amount'];
  //$scope.cashData = [];
  $scope.cashData = [{'currency':null,'amount':null},{'currency':null,'amount':null},
                      {'currency':null,'amount':null},{'currency':null,'amount':null}]
  //$scope.securitiesHeader = ['BBGID','Number of Units'];
  $scope.securities = [{'bbgid':null,'unitNumbe':null},{'bbgid':null,'unitNumbe':null},
                      {'bbgid':null,'unitNumbe':null},{'bbgid':null,'unitNumbe':null}]

  var CashHoldingsPromise = CashHoldings.getOne($rootScope.client_id);
  CashHoldingsPromise.then(function(result) {
    console.log("inside cash holdings promise");
    console.log(result);

    for(var i=0; i<result.length; i++) {
      if (i<5) {
        $scope.cashData[i].currency = result[i].currency;
        $scope.cashData[i].amount = result[i].amount;
      }
      else {
        var tmp = { 'currency': result[i].currency, 'amount': result[i].amount};
        //console.log(tmp);
        $scope.cashData.push(tmp);
      }
    }
  });

  var ExchangeRatePromise = ExchangeRate.getOne(1);
  ExchangeRatePromise.then(function(result){
    console.log("inside exchange rate promise");
    console.log(result.length);

    $scope.exchange_rates = result;
  });

  var ProductPromise = Product.getAll();
  ProductPromise.then(function(result) {
    console.log("in product promise");
    console.log(result.length);
    $scope.product_list = result;
  });

  var CashBondPromise = CashBonds.getAll();
  CashBondPromise.then(function(result) {
    console.log("inside cash bond promise");
    console.log(result);

    if (result) 
      $scope.cashbond_list = result;
  });

  $scope.cashRow = function($index){
    $scope.cashData.push({'currency':null,'amount':null});
  }
      
  $scope.securityRow = function($index){
    $scope.securities.push({'bbgid':null,'unitNumbe':null});
  }

  $scope.validateCurrency = function(currency) {
    if (currency) 
    {
      var cur = currency.toUpperCase();
      console.log("in validateCurrency "+cur);
      if ($scope.exchange_rates) 
      {
        var found = false;
        for (var i=0; i<$scope.exchange_rates.length; i++) 
        {
          if ($scope.exchange_rates[i].currency == cur) 
          {
            console.log("found it");
            found = true;
          }
        }
      }

      if (!found && cur !='' && cur != 'USD') {
        alert("Unknown currency");
      }
    }
  }

      $scope.validateBbgid = function(bbgid) {
        console.log("in validateBbgid "+bbgid);
        var bbg = bbgid.toUpperCase();
        var found = false;
        if ($scope.product_list) {
          for (var i=0; i<$scope.product_list.length; i++) {
            if ($scope.product_list[i].bbgid == bbg) {
              console.log("found it");
              found = true;

              // Look up the fund price for later use
              var data_query = { 'fund_id': $scope.product_list[i].fund_id, 'latest': 1};
              var ProductPricePromise = ProductPrice.getOne($scope.product_list[i].fund_id, data_query);
              ProductPricePromise.then(function(result) {
                console.log("in product price promise");
                console.log(result.fund_price);
                if (result) {
                  $scope.product_prices.push(result);
                }
              });
            }
          }
        }
        if (!found && $scope.cashbond_list) {
          // Check if the bbgid belongs to a cash bond
          for (var i=0; i<$scope.cashbond_list.length; i++) {
            if ($scope.product_list[i].bbgid == bbg) {
              console.log("found it");
              found = true;
            }
          }
        }

        if (!found && bbg != '') {
          alert("Unknown bbgid");
        }
      }           

      $scope.saveAssets = function() {
        console.log("in saveAssets");
        console.log("length = "+$scope.cashData.length);
        var aum = 0.0

        for (var i=0; i < $scope.cashData.length; i++) {
          if ($scope.cashData[i].currency && $scope.cashData[i].amount) {
            var cur = ($scope.cashData[i].currency).toUpperCase();
            console.log("i "+i+" "+cur+" "+$scope.cashData[i].amount);
            var data_query = { 'client_id': $rootScope.client_id,
                  'currency': cur,
                 'amount': $scope.cashData[i].amount,
                 'advisor_id': $rootScope.current_advisor };
            console.log(data_query);
            var res = CashHoldings.updateCash(0, data_query);

            var exchangeRate = 1.0;
            for (var j=0; j<$scope.exchange_rates.length; j++) {
              if ($scope.exchange_rates[j].currency == cur) {
                console.log("found it");
                exchangeRate = $scope.exchange_rates[j].rate;
              }
            }
            aum = aum + $scope.cashData[i].amount/exchangeRate;
          }
        }

        console.log("length2 = "+$scope.securities.length);
        for (var i=0; i < $scope.securities.length; i++) {
          if ($scope.securities[i].bbgid) {
            var bbg = ($scope.securities[i].bbgid).toUpperCase();
            console.log("i2 "+i+" "+bbg+" "+$scope.securities[i].unitNumber);
            var found = false;

            // Check for funds
            for (var j=0; j<$scope.product_list.length; j++) {
              if ($scope.product_list[j].bbgid == bbg) {
                console.log("found it "+$scope.securities[i].unitNumber);
                console.log($scope.product_list[j]);
                found = true;
                var numunits = $scope.securities[i].unitNumber;
                var fundclass = 'Equity';
                var assetclass = $scope.product_list[j].asset_class;
                if ($scope.product_list[j].asset_class >= 4000 && $scope.product_list[j].asset_class < 5000)
                  fundclass = 'Fixed Income';

                var fundprice = 0;
                for (var k=0; k<$scope.product_prices.length; k++) {
                  if ($scope.product_prices[k].fund_id == $scope.product_list[j].fund_id) {
                    fundprice = $scope.product_prices[k].fund_price;
                  }
                } 

                var data_query = {'advisor_id':  $rootScope.current_advisor,
                         'client_id': $rootScope.client_id,
                         'fund_symbol': $scope.product_list[j].ticker_name,
                         'fund_name': $scope.product_list[j].fund_name,
                         'fund_type': 'Fund',
                         'fund_class': fundclass,
                         'num_units': numunits,
                         'holding_value': numunits * fundprice,
                         'asset_class': assetclass,
                         'target_value': numunits * fundprice,
                         'fund_id': $scope.product_list[j].fund_id
                      };
                var res = ClientHoldings.updateHolding(0, data_query);

                var currency = $scope.product_list[j].currency;
                if (currency == 'USD')
                  aum = aum + (numunits * fundprice);
                else {
                  var exchangeRate = 1.0;
                  for (var m=0; m<$scope.exchange_rates.length; m++) {
                    if ($scope.exchange_rates[m].currency == currency) {
                      console.log("found it");
                      exchangeRate = $scope.exchange_rates[m].rate;
                    }
                  }
                  aum = aum + (numunits * fundprice/exchangeRate);
                }
              }
            }

            // Check for cash bonds
            if (!found) {
              for (var j=0; j<$scope.cashbond_list.length; j++) {
                $scope.securities[i].bbgid = ($scope.securities[i].bbgid).toUpperCase();
                if ($scope.cashbond_list[j].bbgid == $scope.securities[i].bbgid) {
                  console.log("found it "+$scope.securities[i].unitNumber);
                  console.log($scope.cashbond_list[j]);
                  found = true;
                  var numunits = $scope.securities[i].unitNumber;
                  var fundclass = 'Bonds';
                
                  var data_query = {'advisor_id':  $rootScope.current_advisor,
                         'client_id': $rootScope.client_id,
                         'fund_symbol': $scope.cashbond_list[j].security_name,
                         'fund_name': $scope.cashbond_list[j].security_name,
                         'fund_type': 'Bond',
                         'fund_class': fundclass,
                         'num_units': numunits,
                         'holding_value': numunits * $scope.cashbond_list[j].bond_price,
                         'target_value': numunits * $scope.cashbond_list[j].bond_price,
                         'asset_class': 0,
                         'bond_id': $scope.cashbond_list[j].bond_id
                      };
                  var res = ClientHoldings.updateHolding(0, data_query);

                  var currency = $scope.cashbond_list[j].currency;
                  if (currency == 'USD')
                    aum = aum + (numunits * $scope.cashbond_list[j].bond_price);
                  else {
                    var exchangeRate = 1.0;
                    for (var m=0; m<$scope.exchange_rates.length; m++) {
                      if ($scope.exchange_rates[m].currency == currency) {
                        console.log("found it");
                        exchangeRate = $scope.exchange_rates[m].rate;
                      }
                    }
                    aum = aum + (numunits * $scope.cashbond_list[j].bond_price/exchangeRate);
                  }
                }
              }
            }
          }
        }
                
        console.log("AUM="+aum);
        // Update the AUM in the database
        var data_query = { 'id': $rootScope.client_id,
              'client_id': $rootScope.client_id,
              'advisor_id': $rootScope.current_advisor,
              'invest_amount': aum/1000000.0};
        console.log(data_query);
        var ClientPromise = Client.updateClient($rootScope.client_id, data_query);
        ClientPromise.then(function(result) {
          console.log("in client promise");
          console.log(result);

          // Update the Client list on the screen
          console.log($rootScope.list);
          if ($rootScope.list) {
            var len = $rootScope.list.length;
            $rootScope.list[len-1].invest_amount = aum/1000000.0;
          }

          $scope.renderPage('clients_list');

        });

      }
 })


finvese.controller('clientDashboardController', function($rootScope,$scope,$routeParams, $route, $location, Client, SavedAllocation, MarketPrice, IndexPrice, ProductAllocation, Product, ProductPrice, RiskProfiles, ClientHoldings, AumValues, CashHoldings, ExchangeRate, CalculatedRisks, Translations, Advisors){
  $rootScope.home = false;
  $rootScope.login = false;
  $rootScope.clients_list = false;
  $rootScope.bookJSON=[];
  $scope.graph1hidden = true;
  $scope.graph2hidden = true;
  $scope.graph3hidden = true;
  $scope.graph4hidden = true;
  $scope.graph5hidden = true;

  console.log("in clientDashboardController "+$rootScope.client_id+" "+$rootScope.client_name);
  console.log($rootScope.current_client);
  console.log($rootScope);
  //console.log($rootScope.current_user);
  //console.log($rootScope.current_user.user);

  setAdvisor($rootScope);
  $scope.client_name = $rootScope.client_name;
  
  //$rootScope.labels = {};
  //$rootScope.default_language = "en";


  $scope.values = {'name':'Bill Gates','since':'Client Since 2013','aum':'-','target':'TARGETS',
                   'savings': '-', 'investments': '-', 'income': '-', 'expenses': '-',
                   'allocation': '-', 'portfolio': '-',
                   'returns':'5.4%','risk':'10.0%','loss':'29.4%', 'date':'',
                   'actual': 'ACTUAL', 'actualreturns': '-', 'actualrisk': '-', 'actualloss': '-' };
  $scope.currentClient ={'savings':'-','investments':'-', 'income':'-', 'expenses':'-',
                          'allocation':'-', 'portfolio':'-'};
  $scope.chartHeaders =['us equity - T Rowe', 'eu equity - henderson','as equity - stdCht','gl bond - PMCO','as bond - cs']

  $scope.values.name = $rootScope.client_name;

  $rootScope.start_year = 2017;
  $rootScope.aum_list = [];


  //console.log($location.$$path);
  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = parseInt(s.slice(pos+1));
    console.log("IN IF "+cid);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    //$rootScope.client_id = cid.toString();
    $rootScope.client_id = cid;
    $scope.clientId = $rootScope.client_id;
    var ClientPromise = Client.getOne($rootScope.current_advisor, cid);
    ClientPromise.then(function(result){
      console.log("Got client");
      console.log(result);
      $rootScope.current_client = result;
      $rootScope.client_name = result.first_name + " " + result.last_name;
      $scope.client_shortName = result.first_name[0] + "." + result.last_name;
      $scope.values.name = $rootScope.client_name;
      var dstring = result.member_since;
      var display_year = dstring.slice(0,4);
      var mm = parseInt(dstring.slice(5,7))-1;
      var display_date = intToMonthName(mm) + " " + display_year;
      $rootScope.start_year = parseInt(display_year);
      $scope.values.since = 'Client Since '+display_date;
      $scope.values.aum = '$'+roundOne(result.invest_amount).toString()+'M';
      $scope.values.portfolio = $scope.values.aum;

      document.title = $scope.client_shortName + " - Dashboard";

    });
  }

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
  
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      var label = document.getElementById('aumLabel');
      label.textContent = $rootScope.labels["L0060"];

      label = document.getElementById('returnsLabel');
      if (label) label.textContent = $rootScope.labels["L0061"];
      label = document.getElementById('riskLabel');
      if (label) label.textContent = $rootScope.labels["L0062"];
      label = document.getElementById('lossLabel');
      if (label) label.textContent = $rootScope.labels["L0063"];
      label = document.getElementById('savingsLabel');
      if (label) label.textContent = $rootScope.labels["L0064"];
      label = document.getElementById('investmentsLabel');
      if (label) label.textContent = $rootScope.labels["L0058"];
      label = document.getElementById('incomeLabel');
      if (label) label.textContent = $rootScope.labels["L0065"];
      label = document.getElementById('expensesLabel');
      if (label) label.textContent = $rootScope.labels["L0066"];
      label = document.getElementById('allocationLabel');
      if (label) label.textContent = $rootScope.labels["L0067"];
      label = document.getElementById('portfolioLabel');
      if (label) label.textContent = $rootScope.labels["L0068"];
      label = document.getElementById('targetsLabel');
      if (label) label.textContent = $rootScope.labels["L0059"];
      label = document.getElementById('investmentsLabel2');
      if (label) label.textContent = $rootScope.labels["L0058"];
      label = document.getElementById('cashLabel');
      if (label) label.textContent = $rootScope.labels["L0051"];
      label = document.getElementById('holdingsLabel');
      if (label) label.textContent = $rootScope.labels["L0069"];
      label = document.getElementById('bondsLabel');
      if (label) label.textContent = $rootScope.labels["L0069"]; 
      label = document.getElementById('fundsPerformanceLabel');
      if (label) label.textContent = $rootScope.labels["L0187"];
      label = document.getElementById('clientAccountLabel');
      if (label) label.textContent = $rootScope.labels["L0076"];
      label = document.getElementById('fixedIncomeLabel');
      if (label)
        label.textContent = $rootScope.labels["L0071"];
      label = document.getElementById('privateEquityLabel');
      if (label)
        label.textContent = $rootScope.labels["L0073"];

      // Hardcode for US Equity (S&P) for the moment
      //$scope.chartHeaders[0] = $scope.asset_description[1];
      $scope.chartHeaders[0] = $rootScope.labels["L0070"]; //'EQUITY';
      $scope.chartHeaders[1] = $rootScope.labels["L0071"]; //'FIXED INCOME';
      $scope.chartHeaders[2] = $rootScope.labels["L0072"]; //'HEDGE FUNDS';
      $scope.chartHeaders[3] = "OTHER ASSETS"; //rootScope.labels["L0073"]; //'PRIVATE EQUITY';
      $scope.chartHeaders[4] = $rootScope.labels["L0074"]; //'REAL ESTATE';

  }


  var CalculatedRisksPromise = CalculatedRisks.getOne($rootScope.client_id);
  CalculatedRisksPromise.then(function(result) {
    console.log("inside calculated risks promise "+$rootScope.client_id);
    console.log(result);
    if (result) {
      var returns = roundOne(result[0].calc_return);
      var risk = roundOne(result[0].calc_risk);
      var loss = roundOne(result[0].calc_loss);
      if (returns > 0)
        $scope.values.actualreturns = returns.toString()+'%';
      else
        $scope.values.actualreturns = '-';
      if (risk > 0)
        $scope.values.actualrisk = risk.toString()+'%';
      else
        $scope.values.actualrisk = '-';
      if (loss > 0)
        $scope.values.actualloss = loss.toString()+'%';
      else
        $scope.values.actualloss = '-';
    }
  });

  $rootScope.noBonds = false;
  $rootScope.noFunds = false;
  $rootScope.noBond = false;
  $rootScope.noCashe = false;
  $rootScope.noEquities = false;
  $rootScope.noHedge = false;
  $rootScope.noPrivate = false;
  $rootScope.noIncome = false;

  //$scope.date_categories = ['M-13', 'J-13', 'S-13', 'D-13', 'M-14', 'J-14', 'S-14', 'D-14', 'M-15', 'J-15', 'S-15', 'D-15'];
  $scope.date_categories2 = []; //['Jun-11', 'Jul-11', 'Aug-11', 'Sep-11', 'Oct-11', 'Nov-11', 'Dec-11', 'Jan-12', 'Feb-12','Mar-12', 'Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12', 'Nov-12', 'Dec-12', 'Jan-13', 'Feb-13', 'Mar-13', 'Apr-13', 'May-13', 'Jun-13', 'Jul-13', 'Aug-13', 'Sep-13', 'Oct-13', 'Nov-13', 'Dec-13', 'Jan-14', 'Feb-14', 'Mar-14', 'Apr-14', 'May-14', 'Jun-14', 'Jul-14', 'Aug-14', 'Sep-14', 'Oct-14', 'Nov-14', 'Dec-14', 'Jan-15', 'Feb-15', 'Mar-15', 'Apr-15', 'Jun-15', 'Jul-15', 'Aug-15', 'Sep-15', 'Oct-15', 'Nov-15', 'Dev-15', 'Jan-16', 'Feb-16', 'Mar-16', 'Apr-16', 'May-16'];

  // Compute the dates to be displayed in the x-axis of the charts
  $scope.date_categories2 = getDateLabels(6, 2017);
  console.log("XX");
  console.log($scope.date_categories2);

  $scope.holdingsHeader = ['Fund Name','Value','Weight'];
  $scope.holdings = [];

  $scope.cashe = [];

  $scope.equities = [];
                  //[{'fund_name':'THE BOND FUND OF AMER-A','value':20.22,'weight':3.5},
                  // {'fund_name':'THE BOND FUND OF AMER-A','value':20.22,'weight':3.5},
                  // ];
  $scope.bond = [];
                //[{'fund_name':'THE BOND FUND OF AMER-A','value':20.22,'weight':3.5},
                //     {'fund_name':'THE BOND FUND OF AMER-A','value':20.22,'weight':3.5}
                //   ];
  $scope.hedgeFunds =[];

  $scope.fixedIncome = [];

  $scope.privateEquity = [];

  // $scope.holdings = [];
  $scope.equity = [];
  $scope.bonds = [];
  $scope.funds = [];
  // $scope.others = [];

  $scope.currency_vals = [];
  $scope.exchange_rates = [];

  $rootScope.nobonds = function(){
    if($scope.bonds.length === 0 )
    $rootScope.noBonds = true;
  }
  $rootScope.nobonds();

   $rootScope.nofunds = function(){
    if($scope.funds.length === 0 )
    $rootScope.noFunds = true;
  }
  $rootScope.nofunds();

   $rootScope.nobond = function(){
    if($scope.bond.length === 0 )
    $rootScope.noBond = true;
  }
  //$rootScope.nobond();

   $rootScope.nocashe = function(){
    if($scope.cashe.length === 0 )
    $rootScope.noCashe = true;
  }
  //$rootScope.nocashe();

  $rootScope.noequities = function(){
    if($scope.equities.length === 0 )
    $rootScope.noEquities = true;
  }
  //$rootScope.noequities();

  $rootScope.nohedge = function(){
    if($scope.hedgeFunds.length === 0 )
    $rootScope.noHedge = true;
  }
  //$rootScope.nohedge();

  $rootScope.noprivate = function(){
    if($scope.privateEquity.length === 0 )
    $rootScope.noPrivate = true;
  }
  //$rootScope.noprivate();

  $rootScope.noincome = function(){
    if($scope.fixedIncome.length === 0 )
    $rootScope.noIncome = true;
  }
  //$rootScope.noincome();

  $scope.asset_class = [];
  $scope.asset_description = [];
  $scope.asset_weight = [];
  $scope.graph1_names = [];
  $scope.graph1_data = [];
  $scope.graph2_names = [];
  $scope.graph2_data = [];
  $scope.graph3_names = [];
  $scope.graph3_data = [];
  $scope.graph4_names = [];
  $scope.graph4_data = [];
  $scope.graph5_names = [];
  $scope.graph5_data = [];

  //rs = document.querySelector('[ng-app]').scope();
  //rsc = rs.client_id;
  //console.log(rsc);
  $rootScope.clientSlug = $route.current.params.slug;
  $rootScope.clientIndex = $route.current.params.clientIndex;
  //console.log($route.current.params.clientIndex);
  $scope.selectedClient = _.findWhere($rootScope.list, {'slug': $rootScope.clientSlug});
  //console.log($scope.selectedClient);


  if ($rootScope.current_client) {
    $scope.values.since = 'Client Since';
//    $scope.values.date = $rootScope.current_client.member_since;
    // date is assumed to be in format yyyy-mm-dd
    var dstring = $rootScope.current_client.member_since;
    var display_year = dstring.slice(0,4);
    var mm = parseInt(dstring.slice(5,7))-1;
    var display_date = intToMonthName(mm) + " " + display_year;
    $scope.values.since = 'Client Since '+display_date;
    $rootScope.start_year = parseInt(display_year);
    console.log("start year "+$rootScope.start_year);

    if ($rootScope.current_client.invest_amount) {
      $rootScope.current_client.invest_amount = roundOne($rootScope.current_client.invest_amount);
      $scope.values.aum = '$'+roundOne($rootScope.current_client.invest_amount).toString()+'M';
      //$scope.values.allocation = $scope.values.aum;
      $scope.values.portfolio = $scope.values.aum;
    }
    else {
      $scope.values.aum = '-';
      //$scope.values.allocation = $scope.values.aum;
      $scope.values.portfolio = $scope.values.aum;
    }
  } else {
    $scope.values.since = 'Client Since 2017';
    $scope.values.aum = '-';
    //$scope.values.allocation = $scope.values.aum;
    $scope.values.portfolio = $scope.values.aum;
  }

  var clientid = $rootScope.client_id;
  //$rootScope.start_year = 2016;

  var AumValuePromise = AumValues.getOne(clientid);
  AumValuePromise.then( function(result) {
    console.log("in aum value promise");
    console.log(result);

    for(var i=0; i<result.length; i++) {
      var year = parseInt(result[i].value_date.slice(0,4));
      var mon = parseInt(result[i].value_date.slice(5,7));
      //console.log(mon);
      if (mon==11) {
        var tmp = [year, result[i].aum_value/1000000.0];
        $rootScope.aum_list.push(tmp);
      }
    }

    console.log($rootScope.aum_list);

    // Risk profile information
    var RiskProfilesPromise = RiskProfiles.getOne(clientid);
    RiskProfilesPromise.then(function(result){
      console.log("got risk profile "+$rootScope.client_id);
      console.log(result);

      if (!result) {
        $scope.values.returns = '-';
        $scope.values.risk = '-';
        $scope.values.loss = '-';
      } else {
        if (result.length > 0) {
          $scope.risk_profile = result[0];
          $scope.values.returns = result[0].target_return.toString() +'%';
          $scope.values.risk = result[0].target_risk.toString() + '%';
          $scope.values.loss = result[0].target_loss.toString() + '%';
          var horizon = result[0].horizon;
          var risk = result[0].target_risk;
          var return_val = result[0].target_return;
          var init_invest = result[0].initial_invest;
          $scope.clientAccount(horizon, risk, return_val, init_invest);
        }
      }

    });
    //if ($scope.riskprofile) {
    //  console.log("in if");
    //  $scope.clientAccount($scope.riskprofile.horizon, $scope.riskprofile.target_risk, $scope.riskprofile.target_return, $scope.riskprofile.initial_invest);
    //}
  }); // AumValuePromise

  //console.log("advisor "+$rootScope.current_advisor+ " client "+$rootScope.client_id);
  //var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  //ClientPromise.then(function(result){
  //  console.log("got client "+$rootScope.client_id);
  //  console.log(result);
  //  var tmp = result.member_since.slice(0,4);
  //  console.log(tmp);
  //  $rootScope.start_year = parseInt(tmp);
  //  console.log("start year "+$rootScope.start_year);
  //  $scope.displayChart();
  //});


  var ExchangeRatePromise = ExchangeRate.getOne(1);
  ExchangeRatePromise.then(function(result){
    console.log("inside exchange rate promise");
    console.log(result.length);

    $scope.exchange_rates = result;

    // Cash Holdings
    var data_query = { 'client_id': clientid };
    var CashHoldingPromise = CashHoldings.getOne(clientid, data_query);
    CashHoldingPromise.then(function(result) {
        console.log("inside cash holding promise");
        console.log(result);
        $scope.currency_vals = [];
        var cashTotal = 0.0;

        var len = result.length;
        for (var i=0; i < len; i++) {
          //console.log("setting cash");
          //$rootScope.clientCurrencyValue = result[i].amount;
          //$scope.clientCurrencyValue = result[i].amount;
          //var tmp = {'fund_name': 'CASH', 'value':20.22,'weight':},
          //var tmp = {'fund_name': 'CASH', 'value': result[i].num_units, 'weight': result[i].num_units };
          var tmp = {'fund_name': result[i].currency, 'value': result[i].amount };
          $scope.cashe.push(tmp);
          if (result[i].currency == "USD")
            cashTotal += result[i].amount;
          else
            cashTotal += convertCurrency(result[i].currency,result[i].amount,$scope.exchange_rates);

          // Compute the values for the donut chart
          //console.log("before");
          //console.log($scope.currency_vals);
          var found = false;
          for (var ii=0; ii<$scope.currency_vals.length; ii++) {
            if ($scope.currency_vals[ii].currency == result[i].currency){
              // found it
              found = true;
              //console.log("result "+" "+ii+" "+result[i].currency+" "+$scope.currency_vals[ii].currency);
              $scope.currency_vals[ii].amount = $scope.currency_vals[ii].amount + result[i].amount;
            }
          }
          if (!found) {
            //console.log("in not found "+result[i].currency);
            var tmp = { 'currency': result[i].currency, 'amount': result[i].amount };
            $scope.currency_vals.push(tmp);
          }
          //console.log($scope.currency_vals);
          //$scope.cash.push(tmp);
          //$rootScope.nocashe();
        }

        console.log($scope.currency_vals);
        console.log($scope.cashe);
        console.log("TOTAL "+cashTotal);
        $rootScope.nocashe();
        $scope.invest();

    }); // CashHoldingPromise
 
  // Client Holdings
  $scope.holdingsTotal = 0;
  console.log("going to call ClientHoldingsPromise");
  var advisor_id = $rootScope.current_advisor;
  var data_query = { 'advisor_id': advisor_id, 'client_id': clientid, 'asset_class': -1 };
  var ClientHoldingPromise = ClientHoldings.getOne(clientid, data_query);
  ClientHoldingPromise.then(function(result) {
      console.log("inside client holding promise");
      console.log(result);

      var len = result.length;
      for (var i=0; i < len; i++) {
        var fundid = result[i].fund_id;
        if (result[i].fund_symbol === "CASH") {
          console.log("setting cash");
          $rootScope.clientCurrencyValue = result[i].num_units;
          $scope.clientCurrencyValue = result[i].num_units;
          //var tmp = {'fund_name': 'CASH', 'value':20.22,'weight':},
          //var tmp = {'fund_name': 'CASH', 'value': result[i].num_units, 'weight': result[i].num_units };
          var tmp = {'fund_name': 'CASH', 'value': result[i].num_units };

          $scope.cashe.push(tmp);
          //$scope.cash.push(tmp);
          //$rootScope.nocashe();
        }
        else {
          console.log("non-cash");
          if (result[i].num_units > 0.01 && result[i].holding_value > 0.01) {
            // Add to table
            var tmp = { //'ticker': result[i].fund_symbol,
                     'fund_name': result[i].fund_name,
                     //'type': result[i].fund_type,
                     //'class': result[i].fund_class,
                     'weight': result[i].num_units,
                     'value': result[i].holding_value,
                     'fund_id': result[i].fund_id };
            console.log(tmp); 
            if (result[i].currency == "USD")
              $scope.holdingsTotal += result[i].holding_value;
            else
              $scope.holdingsTotal += convertCurrency(result[i].currency, result[i].holding_value, $scope.exchange_rates);

            if (result[i].fund_type == "CASH BOND") {
              // cash bond
              $scope.bond.push(tmp);
            }
            else if (result[i].fund_class == "Equity") {
                $scope.equities.push(tmp);
                //$scope.equity.push(tmp);
            }
            else {
                $scope.fixedIncome.push(tmp);
                //$scope.bonds.push(tmp);
            }

            // Compute the values for the donut chart
            var found = false;
            for (var ii=0; ii<$scope.currency_vals.length; ii++) {
              if ($scope.currency_vals[ii].currency == result[i].currency){
                // found it
                found = true;
                $scope.currency_vals[ii].amount = $scope.currency_vals[ii].amount + result[i].amount;
              }
            }
            if (!found) {
              var tmp = { 'currency': result[i].currency, 'amount': result[i].amount };
              $scope.currency_vals.push(tmp);
            }

          }
        }
      }
      //$rootScope.nocashe();
      $rootScope.noequities();
      $rootScope.noincome();
      $rootScope.nobond();
      $rootScope.noprivate();
      $rootScope.nohedge();

      console.log("currency values");
      console.log($scope.currency_vals);
      $scope.invest();
      console.log("HOLDINGS TOTAL="+$scope.holdingsTotal);
      $scope.values.portfolio = roundOne($scope.holdingsTotal/1000000.0);
    }); // ClientHoldingPromise
  }); 


  $scope.clientAccount = function(horizon, risk, target_return, init_invest) {
    Highcharts.setOptions({
      colors: ['#347DCF','red']
    });
    var ranges = getRanges($rootScope.start_year, horizon, risk, target_return, init_invest);
    var disp_range1 = ranges[0];
    var disp_range2 = ranges[1];
    var disp_averages = ranges[2];
    //var disp_range1 = getRange1(horizon, risk, target_return, init_invest);
    //var disp_range2 = getRange2(horizon, risk, target_return, init_invest);
    //var disp_averages = getAverages(horizon, risk, target_return, init_invest);
    //$scope.aum_list = [[2016,10],[2017,10.9],[2018,14.36],[2019,17.28],[2020,16.58]];
    console.log("in clientAccount");
    console.log($rootScope.aum_list);

    $('#client-account').highcharts({
    //chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            type: 'spline'
        },
        exporting: { enabled: false },
        title: {
            text: 'Wealth Projection',
            style:{
                  color:'ffffff',
                  fontWeight:'bold',
                  fontSize: '16px !important',
                 },
            enabled:false
        },

        xAxis: {
            type: 'linear',
            allowDecimals: false
        },

        yAxis: {
            title: {
                text: 'Millions',
                style:{
                  fontSize: '16px !important',
                 }
            }
        },

        tooltip: {
            // crosshairs: true,
            // shared: true,
            // valueSuffix: ' million'
            shared: true,
            useHTML: true,
      // $scope.red();
            headerFormat: '<small>{point.key}</small><table class="tool-client">',
            pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                '<td style="text-align: right"><b>{point.y} million</b></td></tr>',
            footerFormat: '</table>',
            valueDecimals: 2
        },

        legend: {
            enabled:false,
        },
         
        series: [{
            name: 'Projected Wealth',
            data: disp_averages,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        },
         {
            name: '67 Percentile',
            data: disp_range1,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.4,
            zIndex: 0
        }, {
            name: '90 Percentile',
            data: disp_range2,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.2,
            zIndex: 0
        },{
            name: 'Actual',
            data: $rootScope.aum_list,
            zIndex: 1,
            marker: {
                fillColor: 'red',
                lineWidth:2,
                symbol:'circle',
                 radius: 5,
                lineColor: Highcharts.getOptions().colors[1]
            }
        }]
    });
  }

  $scope.usEquity = function(){
    //$scope.cat_dates = ['M-13', 'j-13', 's-13', 'D-13', 'M-14', 'j-14', 's-14', 'D-14', 'M-15', 'j-15', 's-15', 'D-15'];
    //var colors = ['#EB4932', '#347DCF', '#0CB52A'];
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    var styles = ['longdash', 'shortdot'];
    var len = $scope.graph1_data.length;
    var dat = [];
    categories:['25', '50', '75', '100','125', '150', '175', '140'];
    //var aclass = 0;
    //if ($scope.asset_class) {
      // this is the second graph
    //  aclass = $scope.asset_class[1];
    //}

    //if (aclass < 4000) {
      if ($scope.equity_benchmark) {
        console.log("found equity benchmark "+$scope.equity_name);
        var tmp = { name: $scope.equity_name,
                  data: $scope.equity_benchmark,
                  dashStyle: 'shortdot',
                  color: colors[2] };
        dat.push(tmp);
      }
    //}
    //if (aclass >= 4000) {
    //  if ($scope.bond_benchmark) {
    //    console.log("found bond benchmark "+$scope.bond_name);
    //    var tmp = { name: $scope.bond_name,
    //              data: $scope.bond_benchmark,
    //              dashStyle: 'shortdot',
    //              color: colors[2] };
    //    dat.push(tmp);
    //  }
    //}


    if ($scope.graph1_data.length>0) {
      // asset class benchmark
      var tmp = { name: $scope.graph1_names[0],
                  data: $scope.graph1_data[0],
                  //dashStyle: styles[i],
                  color: colors[0] };
      dat.push(tmp);
      for (var i=1; i<len; i++) {
        var tmp = { name: $scope.graph1_names[i],
                  data: $scope.graph1_data[i],
                  //dashStyle: styles[i],
                  color: colors[1] };
        dat.push(tmp);
      }
    }
    //var dat = [{
    //        data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
    //        dashStyle: 'longdash',
    //        color: '#EB4932'
    //    }, {
    //        data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
    //        dashStyle: 'shortdot',
     //       color: '#347DCF'
    //    }];

    $('#usequity').highcharts({
        exporting: { enabled: false },
        chart: {
            type: 'spline',
            title:'usequity',
            enabled:true,
            marginTop:5,
            marginBottom:60,
        },
         title: {
            text: '',
            style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
        },
        legend: {
            symbolWidth: 50
        },
        yAxis: {
            title: {
                text: '$ Million',
                enabled:false,
            },
            tickPositions: [50, 75, 100, 125, 150,175],
            gridLineWidth: 1,
            lineWidth: 0
        },
        xAxis: {
            tickInterval: 3,
            categories: $scope.date_categories2,
            gridLineWidth:0,
        },
        plotOptions: {
            series: {
                color: '#000000'
            }
        },
        series: dat
        //series: [{
        //    data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
        //    dashStyle: 'longdash',
        //    color: '#EB4932'
        //}, {
        //    data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
        //    dashStyle: 'shortdot',
        //    color: '#347DCF'
        //}]
    });
  }

  $scope.euequity = function(){
    //$scope.cat_dates = ['M-13', 'j-13', 's-13', 'D-13', 'M-14', 'j-14', 's-14', 'D-14', 'M-15', 'j-15', 's-15', 'D-15'];
    //var colors = ['#EB4932', '#347DCF', '#0CB52A'];
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    var styles = ['longdash', 'shortdot'];
    var len = $scope.graph2_data.length;

    //console.log("YAZ "+$scope.asset_class);
    //var aclass = 0;
    //if ($scope.asset_class) {
      // this is the second graph
    //  aclass = $scope.asset_class[2];
    //}

    var dat = [];
    //if (aclass < 4000) {
    //  if ($scope.equity_benchmark) {
    //    console.log("found equity benchmark "+$scope.equity_name);
    //    var tmp = { name: $scope.equity_name,
    //              data: $scope.equity_benchmark,
    //              dashStyle: 'shortdot',
    //              color: colors[2] };
    //    dat.push(tmp);
    //  }
    //}
    //if (aclass >= 4000) {
      if ($scope.bond_benchmark) {
        console.log("found bond benchmark "+$scope.bond_name);
        var tmp = { name: $scope.bond_name,
                  data: $scope.bond_benchmark,
                  dashStyle: 'shortdot',
                  color: colors[2] };
        dat.push(tmp);
      }
    //}

    for (var i=0; i<len; i++) {
      var tmp = { name: $scope.graph2_names[i],
                  data: $scope.graph2_data[i],
                  //dashStyle: styles[i],
                  color: colors[i] };
      dat.push(tmp);
    }
    //var dat = [{
    //        data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
    //        dashStyle: 'longdash',
    //        color: '#EB4932'
    //    }, {
    //        data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
    //        dashStyle: 'shortdot',
    //        color: '#347DCF'
    //    }];

    $('#euequity').highcharts({
        exporting: { enabled: false },
        chart: {
            type: 'spline',
            marginTop:5,
            marginBottom:60,
        },
        title: {
            text: '',
            style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
        },
        legend: {
            symbolWidth: 80
        },
        yAxis: {
            title: {
                text: '$ Million',
                enabled:false,
            },
            // categories: ['90','110','130','150','170'],
            tickPositions: [50, 75, 100, 125, 150,175],
            gridLineWidth: 1,
            lineWidth: 0
        },
        xAxis: {
            tickInterval: 3,
            categories: $scope.date_categories2,
            rotation: -5,
            gridLineWidth:0,
        },
        plotOptions: {
            series: {
                color: '#000000'
            }
        },
        series: dat
        //series: [{
        //    data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
        //    dashStyle: 'longdash',
        //    color: '#EB4932'
        //}, {
        //    data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
        //    dashStyle: 'shortdot',
        //    color: '#347DCF'
        //}]
    });
  }


  // Retrieve the client's portfolio
  console.log("getting client portfolio "+clientid);
  var SavedAllocationPromise = SavedAllocation.getOne(clientid);
  SavedAllocationPromise.then(function(result){
    console.log("in saved allocation promise");
  //var ClientPortfolioPromise = ClientPortfolio.getOne(clientid);
  //ClientPortfolioPromise.then(function(result){
  //  console.log("in client porfolio promise");
    console.log(result);
    console.log(result[0]);

    var arrlen = result.length;
    j = 0;
    for (var i=0; i < arrlen; i++) {

      // ignore asset classes that have a weight of 0
      if (result[i].asset_weight > 0.005) {
        $scope.asset_class[j] = result[i].asset_class;
        $scope.asset_description[j] = result[i].asset_description;
        $scope.asset_weight[j] = result[i].asset_weight;
          //$scope.old_weight[i] = result[i].asset_weight;
          //  var w = Math.round(result[i].asset_weight*100)/100;
          //$scope.asset_titles[i] = result[i].asset_description + " " + result[i].asset_weight.toString() + "%";
            //$scope.asset_titles[j] = result[i].asset_description + " " + w.toString() + "%";
        j = j + 1;
      }
    }
    $scope.asset_length = j;
    console.log("AAA found "+j);
    console.log($scope.asset_class);
    console.log($scope.asset_description);
    console.log($scope.asset_weight);

    // Redraw the donut chart
    $scope.investments();

    // Retrieve the information about the global benchmarks
    var IndexPricePromise1 = IndexPrice.getOne(1100);
    IndexPricePromise1.then(function(result) {
      //console.log("in global benchmart promise1");
      //console.log(result);
      //console.log(result.length);
      $scope.equity_name = result[0].asset_description;

      var tmparray = [];
      var tmparray2 = [];
      var sum = 0.0;
      if (result.length > 60)
        start = result.length-60;
      else
        start = 0;
      var normFactor = result[start].index_price;
      for (var i=start; i<result.length; i++) {
        tmparray.push(Math.floor((result[i].index_price*100.0/normFactor)*100.0)/100.0);
        if (i > start) {
          var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
          tmparray2.push(r);
          sum = sum + r;
        }
      }

      //console.log(tmparray);
      //console.log(tmparray.length);
      $scope.equity_benchmark = tmparray;
      console.log("RAB "+$scope.asset_length);
      //for (var k=0; k < $scope.asset_length; k++) {
      //  if ($scope.asset_class[k].asset_class < 4000)
      //}
      //$scope.usEquity();
      //$scope.euequity();
    });

    var IndexPricePromise2 = IndexPrice.getOne(4020);
    IndexPricePromise2.then(function(result) {
      //console.log("in global benchmart promise2");
      //console.log(result);
      $scope.bond_name = result[0].asset_description;

      var tmparray = [];
      var tmparray2 = [];
      var sum = 0.0;
      if (result.length > 60)
        start = result.length-60;
      else
        start = 0;
      var normFactor = result[start].index_price;
      for (var i=start; i<result.length; i++) {
        tmparray.push(Math.floor((result[i].index_price*100.0/normFactor)*100.0)/100.0);
        if (i > start) {
          var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
          tmparray2.push(r);
          sum = sum + r;
        }
      }

      //console.log(tmparray);
      //console.log(tmparray.length);
      $scope.bond_benchmark = tmparray;
    });





    // Production Allocations = Holdings
    console.log("getting product allocation");
    var ProductAllocationPromise = ProductAllocation.getOne(clientid);
    ProductAllocationPromise.then(function(result){
      console.log("in product allocation promise");
      console.log(result);
      console.log(result[0]);

      // $scope.holdings = [];

      var len = result.length;
      for (var i=0; i < len; i++) {
        var fundid = result[i].fund_id;
        var weight = result[i].fund_allocation;
        var ac = result[i].asset_class;

        console.log("fundid "+fundid+ " weight "+weight+" class "+ac);
        data_query = {'asset_class': 0, 'fund_id': fundid };
        var tmp = {'fund_name': fundid.toString(), 'weight': weight, 'fund_id': fundid, 'asset_class': ac};
        $scope.holdings.push(tmp);
        //if (ac < 4000)
        //  $scope.equity.push(tmp);
        //else if (ac >= 4000 && ac < 5000)
        //  $scope.bonds.push(tmp);
        //else if (ac >= 5000 && ac < 6000)
        //  $scope.funds.push(tmp);
       // else
        //  $scope.others.push(tmp);

        var ProductPromise = Product.getOne(-fundid, data_query);
        ProductPromise.then(function(result) {
          //console.log("inner "+fundid);
          //console.log(result[0]);
          if (result) {
            var fundname = result[0].fund_name;
            console.log("RR "+fundname+" "+result[0].asset_class);
            console.log($scope.asset_class);
            var fundid = result[0].fund_id;
            var ac = 0;
            for (var j=0; j<$scope.holdings.length;j++) {
              if (fundid == $scope.holdings[j].fund_id) {
                $scope.holdings[j].fund_name = fundname;
                ac = $scope.holdings[j].asset_class;
              }
            }

            // Get the price data and add it to the appropriate graph
            var data_query = { 'fund_id': fundid, 'latest': 0};
            var ProductPricePromise1 = ProductPrice.getOne(fundid, data_query);
            ProductPricePromise1.then(function(res) {
              //console.log("inside product price promise1");
              //console.log(res);
              //console.log(res.length);

              var fid = 0;
              var ac = 0;
              var fundName = "";
              // Look up the name of the fund
              if (res.length > 0) {
                fid = res[0].fund_id;
                for (var j=0; j<$scope.holdings.length;j++) {
                  if (fid ==$scope.holdings[j].fund_id) {
                    //console.log("JAM "+$scope.holdings[j].fund_name+" "+$scope.holdings[j].asset_class);
                    fundName = $scope.holdings[j].fund_name;
                    ac = $scope.holdings[j].asset_class;
                  }
                }
              }

              if (res.length > 0) {
                var fid = res[0].fund_id;
                //console.log("LLL "+fid + " "+ac + " " + fundName);
                //for (var j=0; j<$scope.holdings.length;j++) {
                  //console.log("inside for "+j);
                  //if (fid == $scope.holdings[j].fund_id) {
                  //  console.log("JAM "+$scope.holdings[j].fund_name+" "+$scope.holdings[j].asset_class);
                  //  $scope.graph1_names.push($scope.holdings[j].fund_name);
                  //}
                //}

                var tmparray = [];
                var tmparray2 = [];
                var sum = 0.0;
                if (res.length > 60)
                  start = res.length-60;
                else
                  start = 0;
                var normFactor = res[start].fund_price;
                for (var i=start; i<res.length; i++) {
                  tmparray.push(Math.floor((res[i].fund_price*100.0/normFactor)*100.0)/100.0);
                  if (i > start) {
                    var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
                    tmparray2.push(r);
                    sum = sum + r;
                  }
                }

                //console.log(tmparray);


                if (ac < 4000) {
                  // stocks
                  console.log("adding to stocks");
                  $scope.graph1_names.push(fundName);
                  $scope.graph1_data.push(tmparray);
                }
                else if (ac >= 4000 && ac < 5000) {
                  // bonds
                  console.log("adding to bonds");
                  $scope.graph2_names.push(fundName);
                  $scope.graph2_data.push(tmparray);
                }
                else if (ac >= 5000 && ac < 6000) {
                  // real estte
                  $scope.graph5_names.push(fundName);
                  $scope.graph5_data.push(tmparray);
                }
                else {
                  // hedge funds and private equity
                  $scope.graph4_names.push(fundName);
                  $scope.graph4_data.push(tmparray);
                }
              } // if result > 0

        //if ($scope.graph1_data.length > 0) {
          $scope.graph1hidden = false;
          $scope.usEquity();
        //}
        //if ($scope.graph2_data.length > 0) {
          $scope.graph2hidden = false;
          $scope.euequity();
        //}
        //if ($scope.graph3_data.length > 0) {
          $scope.graph3hidden = false;
          $scope.asequity();
        //}
        //if ($scope.graph4_data.length > 0) {
          $scope.graph4hidden = false;
          $scope.glequity();
        //}
        //if ($scope.graph5_data.length > 0) {
          $scope.graph5hidden = false;
          $scope.asbond();
        //}
          //$scope.clientAccount(horizon, risk, return_val, init_invest);

            }); // end ProductPrice Promise
          } //  end if (result)
        });

      } // for

    });


    // Get all the product selections for this client
    // Add the product allocations to the graphs
    //var len = $scope.holdings.length;
    //for (var i=0; i<len; i++) {
    //  console.log("RR found "+$scope.holdings[i].asset_class+" "+$scope.holdings[i].fund_id);
    //}

    //var ProductAllocationPromise = ProductAllocation.getOne(1, 0);
    //ProductAllocationPromise.then(function(result) {
    //  console.log("in product selection promise");
    //  console.log(result);
   //   console.log(result.length);
    //  console.log($scope.holdings);

    //  var len = result.length;
    //  for (var i=0; i<len; i++) {
    //    var ac = result[i].asset_class;
    //    var fd = result[i].fund_id;
    //    for (var j=0; j<$scope.asset_class.length;j++) {
    //      if ($scope.asset_class[j] == ac) {
   //         console.log("RR found in class "+j);
    //      }
    //    }
     // }
    //});

    // Hardcode for US Equity (S&P) for the moment
      var a = 2410;
      //var a = $scope.asset_class[1];
      var MarketPricePromise = MarketPrice.getOne(a);
      MarketPricePromise.then(function(result) {
        console.log("processing us equity market price");
        //console.log(result);
        //console.log(result.length);
        $scope.graph1_names.push(result[0].asset_description);

        var tmparray = [];
        var tmparray2 = [];
        var sum = 0.0;
        if (result.length > 60)
          start = result.length-60;
        else
          start = 0;
        var normFactor = result[start].market_price;
        for (var i=start; i<result.length; i++) {
          tmparray.push(Math.floor((result[i].market_price*100.0/normFactor)*100.0)/100.0);
          if (i > start) {
            var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
            tmparray2.push(r);
            sum = sum + r;
          }
        }

        //console.log(tmparray);
        //console.log(tmparray.length);
        $scope.graph1_data.push(tmparray);
        console.log("graph1hidden "+$scope.graph1hidden);

      })

})


  // Risk profile information
  var RiskProfilesPromise = RiskProfiles.getOne(clientid);
  RiskProfilesPromise.then(function(result){
    console.log("got risk profile "+$rootScope.client_id);
    console.log(result);

    if (!result) {
        $scope.values.returns = 'N/A';
        $scope.values.risk = 'N/A';
        $scope.values.loss = 'N/A';
    } else {
        if (result.length > 0) {
          $scope.risk_profile = result[0];
          $scope.values.returns = result[0].target_return.toString() +'%';
          $scope.values.risk = result[0].target_risk.toString() + '%';
          $scope.values.loss = result[0].target_loss.toString() + '%';
          var horizon = result[0].horizon;
          var risk = result[0].target_risk;
          var return_val = result[0].target_return;
          var init_invest = result[0].initial_invest;
          $scope.clientAccount(horizon, risk, return_val, init_invest);
        }
    }

  });

$scope.investments= function() {
  // Donut chart
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    //var color_list =['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
    //           '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049'];
    Highcharts.setOptions({
      colors: colors
    });
    var dat = [];
    var len = $scope.asset_class.length;
    for (var i=0; i<len; i++) {
      var item = { name:  $scope.asset_description[i],
                   y:     $scope.asset_weight[i],
                   color: colors[i] };
      dat.push(item);
    }
    var investamt;
    if ($rootScope.current_client)
      investamt = $rootScope.current_client.invest_amount;
    else
      investamt = 0;

    //var dat =  [{
    //                name: ' TROWE',
    //                y: 10.38,
    //                color: '#D00049',
    //            }, {
    //                name: 'HENDERSON',
    //                y: 56.33,
    //                color: '#EB4932'
    //            }, {
    //                name: 'STDCHART',
    //                y: 24.03,
    //                color: '#8085E9'
    //            }, {
    //                name: 'PIMCO',
    //                y: 4.77,
    //                color: '#347DCF'
    //            } ,{
    //                name: 'CS',
    //                y: 0.91,
    //                color: '#64D0AE'
    //            }];

        $('#dashboard-investments').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'15',
            },
            exporting: { enabled: false },
            title: {
                // text: '$'+investamt.toString()+'M',
                text:'',
                fontSize: '25px',
                align: 'center',
                verticalAlign: 'middle',
                y:-14,
                style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
            },
             subtitle: {
                text: null,
                fontSize: '16px',
                align: 'center',
                x:114,
                y:18,
                verticalAlign: 'middle',
                style:{
                  fontWeight:'bold',
                  fontSize: '16px !important',
                }
            },
              legend: {
                align: 'center',
                verticalAlign: 'bottom',
                itemStyle: {
                  fontWeight: 'normal'
                }
              },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
           },
            // series: [{
            //     type: 'pie',
            //     name: 'All Client Portfolio',
            //     innerSize: '50%',
            //     size:'85%',
            //     data: [
            //         ['Cash',   11.0],
            //         ['Stocks', 35.0],
            //         ['Bonds',  50.0],
            //         ['Alternatives', 4.0]
            //     ]
            // }]

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '60%',
                data: dat
            }]
        });
   }

$scope.investments();

$scope.invest= function() {
  console.log("in scope.invest "+$scope.currency_vals.length);
  // Currency Donut chart
  var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
  //var color_list =['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
  //             '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049'];
  Highcharts.setOptions({
      colors: colors
  });
  var dat = [];
  if ($scope.currency_vals) {
    for (var i=0; i<$scope.currency_vals.length; i++) {
      var amt = $scope.currency_vals[i].amount;
      var cur = $scope.currency_vals[i].currency;
      if (amt < 0) amt = 0.0;
      if (cur != 'USD' && $scope.exchange_rates) {
        console.log("need to do currency conversion");
        for (var j=0; j<$scope.exchange_rates.length; j++) {
          if ($scope.exchange_rates[j].currency == cur) {
            // found it
            console.log("found it");
            amt = amt / $scope.exchange_rates[j].rate;
          }

        }
      }
      var tmp = { name: cur,
                  y: amt,
                  color: colors[i] };
      dat.push(tmp);
    }
  }
  console.log(dat);

   // var dat =  [{
   //                name: ' USD',
   //                y: 70,
   //                color: '#D00049',
   //            }, {
   //                name: 'SGD',
   //                y: 20,
   //                color: '#EB4932'
   //            }, {
   //                name: 'AUD',
   //                y: 10,
   //                color: '#8085E9'
   //            }];

        $('#dashboard-invest').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'15',
                marginTop:'-10',
            },
            exporting: { enabled: false },
            title: {
                text: '',
                fontSize: '25px',
                align: 'center',
                verticalAlign: 'middle',
                y:-14,
                style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
            },
             subtitle: {
                text: null,
                fontSize: '16px',
                align: 'center',
                x:114,
                y:18,
                verticalAlign: 'middle',
                style:{
                  fontWeight:'bold',
                  fontSize: '16px !important',
                }
            },
              legend: {
                align: 'center',
                verticalAlign: 'bottom',
                itemStyle: {
                  fontWeight: 'normal'
                }
              },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
           },
            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '60%',
                data: dat
            }]
        });
   }

$scope.invest();



//$scope.clientAccount();

//$scope.usEquity();
//$scope.euequity();
$scope.asequity = function(){
    //$scope.cat_dates = ['M-13', 'j-13', 's-13', 'D-13', 'M-14', 'j-14', 's-14', 'D-14', 'M-15', 'j-15', 's-15', 'D-15'];

    // This graph is for hedge funds
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    //var colors = ['#EB4932', '#347DCF', '#0CB52A'];
    var styles = ['longdash', 'shortdot'];
    var len = $scope.graph3_data.length;
    var dat = [];
    //if ($scope.equity_benchmark) {
    //  console.log("found equity benchmark");
    //  var tmp = { name: $scope.equity_name,
    //              data: $scope.equity_benchmark,
    //              dashStyle: 'shortdot',
    //              color: colors[2] };
    //  dat.push(tmp);
    //}
    for (var i=0; i<len; i++) {
      var tmp = { name: $scope.graph3_names[i],
                  data: $scope.graph3_data[i],
                  //dashStyle: styles[i],
                  color: colors[i] };
      dat.push(tmp);
    }
    //var dat = [{
    //        data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
    //        dashStyle: 'longdash',
    //        color: '#EB4932'
    //    }, {
    //        data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
    //        dashStyle: 'shortdot',
    //        color: '#347DCF'
    //    }];

    $('#asequity').highcharts({
        exporting: { enabled: false },
        chart: {
            type: 'spline',
            marginTop:5,
            marginBottom:60,
        },
        title: {
            text: '',
            style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
        },
        legend: {
            symbolWidth: 80
        },
        yAxis: {
            title: {
                text: '$ Million',
                enabled:false,
            },
            //categories: ['90','110','130'],
            tickPositions: [50, 75, 100, 125, 150],
            gridLineWidth: 1,
            lineWidth: 0
        },
        xAxis: {
            tickInterval: 3,
            categories: $scope.date_categories2,
            rotation: -5,
            gridLineWidth:0,
        },
        plotOptions: {
            series: {
                color: '#000000'
            }
        },
        series: dat
        //series: [{
        //    data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
        //    dashStyle: 'longdash',
        //    color: '#EB4932'
        //}, {
        //    data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
        //    dashStyle: 'shortdot',
        //    color: '#347DCF'
        //}]
    });
}

$scope.glequity = function(){
    //$scope.cat_dates = ['M-13', 'j-13', 's-13', 'D-13', 'M-14', 'j-14', 's-14', 'D-14', 'M-15', 'j-15', 's-15', 'D-15'];

    // This graph is for PRIVATE EQUITY
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    //var colors = ['#EB4932', '#347DCF', '#0CB52A'];
    var styles = ['longdash', 'shortdot'];
    var len = $scope.graph4_data.length;
    var dat = [];
    //if ($scope.equity_benchmark) {
    //  console.log("found equity benchmark");
    //  var tmp = { name: $scope.equity_name,
    //              data: $scope.equity_benchmark,
    //              dashStyle: 'shortdot',
    //              color: colors[2] };
    //  dat.push(tmp);
    //}
    for (var i=0; i<len; i++) {
      var tmp = { name: $scope.graph4_names[i],
                  data: $scope.graph4_data[i],
                  //dashStyle: styles[i],
                  color: colors[i] };
      dat.push(tmp);
    }
    //var dat =  [{
    //        data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
    //        dashStyle: 'longdash',
    //        color: '#EB4932'
    //    }, {
    //        data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
    //        dashStyle: 'shortdot',
    //        color: '#347DCF'
     //   }];

    $('#glequity').highcharts({
        exporting: { enabled: false },
        chart: {
            type: 'spline',
            marginTop:5,
            marginBottom:60,
        },
        title: {
            text: '',
            style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
        },
        legend: {
            symbolWidth: 80
        },
        yAxis: {
            title: {
                text: '$ Million',
                enabled:false,
            },
            //categories: ['90','95','100','105','110'],
            tickPositions: [50, 75, 100, 125, 150],
            gridLineWidth: 1,
            lineWidth: 0
        },
        xAxis: {
            tickInterval: 3,
            categories: $scope.date_categories2,
            rotation: -5,
            gridLineWidth:0,
        },
        plotOptions: {
            series: {
                color: '#000000'
            }
        },
        series: dat
        //series: [{
        //    data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
        //    dashStyle: 'longdash',
        //    color: '#EB4932'
        //}, {
        //    data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
        //    dashStyle: 'shortdot',
        //    color: '#347DCF'
        //}]
    });
}

$scope.asbond = function(){
    //$scope.cat_dates = ['M-13', 'j-13', 's-13', 'D-13', 'M-14', 'j-14', 's-14', 'D-14', 'M-15', 'j-15', 's-15', 'D-15'];

    // This graph is for REAL ESTATE
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    //var colors = ['#EB4932', '#347DCF', '#0CB52A'];
    var styles = ['longdash', 'shortdot'];
    var len = $scope.graph5_data.length;
    var dat = [];
    //if ($scope.equity_benchmark) {
    //  console.log("found equity benchmark");
    //  var tmp = { name: $scope.equity_name,
    //              data: $scope.equity_benchmark,
    //              dashStyle: 'shortdot',
    //              color: colors[2] };
    //  dat.push(tmp);
    //}
    for (var i=0; i<len; i++) {
      var tmp = { name: $scope.graph5_names[i],
                  data: $scope.graph5_data[i],
                  //dashStyle: styles[i],
                  color: colors[i] };
      dat.push(tmp);
    }
    //var dat = [{
    //        data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
    //        dashStyle: 'longdash',
    //        color: '#EB4932',
    //    }, {
    //        data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
    //        dashStyle: 'shortdot',
    //        color: '#347DCF'
    //    }];

    $('#asbond').highcharts({
        exporting: { enabled: false },
        chart: {
            type: 'spline',
            marginTop:5,
            marginBottom:60,
        },
        title: {
            text: '',
            style:{
                  fontWeight:'bold',
                  fontSize: '25px !important',
                }
        },
        legend: {
            symbolWidth: 80
        },
        yAxis: {
            title: {
                text: '$ Million',
                enabled:false,
            },
            //categories: ['90','95','100','105'],
            tickPositions: [50, 75, 100, 125, 150],
            gridLineWidth: 1,
            lineWidth: 0
        },
        xAxis: {
            categories: $scope.date_categories,
            rotation: 20,
            gridLineWidth:0,
        },
        plotOptions: {
            series: {
                color: '#000000'
            }
        },
        series: dat
        //series: [{
        //    data: [1, 3, 2, 4, 5, 4, 6, 2, 3, 5, 6],
        //    dashStyle: 'longdash',
        //    color: '#EB4932',
        //}, {
        //    data: [2, 4, 1, 3, 4, 2, 9, 1, 2, 3, 4, 5],
        //    dashStyle: 'shortdot',
        //    color: '#347DCF'
        //}]
    });
}
//$scope.asbond();

})


finvese.controller('riskCalibrationsController', function($rootScope, $scope, $location, SwapRates, ExpectedReturns, Montecarlos, RiskProfiles, SavedRiskProfiles, Client, Advisors, Translations){
  $rootScope.login = false;
  $scope.swapRatesLoaded = false;
  $scope.expectedReturnsLoaded = false;
  $rootScope.home = false;
  $scope.iterations = 4;
  $rootScope.clientListMenu = false;
  $rootScope.clients_list = false;
  console.log("AA riskCalibrationsController "+$rootScope.client_id);

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  setAdvisor($rootScope);
  setCurrentClient($rootScope, $rootScope.client_id, Client);

  console.log("in riskCalibrationsController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Risk";
      }
    })



  var SwapRatesPromise = SwapRates.getLatest();
  $scope.color = {
    red: Math.floor(Math.random() * 255),
    green: Math.floor(Math.random() * 255),
    blue: Math.floor(Math.random() * 255)
  };

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);
   
      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('riskProfileLabel');
      if (label) label.textContent = $rootScope.labels["L0078"];
      label = document.getElementById('saveLabel');
      if (label) label.textContent = $rootScope.labels["L0180"];

      label = document.getElementById('wealthProjectionLabel');
      if (label) label.textContent = $rootScope.labels["L0077"];
      label = document.getElementById('simulationLabel');
      if (label) label.textContent = $rootScope.labels["L0088"];
      label = document.getElementById('simulate');
      if (label) label.textContent = $rootScope.labels["L0089"];
      label = document.getElementById('horizonLabel');
      if (label) label.textContent = $rootScope.labels["L0080"];
      label = document.getElementById('targetReturnLabel');
      if (label) label.textContent = $rootScope.labels["L0081"] + " (%)";
      label = document.getElementById('targetRiskLabel');
      if (label) label.textContent = $rootScope.labels["L0082"] + " (%)";
      label = document.getElementById('targetLossLabel');
      if (label) label.textContent = $rootScope.labels["L0083"];
      label = document.getElementById('iterationsLabel');
      if (label) label.textContent = $rootScope.labels["L0090"] + "(1 - 20)";
      label = document.getElementById('initialAUMLabel');
      if (label) label.textContent = $rootScope.labels["L0220"];
      label = document.getElementById('annualLabel');
      if (label) label.textContent = $rootScope.labels["L0084"];
      label = document.getElementById('horizonLabel2');
      if (label) label.textContent = $rootScope.labels["L0079"];
      //console.log("after horizon");
  }

  $scope.loadChart = function(){
    if($scope.swapRatesLoaded && $scope.expectedReturnsLoaded ){
      $scope.displayChart();
    }
  }

  //$rootScope.client_id = 2;
  $rootScope.start_year = 2016;

  if ($rootScope.current_client) {
    $scope.intialAmount = roundOne($rootScope.current_client.invest_amount);
    $scope.initialAmount = $scope.intialAmount;
    $scope.loadChart();
  }
  else {
    //setCurrentClient($rootScope, $rootScope.client_id, Client);
    var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
    ClientPromise.then(function(result){
      //console.log("Got client");
      //console.log(result);
      if (result) {
        $rootScope.current_client = result;
        $rootScope.client_name = result.first_name + " " + result.last_name;
        $rootScope.clientSlug = result.slug;
        $rootScope.clientCurrencyValue = $rootScope.current_client.invest_amount*1000000;

        $scope.intialAmount = roundOne($rootScope.current_client.invest_amount);
        $scope.initialAmount = roundOne($rootScope.current_client.invest_amount);
        $scope.loadChart();
      }
    });

  }

  $scope.riskAmount = 6;
  $scope.returnAmount = 6;
  $scope.lossAmount = 6;
  //$scope.intialAmount = 20;
  $scope.horizon_val = 10;
  $scope.slideAmount = 10;

  var marketRisk = 0.15;
  var marketReturn = 0.08;
  var swapRate = 0.018;
  var curRisk = $scope.riskAmount/100.0;
  var z = (swapRate/curRisk) + (marketReturn-swapRate)/marketRisk;
  $scope.returnHorizon = $scope.returnAmount * $scope.horizon_val;
  $scope.riskHorizon = roundOne($scope.riskAmount * Math.sqrt($scope.horizon_val));
  $scope.lossHorizon = roundOne(CDF(-z*Math.sqrt($scope.horizon_val))*100.0);

  //document.getElementById("chosenHorizon").innerHTML = $scope.horizon_val;

  var RiskProfilesPromise = RiskProfiles.getOne($rootScope.client_id);
  RiskProfilesPromise.then(function(result){
    console.log("got risk profile "+$rootScope.client_id);
    console.log(result);
    if (result.length > 0) {
      $scope.riskAmount = result[0].target_risk;
      $scope.returnAmount = result[0].target_return;
      $scope.lossAmount = result[0].target_loss;
      $scope.initialAmount = result[0].initial_invest;
      $scope.horizon_val = result[0].horizon;
      $scope.slideAmount = result[0].horizon;
      $scope.displayChart();
    }
  })

  var SwapRatesPromise = SwapRates.getLatest();
  SwapRatesPromise.then(function(result) {
    $scope.swapRatesObject = result[0];
    console.log("got swap rates");
    console.log(result[0]);
    var latest_result = result[0];
    delete latest_result["created_at"];
    delete latest_result["id"];
    delete latest_result["maturity"];
    delete latest_result["updated_at"];
    $scope.swapRates = _.values(latest_result);
    $scope.swapRatesLoaded = true;
    console.log($scope.swapRates);
    $scope.loadChart();
  })

  var ExpectedReturnsPromise = ExpectedReturns.getAll();
  ExpectedReturnsPromise.then(function(result) {
    console.log("got expected returns");
    $scope.mean = _.pluck(result, 'mean');
    $scope.mu = _.pluck(result, 'mu');
    $scope.cov = _.pluck(result, 'cov');
    $scope.sigma = _.pluck(result, 'sigma');
    $scope.cvar = _.pluck(result, 'cvar');
    $scope.var1 = _.pluck(result, 'var1');
    $scope.swapRatesLoaded = true;
    $scope.expectedReturnsLoaded = true;
    console.log(result);
    $scope.loadChart();
  });

  console.log("advisor "+$rootScope.current_advisor+ " client "+$rootScope.client_id);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result){
    console.log("got client "+$rootScope.client_id);
    console.log(result);
    var tmp = result.member_since.slice(0,4);
    console.log(tmp);
    $rootScope.start_year = parseInt(tmp);
    console.log("start year "+$rootScope.start_year);
    $scope.displayChart();
  });

  // The following should be removed.
  $scope.Montecarlos = Montecarlos;
  var MontecarlosPromise = Montecarlos.getAll($rootScope.client_id);
  MontecarlosPromise.then(function(result) {
    //console.log(result);
    if (result[0]) {
      $scope.montecarlos = result[0].mcvals;
      //console.log(result[0].mcvals);
    }
  })

  $scope.getSwapRate = function(horizon) {
    if (horizon > 20) {
      horizon = 20;
    }
    console.log($scope.swapRates);
    if($scope.swapRates)
      return  parseFloat($scope.swapRates[horizon-1]/100.0);
    else
      return 0;
  }

  $scope.getExpectedReturn = function(horizon) {
    if (horizon > 20)
      horizon = 20;
      var mean = $scope.mean;
      var mu =   $scope.mu;
      console.log($scope.mean);
    if($scope.mean)
      return parseFloat($scope.mean[horizon-1]);
    else
      return 0;
  }

  $scope.getExpectedRisk = function(horizon) {
    if (horizon > 20)
      horizon = 20;
    var cov =    $scope.cov;
    var sigma =  $scope.sigma;
    var cvar =   $scope.cvar;
    var var1 =   $scope.var1;
    console.log($scope.sigma);
    console.log(horizon);
    return parseFloat(sigma[horizon-1]);
  }

  $scope.computeWealth = function(horizon, riskRate, returnRate) {
    var range1 = [];
    var range2 = [];
    var averages = [];
    var currentYear = new Date().getFullYear();;
    var currentWealth = 5.0;
    var currentSTD = 0.0;
    var tmp = [];

    averages.push([currentYear, currentWealth]);
    range1.push([currentYear, currentWealth, currentWealth]);
    range2.push([currentYear, currentWealth, currentWealth]);

    for (i = 1; i <= horizon; i++) {
      currentYear += 1;
      currentWealth = currentWealth * (1 + returnRate);
      currentSTD = riskRate*Math.sqrt(i);

      tmp = [currentYear, currentWealth.toFixed(2)];
      averages.push(tmp);
      //averages.push([currentYear, currentWealth]);
      range1.push([currentYear, currentWealth*(1-currentSTD), currentWealth*(1+currentSTD)]);
      range2.push([currentYear, currentWealth*(1-2*currentSTD), currentWealth*(1+2*currentSTD)]);
    }
    return currentWealth;
  }

  $scope.updateInitiaAmount = function(intiialAmt) {
    console.log("in updateInitialAmount");
    $scope.intialAmount = initialAmt;

    // Update the chart
    $scope.displayChart();
  }

  $scope.updateHorizon = function(slideAmount) {
    //get the element
    $scope.horizon_val = slideAmount;
    //var display = document.getElementById("chosenHorizon");
    //show the amount
    //display.innerHTML=slideAmount;
    var marketRisk = 0.15;
    var marketReturn = 0.08;
    var swapRate = 0.018;
    var curRisk = $scope.riskAmount/100.0;
    var z = (swapRate/curRisk) + (marketReturn-swapRate)/marketRisk;
    $scope.returnHorizon = $scope.returnAmount * $scope.horizon_val;
    $scope.riskHorizon = roundOne($scope.riskAmount * Math.sqrt($scope.horizon_val));
    $scope.lossHorizon = roundOne(CDF(-z*Math.sqrt($scope.horizon_val))*100.0);

    //update the chart
    $scope.displayChart();
  } // end updateHorizon

  $scope.updateRisk = function(slideAmount) {
    //get the element
    $scope.riskAmount = slideAmount;
    //var chRisk = document.getElementById("chosenRisk");
    //show the amount
    //chRisk.innerHTML=slideAmount;
    //var horizon = document.getElementById("chosenHorizon").innerHTML;
    var horizon = $scope.horizon_val;

    //get the element
    var swapRate = $scope.getSwapRate(horizon);
    //var expectedReturn = $scope.getExpectedReturn(horizon);
    //var expectedRisk = $scope.getExpectedRisk(horizon);
    var chosenRisk = slideAmount;
    var marketRisk = 0.15;
    var marketReturn = 0.08;
    swapRate = 0.018;

    console.log("in updateRisk swapRate="+swapRate);
    //r = computeWealth(horizon, chosenRisk, calcReturn);

    // Loss
    curRisk = slideAmount/100.0;
    console.log("swap rate "+swapRate);
    console.log("market return "+marketReturn);
    console.log("market risk "+marketRisk);
    console.log("current risk "+curRisk);

    //z = (swapRate/curRisk) + (expectedReturn-swapRate)/expectedRisk;
    z = (swapRate/curRisk) + (marketReturn-swapRate)/marketRisk;
    console.log("z "+z);
    p = CDF(-z);
    console.log("in updateRisk p="+p);

    // Set the loss value
    $scope.lossAmount = Math.round(p*100.0);
    //document.getElementById("chosenLoss").innerHTML = Math.round(p*100.0);

    // Formula from Aaron
    //var calcReturn = (chosenRisk/expectedRisk)*(expectedReturn-swapRate) + swapRate;
    //var calcReturn = (chosenRisk/marketRisk)*(marketReturn-swapRate) + swapRate;
    var calcReturn = ((z*swapRate*marketRisk)/(z*marketRisk-marketReturn+swapRate))*100.0;

    console.log("new return="+calcReturn);
    $('#slide3').val(calcReturn);
    $scope.returnAmount = Math.round(calcReturn);
    // Update the returns
    //chosenReturn.innerHTML = Math.floor(calcReturn);
    $scope.returnHorizon = $scope.returnAmount * $scope.horizon_val;
    $scope.riskHorizon = roundOne($scope.riskAmount * Math.sqrt($scope.horizon_val));
    $scope.lossHorizon = roundOne(CDF(-z*Math.sqrt($scope.horizon_val))*100.0);

    //update the chart
    $scope.displayChart();
    console.log($scope.riskAmount);
  } // end updateRisk

  $scope.updateReturn = function(slideAmount) {

    //get the element
    $scope.returnAmount = slideAmount;
    //var display = document.getElementById("chosenReturn");
    //show the amount
    //display.innerHTML=slideAmount;
    //var horizon = document.getElementById("chosenHorizon").innerHTML;
    var horizon = $scope.horizon_val;

    //get the element
    var swapRate = $scope.getSwapRate(horizon);
    //var expectedReturn = $scope.getExpectedReturn(horizon);
    //var expectedRisk = $scope.getExpectedRisk(horizon);
    var chosenReturn = slideAmount;
    var marketReturn = 0.08;
    var marketRisk = 0.15;

    // Loss
    curReturn = slideAmount/100.0;
    console.log("swap rate "+swapRate);
    console.log("market return "+marketReturn);
    console.log("market risk "+marketRisk);
    console.log("current return "+curReturn);

    z = curReturn*(marketReturn-swapRate)/(marketRisk*(curReturn-swapRate));
    console.log("z "+z);
    p = CDF(-z);
    console.log("in updatereturn p="+p);

    // Update the loss
    $scope.lossAmount = Math.round(p*100.0);

    // Formula from Aaron
    //var calcRisk = expectedRisk*(chosenReturn-swapRate)/(expectedReturn-swapRate);
    //var calcRisk = marketRisk*(chosenReturn-swapRate)/(marketReturn-swapRate);
    var calcRisk = ((swapRate*marketRisk)/(z*marketRisk-marketReturn+swapRate))*100.0;
    console.log("new risk="+calcRisk);

    // Update the risk
    $('#slide2').val(calcRisk);
    $scope.riskAmount = Math.round(calcRisk);
    // chosenRisk.innerHTML = Math.floor(calcRisk);

    //update the chart
    $scope.displayChart();
    $scope.returnAmount = slideAmount;
    $scope.returnHorizon = $scope.returnAmount * $scope.horizon_val;
    $scope.riskHorizon = roundOne($scope.riskAmount * Math.sqrt($scope.horizon_val));
    $scope.lossHorizon = roundOne(CDF(-z*Math.sqrt($scope.horizon_val))*100.0);

    console.log(slideAmount);

  } // end updateReturn

  $scope.updateLoss = function(lossAmount) {

    //get the element
    console.log("in updateLoss "+lossAmount);
    $scope.lossAmount = lossAmount;

    //Z = NormSInv(0.10);
    Z = -NormSInv(lossAmount/100.0);
    console.log(Z);
    console.log(CDF(-Z));

    //var horizon = document.getElementById("chosenHorizon").innerHTML;
    var horizon = $scope.horizon_val;

    //var swapRate = $scope.getSwapRate(horizon);
    //var current_return = document.getElementById("chosenReturn").innerHTML;
    //var current_risk = document.getElementById("chosenRisk").innerHTML;

    //current_return = current_return / 100.0;
    //current_risk = current_risk / 100.0;
    var market_return = 0.08;
    var market_risk = 0.15;
    var swapRate = 0.018;
    console.log("swap rate "+swapRate);
    console.log("market return "+market_return);
    console.log("market risk "+market_risk);

    new_return = ((Z * swapRate * market_risk)/(Z * market_risk - market_return + swapRate))*100.0;
    console.log("new return "+new_return);
    //if (new_return < 0) new_return = 0;

    new_risk = ((swapRate * market_risk)/(Z*market_risk - market_return + swapRate))*100.0;
    console.log("new risk "+new_risk);
    //if (new_risk < -0) new_risk = 0;

    $scope.riskAmount = Math.round(new_risk);
    //document.getElementById("chosenRisk").innerHTML = Math.round(new_risk);

    //update the chart
    //  $scope.displayChart();
    $scope.returnAmount = Math.round(new_return);
    //document.getElementById("chosenReturn").innerHTML = Math.round(new_return);
    $scope.returnHorizon = $scope.returnAmount * $scope.horizon_val;
    $scope.riskHorizon = roundOne($scope.riskAmount * Math.sqrt($scope.horizon_val));
    $scope.lossHorizon = roundOne(CDF(-Z*Math.sqrt($scope.horizon_val))*100.0);

    // Formula from Aaron
    //var calcRisk = expectedRisk*(chosenReturn-swapRate)/(expectedReturn-swapRate);

    // Update the risk
    //$('#slide2').val(calcRisk);
    //$scope.riskAmount = Math.round(calcRisk);
    //chosenRisk.innerHTML = Math.floor(calcRisk);

    //update the chart
    $scope.displayChart();
    //  $scope.returnAmount = slideAmount;
    // console.log(slideAmount);

  } // end updateLoss

  $scope.saveRiskCalibrations = function(intialAmount,slideAmount,riskAmount,returnAmount,lossAmount){
    console.log(intialAmount);
    console.log(slideAmount);
    console.log(riskAmount);
    console.log("target return "+returnAmount);
    console.log("larget loss "+lossAmount);
    console.log("client id "+$rootScope.client_id);

    data_query = { 'client_id': $rootScope.client_id, 'initial_invest': intialAmount, 'horizon': slideAmount,
                'target_risk': riskAmount, 'target_return': returnAmount,
                'target': 'Return', 'target_loss': lossAmount };
    console.log(data_query);
    res = RiskProfiles.updateRiskProfile(data_query);

    // Save Risk Profile History
    console.log("saving risk profile");
    data_query = { 'advisor_id': 1, 'client_id': $rootScope.client_id, 'initial_invest': intialAmount, 'horizon': slideAmount,
                'target_risk': riskAmount, 'target_return': returnAmount,
                'target': 'Return', 'target_loss': lossAmount };
    console.log(data_query);
    res = SavedRiskProfiles.saveProfile($rootScope.client_id, data_query);
  } // end saveRiskAllocation

  $scope.displayChart = function() {
    Highcharts.setOptions({
      colors: ['#347DCF']
    });
    var chart;
    console.log("in displayChart "+$scope.returnAmount+" "+$scope.riskAmount+" "+$scope.horizon_val);
    //var horizon = document.getElementById("chosenHorizon").innerHTML;
    var horizon = $scope.horizon_val;
    //var chRisk = document.getElementById("chosenRisk").innerHTML;
    var chRisk = $scope.riskAmount;
    //var chReturn = document.getElementById("chosenReturn").innerHTML;
    var chReturn = $scope.returnAmount;
    //var initWealth = parseFloat(document.getElementById("intial_invest").value);
    var initWealth = $scope.initialAmount;
    console.log("horizon "+horizon+" "+"initialAmount "+initWealth);
    console.log("start year "+$rootScope.start_year);

    var ranges = getRanges($rootScope.start_year, horizon, chRisk, chReturn, initWealth);
    var disp_range1 = ranges[0];
    var disp_range2 = ranges[1];
    var disp_averages = ranges[2];

    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'wealthchart',
            type: 'spline'
        },
        exporting: { enabled: false },
        title: {
            text: 'Wealth Projection',
            style:{
                  color:'ffffff',
                  fontWeight:'bold',
                  fontSize: '16px !important',
                 },
            enabled:false
        },

        xAxis: {
            type: 'linear',
            allowDecimals: false
        },

        yAxis: {
            title: {
                text: 'Millions',
                style:{
                  fontSize: '16px !important',
                 }
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: ' million'
        },

        legend: {
            enabled:false,
        },

        series: [{
            name: 'Projected Wealth', //$rootScope.labels["L0085"], //'Projected Wealth',
            data: disp_averages,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        }, {
            name: '67 Percentile', //$rootScope.labels["L0086"], //'67 Percentile',
            data: disp_range1,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.4,
            zIndex: 0
        }, {
            name: '90 Percentile', //$rootScope.labels["L0087"], //'90 Percentile',
            data: disp_range2,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.2,
            zIndex: 0
        }]
    });
    $scope.runMCSimulation(chRisk, chReturn);
    //$scope.runMCSimulation2(chRisk, chReturn);
    // $scope.updateInitiaAmount(intiialAmt);
  }


 $scope.displayLineCharts = function(xLabels) {
    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];

    Highcharts.setOptions({
      colors: colors
      //colors: ['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
      //         '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
    $('#simulation').highcharts({
      exporting: { enabled: false },
        title: {
            text: 'Simulation',
            enabled:false,
            x: -20,
            style:{
                  color:'#ffffff',
                  fontWeight:'bold',
                  fontSize: '13px !important',
                 }
        },
   /*     subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        }, */
        xAxis: {
            categories: xLabels,
        },
        yAxis: {
            title: {
                text: 'Projected Wealth (millions)',
                style:{
                  fontSize: '16px !important',
                 }
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' million'
        },
        legend: {
            layout: 'vertical',
            align: 'middle',
            verticalAlign: 'top',
            borderWidth: 0,
            enabled: false,
            style:{
                  color:'#4D7FBA',
                  fontWeight:'bold',
                  fontSize: '13px !important',
                }
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            },
            line: {
                dataLabels: {
                  align: 'top',
                  enabled: true,
                },
                enableMouseTracking: true
            }
        },
        series: $scope.selected_iteration
    });
}

$scope.displayLineCharts2 = function(xLabels) {  

    var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
    Highcharts.setOptions({
      colors: colors
      //colors: ['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
      //         '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
    $('#simulation2').highcharts({
      exporting: { enabled: false },
        title: {
            text: 'Simulation',
            enabled:false,
            x: -20,
            style:{
                  color:'#ffffff',
                  fontWeight:'bold',
                  fontSize: '13px !important',
                 }
        },
   /*     subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        }, */
        xAxis: {
            categories: xLabels,
        },
        yAxis: {
            title: {
                text: 'Projected Wealth (millions)',
                style:{
                  fontSize: '16px !important',
                 }
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' million'
        },
        legend: {
            layout: 'vertical',
            align: 'middle',
            verticalAlign: 'top',
            enabled: false,
            borderWidth: 0,
            style:{
                  color:'#4D7FBA',
                  fontWeight:'bold',
                  fontSize: '13px !important',
                 }
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            },
            line: {
                dataLabels: {
                    align: 'top',
                  enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: $scope.selected_iteration
    });
}

$scope.runMCSimulation = function(returnAmount,riskAmount)
{
  // Perform the monte carlo simulation and display the graph

  //var horizon_val = parseInt(document.getElementById("chosenHorizon").innerHTML);
  var iters = parseInt(document.getElementById("iterations").value);
  //risk_val = document.getElementById("slide2").value;
  //return_val = document.getElementById("slide3").value;
  //var start_val = parseFloat(document.getElementById("intial_invest").value);
  //console.log(returnAmount);
  //console.log(riskAmount);
  var horizon_val = $scope.horizon_val;
  var risk_val = $scope.riskAmount;
  var return_val = $scope.returnAmount;
  var start_val = $scope.initialAmount;

  // Horizontal labels for the graph
  //curYear = new Date().getFullYear();
  curYear = $rootScope.start_year;
  xLabels = [];
  for (i=0; i <= horizon_val; i++) {
    xLabels[i] = curYear.toString();
    curYear++;
  }

  //console.log($scope.Montecarlos);
  console.log(risk_val);
  console.log(return_val);
  console.log(iters);
  console.log("updating runMCSimulation "+$rootScope.client_id);
  //clientid = 2;
  data_query = {client_id: $rootScope.client_id, horizon: horizon_val, iterations: iters, startval: start_val, riskval: risk_val, returnval: return_val, mcvals: "[]"};
  console.log(data_query);
  //res = Montecarlos.updateOne(data_query);
  //console.log(res);
  //console.log("at end");

  //$rootScope.client_id = 2;
  //console.log("*** client_id="+$rootScope.client_id)
  //var MontecarlosPromise = Montecarlos.getOne(2, data_query);
  var MontecarlosPromise = Montecarlos.getOne($rootScope.client_id, data_query);
  MontecarlosPromise.then(function(result) {
    console.log("processing get one runMCSimulation ");
    console.log(result[0]);
    if (result) {
      $scope.montecarlos = result[0].mcvals;
      //console.log(result[0].mcvals);
      s1 = $scope.montecarlos;
      //console.log(s1);
      if (s1) {
        sarray = [];
        start1 = 1;
        end1 = 0;
        for(var idx = 0; idx < iters; idx++){
          end1 = s1.indexOf(']',start1);
          //check that end1 is not -1
          sub1 = s1.substring(start1+1,end1);
          start1 = end1+2;
          //console.log(idx);
          //console.log(sub1);

          ssarray = sub1.split(',')
          for (var idx2=0; idx2 < horizon_val; idx2++) {
            ssarray[idx2] = Math.round(ssarray[idx2] * 100)/100;
          }
          //console.log(ssarray);

          var lastVal = ssarray[horizon_val-1];
          n1 = lastVal.toString();
          //console.log("last value = "+n1);

          //n1 = "Run " + (idx+1).toString();
          elem1 = { name: n1, data: ssarray };
          //console.log(elem1);
          sarray.push(elem1);
        }
        //console.log(sarray);
        series_array = sarray;
        $scope.selected_iteration = series_array.slice(0, $scope.iterations);
      }
      $scope.displayLineCharts(xLabels);

      // $scope.displayLineCharts2(xLabels);
    }
  })
} // end runMCSimulation

$scope.runMCSimulation2 = function(returnAmount,riskAmount)
{
  // Perform the monte carlo simulation and display the graph

  //var horizon_val = parseInt(document.getElementById("chosenHorizon").innerHTML);
  var iters = parseInt(document.getElementById("iterations").value);
  //risk_val = document.getElementById("slide2").value;
  //return_val = document.getElementById("slide3").value;
  //var start_val = parseFloat(document.getElementById("intial_invest").value);
  //console.log(returnAmount);
  //console.log(riskAmount);
  var horizon_val = $scope.horizon_val;
  var risk_val = $scope.riskAmount;
  var return_val = $scope.returnAmount;
  var start_val = $scope.initialAmount;

  // Horizontal labels for the graph
  //curYear = new Date().getFullYear();
  curYear = $rootScope.start_year;
  xLabels = [];
  for (i=0; i <= horizon_val; i++) {
    xLabels[i] = curYear.toString();
    curYear++;
  }

  //console.log($scope.Montecarlos);
  console.log(risk_val);
  console.log(return_val);
  console.log(iters);
  console.log("updating runMCSimulation2 "+$rootScope.client_id);
  //clientid = 2;
  data_query = {client_id: $rootScope.client_id, horizon: horizon_val, iterations: iters, startval: start_val, riskval: risk_val, returnval: return_val, mcvals: "[]"};
  console.log(data_query);
  //res = Montecarlos.updateOne(data_query);
  //console.log(res);
  //console.log("at end");

  //$rootScope.client_id = 2;
  //console.log("*** client_id="+$rootScope.client_id)
  //var MontecarlosPromise = Montecarlos.getOne(2, data_query);
  var MontecarlosPromise = Montecarlos.getOne($rootScope.client_id, data_query);
  MontecarlosPromise.then(function(result) {
    console.log("processing get one runMCSimulation2 ");
    console.log(result[0]);
    $scope.montecarlos = result[0].mcvals;
    //console.log(result[0].mcvals);
    s1 = $scope.montecarlos;
    //console.log(s1);
    if (s1) {
      sarray = [];
      start1 = 1;
      end1 = 0;
      for(idx = 0; idx < iters; idx++){
        end1 = s1.indexOf(']',start1);
        //check that end1 is not -1
        sub1 = s1.substring(start1+1,end1);
        start1 = end1+2;
        //console.log(idx);

        ssarray = sub1.split(',');
        for (idx2=0; idx2 < horizon_val; idx2++) {
          ssarray[idx2] = Math.round(ssarray[idx2] * 100)/100;
          // ssarray[idx2] = parseFloat(ssarray[idx2]);
        }

        //RAB
        var lastVal = ssarray[horizon_val-1];
        n1 = lastVal.toString();
        //console.log("last value = "+n1);

        //console.log(ssarray);
        //n1 = "Run " + (idx+1).toString();
        elem1 = { name: n1, data: ssarray };
        //console.log(elem1);
        sarray.push(elem1);
      }
      //console.log(sarray);
      series_array = sarray;
      $scope.selected_iteration = series_array.slice(0, $scope.iterations);
    }
    // $scope.displayLineCharts(xLabels);
    $scope.displayLineCharts2(xLabels);
  })

} // end runMCSimulation2

})

finvese.controller('assetsAllocationController', function($rootScope, $scope, $location, RiskProfiles, ModelPortfolio, ClientPortfolio, AdvisorAllocation, AdvisorPortfolio, SavedPortfolio, SavedAllocation, Advisors, Client, Translations) {
  $rootScope.assets_allocation = true;
  $rootScope.assets_class  = false;
  $rootScope.investments = false;
  $rootScope.savings = false;
  $rootScope.income = false;
  $rootScope.expenses = false;
  $rootScope.asset_names = false;

  $rootScope.home = false;
  $scope.target_val = 'Return';
  $scope.horizon = 10;
  $rootScope.clients_list = false;
  $scope.initial_invest = 5.0;
  $rootScope.login = false;
  $rootScope.clientListMenu = false;
  console.log("in assetsAllocationController "+$rootScope.client_id);

  //console.log($location.$$path);
  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  setAdvisor($rootScope);

  console.log("in assetsAllocationController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Allocation";
      }
    })


  //$rootScope.labels = [];

  $scope.radioData = [ { label: 'Risk', value: 'Risk' }, { label: 'Return', value: 'Return' }, { label: 'Loss', value: 'Loss' } ];

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);
   
      if ($rootScope.client_id == 0) 
        setAdvisorMenuLabels($rootScope);

      var label = document.getElementById('riskToleranceLabel');
      if (label) label.textContent = $rootScope.labels["L0100"];
      label = document.getElementById('portfolioLabel');
      if (label) label.textContent = $rootScope.labels["L0068"];
      label = document.getElementById('advisoryModelPortfolioLabel');
      if (label) label.textContent = $rootScope.labels["L0098"];
      label = document.getElementById('default');
      if (label) label.textContent = $rootScope.labels["L0101"];
      label = document.getElementById('optimize');
      if (label) label.textContent = $rootScope.labels["L0110"];
      label = document.getElementById('save');
      if (label) label.textContent = $rootScope.labels["L0099"];

      //$scope.assets_head = ['Asset Class','Estimated Returns','Historical Returns','Estimated Risks','Model Weight','Client Weight','Min (%)','Max (%)','Returns Views','Risk Views'];
      $scope.assets_head = [$rootScope.labels["L0091"], //'Asset Class',
                            $rootScope.labels["L0102"], //'Estimated Returns',
                            $rootScope.labels["L0103"], //'Historical Returns',
                            $rootScope.labels["L0104"], //'Estimated Risks',
                            $rootScope.labels["L0105"], //'Model Weight',
                            $rootScope.labels["L0106"], //'Client Weight',
                            $rootScope.labels["L0107"] + " (%)", //'Min (%)',
                            $rootScope.labels["L0108"] + " (%)", //'Max (%)',
                            $rootScope.labels["L0109"], //'Returns Views',
                            "Risk Views"]; //$rootScope.labels["L0099"] ]; //'Risk Views'];

      $scope.radioData[0].label = $rootScope.labels["L0062"]; // Risk
      $scope.radioData[1].label = $rootScope.labels["L0061"]; // Return
      $scope.radioData[2].label = $rootScope.labels["L0063"]; // Loss
      $scope.radioData[0].value = $rootScope.labels["L0062"]; // Risk
      $scope.radioData[1].value = $rootScope.labels["L0061"]; // Return
      $scope.radioData[2].value = $rootScope.labels["L0063"]; // Loss
      //label = document.getElementById('risk');
      //label.value = $rootScope.labels["L0062"];
      //label = document.getElementById('return');
      //label.value = $rootScope.labels["L0061"];
      //label = document.getElementById('loss');
      //label.value = $rootScope.labels["L0063"];
  }

   // $rootScope.hideHeader = function(){
   //      $('.navbar-fixed-top').addClass('ng-hide');
   // }

    // Run the initial allocation
//      var ClientPortfolioPromise = ClientPortfolio.getAll(-1);
//      ClientPortfolioPromise.then(function(result) {
 //       console.log("processing clientportfolio getAll-1");
 //       console.log(result);

 //       $scope.asset_allocations = result;
 //       arrlen = $scope.asset_allocations.length;
 //       for (var i=0; i < arrlen; i++) {
 //         $scope.asset_names[i] = $scope.asset_allocations[i].asset_description
 //         console.log($scope.asset_allocations[i].asset_description);
  //      }
 //       console.log($scope.asset_names);
 //     });

 //     $scope.reloadCharts();
  $scope.target = {'risk': 3.0, 'return': 3.0,'loss': 3.0}

  $scope.targetRadio = {
      group1 : 'Risk',
      group2 : 'Return',
      group3 : 'Loss'
    };
    $scope.data = {
      group1 : 'Return',
    };
    // $scope.desiredRisk = function(){
    //   $(".desired-risk, .index6").css("display","table-cell");
    //   $(".desired-return, .index5").css("display","none");

    // }
    // $scope.checked= function(){
    //   if ( $( "#risk" ).is( ".md-checked" ) ) {
    //     $(".desired-risk, .index6").css("display","table-cell");
    //     $(".desired-return, .index5").css("display","none");
    //   }
    //   if ( $( "#return" ).is( ".md-checked" ) ) {
    //     $(".desired-risk, .index6").css("display","none");
    //     $(".desired-return, .index5").css("display","table-cell");
    //   }
    // }
    // $scope.checked();
    // $scope.desiredReturn = function(){
    //   $(".desired-risk, .index6").css("display","none");
    //   $(".desired-return, .index5").css("display","table-cell");
    // }

  console.log("*** client_id="+$rootScope.client_id)
  if ($rootScope.client_id == 0) {
    $scope.target_risk = 3;
    $scope.target_return = 3;
    $scope.target_loss = 3;
    $scope.horizon = 10;
    $scope.initial_invest = 10;
    document.getElementById("target_risk").value = $scope.target_risk;
    document.getElementById("target_return").value = $scope.target_return;
    document.getElementById("target_loss").value = $scope.target_loss;
  } else {
    var RiskProfilesPromise = RiskProfiles.getOne($rootScope.client_id);
    console.log("getting risk profile");
    RiskProfilesPromise.then(function(result) {
      console.log("inside risk profiles");
      console.log(result[0].horizon);
      $scope.target_risk = result[0].target_risk;
      $scope.target_return = result[0].target_return;
      $scope.target_loss = result[0].target_loss;
      $scope.horizon = result[0].horizon;
      $scope.initial_invest = result[0].initial_invest;

      //document.getElementById("horizon").value = result[0].horizon;
      document.getElementById("target_risk").value = result[0].target_risk;
      document.getElementById("target_return").value = result[0].target_return;
      document.getElementById("target_loss").value = result[0].target_loss;
      // see also result[0].initial_invest

      console.log("target "+result[0].target);
      console.log("horizon "+$scope.horizon);
      console.log("initial_invest "+$scope.initial_invest);
      if (result[0].target == 0) {
        $scope.target_val = 'Risk';
        $scope.data.group1 = 'Risk';
      }
      else if (result[0].target == 1) {
        $scope.target_val = 'Return';
        $scope.data.group1 = 'Return';
      }
      else if (result[0].target == 2) {
        $scope.target_val = 'Loss';
        $scope.data.group1 = 'Loss';
      }

      //$scope.swapRatesLoaded = true;
      //$scope.expectedReturnsLoaded = true;
      console.log(result);
      //$scope.loadChart();
    })
    console.log("got risk profile");
  }

  //var AssetSelectionPromise = AssetSelection.getOne($rootScope.client_id);
  //console.log("getting asset selction");
  //AssetSelectionPromise.then(function(result) {
  //  console.log("in asset selection promise");
 //   console.log(result);
 //   if (result.length > 0) {
  //    selection_string = result[0].asset_selections;
  //    console.log(selection_string);
  //  }
  //})
  //console.log("got asset selections");


  var clientid  = $rootScope.client_id;

  var ClientPortfolioPromise = ClientPortfolio.getOne(-clientid);
  console.log("getting client portfolio");
  ClientPortfolioPromise.then(function(result) {
    console.log("inside client portfolio promise getAll1 "+clientid);
    console.log(result);
    $scope.asset_allocations = result;

    //arrlen = $scope.asset_allocations.length;
    arrlen = result.length;
    for (var i=0; i < arrlen; i++) {
      $scope.asset_allocations[i] = result[i];
      if (!result[i].desired_risk) $scope.asset_allocations[i].desired_risk = roundTwo(result[i].estimated_risk);
      $scope.asset_allocations[i].client_weight = roundTwo(result[i].asset_weight);
      $scope.asset_names[i] = $scope.asset_allocations[i].asset_description;
      console.log($scope.asset_allocations[i].asset_description);
    }
    console.log($scope.asset_names);

    var ModelPortfolioPromise = ModelPortfolio.getOne(clientid);
    console.log("getting model portfolio "+clientid);
    ModelPortfolioPromise.then(function(result) {
      console.log("inside model portfolio promise");
      console.log(result);
      arrlen2 = result.length;
      //$scope.asset_allocations[0].model
      for (var i=0; i < arrlen2; i++) {
        for (var j=0; j < arrlen; j++) {
          if (result[i].asset_class == $scope.asset_allocations[j].asset_class) {
            console.log("found match "+result[i].asset_class+ " "+result[i].asset_weight);
            $scope.asset_allocations[j].asset_weight = result[i].asset_weight;
          }
        }
      }

    })
    console.log("got model portfolio");
  });
  console.log("got client portfolio");

  // Invoke initial allocation
  //$scope.optimizeAllocation();

  //$scope.assets_head = ['Asset Class','Estimated Returns','Historical Returns','Estimated Risks','Model Weight','Client Weight','Min (%)','Max (%)','Returns Views','Risk Views'];


//   $scope.asset_allocations = [{'asset_description': 'Cash', 'estimated_return': 0.6,
//                                'historical_return': 0.6, 'desired_return': 0.6, 'estimated_risk': 0.0,
//                                'desired_risk' : 0.0, 'asset_weight' : 10.38, 'client_portfolio' : 0.0, 'id' : 1},
//                                {'asset_description': 'AS-EQ', 'estimated_return':13.4,
//                                'historical_return': 2.2, 'desired_return': 18.8, 'estimated_risk': 18.8,
//                                'desired_risk' : 0.0, 'asset_weight' : 0.91, 'client_portfolio' : 0.0, 'id' : 5}
//                               ]

    // $scope.updateValues = function(){
    //   console.log("updating");
    // }
    $scope.updateValues = function(desired_return, index) {
      console.log('in updateValues ' + desired_return+ " "+ index);
      console.log($scope.asset_allocations[index]);
      data_query = $scope.asset_allocations[index];
      console.log(data_query);
      id = $scope.asset_allocations[index].id;
      console.log(id);
      var CPPromise = ClientPortfolio.saveClientPortfolio(id, data_query);
      CPPromise.then(function(result){
        console.log("in CPPromise");
        $scope.optimizeAllocation();
      });
    }

    $scope.updateRisk = function(desired_risk, index) {
      console.log('in updateRisk ' + desired_risk+ " "+ index);
      console.log($scope.asset_allocations[index]);
      data_query = $scope.asset_allocations[index];
      console.log(data_query);
      id = $scope.asset_allocations[index].id;
      console.log(id);
      var CPPromise = ClientPortfolio.saveClientPortfolio(id, data_query);
      CPPromise.then(function(result){
        console.log("in CPPromise");
        $scope.optimizeAllocation();
      });
    }

    $scope.updateValuesMin = function(minval, index) {
      console.log('in updateValuesMin ' + minval+ " "+ index);
      console.log($scope.asset_allocations[index]);
      data_query = $scope.asset_allocations[index];
      console.log(data_query);
      id = $scope.asset_allocations[index].id;
      console.log(id);
      var CPPromise = ClientPortfolio.saveClientPortfolio(id, data_query);
      CPPromise.then(function(result){
        console.log("in CPPromise");
        $scope.optimizeAllocation();
      });
    }

    $scope.updateValuesMax = function(maxval, index) {
      console.log('in updateValuesMax ' + maxval+ " "+ index);
      console.log($scope.asset_allocations[index]);
      data_query = $scope.asset_allocations[index];
      console.log(data_query);
      id = $scope.asset_allocations[index].id;
      console.log(id);
      var CPPromise = ClientPortfolio.saveClientPortfolio(id, data_query);
      CPPromose.then(function(result){
        console.log("in CPPromise");
        $scope.optimizeAllocation();
      });
    }


    $scope.showDetail = function(event)
    {
     console.log($(event.target).attr("data-id"));
    }

    $scope.buildChartData= function(asset_allocations){
      var value_for_chart = [];
      angular.forEach(asset_allocations, function (value, key) {
        value_for_chart[key] = [value.asset_description, value.asset_weight];
      })
      //value_for_chart.push({
      //                name: 'Proprietary or Undetectable',
      //                y: 0.2,
      //                dataLabels: {
      //                    enabled: false
      //                }
      //            });
      console.log(value_for_chart);
      return value_for_chart;
    }

    $scope.pieCharts = function(dom_id, title_text, halign, valign, yangle, style_text_object, chart_data){
      var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
      Highcharts.setOptions({
        colors: colors
        //   colors: ['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE','#60C379','#000000','#F2AE33','#FF0000',
        //            '#656FEB','#2C8D5C','#92D9DB','#2C57BE','#ED7A2A','#FF095F']
      });
      $(dom_id).highcharts({
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: 0,
              plotShadow: false,
              marginTop:10,
              type: 'pie'
          },
          exporting: { enabled: false },
          title: {
              text: title_text,
              align: halign,
              verticalAlign: valign,
              y: yangle,
              style: style_text_object
          },
          legend: {
            itemStyle: {
                fontWeight: 'normal'
              }
            },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
              enabled:true,
              animation:false,
              borderColor: 'null',
          },
          plotOptions: {
              // pie: {
              //     dataLabels: {
              //         enabled: true,
              //         distance: -40,
              //         rotate: 'left',
              //         style: {
              //             fontWeight: 'bold',
              //             color: 'white',
              //             textShadow: '0px 1px 2px black',
              //             fontSize:'10px',
              //             letterSpacing:'0px',
              //         },
              //         y: 0,
              //         format: '{point.name} , {point.y:.1f}%',
              //     },
              //     startAngle: -240,
              //     endAngle: 120,
              //     center: ['50%', '52%']
              // }
             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
          },
          // series: [{
          //     type: 'pie',
          //     name: 'Advisor Model Portfolio',
          //     innerSize: '40%',
          //     data: chart_data,
          //     states: {
          //             hover: {
          //               enabled: false,
          //               halo: {
          //                 size: 0
          //               }
          //             }
          //           }
          // }]
           //series: [{
           //     name: 'Advisor Model Portfolio',
           //     colorByPoint: true,
           //     data: [{
           //         name: 'Cash',
           //         y: 56.33,
           //         color: '#EB4932',
           //     }, {
           //         name: 'Europe',
           //         y: 24.03,
           //         color: '#8085E9',
           //         sliced: true,
           //         selected: true
           //     }, {
           //         name: 'Asia',
           //         y: 10.38,
           //         color: '#D00049'
           //     }, {
           //         name: 'N_America',
           //         y: 4.77,
           //         color: '#347DCF'
           //     }, {
           //         name: 'GL-IG',
           //         y: 0.91,
           //         color: '#64D0AE'
           //     }]
           // }]
            series: [{
              data: chart_data
            }]

      });
    }

    $scope.barCharts = function(dom_id, title_text, halign, valign, yangle, style_text_object, chart_data){

          var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
          Highcharts.setOptions({
            colors: colors
            //colors: ['#4374AE', '#3E6697', '#335680', '#92A5D0', '#B7C6E0']
          });
         $(dom_id).highcharts({
          chart: {
              type: 'column',
              marginTop: 80,
              marginRight:-30,
          },
          exporting: { enabled: false },
         title: {
              text: title_text,
              align: halign,
              verticalAlign: valign,
              y: yangle,
              style: style_text_object
          },
          plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
          },
          xAxis: {
              //categories: ['Cash', 'US-EQ', 'EU-EQ', 'GL-IB', 'AS-EQ']
              categories: $scope.asset_names
          },
          xAxis: {
            type: 'category',
            labels: {
                autoRotationLimit:0
            }
        },
        yAxis: {
            gridLineWidth: 0
        },
          tooltip: {
              pointFormat: 'Percentage Weight: <b>{point.y:.1f} %</b>',
              enabled:true
          },
          credits: {
              enabled: false
          },
          series: [{
              data: chart_data
          }]
      });

    }


    $scope.saveAllocation = function() {
      console.log("in saveAllocation "+$rootScope.client_id);

      if ($rootScope.client_id == 0) {
        console.log("Saving advisor allocation "+$rootScope.current_advisor);
        var advisor_id = $rootScope.current_advisor;
        //if ($rootScope.advisor_id)
        //  advisor_id = $rootScope.advisor_id;

        // Compute the expected return for the portfolio
        var expr = 0.0;
        for (var k=0; k < arrlen; k++) {
            var w = $scope.asset_allocations[k].client_weight;
            var r = $scope.asset_allocations[k].desired_return;
            //console.log("w "+w+" r "+r);
            expr = expr + w * r;
        }
        console.log("expected return "+expr);
        var exprisk = $scope.asset_allocations[0].portfolio_risk;
        console.log("expected risk "+exprisk);


        // Save the portfolio
        var data_query = { 'advisor_id': advisor_id, 'target_risk': $scope.target_risk,
              'target_return': $scope.target_return, 'target': 1, 'portfolio_id': 0,
              'target_loss': $scope.target_loss,
              'horizon' : $scope.horizon,
              'initial_invest' : $scope.initial_invest,
              'expected_return' : expr/100.0, 'expected_risk': exprisk };
        console.log(data_query);

        var AdvisorPortfolioPromise = AdvisorPortfolio.savePortfolio(advisor_id, data_query);
        AdvisorPortfolioPromise.then(function(result) {
          console.log("inside advisor portfolio promise");
          console.log(result);

          //res = AdvisorPortfolio.savePortfolio(advisor_id, data_query);

          arrlen = $scope.asset_allocations.length;
          for (var i=0; i < arrlen; i++) {
            data_query = $scope.asset_allocations[i];
            data_query.advisor_id = advisor_id;
            data_query.portfolio_id = 0;
            console.log(data_query);
            id = $scope.asset_allocations[i].id;
            console.log(id);
            res = AdvisorAllocation.saveAdvisorAllocation(id, data_query);
          }

        })

        //res = AdvisorPortfolio.savePortfolio(advisor_id, data_query);

        //arrlen = $scope.asset_allocations.length;
        //for (var i=0; i < arrlen; i++) {
        //  data_query = $scope.asset_allocations[i];
        //  data_query.advisor_id = 1;
        //  data_query.portfolio_id = 0;
        //  console.log(data_query);
        //  id = $scope.asset_allocations[i].id;
        //  console.log(id);
        //  res = AdvisorAllocation.saveAdvisorAllocation(id, data_query);
        //}
      }
      else {
        arrlen = $scope.asset_allocations.length;
        console.log("Saving client allocation "+arrlen);
        arrlen = $scope.asset_allocations.length;
        var advisor_id = $rootScope.current_advisor;
        var client_id = $rootScope.client_id;
        console.log($scope.asset_allocations);

        // Compute the expected return for the portfolio
        var expr = 0.0;
        for (var k=0; k < arrlen; k++) {
            //console.log($scope.asset_allocations[k]);
            var w = $scope.asset_allocations[k].client_weight;
            var r = $scope.asset_allocations[k].desired_return;
            //console.log("w "+w+" r "+r);
            expr = expr + w * r;
        }
        console.log("expected return "+expr);
        var exprisk = $scope.asset_allocations[0].portfolio_risk;
        console.log("expected risk "+exprisk);

        // Save the portfolio
        var data_query = { 'advisor_id': advisor_id, 'client_id': client_id, 'target_risk': $scope.target_risk,
              'target_return': $scope.target_return, 'target': 1, 'portfolio_id': 0,
              'target_loss': $scope.target_loss,
              'horizon' : $scope.horizon,
              'expected_return': expr/100.0,
              'expected_risk': exprisk,
              'aum' : $scope.initial_invest };
        console.log(data_query);

        var SavedPortfolioPromise = SavedPortfolio.savePortfolio(client_id, data_query);
        SavedPortfolioPromise.then(function(result) {
          console.log("inside saved portfolio promise");
          console.log(result);

          //arrlen = $scope.asset_allocations.length;
          for (var i=0; i < arrlen; i++) {
            data_query = $scope.asset_allocations[i];
            data_query.advisor_id = $rootScope.current_advisor;
            data_query.portfolio_id = 0;
            console.log(data_query);
            id = $scope.asset_allocations[i].id;
            console.log(id);
            res = SavedAllocation.saveAllocation(id, data_query);
          }

        })

        for (var i=0; i < arrlen; i++) {
          //data_query = {id: $scope.asset_allocations4[i].id,
          //            client_id: 1,
          //            asset_class: $scope.product_allocations4[i].asset_class,
          //            fund_id: $scope.product_allocations4[i].fund_id,
          //            fund_allocation: $scope.product_allocations4[i].portfolio_weight};
          data_query = $scope.asset_allocations[i];
          console.log(data_query);
          id = $scope.asset_allocations[i].id;
          console.log(id);
          res = ClientPortfolio.saveClientPortfolio(id, data_query);
        }
      }
      console.log("at end of saveAllocation");
    }


    $scope.copyAllocation = function() {
      console.log("in copyAllocation");
      arrlen = $scope.asset_allocations.length;
      for (var i=0; i < arrlen; i++) {
        $scope.asset_allocations[i].desired_return = Math.round($scope.asset_allocations[i].estimated_return*100)/100;
        $scope.asset_allocations[i].desired_risk = Math.round($scope.asset_allocations[i].estimated_risk*100)/100;

        var data_query = $scope.asset_allocations[i];
        res = ClientPortfolio.saveClientPortfolio($scope.asset_allocations[i].id, data_query);
      }
      //$scope.saveAllocation();
    }

    $scope.optimizeAllocation = function() {
      console.log("in optimizeAllocation");
      console.log($scope.asset_allocations);


      data_query = $scope.asset_allocations;
      //console.log(data_query);
      //res = ClientPortfolio.updateClientPortfolio(data_query);
      console.log("at end of optimizeAllocation");

      var clientid = $rootScope.client_id;
      var ClientPortfolioPromise = ClientPortfolio.getAll(-clientid);
      ClientPortfolioPromise.then(function(result) {
        console.log("processing clientportfolio getAll-1");
        console.log(result);

        $scope.asset_allocations = result;
        arrlen = $scope.asset_allocations.length;
        for (var i=0; i < arrlen; i++) {
          $scope.asset_allocations[i] = result[i];
          $scope.asset_names[i] = $scope.asset_allocations[i].asset_description;
          $scope.asset_allocations[i].client_weight = Math.round(result[i].asset_weight*100.0)/100.0;
          console.log($scope.asset_allocations[i].asset_description);
        }
        console.log($scope.asset_names);

        var ModelPortfolioPromise = ModelPortfolio.getOne(clientid);
        console.log("getting model portfolio");
        ModelPortfolioPromise.then(function(result) {
          console.log("inside model portfolio promise");
          console.log(result);
          arrlen2 = result.length;
          //$scope.asset_allocations[0].model
          for (var i=0; i < arrlen2; i++) {
            for (var j=0; j < arrlen; j++) {
              if (result[i].asset_class == $scope.asset_allocations[j].asset_class) {
                console.log("found match "+result[i].asset_class+ " "+result[i].asset_weight);
                $scope.asset_allocations[j].asset_weight = result[i].asset_weight;
              }
            }
          }

        })
        console.log("got model portfolio");
        $scope.reloadCharts();
      });

      //$scope.reloadCharts();
    }

    $scope.updateTargetRisk = function(value){
      console.log("in updateTargetRisk "+value);
      $scope.target_risk = value;
      console.log($scope.target.risk + " " + 'target risk value');
      //$scope.optimizeAllocation();

      var horizon = $scope.horizon;
      var marketRisk = 0.15;
      var marketReturn = 0.08;
      var swapRate = 0.018;
      var curRisk = value/100.0;
      console.log("swap rate "+swapRate);
      console.log("market return "+marketReturn);
      console.log("market risk "+marketRisk);
      console.log("current risk "+curRisk);
      var z = (swapRate/curRisk) + (marketReturn-swapRate)/marketRisk;
      var p = CDF(-z);
     

      // Set the loss value
      $scope.target_loss = Math.round(p*100.0);
      $scope.target.loss = $scope.target_loss;
      console.log("in updateRisk target loss="+$scope.target_loss);

      // Formula from Aaron
      var calcReturn = ((z*swapRate*marketRisk)/(z*marketRisk-marketReturn+swapRate))*100.0;
      $scope.target_return = Math.round(calcReturn);
      $scope.target.return = $scope.target_return;
      console.log("in updateRisk target return="+$scope.target_return);

      data_query = { 'client_id': $rootScope.client_id, 'target_risk': $scope.target_risk,
          'target_return': $scope.target_return,
          'target_loss': $scope.target_loss,
          'horizon' : $scope.horizon,
          'initial_invest' : $scope.initial_invest };
      console.log(data_query);
      var RPProfile = RiskProfiles.updateRiskProfile(data_query);
      RPProfile.then(function(result) {
           console.log("in RPPromise");
          $scope.optimizeAllocation();
      });


      // Update the returns
      //chosenReturn.innerHTML = Math.floor(calcReturn);
      //$scope.returnHorizon = $scope.returnAmount * $scope.horizon_val;
      //$scope.riskHorizon = roundOne($scope.riskAmount * Math.sqrt($scope.horizon_val));
      //$scope.lossHorizon = roundOne(CDF(-z*Math.sqrt($scope.horizon_val))*100.0);
    }

    $scope.updateTargetReturn = function(value){
      console.log("in updateTargetReturn "+value);
      $scope.target_return = value;
      console.log($scope.target.return + " " + 'target return value');

      var horizon = $scope.horizon;
      var swapRate =  0.018;
      var chosenReturn = value;
      var marketReturn = 0.08;
      var marketRisk = 0.15;
      var curReturn = value/100.0;
      console.log("swap rate "+swapRate);
      console.log("market return "+marketReturn);
      console.log("market risk "+marketRisk);
      console.log("current return "+curReturn);

      var z = curReturn*(marketReturn-swapRate)/(marketRisk*(curReturn-swapRate));
      var p = CDF(-z);
      $scope.target_loss = Math.round(p*100.0);
      $scope.target.loss = $scope.target_loss;
      console.log("in update return target loss="+$scope.target_loss);

      // Formula from Aaron
      var calcRisk = ((swapRate*marketRisk)/(z*marketRisk-marketReturn+swapRate))*100.0;
      $scope.target_risk = Math.round(calcRisk);
      $scope.target.risk = $scope.target_risk;
      console.log("new risk="+$scope.target_risk);
 
      data_query = { 'client_id': $rootScope.client_id, 'target_risk': $scope.target_risk,
          'target_return': $scope.target_return,
          'target_loss': $scope.target_loss,
          'horizon': $scope.horizon,
          'initial_invest': $scope.initial_invest };
      console.log(data_query);

      var RPProfile = RiskProfiles.updateRiskProfile(data_query);
      RPProfile.then(function(result) {
          console.log("in RPPromise");
          $scope.optimizeAllocation();
      });

      //$scope.optimizeAllocation();
    }

    $scope.updateTargetLoss = function(value){
      console.log("in updateTargetLoss "+value);
      $scope.target_loss = value;
      console.log($scope.target.loss + " " + 'target loss value');

      //Z = NormSInv(0.10);
      Z = -NormSInv(value/100.0);
      console.log(Z);
      console.log(CDF(-Z));

      var horizon = $scope.horizon;
      var market_return = 0.08;
      var market_risk = 0.15;
      var swapRate = 0.018;
      console.log("swap rate "+swapRate);
      console.log("market return "+market_return);
      console.log("market risk "+market_risk);

      var new_return = ((Z * swapRate * market_risk)/(Z * market_risk - market_return + swapRate))*100.0;
      console.log("new return "+new_return);
      $scope.target_return = Math.round(new_return);
      $scope.target.return = $scope.target_return;
    
      var new_risk = ((swapRate * market_risk)/(Z*market_risk - market_return + swapRate))*100.0;
      console.log("new risk "+new_risk);
      $scope.target_risk = Math.round(new_risk);
      $scope.target.risk = $scope.target_risk;

      data_query = { 'client_id': $rootScope.client_id, 'target_risk': $scope.target_risk,
          'target_return': $scope.target_return,
          'target_loss': $scope.target_loss,
          'horizon' : $scope.horizon,
          'initial_invest': $scope.initial_invest };
      console.log(data_query);

      var RPProfile = RiskProfiles.updateRiskProfile(data_query);
      RPProfile.then(function(result) {
          console.log("in RPPromise");
          $scope.optimizeAllocation();
      });

      //$scope.optimizeAllocation();
    }

    $scope.updateTarget = function(selection){
      // console.log($scope.targetRadio.group1);
      console.log("in updateTarget "+selection+" "+$scope.data.group1);
      //console.log(selection);
      if ($scope.target_val !== $scope.data.group1) {
        console.log("*** target value has changed");
        val = 0;
        if ($scope.data.group1 == 'Return')
          val = 1;
        else if ($scope.data.group1 == 'Loss')
          val = 2;

        data_query = { 'client_id': $rootScope.client_id, 'target': val,
            'target_risk': $scope.target_risk,
            'target_return': $scope.target_return,
            'target_loss': $scope.target_loss,
            'horizon' : $scope.horizon,
            'initial_invest': $scope.initial_invest };
        console.log(data_query);

        var RPProfile = RiskProfiles.updateRiskProfile(data_query);
        RPProfile.then(function(result) {
            console.log("in RPPromise");
            $scope.optimizeAllocation();
        });

        $scope.target_val = $scope.data.group1;
      }
    }

    $scope.reloadCharts = function(){
      $scope.pieCharts('#semi_circle_chart', 'Advisory Model Portfolio',
        'center', 'top', 30, {color: '#4D7FBA',fontWeight: 'bold',fontSize: '13px !important'},
        $scope.buildChartData($scope.asset_allocations));

      $scope.barCharts('#bar_chart', 'Client Portfolio',
       'center', 'top', 30, {color: '#4D7FBA',fontWeight: 'bold',fontSize: '13px !important'},
       $scope.buildChartData($scope.asset_allocations));
    }

  })

finvese.controller('assetsClassController', function($rootScope, $scope,$location, AssetClass, ClientSelection, MarketPrice, Advisors, Client, Translations) {
  $rootScope.home = false;
  $rootScope.login = false;
  $rootScope.clientListMenu = false;
  $rootScope.clients_list = false;
  console.log("in assetClassController "+$rootScope.client_id);

  var sp = $location.$$path.slice(1,19);
  if (sp == "advisor_allocation") {
    console.log("setting client to 0");
    $rootScope.client_id = 0;
    $scope.clientId = $rootScope.client_id;
  } else {
    var s = $location.$$path;
    var pos = s.indexOf("/",2);
    //console.log("pos="+pos);
    if (pos >= 0) {
      var cid = s.slice(pos+1);
      //console.log(s.slice(pos+1));
      //$rootScope.current_client = cid.toString();
      $rootScope.client_id = parseInt(cid);
      $scope.clientId = $rootScope.client_id;
    }
  }

  console.log("in assetsClassController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  if ($rootScope.client_id > 0)
  {
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Asset Selection";
      }
    })
  }
  else
  {
    document.title = "Model Allocation";
  }

  //$rootScope.labels = [];

  $scope.noSelectedItems = function(){
    $scope.count = $('.stocks-assets .assets_class_section').children().length;
    if($scope.count == 0)
      $('.stocks-assets').addClass("noselected");
    else
      $('.stocks-assets').removeClass("noselected");
  }

  aarray = [];

  $scope.addingClass = function(){
    if(window.location.hash == '#/advisor_allocation/clientId/0')
    {
      $scope.edit_class = 'advisor_allocation';
    }
    else //if(window.location.hash == '#/assets_class')
      $scope.edit_class = 'asset_allocation';
  }
  console.log("DDD edit class="+$scope.edit_class);
  $scope.addingClass();

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    //console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      }); //TranslationsPromise
    }
  }); //AdvisorPromise

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);
   
      if ($rootScope.client_id == 0)
        setAdvisorMenuLabels($rootScope);

      var label = document.getElementById('submitLabel');
      //label.textContent = $rootScope.labels["L0020"];
      if (label) label.textContent = $rootScope.labels["L0188"]; // "Next"

      label = document.getElementById('assetClassSelectionLabel');
      if (label) label.textContent = $rootScope.labels["L0092"];
      label = document.getElementById('selectedAssetsLabel');
      if (label) label.textContent = $rootScope.labels["L0093"];
      label = document.getElementById('assetClassRiskReturnLabel');
      if (label) label.textContent = $rootScope.labels["L0097"];
      label = document.getElementById('equitiesLabel');
      if (label) label.textContent = $rootScope.labels["L0070"];
      label = document.getElementById('fixedIncomeLabel');
      if (label) label.textContent = $rootScope.labels["L0071"];
      label = document.getElementById('hedgeFundsLabel');
      if (label) label.textContent = "Other Assets"; //$rootScope.labels["L0072"];
      label = document.getElementById('numSelectedLabel');
      if (label) label.textContent = $rootScope.labels["L0217"];

      stocksRegionalLabel = $rootScope.labels["L0094"];
      stocksCountryLabel = $rootScope.labels["L0095"];
      stocksSectorLabel = $rootScope.labels["L0096"];
      fixedIncomeLabel1 = $rootScope.labels["L0071"];
      realEstateLabel = $rootScope.labels["L0074"];
      otherAssetsLabel = $rootScope.labels["L0216"];
      
      $scope.fixed_income = [];
      $scope.real_estate = [];
      $scope.other_assets = [];
      $scope.stocks_regional = [];
      $scope.stocks_country = [];
      $scope.stocks_sector = [];

      var AssetClassPromise = AssetClass.getAll();
      AssetClassPromise.then(function(result) {
        $scope.ids = _.pluck(result, 'asset_id');
        $scope.category_names = _.pluck(result, 'category_name');
        $scope.assetnames = _.pluck(result, 'asset_name');
        console.log("processing asset classes");
        //console.log(result);
        var len = result.length;
        for (var i=0; i<len; i++) {
          $scope.assetnames[i] = result[i].asset_name + ' (' + (result[i].num_funds).toString() + ')';
        }
        //console.log($scope.ids);
        //console.log(catnames);
        //$scope.category_names = catnames;
        //console.log($scope.assetnames);
        idx = 0;
        sarray = [];
        while ($scope.category_names[idx] === "STOCKS - REGIONAL") {
          elem1 = {'name': $scope.assetnames[idx], 'id': $scope.ids[idx]};
          sarray.push(elem1);
          idx = idx + 1;
        }
        //console.log(sarray);
        //elem2 = {'name': 'STOCKS - REGIONAL', 'id': 1, 'asset_classes': sarray};
        elem2 = {'name': stocksRegionalLabel, 'id': 1, 'asset_classes': sarray};
        aarray.push(elem2);
        $scope.stocks_regional = sarray;

        sarray = [];
        while ($scope.category_names[idx] === "STOCKS - COUNTRY") {
          elem1 = {'name': $scope.assetnames[idx], 'id': $scope.ids[idx]};
          sarray.push(elem1);
          idx = idx + 1;
        }
        //console.log(sarray);
        //elem2 = {'name': 'STOCKS - COUNTRY', 'id': 2, 'asset_classes': sarray};
        elem2 = {'name': stocksCountryLabel, 'id': 2, 'asset_classes': sarray};
        aarray.push(elem2);
        $scope.stocks_country = sarray;

        sarray = [];
        while ($scope.category_names[idx] === "STOCKS - SECTOR") {
          elem1 = {'name': $scope.assetnames[idx], 'id': $scope.ids[idx]};
          sarray.push(elem1);
          idx = idx + 1;
        }
        //console.log(sarray);
        //elem2 = {'name': 'STOCKS - SECTOR', 'id': 3, 'asset_classes': sarray};
        elem2 = {'name': stocksSectorLabel, 'id': 3, 'asset_classes': sarray};
        aarray.push(elem2);
        $scope.stocks_sector = sarray;

        sarray = [];
        while ($scope.category_names[idx] === "FIXED INCOME") {
          elem1 = {'name': $scope.assetnames[idx], 'id': $scope.ids[idx]};
          sarray.push(elem1);
          idx = idx + 1;
        }
        //console.log(sarray);
        //elem2 = {'name': 'FIXED INCOME', 'id': 4, 'asset_classes': sarray};
        elem2 = {'name': fixedIncomeLabel1, 'id': 4, 'asset_classes': sarray};
        aarray.push(elem2);
        $scope.fixed_income = sarray;
        console.log($scope.fixed_income);

        sarray = [];
        while ($scope.category_names[idx] === "REAL ESTATE") {
          elem1 = {'name': $scope.assetnames[idx], 'id': $scope.ids[idx]};
          sarray.push(elem1);
          idx = idx + 1;
        }
        //console.log(sarray);
        //elem2 = {'name': 'REAL ESTATE', 'id': 5, 'asset_classes': sarray};
        elem2 = {'name': realEstateLabel, 'id': 5, 'asset_classes': sarray};
        aarray.push(elem2);
        $scope.real_estate = sarray;
        console.log($scope.real_estate);

        sarray = [];
        while ($scope.category_names[idx] === "OTHER ASSETS") {
          elem1 = {'name': $scope.assetnames[idx], 'id': $scope.ids[idx]};
          sarray.push(elem1);
          idx = idx + 1;
        }
        //console.log(sarray);
        //elem2 = {'name': 'OTHER ASSETS', 'id': 6, 'asset_classes': sarray};
        elem2 = {'name': otherAssetsLabel, 'id': 6, 'asset_classes': sarray};
        aarray.push(elem2);
        $scope.other_assets = sarray;
        console.log($scope.other_assets);

        $scope.asset_classes = aarray;


        var ClientSelectionPromise = ClientSelection.getOne($rootScope.client_id);
        ClientSelectionPromise.then(function(result) {
          console.log("got client selections");
          console.log(result);
          if (result.length > 0) {
            for (var i=0; i < result.length; i++) {
              console.log(result[i].asset_id);
              console.log(result[i].category_id);
              console.log($scope.asset_classes);
              var aclass = null;
              var acat = null;
              for (var j=0; j < $scope.asset_classes.length; j++) {
                if ($scope.asset_classes[j].id == result[i].category_id) {
                  acat = $scope.asset_classes[j];
                  var aa = $scope.asset_classes[j].asset_classes;
                  console.log(aa);
                  for (var k=0; k < aa.length; k++) {
                    if (aa[k].id == result[i].asset_id)
                      aclass = aa[k];
                  }
                }
              }
              console.log("after ifs");
              console.log(aclass);
              console.log(acat);

              $scope.changebg(acat.id, aclass.id, false);
            }
          }

          // Expand the categories
          $scope.showToggleSubcategory(0);
          $scope.showToggleSubcategory(1);
          $scope.showToggleSubcategory(2);
          //$scope.showToggleSubcategory(4);
          //$scope.showToggleSubcategory(5);
          //$scope.showToggleSubcategory(6);

        });
    
      });
  }


  //TODO: Take myids[0] and use it to set the initial selections

  $scope.itemselect=false;
  $rootScope.investments = false;
  $rootScope.expenses  = false;
  $rootScope.income = false;
  $rootScope.savings = false;
  $rootScope.assets_class = true;

  $scope.count = 0;
  $scope.selected_asset_classes = [];
  $scope.openall = false;
  $scope.bond_assets = [];
  $scope.equity_assets = [];
  $scope.bond_assets = [];
  $scope.reit_assets = [];
  $scope.hedge_assets = [];
  //$scope.date_categories = ['J-11', 'D-11', 'M-12', 'O-12', 'M-13', 'A-13', 'J-14', 'J-14', 'N-14', 'A-15', 'S-15', 'F-16'];
  $scope.date_categories2 = []; //['Jun-11', 'Jul-11', 'Aug-11', 'Sep-11', 'Oct-11', 'Nov-11', 'Dec-11', 'Jan-12', 'Feb-12','Mar-12', 'Apr-12', 'May-12', 'Jun-12', 'Jul-12', 'Aug-12', 'Sep-12', 'Oct-12', 'Nov-12', 'Dec-12', 'Jan-13', 'Feb-13', 'Mar-13', 'Apr-13', 'May-13', 'Jun-13', 'Jul-13', 'Aug-13', 'Sep-13', 'Oct-13', 'Nov-13', 'Dec-13', 'Jan-14', 'Feb-14', 'Mar-14', 'Apr-14', 'May-14', 'Jun-14', 'Jul-14', 'Aug-14', 'Sep-14', 'Oct-14', 'Nov-14', 'Dec-14', 'Jan-15', 'Feb-15', 'Mar-15', 'Apr-15', 'Jun-15', 'Jul-15', 'Aug-15', 'Sep-15', 'Oct-15', 'Nov-15', 'Dev-15', 'Jan-16', 'Feb-16', 'Mar-16', 'Apr-16', 'May-16'];
  $scope.bond_prices = [];
  $scope.bond_names = [];
  $scope.equity_prices = [];
  $scope.equity_names = [];
  $scope.reit_names = [];
  $scope.reit_prices = [];
  $scope.hedge_names = [];
  $scope.hedge_prices = [];
  $scope.bond_returns = [];
  $scope.equity_returns = [];
  $scope.reit_returns = [];
  $scope.hedge_returns = [];
  $scope.bond_avgret = [];
  $scope.equity_avgret = [];
  $scope.reit_avgret = [];
  $scope.hedge_avgret = [];
  $scope.bond_stddev = [];
  $scope.equity_stddev = [];
  $scope.reit_stddev = [];
  $scope.hedge_stddev = [];

  // Compute the dates to be displayed in the x-axis of the charts
  $scope.date_categories2 = getDateLabels(6, 2017);
  console.log("XX");
  console.log($scope.date_categories2);

  $scope.changeOpenAll = function (var_name) {
    $scope.openall = !var_name;
    console.log($scope.openall);

    if($scope.openall == false){
      $('.subcategories_0').addClass('hide');
      $('.subcategories_0').removeClass('show');
       $('.categories_0').find('.plus').addClass('show');
      $('.categories_0').find('.plus').removeClass('hide');
      $('.categories_0').find('.minus').addClass('hide');
       $('.categories_0').find('.minus').removeClass('show');
    }else{

      $('.subcategories_0').removeClass('hide');
      $('.subcategories_0').addClass('show');
      $('.categories_0').find('.plus').addClass('hide');
       $('.categories_0').find('.plus').removeClass('show');
       $('.categories_0').find('.minus').removeClass('hide');
      $('.categories_0').find('.minus').addClass('show');


    }
  }

  $scope.showToggleSubcategory = function(index_val){
    console.log("in showToggleSubcategory "+index_val);
    console.log('.subcategories_'+index_val);
    $('.subcategories_'+index_val).toggleClass('show');

    $('.categories_'+index_val+' .plus').toggleClass('hide');
    $('.categories_'+index_val+' .plus').toggleClass('show');

    $('.categories_'+index_val+' .minus').toggleClass('hide');
    $('.categories_'+index_val+' .minus').toggleClass('show');

    $scope.show_plus = true;
  }

  $scope.showToggelSubcategory2 = function(index_val){
    // This method is similar to showToggelSubcategory2 except that it works on the selected assets (Can Be Removed)

    console.log('.subcategories2_'+index_val);
    $('.subcategories2_'+index_val).toggleClass('show');

    $('.categories2_'+index_val+' .plus').toggleClass('hide');
    $('.categories2_'+index_val+' .plus').toggleClass('show');

    $('.categories2_'+index_val+' .minus').toggleClass('hide');
    $('.categories2_'+index_val+' .minus').toggleClass('show');

    $scope.show_plus = false;
  }
  $scope.selected_category_assets = {};

  $scope.changebg = function(category_id, asset_class_id, updateFlag){
    // This method is called when an asset class is selected/deselected
    console.log("in changebg "+asset_class_id+" category "+category_id);
    console.log($scope.asset_classes);
    console.log('.subcategories li#asset_'+asset_class_id);
    $('.subcategories li').removeClass('background-blue');
    $('.subcategories li#asset_'+asset_class_id).addClass('background-blue');
    if($scope.selected_category_assets && $scope.selected_category_assets[category_id])
      category_assets = $scope.selected_category_assets[category_id];
    else
      category_assets = [];
    asset_class_pre_selected = $scope.selected_asset_classes.indexOf(asset_class_id);
    console.log(asset_class_pre_selected);

    if(asset_class_pre_selected >= 0){
      console.log("XXX in if = unselected");
      //$('.subcategories li#asset_'+asset_class_id).addClass('itemselect');

        // Delete the record from the database
        if (updateFlag) {
          data_query = {id: 1, client_id: $rootScope.client_id, asset_id: asset_class_id, category_id: category_id+1, operation: 'D'};
          res = ClientSelection.updateSelection(data_query);
        }

      if (category_id == 4) {
        console.log("removing bond");
        var idx = $scope.bond_assets.indexOf(asset_class_id);
        console.log("index "+idx);
        $scope.bond_assets.splice(idx, 1);
        $scope.bond_names.splice(idx, 1);
        $scope.bond_prices.splice(idx, 1);
        $scope.bond_returns.splice(idx, 1);
        $scope.bond_avgret.splice(idx, 1);
        $scope.bond_stddev.splice(idx, 1);
        console.log($scope.bond_assets);
        console.log($scope.bond_names);
        $scope.assetBonds();
        $scope.assetClassRiskReturn();
      }
      if (category_id < 4) {
        console.log("removing equity");
        var idx = $scope.equity_assets.indexOf(asset_class_id);
        console.log("index "+idx);
        $scope.equity_assets.splice(idx, 1);
        $scope.equity_names.splice(idx, 1);
        $scope.equity_prices.splice(idx, 1);
        $scope.equity_returns.splice(idx, 1);
        $scope.equity_avgret.splice(idx, 1);
        $scope.equity_stddev.splice(idx, 1);
        console.log($scope.equity_assets);
        $scope.assetEquities();
        $scope.assetClassRiskReturn();
      }
       if (category_id == 5) {
        console.log("removing reit");
        var idx = $scope.reit_assets.indexOf(asset_class_id);
        console.log("index "+idx);

        $scope.reit_assets.splice(idx, 1);
        $scope.reit_names.splice(idx, 1);
        $scope.reit_prices.splice(idx, 1);
        $scope.reit_returns.splice(idx, 1);
        $scope.reit_avgret.splice(idx, 1);
        $scope.reit_stddev.splice(idx, 1);
        console.log($scope.reit_assets);
        $scope.assetClassRiskReturn();
      }
      if (category_id == 6) {
        console.log("removing hedge");
        var idx = $scope.hedge_assets.indexOf(asset_class_id);
        console.log("index "+idx);

        $scope.hedge_assets.splice(idx, 1);
        $scope.hedge_names.splice(idx, 1);
        $scope.hedge_prices.splice(idx, 1);
        $scope.hedge_returns.splice(idx, 1);
        $scope.hedge_avgret.splice(idx, 1);
        $scope.hedge_stddev.splice(idx, 1);
        console.log($scope.hedge_assets);
        $scope.assetHedge();
        $scope.assetClassRiskReturn();
      }
      $scope.selected_asset_classes.splice(asset_class_pre_selected, 1);
      if(category_assets) {
        var idx = category_assets.indexOf(asset_class_id);
        category_assets.splice(idx, 1);
      }
      $('.subcategories li#asset_'+asset_class_id).removeClass('subselected');
      // $('.subcategories li#asset_'+asset_class.id).removeClass('background-blue');

    }else{
      console.log("XXX in else = selected");

        // Insert record into database
        if (updateFlag) {
          data_query = {id: 1, client_id: $rootScope.client_id, asset_id: asset_class_id, category_id: category_id+1, operation: 'I'};
          res = ClientSelection.updateSelection(data_query);
        } else {
          console.log("RR setting itemselect "+asset_class_id);
          $('.subcategories li#asset_'+asset_class_id).addClass('itemselect');
          $('.subcategories li#asset_'+asset_class_id).addClass('subselected');

        }

      if (category_id == 4) {
        console.log("adding bond");
        $scope.bond_assets.push(asset_class_id);
        console.log($scope.bond_assets);

        // Fetch marget price for this bond
        var MarketPricePromise = MarketPrice.getOne(asset_class_id);
        MarketPricePromise.then(function(result) {
          console.log("processing bond market price");
          $scope.bond_names.push(result[0].asset_description);
          //console.log($scope.bond_names);
          var tmparray = [];
          var tmparray2 = []; // used to store the returns
          var sum = 0.0;      // used to compute average return
          if (result.length > 60)
            start = result.length-60;
          else
            start = 0;
          var normFactor = result[start].market_price;
          for (var i=start; i<result.length; i++) {
            tmparray.push(Math.floor((result[i].market_price*100.0/normFactor)*100.0)/100.0);
            if (i > start) {
              var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
              tmparray2.push(r);
              sum = sum + r;
            }
          }

          // Compute the average return and risk
          var mean = sum/tmparray2.length;
          var tmpsum = 0.0;
          for (var k=0; k<tmparray2.length; k++) {
            tmpsum = tmpsum + Math.pow(tmparray2[k]-mean,2);
          }
          var stddev = Math.sqrt(tmpsum/tmparray2.length) * Math.sqrt(12.0);

          $scope.bond_avgret.push(Math.floor(12.0*100.0*sum/tmparray2.length)/100.0);
          $scope.bond_stddev.push(Math.floor(stddev*100.0)/100.0);
          $scope.bond_prices.push(tmparray);
          $scope.bond_returns.push(tmparray2);
 
          $scope.assetBonds();
          $scope.assetClassRiskReturn();
        })

      }
      if (category_id < 4) {
        console.log("adding equity");
        $scope.equity_assets.push(asset_class_id);
        //console.log($scope.equity_assets);

        // Compute numbers to show in chart
        var MarketPricePromise = MarketPrice.getOne(asset_class_id);
        MarketPricePromise.then(function(result) {
          console.log("processing equity market price");
          $scope.equity_names.push(result[0].asset_description);
          var tmparray = [];
          var tmparray2 = [];
          var sum = 0.0;
          if (result.length > 60)
            start = result.length-60;
          else
            start = 0;
          var normFactor = result[start].market_price;
          for (var i=start; i<result.length; i++) {
            tmparray.push(Math.floor((result[i].market_price*100.0/normFactor)*100.0)/100.0);
            if (i > start) {
              var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
              //console.log(r);
              tmparray2.push(r);
              sum = sum + r;
            }
          }

          // Compute the average return and risk
          var mean = sum/tmparray2.length;
          var tmpsum = 0.0;
          for (var k=0; k<tmparray2.length; k++) {
            tmpsum = tmpsum + Math.pow(tmparray2[k]-mean,2);
          }
          var stddev = Math.sqrt(tmpsum/tmparray2.length) * Math.sqrt(12.0);

          $scope.equity_avgret.push(Math.floor(12.0*100.0*sum/tmparray2.length)/100.0);
          $scope.equity_stddev.push(Math.floor(stddev*100.0)/100.0);
          $scope.equity_prices.push(tmparray);
          $scope.equity_returns.push(tmparray2);

          $scope.assetEquities();
          $scope.assetClassRiskReturn();
        })

      }
      if (category_id == 5) {
        console.log("adding reit "+asset_class_id);
        $scope.reit_assets.push(asset_class_id);
        //console.log($scope.reit_assets);

        // Compute numbers to show in charts
        var MarketPricePromise = MarketPrice.getOne(asset_class_id);
        MarketPricePromise.then(function(result) {
          console.log("processing reit market price");
          $scope.reit_names.push(result[0].asset_description);
          console.log($scope.reit_names);
          var tmparray = [];
          var tmparray2 = [];
          var sum = 0;
          if (result.length > 60)
            start = result.length-60;
          else
            start = 0;
          var normFactor = result[start].market_price;
          for (var i=start; i<result.length; i++) {
            tmparray.push(Math.floor((result[i].market_price*100.0/normFactor)*100.0)/100.0);
            if (i > start) {
              var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
              tmparray2.push(r);
              sum = sum + r;
            }
          }

          // Compute the average return and risk
          var mean = sum/tmparray2.length;
          var tmpsum = 0.0;
          for (var k=0; k<tmparray2.length; k++) {
            tmpsum = tmpsum + Math.pow(tmparray2[k]-mean,2);
          }
          var stddev = Math.sqrt(tmpsum/tmparray2.length) * Math.sqrt(12.0);

          $scope.reit_avgret.push(Math.floor(12.0*100.0*sum/tmparray2.length)/100.0);
          $scope.reit_stddev.push(Math.floor(stddev*100.0)/100.0);
          $scope.reit_prices.push(tmparray);
          $scope.reit_returns.push(tmparray2);

          //$scope.assetHedge();
          $scope.assetClassRiskReturn();
        })

      }
      if (category_id == 6) {
        console.log("adding hedge fund "+asset_class_id);
        $scope.hedge_assets.push(asset_class_id);
        //console.log($scope.hedge_assets);

        // Compute numbers to show in charts
        var MarketPricePromise = MarketPrice.getOne(asset_class_id);
        MarketPricePromise.then(function(result) {
          console.log("processing hedge market price");
          //console.log(result);
          //console.log(result.length);
          $scope.hedge_names.push(result[0].asset_description);
          console.log($scope.hedge_names);
          var tmparray = [];
          var tmparray2 = [];
          var sum = 0.0;
          if (result.length > 60)
            start = result.length-60;
          else
            start = 0;
          var normFactor = result[start].market_price;
          for (var i=start; i<result.length; i++) {
            tmparray.push(Math.floor((result[i].market_price*100.0/normFactor)*100.0)/100.0);
            if (i > start) {
              var r = (tmparray[i-start]/tmparray[i-start-1])-1.0;
              tmparray2.push(r);
              sum = sum + r;
            }
          }

          // Compute the average return and risk
          var mean = sum/tmparray2.length;
          var tmpsum = 0.0;
          for (var k=0; k<tmparray2.length; k++) {
            tmpsum = tmpsum + Math.pow(tmparray2[k]-mean,2);
          }
          var stddev = Math.sqrt(tmpsum/tmparray2.length) * Math.sqrt(12.0);

          $scope.hedge_avgret.push(Math.floor(12.0*100.0*sum/tmparray2.length)/100.0);
          $scope.hedge_stddev.push(Math.floor(stddev*100.0)/100.0);
          $scope.hedge_prices.push(tmparray);
          $scope.hedge_returns.push(tmparray2);


          $scope.assetHedge();
          $scope.assetClassRiskReturn();
        }); // MarketPricePromise

      }
      $scope.selected_asset_classes.push(asset_class_id);
      if(category_assets)
        category_assets.push(asset_class_id);
      else
        category_assets = [asset_class_id]
    }
    $scope.selected_category_assets[category_id] = category_assets;
    console.log($scope.selected_category_assets);
  }
  
  $scope.closePopup = function(){
    $('.submitPopup').addClass('hide');
  }

  $scope.saveAssetClasses = function() {
    console.log("AA saveAssetClasses "+$rootScope.client_id);
    console.log($scope.selected_asset_classes);

    // 28.08.2017 Remove updating of database
    //var len = $scope.selected_asset_classes.length;
    //console.log(len);
    //var clientid = $rootScope.client_id;
    //data_query = {id: 1, client_id: clientid, asset_id: 0, category_id: 1};
    //res = ClientSelection.updateSelection(data_query);

    // insert new records
    //for (var i=0; i < len; i++) {
    //  var aid = $scope.selected_asset_classes[i];
    //  var cid = Math.trunc(aid / 1000);
    //  console.log("aid "+ aid+ " cid "+ cid);
    //  data_query = {id: 1, client_id: clientid, asset_id: aid, category_id: cid};
    //  res = ClientSelection.updateSelection(data_query);
    //}

    if ($scope.selected_asset_classes.length==0)
    {
        console.log($scope.selected_asset_classes.length);
        $('.submitPopup').removeClass('hide');
        return false;
    }
    else {
      if ($rootScope.client_id > 0)
      //if(window.location.hash == '#/assets_class')
        $location.path('/assets_allocation/'+$rootScope.client_id.toString());
      else
        $location.path('/advisor_assetAllocation');
    }
  }
 
  $scope.assetClassRiskReturn = function() {
    //$(function () {
    var col = '#347dcf';
    var dat = [];
    for (var i=0; i<$scope.equity_assets.length; i++) {
      var tmp = { x: $scope.equity_stddev[i]*100.0,
                  y: $scope.equity_avgret[i]*100.0,
                  name: $scope.equity_names[i],
                  color: col };
      dat.push(tmp);
    }
    for (var i=0; i<$scope.bond_assets.length; i++) {
      var tmp = { x: $scope.bond_stddev[i]*100.0,
                  y: $scope.bond_avgret[i]*100.0,
                  name: $scope.bond_names[i],
                  color: col };
      dat.push(tmp);
    }
    for (var i=0; i<$scope.reit_assets.length; i++) {
      var tmp = { x: $scope.reit_stddev[i]*100.0,
                  y: $scope.reit_avgret[i]*100.0,
                  name: $scope.reit_names[i],
                  color: col };
      dat.push(tmp);
    }
    for (var i=0; i<$scope.hedge_assets.length; i++) {
      var tmp = { x: Math.floor($scope.hedge_stddev[i]*100.0*100.0)/100.0,
                  y: Math.floor($scope.hedge_avgret[i]*100.0*100.0)/100.0,
                  name: $scope.hedge_names[i],
                  color: col };
      dat.push(tmp);
    }


      $('#assetClassRiskReturn').highcharts({
            chart: {
                type: 'scatter',
                // zoomType: 'xy',
                marginLeft:65,
                reflow: true
            },
            exporting: { enabled: false },
            title: {
                // text: 'Asset Class Risk Return'
                text:null,
            },
            //subtitle: {
            //    text: 'Source: Heinz  2003'
            //},
            xAxis: {
                title: {
                    enabled: true,
                    text: 'RISK',
                    style:{
                      fontSize:'12px',
                      color:'#333',
                      fontWeight:'normal',
                    }
                },
                tickPosition: 'inside',
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true,
                gridLineWidth: '1',
                gridLineColor: '#d8d8d8',
                tickPositions: [0, 5, 10, 15, 20],
                labels: {
                    format: '{value}%',
                    style:{
                      fontSize:'10px',
                    }
                },
            },
            yAxis: {
                title: {
                    text: 'RETURN',
                    style:{
                      fontSize:'12px',
                      color:'#333',
                      fontWeight:'normal',
                    }
                },
                tickPositions: [-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10],
                dataLabels: {
                    enabled: true,
                },
                labels: {
                    format: '{value}%',
                    style:{
                      fontSize:'10px',
                    }
                },

            },
           // legend: {
           //     layout: 'vertical',
           //     align: 'left',
           //     verticalAlign: 'top',
           //     x: 100,
           //     y: 70,
           //     floating: true,
           //     backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
           //     borderWidth: 1
           // },
           legend: {
            enabled:false,
           },
            plotOptions: {
                scatter: {
                    marker: {
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        }
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                       headerFormat: '',
                        pointFormat: '<b>{point.name}</b><br>Risk {point.x}%, Return {point.y}%'
                    }
                }
            },
            series: [
                {
                //name: 'BGSV',
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                    return this.point.name;
                    },
                },
                color: col,
                data: dat
                //data: [
                //    {x: 5, y: 1.1, name: 'BGSV', color: col},
                //    {x: 19, y: -3.5, name: 'MXEF', color: col},
                //    {x: 18, y: 0, name: 'MXASJ', color: col},
                //    {x: 17, y: 3.5, name: 'SX5E', color:col}
                //]
                }
            ]
        });
    };
    $scope.assetClassRiskReturn();

    $scope.assetEquities = function() {
      //$(function () {
    //$scope.assetEquities = function() {
       var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
       //var colors = ['#EB4932','#347DCF','#d20f53'];
       var d = [];
       len = $scope.equity_assets.length;
       for (var i=0; i<len; i++) {
         var tmp1 = { name: $scope.equity_names[i],
                   data: $scope.equity_prices[i],
                   color: colors[i] };
         d.push(tmp1);
       }

        $('#assetEquities').highcharts({
            chart: {
                type: 'line',
                reflow: true
            },
            exporting: { enabled: false },
            title: {
                text: null
            },
            subtitle: {
                text: null,
            },
            xAxis: {
                categories: $scope.date_categories2
            },
            yAxis: {
                title: {
                    text: null,
                },

            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true,
                        align: 'top',
                        y:10,
                    },
                    enableMouseTracking: false
                }
            },
            series: d
            //series: [{
            //    name: 'MXASJ',
            //    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
            //    color: '#EB4932'
            //}, {
            //    name: 'SX5E',
            //    data: [4.9, 5.2, 6.7, 9.5, 12.9, 14.2, 18.0, 19.6, 15.2, 11.3, 7.6, 5.8],
            //    color: '#347DCF'
            //},{
            //    name: 'SX5E',
            //    data: [2.9, 3.2, 3.7, 7.5, 10.9, 12.2, 14.0, 14.0, 13.0, 9.0, 5.2, 3.2],
            //    color: '#d20f53'
            //}]
        });
    //});
    }
    $scope.assetEquities();

    $scope.assetBonds = function() {
    //$(function () {
      var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
      //var colors = ['#EB4932','#347DCF','#d20f53'];
      var d = [];
      len = $scope.bond_assets.length;
      for (var i=0; i<len; i++) {
        var tmp1 = { name: $scope.bond_names[i],
                   data: $scope.bond_prices[i],
                   color: colors[i] };
        d.push(tmp1);
      }
      //var tmp1 = { name: 'BGSV',
      //             data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
      //             color: colors[0] };
      //d.push(tmp1);

        $('#assetBonds').highcharts({
            chart: {
                type: 'line',
                reflow: true
            },
            exporting: { enabled: false },
            title: {
                text: null
            },
            subtitle: {
                text: null,
            },
            xAxis: {
                categories: $scope.date_categories2
            },
            yAxis: {
                title: {
                    text: null,
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true,
                        align: 'top',
                        y:10,
                    },
                    enableMouseTracking: false
                }
            },
            series: d
            //series: [{
            //    name: 'BGSV',
            //    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
            //    color: '#EB4932',
            //}]
        });
    //});
    }
    $scope.assetBonds();


    //$scope.assetHedge = function() {
    $scope.assetHedge = function() {
    //$(function () {
      var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
      //var colors = ['#EB4932','#347DCF','#d20f53'];
      var d = [];
      len = $scope.hedge_assets.length;
       for (var i=0; i<len; i++) {
         var tmp1 = { name: $scope.hedge_names[i],
                   data: $scope.hedge_prices[i],
                   color: colors[i] };
         d.push(tmp1);
       }

      //var tmp1 = { name: 'BGSV',
      //             data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
      //             color: colors[0] };
      //d.push(tmp1);

        $('#assetHedge').highcharts({
            chart: {
                type: 'line',
                reflow: true
            },
            exporting: { enabled: false },
            title: {
                text: null,
            },
            subtitle: {
                text: null,
            },
            xAxis: {
                categories: $scope.date_categories2
            },
            yAxis: {
                title: {
                    text: null,
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true,
                        align: 'top',
                        y:10,
                    },
                    enableMouseTracking: false
                }
            },
            series: d
            //series: [{
            //    name: 'BGSV',
            //    data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
            //    color: '#EB4932',
            //}]
        });
    //});
    }
    $scope.assetHedge();

})

finvese.controller('investmentsController', function($rootScope, $scope, $location, Investment, Advisors, Client, Translations) {
  $rootScope.investments = [];
  $rootScope.savings  = false;
  $rootScope.login = false;
  $rootScope.income = false;
  $rootScope.expenses = false;
  $rootScope.clientListMenu = false;
  $rootScope.clients_list = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }
  $scope.client_name = $rootScope.client_name;
  
  //$rootScope.labels = [];

  $scope.default_form = { 'account_owner':null,'taxable' :'select taxable',
                                   'description':null, 'account_value':null, 'cost_basis':null};

  $scope.tax_values= [{'name': 'Taxable', 'value' :'taxable'},
                          {'name': 'Tax-deferred', 'value' :'tax-deferred'},
                          {'name': 'Tax-Free', 'value' : 'tax-free'}
                         ];

  console.log("in investmentController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Investments";
      }
    })



  $scope.taxable = 'Taxable';
  $scope.taxable_pre = false;
  $rootScope.home = false;

  $scope.name_values = [{'name': 'Client', 'value': 'Client'},
                        {'name': 'Spouse', 'value': 'Spouse'}];

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      }); // TranslationPromise
    }
  }); // AdvisorPromise

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);
    
      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('submitLabel');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('investmentsLabel');
      if (label) label.textContent = $rootScope.labels["L0058"];
      
      // Taxation
      $scope.default_form.taxable = $rootScope.labels["L0197"]; // Select taxable
      var taxable1 = $rootScope.labels["L0194"];
      var taxdeferred1 = $rootScope.labels["L0195"];
      var taxfree1 = $rootScope.labels["L0196"];
      $scope.tax_values=[{'name': taxable1, 'value': taxable1},
                       {'name': taxdeferred1, 'value': taxdeferred1},
                       {'name': taxfree1, 'value' : taxfree1}
                       ];
      $scope.taxable = taxable1; //'Taxable';

      // Name
      var client1 = $rootScope.labels["L0032"];
      var spouse1 = $rootScope.labels["L0229"];
      $scope.name_values = [{'name': client1, 'value': client1},
                        {'name': spouse1, 'value': spouse1}];

      //$scope.investments_head = ['Account Owner','Type Of Account','Description','Account Value','Cost Basis (if taxable)','Action'];
      $scope.investments_head = [ $rootScope.labels["L0167"], //'Account Owner',
                                  $rootScope.labels["L0160"], //'Type Of Account',
                                  $rootScope.labels["L0161"], //'Description',
                                  $rootScope.labels["L0168"], //'Account Value',
                                  $rootScope.labels["L0169"], //'Cost Basis (if taxable)',
                                  $rootScope.labels["L0026"] ]; //'Action'];
 
      label = document.getElementById('newInvestmentLabel1');
      if (label) label.textContent = " + " + $rootScope.labels["L0170"];
      label = document.getElementById('newInvestmentLabel');
      if (label) label.textContent = $rootScope.labels["L0170"];
      label = document.getElementById('accountOwnerLabel');
      if (label) label.textContent = $rootScope.labels["L0167"];
      label = document.getElementById('typeAccountLabel');
      if (label) label.textContent = $rootScope.labels["L0160"];
      label = document.getElementById('descriptionLabel');
      if (label) label.textContent = $rootScope.labels["L0161"];
      label = document.getElementById('accountValueLabel');
      if (label) label.textContent = $rootScope.labels["L0168"];
      label = document.getElementById('costBasisLabel');
      if (label) label.textContent = $rootScope.labels["L0169"];

      var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
      ClientPromise.then(function(result) {
        console.log("inside client promise");

        if (result)
        {
          console.log(result);
          $scope.client_name = 'Client';
          $scope.spouse_name = null;
          if (result.first_name)
          {
            $scope.client_name = result.first_name + " " + result.last_name;
          }
          if (result.spouse_name)
          {
            // With spouse
            $scope.spouse_name = result.spouse_name;
            $scope.name_values = [{'name': $scope.client_name, 'value': $scope.client_name},
                        {'name': $scope.spouse_name, 'value': $scope.spouse_name}];
          }
          else 
          {
            // No spouse
            $scope.name_values = [{'name': $scope.client_name, 'value': $scope.client_name}];
          }
          $scope.saveings_plan_form.account_owner = $scope.client_name;
          $scope.default_form.account_owner = $scope.client_name;
        }

        console.log("getting investments");
        var InvestmentPromise = Investment.getOne($rootScope.client_id);
        InvestmentPromise.then(function(result){
          console.log("in investment promise");
          console.log(result[0]);

          // Strangely the annual need gets retrieved as a string
          // Convert it to a number
          len = result.length;
          for (var i=0; i < len; i++) {
            result[i].account_value = parseFloat(result[i].account_value);
            result[i].cost_basis = parseFloat(result[i].cost_basis);

            // Translate the taxable field
            if (result[i].taxable === "Taxable")
              result[i].taxable = $rootScope.labels["L0194"];
            else if (result[i].taxable === "Tax-deferred")
              result[i].taxable = $rootScope.labels["L0195"];
            else if (result[i].taxable === "Tax-Free")
              result[i].taxable = $rootScope.labels["L0196"];

            // Translate the name field
            if (result[i].account_owner === "Spouse")
              result[i].account_owner = $scope.spouse_name;
            else
              result[i].account_owner = $scope.client_name;

          }

          if (len > 0)
            $scope.investments = result;
          else
            $scope.investments = [];

        }); // InvestmentPromise

      }); // ClientPromise
  }


  $scope.resetForm = function(){
        $("#new_row").modal();
        $scope.saveings_plan_form = Object.assign({}, $scope.default_form);

        $('.input-field input').each(function(){
             $(this).siblings().removeClass('active');
         });
  }

  $scope.editRow = function(array_index){
        $scope.edit_index = array_index;
        $scope.saveings_plan_form  = $scope.investments[array_index];
        $scope.editable_form = true;
        $("#new_row").modal();
        $('.input-field input').each(function(){
             $(this).siblings().addClass('active');
         });
  }
  
  $scope.setValues = function() {
      //$scope.investments_head = ['Account Owner','Type Of Account','Description','Account Value','Cost Basis (if taxable)','Action'];
      // $scope.investments = [{'account_owner':'Bill Gates', 'taxable' : 'Taxable',
      //                       'description':'High School Education',  'account_value':10000, 'cost_basis':50000}];
      $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
      setTimeout(function(){
          $('.dropdown-button').dropdown();
        }, 300);

  }


  $scope.changeName = function(name_selected) {
    console.log("in changeName");
    $scope.saveings_plan_form.account_owner = name_selected;
    //$scope.new_taxable = null;
  }

  $scope.changeTaxable = function(taxable_selected) {
        $scope.saveings_plan_form.taxable = taxable_selected;
        console.log("Taxable Selected "+taxable_selected);
        //if(taxable_selected != 'Taxable')
        if(taxable_selected != $rootScope.labels["L0194"])
          $scope.taxable_pre = true;
        else
          $scope.taxable_pre = false;
  }

  $scope.removeRow = function(index){
        console.log(index);
        console.log("in investment removeRow index "+index);
        console.log($scope.investments[index]);
        $scope.investments[index].action="delete";
        data_query = $scope.investments[index];
        console.log(data_query);
        res = Investment.deleteInvestment($scope.investments[index].id,data_query);
        $scope.investments.splice( index, 1 );
 //
  };

  $scope.notCommit = function(){
        if($scope.editable_form)
          $scope.editable_form = false;
          // $scope.investments[$scope.edit_index] = $scope.investments;
  }

    $scope.addRow = function(saveings_plan_form){
      console.log("in addRow "+$scope.saveings_plan_form.taxable);
      console.log("default "+$scope.default_form.taxable);

      if ($scope.saveings_plan_form.account_owner === null 
        || $scope.saveings_plan_form.account_value === null 
        || $scope.saveings_plan_form.taxable === $rootScope.labels["L0197"] )
          return false;

      console.log('asdsadas');
      var edt = $scope.editable_form;
      if($scope.editable_form)
        $scope.investments[$scope.edit_index] = saveings_plan_form;
      else
        $scope.investments.push(saveings_plan_form);

        $scope.editable_form = false;
        $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
        $('.close').trigger('click');
        // $scope.name='';
        setTimeout(function(){
          $('.dropdown-button').dropdown();
        }, 300);

        console.log("at end of addRow "+edt+" "+$scope.edit_index);
        if ($scope.edit_index >= 0) 
        {
          // Edit existing record

          // Translate the taxable field
          //if ($scope.investments[$scope.edit_index].taxable === "Taxable")
          //  $scope.investments[$scope.edit_index].taxable = $rootScope.labels["L0194"];
          //else if ($scope.investments[$scope.edit_index].taxable === "Tax-deferred")
          //  $scope.investments[$scope.edit_index].taxable = $rootScope.labels["L0195"];
          //else if ($scope.investments[$scope.edit_index].taxable === "Tax-Free")
          //  $scope.investments[$scope.edit_index].taxable = $rootScope.labels["L0196"];

          console.log("in editing an investment "+$scope.edit_index);
          console.log($scope.investments[$scope.edit_index]);

          //data_query = $scope.investments[$scope.edit_index];
          data_query = Object.assign({}, $scope.investments[$scope.edit_index]);
          var id = $scope.investments[$scope.edit_index].id;

          // Translate the taxable field
          if (data_query.taxable === $rootScope.labels["L0194"])
            data_query.taxable = "Taxable";
          else if (data_query.taxable === $rootScope.labels["L0195"])
            data_query.taxable = "Tax-deferred";
          else if (data_query.taxable === $rootScope.labels["L0196"])
            data_query.taxable = "Tax-Free";
          
          res = Investment.updateInvestment(id, data_query);
        } 
        else 
        {
          // Insert a new record
          len = $scope.investments.length;
          console.log("in adding a new investment");
          console.log($scope.investments[len-1]);

          // Translate the taxable field
          //if ($scope.investments[len-1].taxable === "Taxable")
          //  $scope.investments[len-1].taxable = $rootScope.labels["L0194"];
          //else if ($scope.investments[len-1].taxable === "Tax-deferred")
          //  $scope.investments[len-1].taxable = $rootScope.labels["L0195"];
          //else if ($scope.investments[len-1].taxable === "Tax-Free")
          //  $scope.investments[len-1].taxable = $rootScope.labels["L0196"];

          $scope.investments[len-1].client_id = $rootScope.client_id;
          $scope.investments[len-1].id = 0;

          //data_query = $scope.investments[len-1];
          data_query = Object.assign({}, $scope.investments[len-1]);

          // Translate the taxable field
          if (data_query.taxable === $rootScope.labels["L0194"])
            data_query.taxable = "Taxable";
          else if (data_query.taxable === $rootScope.labels["L0195"])
            data_query.taxable = "Tax-deferred";
          else if (data_query.taxable === $rootScope.labels["L0196"])
            data_query.taxable = "Tax-Free";

          // Translate the account owner field
          if (data_query.account_owner === $scope.spouse_name)
            data_query.account_owner = "Spouse";
          else
            data_query.account_owner = "Client";

          res = Investment.createInvestment(data_query);
        }
    };


     $scope.setValues();

})

finvese.controller('savingsController', function($rootScope, $scope, $location, SavingsPlan, Advisors, Client, Translations) {
  $rootScope.savings = true;
  $rootScope.investments  = [];
  $rootScope.income = false;
  $rootScope.expenses = false;
  $rootScope.login = false;
  $rootScope.home = false;
  $rootScope.clients_list = false;
  $rootScope.clientListMenu = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  console.log("in savingsController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Savings";
      }
  })

  $scope.default_form = {'saver_name': 'Client', 'taxable': 'Select taxable','description': null,
                                 'date_range_in':'Select Date Range', 'date_range_begin' : null,
                                 'date_range_end': null, 'measure':'Select Compensation',
                                 'amount': null, 'inflation':'Select Inflation'};
  $scope.saveings_plan_form = Object.assign({}, $scope.default_form);

  $scope.date_range_in_vals = {'Age': [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50], 'Years': [2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026]};
  $scope.compansation = {'$': [2000,2001], '%': [10,11]};
  $scope.inflation_values = {'$': ['Yes', 'No'], '%': ['N/A']};

  $scope.tax_values=[{'name': 'Taxable', 'value' :'taxable'},
                       {'name': 'Tax-deferred', 'value' :'tax-deferred'},
                       {'name': 'Tax-Free', 'value' : 'tax-free'} ];
  $scope.inflation_vals   = ['Yes', 'No', 'N/A'];
  //$scope.percentage=[{'name': '$', 'value' :'$'},
  //                     {'name': '%', 'value' :'%'}  ];
  $scope.percentage=['$', '%'];
  $scope.name_values = [{'name': 'Client', 'value': 'Client'},
                        {'name': 'Spouse', 'value': 'Spouse'}];

  $scope.changeBeginEnd = function(date_range_in) 
  {
    console.log("in changeBeginEnd "+date_range_in);
    $scope.saveings_plan_form.date_range_in = date_range_in;
    console.log($scope.date_range_in_vals[date_range_in]);
    $scope.saveings_plan_form.date_range_begin = $scope.date_range_in_vals[date_range_in][0];
    $scope.saveings_plan_form.date_range_end = $scope.date_range_in_vals[date_range_in][$scope.date_range_in_vals[date_range_in].length-1];
  }

  $scope.changeOption = function(options_selected) 
  {
    console.log("in changeOption "+options_selected);
    console.log("inflation_values "+$scope.inflation_values);
    console.log("compansation "+$scope.compansation);

        $scope.saveings_plan_form.measure = options_selected;
        console.log($scope.measure);
        console.log($scope.compansation[options_selected]);
        $scope.saveings_plan_form.amount = $scope.compansation[options_selected][0];
        console.log($scope.saveings_plan_form.amount);
        console.log($scope.inflation_values[options_selected]);
        $scope.saveings_plan_form.inflation_vals = $scope.inflation_values[options_selected];
        console.log($scope.saveings_plan_form.inflation_vals);
        $scope.saveings_plan_form.inflation = $scope.saveings_plan_form.inflation_vals[0];
  }

  $scope.date_range_in = 'Age';
  $scope.taxable = 'Taxable';
  $scope.measure = '$';
  $scope.inflation = 'Yes';
  $scope.saver_name = 'Client';

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('submitLabel');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('savingsPlanLabel');
      if (label) label.textContent = $rootScope.labels["L0157"];
      
      // Date Range
      $scope.default_form.date_range_in = $rootScope.labels["L0202"]; // Select Date Range
      var age = $rootScope.labels["L0227"];
      var years = $rootScope.labels["L0228"];
      console.log(age);
      $scope.date_range_in = age; // Age
      $scope.ages = age; // Age
      //$scope.date_range_in_vals = {'Age': [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50], 'Years': [2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026]};
      //$scope.date_range_in_vals = {age: [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50], years: [2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026]};
      $scope.date_range_in_vals = {};
      $scope.date_range_in_vals[age] = [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50];
      $scope.date_range_in_vals[years] = [2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026];
      console.log($scope.date_range_in_vals[age]);

      // Inflation
      $scope.default_form.inflation = $rootScope.labels["L0224"]; // Select Inflation
      var yes1 = $rootScope.labels["L0199"];
      var no1 = $rootScope.labels["L0200"];
      var na1 = $rootScope.labels["L0201"];
      //$scope.inflation_values = {'$': ['Yes', 'No'], '%': ['N/A']}
      $scope.inflation_values = {'$': [yes1, no1], '%': [na1]};
      $scope.inflation_vals = [yes1, no1, na1];
      //$scope.inflation_vals=[{'name': yes1, 'value': yes1},
      //                       {'name': no1, 'value' : no1},
      //                       {'name': na1, 'value' : na1}  ];
      $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
      //console.log("inflation values "+$scope.inflation_values);
      $scope.inflation = yes1;

      // Taxation
      $scope.default_form.taxable = $rootScope.labels["L0197"]; // Select taxable
      var taxable1 = $rootScope.labels["L0194"];
      var taxdeferred1 = $rootScope.labels["L0195"];
      var taxfree1 = $rootScope.labels["L0196"];
      $scope.tax_values=[{'name': taxable1, 'value': taxable1},
                       {'name': taxdeferred1, 'value': taxdeferred1},
                       {'name': taxfree1, 'value' : taxfree1}
                       ];
      $scope.taxable = taxable1; //'Taxable';

      // Name
      var client1 = $rootScope.labels["L0032"];
      var spouse1 = $rootScope.labels["L0229"];
      $scope.name_values = [{'name': client1, 'value': client1},
                        {'name': spouse1, 'value': spouse1}];

      // Compensation
      $scope.default_form.measure = $rootScope.labels["L0226"];

      $scope.changeBeginEnd(age); // Age
      //$scope.changeOption('&#36');
      console.log("calling changeOption");
      $scope.changeOption('$');

      $scope.savings_head = [ $rootScope.labels["L0159"], //'Saver Name',
                              $rootScope.labels["L0160"], //'Type Of Account',
                              $rootScope.labels["L0161"], //'Description',
                              $rootScope.labels["L0162"], //'Date Range In',
                              $rootScope.labels["L0163"], //'Begin',
                              $rootScope.labels["L0164"], //'End',
                              $rootScope.labels["L0166"], //'$ or % of Compensation',
                              $rootScope.labels["L0054"], //'Amount',
                              $rootScope.labels["L0165"], //'Inflation',
                              $rootScope.labels["L0026"] ]; //'Action'];
 
      label = document.getElementById('newSavingsPlanLabel');
      if (label) label.textContent = " + " + $rootScope.labels["L0158"];
      label = document.getElementById('savingsPlanLabel2');
      if (label) label.textContent = $rootScope.labels["L0157"];
      label = document.getElementById('saverNameLabel');
      if (label) label.textContent = $rootScope.labels["L0159"];
      label = document.getElementById('typeAccountLabel');
      if (label) label.textContent = $rootScope.labels["L0160"];
      label = document.getElementById('descriptionLabel');
      if (label) label.textContent = $rootScope.labels["L0161"];
      label = document.getElementById('dateRangeLabel');
      if (label) label.textContent = $rootScope.labels["L0162"];
      label = document.getElementById('beginLabel');
      if (label) label.textContent = $rootScope.labels["L0163"];
      label = document.getElementById('endLabel');
      if (label) label.textContent = $rootScope.labels["L0164"];
      label = document.getElementById('compensationLabel');
      if (label) label.textContent = $rootScope.labels["L0166"];
      label = document.getElementById('amountLabel');
      if (label) label.textContent = $rootScope.labels["L0054"];
      label = document.getElementById('inflationLabel');
      if (label) label.textContent = $rootScope.labels["L0165"];

      console.log("getting savings plans");
      var SavingsPlanPromise = SavingsPlan.getOne($rootScope.client_id);
      SavingsPlanPromise.then(function(result){
        console.log("in savings plan promise");

        //console.log(result);
        console.log(result[0]);

        len = result.length;
        for (var i=0; i < len; i++) {
          result[i].amount = parseFloat(result[i].amount);

          // Translate the Date Range in field
          if (result[i].date_range_in === "Age")
            result[i].date_range_in = $rootScope.labels["L0227"];
          else if (result[i].date_range_in === "Years")
            result[i].date_range_in = $rootScope.labels["L0228"];

          // Translate the taxable field
          if (result[i].taxable === "Taxable")
            result[i].taxable = $rootScope.labels["L0194"];
          else if (result[i].taxable === "Tax-deferred")
            result[i].taxable = $rootScope.labels["L0195"];
          else if (result[i].taxable === "Tax-Free")
            result[i].taxable = $rootScope.labels["L0196"];


          // Translate the Name field
          if (result[i].saver_name === "Client")
            result[i].saver_name = $rootScope.labels["L0032"];
          else if (result[i].saver_name === "Spouse")
            result[i].saver_name = $rootScope.labels["L0229"];


          // Translate the Inflation field
          if (result[i].inflation === "Yes")
            result[i].inflation = $rootScope.labels["L0199"];
          else if (result[i].inflation === "No")
            result[i].inflation = $rootScope.labels["L0200"];
          else if (result[i].inflation === "N/A")
            result[i].inflation = $rootScope.labels["L0201"];
        }

        if (len > 0)
          $scope.investments = result;
        else
          $scope.investments = [];

      });
      $scope.getValues();

  }

  //console.log("in savingsController "+$rootScope.client_id);

      
  $scope.resetForm = function(){
    $("#new_row").modal();
    $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
    $scope.changeBeginEnd($rootScope.labels["L0227"]); // Age
    $scope.changeOption('$');
    $('.input-field input').each(function(){
             $(this).siblings().removeClass('active');
      });
  }

  $scope.changeName = function(name_selected) {
    console.log("in changeName");
    $scope.saveings_plan_form.saver_name = name_selected;
    //$scope.new_taxable = null;
  }

  $scope.editRow = function(array_index){
    console.log(array_index);
    console.log("name valuesA");
    console.log($scope.name_values);
    $scope.edit_index = array_index;
    $scope.saveings_plan_form  = $scope.investments[array_index];
    $scope.editable_form = false;
    $("#new_row").modal();
    // $('.input-field input').each(function(){
    //      $(this).siblings().addClass('active');
    //  });
    // $("label").addClass('active');
  }

  $scope.getValues = function() {
    //$scope.savings_head = ['Saver Name','Type Of Account','Description','Date Range In','Begin','End',
      //                     '$ or % of Compensation','Amount','Inflation','Action'];
        //$scope.investments = [{'saver_name':'Bill Gates', 'description':'High School Education',
        //                        'taxable' : 'Taxable',
        //                        'date_range_in_selected':'Age', 'date_range_begin' :40, 'date_range_end': 42,
        //                        'options':'$', 'opt_begin':2000, 'answer':'yes'}];
    $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
    console.log("name values");
    console.log($scope.name_values);
    //$scope.compansation = {'&#36': [2000,2001], '%': [10,11]};
    //$scope.percentage=[{'name': '$', 'value' :'percentage'},
    //                   {'name': '%', 'value' :'dollar'}
    //                   ];

    //$scope.changeBeginEnd('Age');
    //$scope.changeOption('&#36');

    setTimeout(function(){
      $('.dropdown-button').dropdown();
    }, 300);
  }

      $scope.changeHeight = function(){
        $("#begin, #end").siblings().addClass('active');
      }
      $scope.changeHgt = function(){
        $("#cost_basi").siblings().addClass('active');
      }

      $scope.changeTaxable = function(taxable_selected) {
        console.log("in changeTaxable");
        $scope.saveings_plan_form.taxable = taxable_selected;
        $scope.new_taxable = null;

      }
      // $scope.changeOption = function(option_selected) {
      //   $scope.options = option_selected;
      // }
      $scope.changeAnswer = function(answer_selected) {
        console.log("in changeAnswer");
        $scope.saveings_plan_form.inflation = answer_selected;
      }


      $scope.removeRow = function(index){
        console.log(index);
        console.log("in savings plan removeRow index "+index);
        console.log($scope.investments[index]);
        //$scope.investments[index].client_id = -1;
        data_query = $scope.investments[index];
        console.log(data_query);
        res = SavingsPlan.deleteSavings($scope.investments[index].id, data_query);
        $scope.investments.splice( index, 1 );
      };

     $scope.notCommit = function(){
        if($scope.editable_form)
          $scope.editable_form = false;
          // $scope.investments[$scope.edit_index] = $scope.investments;

      }

     $scope.addRow = function(saveings_plan_form){
      if($scope.saveings_plan_form.saver_name === null 
        || $scope.saveings_plan_form.taxable === $rootScope.labels["L0197"] 
        || $scope.saveings_plan_form.date_range_in === $rootScope.labels["L0202"]
        || $scope.saveings_plan_form.measure === $rootScope.labels["L0226"] 
        || $scope.saveings_plan_form.inflation === $rootScope.labels["L0224"])
          return false;
      console.log('asdsadas');
      var edt = $scope.editable_form;
      if($scope.editable_form)
        $scope.investments[$scope.edit_index] = saveings_plan_form;
      else
        $scope.investments.push(saveings_plan_form);

        $scope.editable_form = false;
        $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
        $('.close').trigger('click');
        // $scope.name='';
        setTimeout(function(){
          $('.dropdown-button').dropdown();
        }, 300);
        $("#begin, #end").siblings().removeClass('active');
        $("#cost_basi").siblings().removeClass('active');
        console.log($scope.saveings_plan_form.taxable);

        console.log("at end of addRow "+edt+" "+$scope.edit_index);
        if ($scope.edit_index >= 0) 
        {
          // Edit an existing record

          console.log("in editing a savings plan");
          console.log($scope.investments[$scope.edit_index]);
          //data_query = $scope.investments[$scope.edit_index];
          data_query = Object.assign({}, $scope.investments[$scope.edit_index]);

          // Translate the Date Range in field
          if (data_query.date_range_in === $rootScope.labels["L0227"])
            data_query.date_range_in = "Age";
          else if (data_query.date_range_in === $rootScope.labels["L0228"])
            data_query.date_range_in = "Years";

          // Translate the taxable field
          if (data_query.taxable === $rootScope.labels["L0194"])
            data_query.taxable = "Taxable";
          else if (data_query.taxable === $rootScope.labels["L0195"])
            data_query.taxable = "Tax-deferred";
          else if (data_query.taxable === $rootScope.labels["L0196"])
            data_query.taxable = "Tax-Free";

          // Translate the name field
          if (data_query.saver_name === $rootScope.labels["L0229"])
            data_query.saver_name = "Spouse";
          else if (data_query.saver_name === $rootScope.labels["L0032"])
            data_query.saver_name = "Client";

          // Translate the Inflation field
          if (data_query.inflation === $rootScope.labels["L0199"])
            data_query.inflation = "Yes";
          else if (data_query.inflation === $rootScope.labels["L0200"])
            data_query.inflation = "No";
          else if (data_query.inflation === $rootScope.labels["L0201"])
            data_query.inflation = "N/A";

          var id = $scope.investments[$scope.edit_index].id;
          res = SavingsPlan.updateSavings(id, data_query);
        } 
        else 
        {
          // Inserting a new record
          len = $scope.investments.length;
          console.log("in adding a new savings plan");
          console.log($scope.investments[len-1]);
          $scope.investments[len-1].client_id = $rootScope.client_id;
          $scope.investments[len-1].id = 0;
          //data_query = $scope.investments[len-1];
          data_query = Object.assign({}, $scope.investments[len-1]);

          // Translate the Date Range in field
          if (data_query.date_range_in === $rootScope.labels["L0227"])
            data_query.date_range_in = "Age";
          else if (data_query.date_range_in === $rootScope.labels["L0228"])
            data_query.date_range_in = "Years";

          // Translate the taxable field
          if (data_query.taxable === $rootScope.labels["L0194"])
            data_query.taxable = "Taxable";
          else if (data_query.taxable === $rootScope.labels["L0195"])
            data_query.taxable = "Tax-deferred";
          else if (data_query.taxable === $rootScope.labels["L0196"])
            data_query.taxable = "Tax-Free";

          // Translate the name field
          if (data_query.saver_name === $rootScope.labels["L0229"])
            data_query.saver_name = "Spouse";
          else if (data_query.saver_name === $rootScope.labels["L0032"])
            data_query.saver_name = "Client";

          // Translate the Inflation field
          if (data_query.inflation === $rootScope.labels["L0199"])
            data_query.inflation = "Yes";
          else if (data_query.inflation === $rootScope.labels["L0200"])
            data_query.inflation = "No";
          else if (data_query.inflation === $rootScope.labels["L0201"])
            data_query.inflation = "N/A";

          res = SavingsPlan.createSavings(data_query);
        }

     };


     // $scope.$watch('getValues', function(){
     //    console.log('page loaded');
     //    $('select').material_select();
     //  });
})

finvese.controller('incomeController', function($rootScope, $scope, $location, Income, Advisors, Client, Translations) {
  $rootScope.income = true;
  $rootScope.savings  = false;
  $rootScope.investments = [];
  $rootScope.expenses = false;
  $rootScope.login = false;
  $rootScope.home = false;
  $rootScope.clients_list = false;
  $rootScope.clientListMenu = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  console.log("in incomeController advisor " + $rootScope.current_advisor + " client " + $scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Income";
      }
    })


  $scope.default_form = {'description':null,'date_range_in' :'select Date Range',
                'income_name': 'Client', 'date_range_begin':null, 'date_range_end':null, 
                'taxable': 'Select taxable', 'annual_flow':null,
                'inflation':'Select Inflation'};
  $scope.date_rage_in = {'Age': [40, 41, 42, 43, 45], 'Years': [2016, 2017, 2018, 2019, 2020]};
  $scope.age=[{'name': 'Age', 'value' :'age'},
                {'name': 'Years', 'value' : 'years'} ];
  $scope.inflation_values=[{'name': 'Yes', 'value' :'yes'},
                             {'name': 'No', 'value' : 'no'},
                             {'name': 'N/A', 'value' : 'na'} ];
  $scope.tax_values=[{'name': 'Taxable', 'value' :'taxable'},
                       {'name': 'Tax-deferred', 'value' :'tax-deferred'},
                       {'name': 'Tax-Free', 'value' : 'tax-free'} ];
  $scope.name_values = [{'name': 'Client', 'value': 'Client'},
                        {'name': 'Spouse', 'value': 'Spouse'}];

  console.log("in incomeController "+$rootScope.client_id);
  $scope.date_range_in = 'Age';
  $scope.ages = 'Age';
  $scope.inflation = 'Yes';
  $scope.taxable = 'Taxable';
  $scope.income_name = 'Client';

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    //console.log("in advisor promise");
    //console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {

      setMenuLabels($rootScope);

      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('submitLabel');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('otherIncomeLabel');
      if (label) label.textContent = $rootScope.labels["L0171"];
      
      // Date Range
      $scope.default_form.date_range_in = $rootScope.labels["L0202"]; // Select Date Range
      var age = $rootScope.labels["L0227"];
      var years =  $rootScope.labels["L0228"];
      $scope.date_range_in = age; // Age
      $scope.ages = age; // Age
      //$scope.date_rage_in = {'Age': [40, 41, 42, 43, 45], 'Years': [2016, 2017, 2018, 2019, 2020]};
      $scope.date_rage_in = {};
      $scope.date_rage_in[age] = [40, 41, 42, 43, 45];
      $scope.date_rage_in[years] = [2016, 2017, 2018, 2019, 2020];

      // Taxation
      $scope.default_form.taxable = $rootScope.labels["L0197"]; // Select taxable
      var taxable1 = $rootScope.labels["L0194"];
      var taxdeferred1 = $rootScope.labels["L0195"];
      var taxfree1 = $rootScope.labels["L0196"];
      $scope.tax_values=[{'name': taxable1, 'value': taxable1},
                       {'name': taxdeferred1, 'value': taxdeferred1},
                       {'name': taxfree1, 'value' : taxfree1}
                       ];
      $scope.taxable = taxable1; //'Taxable';

      // Name
      var client1 = $rootScope.labels["L0032"];
      var spouse1 = $rootScope.labels["L0229"];
      $scope.name_values = [{'name': client1, 'value': client1},
                        {'name': spouse1, 'value': spouse1}];

      // Inflation
      $scope.default_form.inflation = $rootScope.labels["L0224"]; // Select Inflation
      var yes1 = $rootScope.labels["L0199"];
      var no1 = $rootScope.labels["L0200"];
      var na1 = $rootScope.labels["L0201"];
      $scope.inflation_vals = [{'$': [yes1, no1], '%': [na1]}];
      $scope.inflation_values=[{'name': yes1, 'value': yes1},
                             {'name': no1, 'value' : no1},
                             {'name': na1, 'value' : na1}  ];
      $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
      console.log("inflation values "+$scope.inflation_values);
      $scope.inflation = yes1; // 'Yes';

      //$scope.income_head = ['Description','Date Range In','Begin','End','Annual Flow','Inflation','Action'];
      $scope.income_head = [ $rootScope.labels["L0161"], //'Description',
                              $rootScope.labels["L0162"], //'Date Range In',
                              "Name (if Age)",
                              $rootScope.labels["L0163"], //'Begin',
                              $rootScope.labels["L0164"], //'End',
                              $rootScope.labels["L0172"], //'Annual Flow',
                              $rootScope.labels["L0194"], // Taxable,
                              $rootScope.labels["L0165"], //'Inflation',
                              $rootScope.labels["L0026"] ]; //'Action'];
 
      label = document.getElementById('otherIncomeLabel2');
      if (label) label.textContent = " + " + $rootScope.labels["L0171"];
      label = document.getElementById('otherIncomeLabel3');
      if (label) label.textContent = $rootScope.labels["L0171"];
      label = document.getElementById('descriptionLabel');
      if (label) label.textContent = $rootScope.labels["L0161"];
      label = document.getElementById('dateRangeLabel');
      if (label) label.textContent = $rootScope.labels["L0162"];
      label = document.getElementById('beginLabel');
      if (label) label.textContent = $rootScope.labels["L0163"];
      label = document.getElementById('endLabel');
      if (label) label.textContent = $rootScope.labels["L0164"];
      label = document.getElementById('annualFlowLabel');
      if (label) label.textContent = $rootScope.labels["L0172"];
      label = document.getElementById('taxableLabel');
      if (label) label.textContent = $rootScope.labels["L0194"];
      label = document.getElementById('inflationLabel');
      if (label) label.textContent = $rootScope.labels["L0165"];

      console.log("getting other income");
      console.log($rootScope.client_id);
      var IncomePromise = Income.getOne($rootScope.client_id);
      IncomePromise.then(function(result){
        console.log("in income promise");

        //console.log(result);
        console.log(result[0]);

        len = result.length;
        for (var i=0; i < len; i++) {
          result[i].annual_flow = parseFloat(result[i].annual_flow);

          // Translate the Date Range in field
          if (result[i].date_range_in === "Age")
            result[i].date_range_in = $rootScope.labels["L0227"];
          else if (result[i].date_range_in === "Years")
            result[i].date_range_in = $rootScope.labels["L0228"];

          // Translate the taxable field
          //result[i].taxable = "Taxable";
          if (result[i].taxable === "Taxable")
            result[i].taxable = $rootScope.labels["L0194"];
          else if (result[i].taxable === "Tax-deferred")
            result[i].taxable = $rootScope.labels["L0195"];
          else if (result[i].taxable === "Tax-Free")
            result[i].taxable = $rootScope.labels["L0196"];

          // Translate the Name field
          if (result[i].income_name === "Client")
            result[i].income_name = $rootScope.labels["L0032"];
          else if (result[i].income_name === "Spouse")
            result[i].income_name = $rootScope.labels["L0229"];

          // Translate the Inflation field
          if (result[i].inflation === "Yes")
            result[i].inflation = $rootScope.labels["L0199"];
          else if (result[i].inflation === "No")
            result[i].inflation = $rootScope.labels["L0200"];
          else if (result[i].inflation === "N/A")
            result[i].inflation = $rootScope.labels["L0201"];

        }

        if (len > 0)
          $scope.investments = result;
        else
          $scope.investments = [];

      });

      $scope.intValues();
  }


  $scope.resetForm = function(){
    $("#new_row").modal();
    //$scope.saveings_plan_form = {'description':null,'date_range_in' :'select Date Range',
    //                               'date_range_begin':null, 'date_range_end':null, 'annual_flow':null,
    //                               'inflation':'Select Inflation'};
    $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
    $('.input-field input').each(function(){
      $(this).siblings().removeClass('active');
    });
  }

  $scope.changeTaxable = function(taxable_selected) {
    console.log("in changeTaxable");
    $scope.saveings_plan_form.taxable = taxable_selected;
    $scope.new_taxable = null;
  }

  $scope.changeName = function(name_selected) {
    console.log("in changeName");
    $scope.saveings_plan_form.income_name = name_selected;
    //$scope.new_taxable = null;
  }


  $scope.editRow = function(array_index){
    console.log(array_index);
    $scope.edit_index = array_index;
    $scope.saveings_plan_form  = $scope.investments[array_index];
    $scope.editable_form = true;
    $("#new_row").modal();
  }

  $scope.intValues = function() {
    //$scope.income_head = ['Description','Date Range In','Begin','End','Annual Flow','Inflation','Action'];
    $scope.investments = [{'description':'Rental Income','date_range_in':'Age',
                           'date_range_begin' :40, 'date_range_end': 45,'annual_flow':50000,'inflation':'Yes'}];

    //$scope.saveings_plan_form = {'description':null,'date_range_in' :'Select Date Range',
    //                             'date_range_begin':null, 'date_range_end':null, 'annual_flow':null,
    //                             'inflation_vals':'Select Inflation'};
    $scope.saveings_plan_form = Object.assign({}, $scope.default_form);


    $scope.changeBeginEnd($rootScope.labels["L0227"]); // Age
    setTimeout(function(){
      $('.dropdown-button').dropdown();
    }, 300);
  }

      $scope.changeHeight = function(){
        $("#begin, #end").siblings().addClass('active');
      }
      $scope.changeAge = function(age_selected) {
        $scope.saveings_plan_form.ages = age_selected;
      }
      $scope.changeAnswers = function(answer_selected) {
        $scope.saveings_plan_form.inflation = answer_selected;
      }
      $scope.changeBeginEnd = function(date_range_in_selected) {
        $scope.saveings_plan_form.date_range_in = date_range_in_selected;
        console.log($scope.saveings_plan_form.date_range_in);
        $scope.saveings_plan_form.date_range_begin = $scope.date_rage_in[date_range_in_selected][0];
        $scope.saveings_plan_form.date_range_end = $scope.date_rage_in[date_range_in_selected][$scope.date_rage_in[date_range_in_selected].length-1];
      }

      $scope.notCommit = function(){
        if($scope.editable_form)
          $scope.editable_form = false;
          // $scope.investments[$scope.edit_index] = $scope.investments;

      }

      $scope.removeRow = function(index){
        console.log(index);
        console.log("in other income removeRow index "+index);
        console.log($scope.investments[index]);
        //$scope.investments[index].client_id = -1;
        data_query = $scope.investments[index];
        console.log(data_query);
        res = Income.deleteIncome($scope.investments[index].id, data_query);
        $scope.investments.splice( index, 1 );
      };


     $scope.addRow = function(saveings_plan_form){
      if($scope.saveings_plan_form.date_range_in === $rootScope.labels["L0202"]
          || $scope.saveings_plan_form.date_range_begin === null 
          || $scope.saveings_plan_form.taxable === $rootScope.labels["L0197"] 
          || $scope.saveings_plan_form.inflation === $rootScope.labels["L0224"]
          || $scope.saveings_plan_form.date_range_end === null 
          || $scope.saveings_plan_form.annual_flow === null)
          return false;
      console.log('asdsadas');
      var edt = $scope.editable_form;
      if($scope.editable_form)
        $scope.investments[$scope.edit_index] = saveings_plan_form;
      else
        $scope.investments.push(saveings_plan_form);

        $scope.editable_form = false;
        //$scope.saveings_plan_form = {'saver_name': null, 'taxable': 'Select taxable','description': null,
        //'date_range_in_selected': null, 'date_range_begin' : null, 'date_range_end': null,
        //'options': null, 'opt_begin': null, 'inflation': null};
        $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
        $('.close').trigger('click');
        // $scope.name='';
        setTimeout(function(){
          $('.dropdown-button').dropdown();
        }, 300);
        $("#begin, #end").siblings().removeClass('active');

        console.log("at end of addRow "+edt+" "+$scope.edit_index);
        if ($scope.edit_index >= 0) 
        {
          // Edit an existing record
          console.log("in editing an other income");
          console.log($scope.investments[$scope.edit_index]);
          //data_query = $scope.investments[$scope.edit_index];
          data_query = Object.assign({}, $scope.investments[$scope.edit_index]);
          var id = $scope.investments[$scope.edit_index].id;

          // Translate the Date Range in field
          if (data_query.date_range_in === $rootScope.labels["L0227"])
            data_query.date_range_in = "Age";
          else if (data_query.date_range_in === $rootScope.labels["L0228"])
            data_query.date_range_in = "Years";

          // Translate the taxable field
          if (data_query.taxable === $rootScope.labels["L0194"])
            data_query.taxable = "Taxable";
          else if (data_query.taxable === $rootScope.labels["L0195"])
            data_query.taxable = "Tax-deferred";
          else if (data_query.taxable === $rootScope.labels["L0196"])
            data_query.taxable = "Tax-Free";

          // Translate the name field
          if (data_query.income_name === $rootScope.labels["L0229"])
            data_query.income_name = "Spouse";
          else if (data_query.income_name === $rootScope.labels["L0032"])
            data_query.income_name = "Client";

          // Translate the Inflation field
          if (data_query.inflation === $rootScope.labels["L0199"])
            data_query.inflation = "Yes";
          else if (data_query.inflation === $rootScope.labels["L0200"])
            data_query.inflation = "No";
          else if (data_query.inflation === $rootScope.labels["L0201"])
            data_query.inflation = "N/A";

          res = Income.updateIncome(id, data_query);
        } 
        else 
        {
          // Insert a new record
          len = $scope.investments.length;
          console.log("in adding a new other income");
          console.log($scope.investments[len-1]);
          $scope.investments[len-1].client_id = $rootScope.client_id;
          $scope.investments[len-1].id = 0;
          //data_query = $scope.investments[len-1];
          data_query = Object.assign({}, $scope.investments[len-1]);

          // Translate the Date Range in field
          if (data_query.date_range_in === $rootScope.labels["L0227"])
            data_query.date_range_in = "Age";
          else if (data_query.date_range_in === $rootScope.labels["L0228"])
            data_query.date_range_in = "Years";

          // Translate the taxable field
          if (data_query.taxable === $rootScope.labels["L0194"])
            data_query.taxable = "Taxable";
          else if (data_query.taxable === $rootScope.labels["L0195"])
            data_query.taxable = "Tax-deferred";
          else if (data_query.taxable === $rootScope.labels["L0196"])
            data_query.taxable = "Tax-Free";

          // Translate the name field
          if (data_query.income_name === $rootScope.labels["L0229"])
            data_query.income_name = "Spouse";
          else if (data_query.income_name === $rootScope.labels["L0032"])
            data_query.income_name = "Client";

          // Translate the Inflation field
          if (data_query.inflation === $rootScope.labels["L0199"])
            data_query.inflation = "Yes";
          else if (data_query.inflation === $rootScope.labels["L0200"])
            data_query.inflation = "No";
          else if (data_query.inflation === $rootScope.labels["L0201"])
            data_query.inflation = "N/A";

          res = Income.createIncome(data_query);
        }

     };


})

finvese.controller('expensesController', function($rootScope, $scope, $location, Expense, Advisors, Client, Translations) {
  $rootScope.expenses = true;
  $rootScope.income  = false;
  $rootScope.savings = false;
  $rootScope.login = false;
  $rootScope.clientListMenu = false;
  $rootScope.clients_list = false;
  $rootScope.investments = [];
  //$rootScope.client_id = 2;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  console.log("in expensesController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Expenses";
      }
    })

  $rootScope.home = false;
  $scope.date_range_in = 'Age';
  $scope.ages = 'Age';
  $scope.inflation = 'Yes';
  $scope.edit_index = -1;
  $scope.expense_name = 'Client';

  $scope.default_form = {'description':null, 'date_range_in':'Select Date Range',
            'expense_name': 'Client', 'date_range_begin':null, 'date_range_end':null, 
            'annual_need':null, 'inflation':'Select Inflation'};
  $scope.date_rage_in = {'Age': [40, 41, 42, 43, 44, 45], 'Years': [2016, 2017, 2018, 2019, 2020, 2021]};
  $scope.compansation = [{'$': [20000], '%': ['10%']}];
  $scope.inflation_vals = [{'$': ['yes', 'no'], '%': ['N/A']}];
  $scope.inflation_values=[{'name': 'Yes', 'value' :'yes'},
                             {'name': 'No', 'value' : 'no'},
                             {'name': 'N/A', 'value' : 'na'}  ];
  $scope.name_values = [{'name': 'Client', 'value': 'Client'},
                        {'name': 'Spouse', 'value': 'Spouse'}];

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }


    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('submitLabel');
      if (label)
        label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label)
        label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('expensesLabel2');
      if (label)
        label.textContent = $rootScope.labels["L0066"];
      //label = document.getElementById('newExpenseLabel');
      //if (label)
      //  label.textContent = $rootScope.labels["L0173"];

      // Date Range
      $scope.default_form.date_range_in = $rootScope.labels["L0202"]; // Select Date Range
      var age = $rootScope.labels["L0227"]; // Age
      var years = $rootScope.labels["L0228"]; // Years
      $scope.date_range_in = age; // Age
      $scope.ages = age; // Age
      //$scope.date_rage_in = {'Age': [40, 41, 42, 43, 44, 45], 'Years': [2016, 2017, 2018, 2019, 2020, 2021]};
      $scope.date_rage_in = {};
      $scope.date_rage_in[age] = [40, 41, 42, 43, 44, 45];
      $scope.date_rage_in[years] = [2016, 2017, 2018, 2019, 2020, 2021];

      // Inflation
      $scope.default_form.inflation = $rootScope.labels["L0224"]; // Select Inflation
      var yes1 = $rootScope.labels["L0199"];
      var no1 = $rootScope.labels["L0200"];
      var na1 = $rootScope.labels["L0201"];
      $scope.inflation_vals = [{'$': [yes1, no1], '%': [na1]}];
      $scope.inflation_values=[{'name': yes1, 'value': yes1},
                             {'name': no1, 'value' : no1},
                             {'name': na1, 'value' : na1}  ];
      $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
      //console.log("inflation values "+$scope.inflation_values);
      $scope.inflation = yes1; //'Yes';

      // Name
      var client1 = $rootScope.labels["L0032"];
      var spouse1 = $rootScope.labels["L0229"];
      $scope.name_values = [{'name': client1, 'value': client1},
                        {'name': spouse1, 'value': spouse1}];


      //$scope.expenses_head = ['Description','Date Range In','Begin','End','Annual Need','Inflation','Action'];
      $scope.expenses_head = [ $rootScope.labels["L0161"], //'Description',
                              $rootScope.labels["L0162"], //'Date Range In',
                              "Name (if Age)",
                              $rootScope.labels["L0163"], //'Begin',
                              $rootScope.labels["L0164"], //'End',
                              $rootScope.labels["L0174"], //'Annual Need',
                              $rootScope.labels["L0165"], //'Inflation',
                              $rootScope.labels["L0026"] ]; //'Action'];
 
      label = document.getElementById('newExpenseLabel');
      if (label)
        label.textContent = " + " + $rootScope.labels["L0173"];
      label = document.getElementById('newExpenseLabel2');
      if (label)
        label.textContent = $rootScope.labels["L0173"];

      label = document.getElementById('descriptionLabel');
      if (label)
        label.textContent = $rootScope.labels["L0161"];
      label = document.getElementById('dateRangeLabel');
      if (label)
        label.textContent = $rootScope.labels["L0162"];
      label = document.getElementById('beginLabel');
      if (label)
        label.textContent = $rootScope.labels["L0163"];
      label = document.getElementById('endLabel');
      if (label)
        label.textContent = $rootScope.labels["L0164"];
      label = document.getElementById('annualNeedLabel');
      if (label)
        label.textContent = $rootScope.labels["L0172"];
      label = document.getElementById('inflationLabel');
      if (label)
        label.textContent = $rootScope.labels["L0165"];

      console.log("getting expenses");
      console.log($rootScope.client_id);
      var ExpensePromise = Expense.getOne($rootScope.client_id);
      ExpensePromise.then(function(result){
        console.log("in expense promise");
        //console.log(result);
        console.log(result[0]);

        // Strangely the annual need gets retrieved as a string
        // Convert it to a number
        len = result.length;
        for (var i=0; i < len; i++) {
          result[i].annual_need = parseFloat(result[i].annual_need);

          // Translate the Date Range in field
          if (result[i].date_range_in === "Age")
            result[i].date_range_in = $rootScope.labels["L0227"];
          else if (result[i].date_range_in === "Years")
            result[i].date_range_in = $rootScope.labels["L0228"];

          // Translate the Inflation field
          if (result[i].inflation === "Yes")
            result[i].inflation = $rootScope.labels["L0199"];
          else if (result[i].inflation === "No")
            result[i].inflation = $rootScope.labels["L0200"];
          else if (result[i].inflation === "N/A")
            result[i].inflation = $rootScope.labels["L0201"];

          // Translate the Name field
          if (result[i].expense_name === "Client")
            result[i].expense_name = $rootScope.labels["L0032"];
          else if (result[i].expense_name === "Spouse")
            result[i].expense_name = $rootScope.labels["L0229"];

        }

        if (len > 0)
          $scope.investments = result;
        else
          $scope.investments = [];
      });

      $scope.intValues();    
  }

      
  $scope.resetForm = function(){
    $("#new_row").modal();
    //$scope.saveings_plan_form = {'description':null, 'date_range_in':'select Date Range',
    //                             'date_range_begin':null, 'date_range_end':null, 'annual_need':null,
    //                             'inflation':'Select Inflation'};
    $scope.saveings_plan_form = Object.assign({}, $scope.default_form);
    $('.input-field input').each(function(){
      $(this).siblings().removeClass('active');
    });
  }

  $scope.changeHeight = function(){
    $("#begin, #end").siblings().addClass('active');
  }

  $scope.changeName = function(name_selected) {
    console.log("in changeName "+name_selected);
    $scope.saveings_plan_form.expense_name = name_selected;
    //$scope.expense_name = name_selected;
  }

  $scope.editRow = function(array_index){
    console.log("in edit_row "+array_index);
    console.log($scope.investments[array_index]);
    $scope.edit_index = array_index;
    $scope.saveings_plan_form  = $scope.investments[array_index];
    $scope.editable_form = true;
    $("#new_row").modal();

    //console.log("at end of editRow");
    //console.log(array_index);
    //console.log($scope.investments);
  }

  $scope.intValues = function() {
    //$scope.expenses_head = ['Description','Date Range In','Begin','End','Annual Need','Inflation','Action'];
    // $scope.investments = [{'description':'Rental Income','date_range_in_selected':'Age',
    //                       'date_range_begin' :40, 'date_range_end': 45,'annual_need':50000,'answers':'Yes'}];

    //$scope.saveings_plan_form = {'description':null, 'date_range_in':'Select Date Range',
    //                             'date_range_begin':null, 'date_range_end':null, 'annual_need':null,
    //                             'inflation':'Select Inflation'};
    $scope.saveings_plan_form = Object.assign({},$scope.default_form);
    $scope.changeBeginEnd($rootScope.labels["L0227"]);
    setTimeout(function(){
      $('.dropdown-button').dropdown();
    }, 300);
  }

      $scope.changeAge = function(age_selected) {
        $scope.saveings_plan_form.ages = age_selected;
      }
      $scope.changeAnswers = function(answer_selected) {
        console.log("in changeAnswers "+answer_selected);
        $scope.saveings_plan_form.inflation = answer_selected;
      }
      $scope.changeBeginEnd = function(date_range_in_selected) {
        $scope.saveings_plan_form.date_range_in = date_range_in_selected;
        console.log($scope.saveings_plan_form.date_range_in);
        $scope.saveings_plan_form.date_range_begin = $scope.date_rage_in[date_range_in_selected][0];
        $scope.saveings_plan_form.date_range_end = $scope.date_rage_in[date_range_in_selected][$scope.date_rage_in[date_range_in_selected].length-1];
      }
      $scope.removeRow = function(index){
        console.log(index);
        console.log("in expense removeRow index "+index);
        console.log($scope.investments[index]);
        //$scope.investments[index].client_id = -1;
        data_query = $scope.investments[index];
        console.log(data_query);
        res = Expense.deleteExpense($scope.investments[index].id, data_query);
        $scope.investments.splice( index, 1 );
      };

      $scope.notCommit = function(){
        if($scope.editable_form)
          $scope.editable_form = false;
          // $scope.investments[$scope.edit_index] = $scope.investments;

      }

     $scope.addRow = function(saveings_plan_form)
     {
      //if($scope.saveings_plan_form.date_range_in === 'Select Date Range' 
      if($scope.saveings_plan_form.date_range_in === $rootScope.labels["L0224"] 
          || $scope.saveings_plan_form.date_range_begin === null 
          || $scope.saveings_plan_form.inflation === $rootScope.labels["L0224"]
          || $scope.saveings_plan_form.date_range_end === null 
          || $scope.saveings_plan_form.annual_need === null)
          return false;
       console.log('asdsadas');
       var edt = $scope.editable_form;
       if($scope.editable_form)
         $scope.investments[$scope.edit_index] = saveings_plan_form;
      else
        $scope.investments.push(saveings_plan_form);

        $scope.editable_form = false;
        $scope.saveings_plan_form = {'saver_name': null, 'taxable': 'Select taxable','description': null,
        'date_range_in': null, 'date_range_begin' : null, 'date_range_end': null,
        'options': null, 'opt_begin': null, 'inflation': null};
        $('.close').trigger('click');
        // $scope.name='';
        setTimeout(function(){
          $('.dropdown-button').dropdown();
        }, 300);
        $("#begin, #end").siblings().removeClass('active');
        console.log("at end of addRow");
        console.log($scope.edit_index);
        console.log(edt);
        console.log($scope.investments);
        if($scope.edit_index >= 0) 
        {
          // An expense was edited
          console.log("in editing an expense");
          console.log($scope.investments[$scope.edit_index]);
          //data_query = {client_id: 1, horizon: horizon_val, iterations: iters, startval: start_val, riskval: risk_val, returnval: return_val, mcvals: "[]"};
          data_query = Object.assign({}, $scope.investments[$scope.edit_index]);

          // Translate the Date Range in field
          if (data_query.date_range_in === $rootScope.labels["L0227"])
            data_query.date_range_in = "Age";
          else if (data_query.date_range_in === $rootScope.labels["L0228"])
            data_query.date_range_in = "Years";

          // Translate the Inflation field
          if (data_query.inflation === $rootScope.labels["L0199"])
            data_query.inflation = "Yes";
          else if (data_query.inflation === $rootScope.labels["L0200"])
            data_query.inflation = "No";
          else if (data_query.inflation === $rootScope.labels["L0201"])
            data_query.inflation = "N/A";

          // Translate the name field
          if (data_query.expense_name === $rootScope.labels["L0229"])
            data_query.expense_name = "Spouse";
          else if (data_query.expense_name === $rootScope.labels["L0032"])
            data_query.expense_name = "Client";

          console.log(data_query);
          var id = $scope.investments[$scope.edit_index].id;
          res = Expense.updateExpense(id, data_query);
        } else {
          // A new expense was added
          len = $scope.investments.length;
          $scope.investments[len-1].id = 0;
          console.log("in adding an expense");
          console.log(len);
          console.log($scope.investments[len-1]);
          $scope.investments[len-1].client_id = $rootScope.client_id;
          data_query = Object.assign({}, $scope.investments[len-1]);

          // Translate the Date Range in field
          if (data_query.date_range_in === $rootScope.labels["L0227"])
            data_query.date_range_in = "Age";
          else if (data_query.date_range_in === $rootScope.labels["L0228"])
            data_query.date_range_in = "Years";

          // Translate the Inflation field
          if (data_query.inflation === $rootScope.labels["L0199"])
            data_query.inflation = "Yes";
          else if (data_query.inflation === $rootScope.labels["L0200"])
            data_query.inflation = "No";
          else if (data_query.inflation === $rootScope.labels["L0201"])
            data_query.inflation = "N/A";

          // Translate the name field
          if (data_query.expense_name === $rootScope.labels["L0229"])
            data_query.expense_name = "Spouse";
          else if (data_query.expense_name === $rootScope.labels["L0032"])
            data_query.expense_name = "Client";

          res = Expense.createExpense(data_query);
        }
     };


})

finvese.controller('clientDetailsController', function($rootScope, $scope, $routeParams, $route, $location, Client, ClientHoldings, CashHoldings, Advisors, Translations){
  $rootScope.clientSlug = $route.current.params.slug;
  $rootScope.clientIndex = $route.current.params.clientIndex;
  $rootScope.home = false;
  $rootScope.clientListMenu = false;
  $rootScope.clients_list = false;
  $rootScope.register = false;
  $rootScope.login = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log("IN IF "+cid);
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
    setCurrentClient($rootScope, $rootScope.client_id, Client)
  }

  setAdvisor($rootScope);

  console.log("in clientDetailsController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Client Details";
      }
    })

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('nextLabel');
      if (label) label.textContent = $rootScope.labels["L0188"];
      label = document.getElementById('clientInformationLabel');
      if (label) label.textContent = $rootScope.labels["L0031"];
      

      //$scope.expenses_head = ['Description','Date Range In','Begin','End','Annual Need','Inflation','Action'];
      //$scope.expenses_head = [ $rootScope.labels["L0161"], //'Description',
      //                        $rootScope.labels["L0162"], //'Date Range In',
      //                        $rootScope.labels["L0163"], //'Begin',
      //                        $rootScope.labels["L0164"], //'End',
      //                        $rootScope.labels["L0174"], //'Annual Need',
      //                        $rootScope.labels["L0165"], //'Inflation',
      //                        $rootScope.labels["L0026"] ]; //'Action'];
 
      label = document.getElementById('firstNameLabel');
      if (label) label.textContent = $rootScope.labels["L0033"];
      label = document.getElementById('middleNameLabel');
      if (label) label.textContent = $rootScope.labels["L0034"];
      label = document.getElementById('lastNameLabel');
      if (label) label.textContent = $rootScope.labels["L0035"];
      label = document.getElementById('titleLabel');
      if (label) label.textContent = $rootScope.labels["L0036"];
      label = document.getElementById('genderLabel');
      if (label) label.textContent = $rootScope.labels["L0037"];
      label = document.getElementById('nationalityLabel');
      if (label) label.textContent = $rootScope.labels["L0043"];
      label = document.getElementById('dateBirthLabel');
      if (label) label.textContent = $rootScope.labels["L0038"];
      label = document.getElementById('residenceLabel');
      if (label) label.textContent = $rootScope.labels["L0040"];
      label = document.getElementById('retirementLabel');
      if (label) label.textContent = $rootScope.labels["L0039"];
      label = document.getElementById('maritalStatusLabel');
      if (label) label.textContent = $rootScope.labels["L0041"];
      label = document.getElementById('childrenLabel');
      if (label) label.textContent = $rootScope.labels["L0042"];
      label = document.getElementById('contactLabel');
      if (label) label.textContent = $rootScope.labels["L0010"];
      label = document.getElementById('emailLabel');
      if (label) label.textContent = $rootScope.labels["L0011"];
      label = document.getElementById('bestNumberLabel');
      if (label) label.textContent = $rootScope.labels["L0045"];
      label = document.getElementById('sourceIncomeLabel');
      if (label) label.textContent = $rootScope.labels["L0044"];

      label = document.getElementById('additionalPhoneLabel');
      if (label) label.textContent = $rootScope.labels["L0013"];
      label = document.getElementById('addressLine1Label');
      if (label) label.textContent = $rootScope.labels["L0014"];
      label = document.getElementById('addressLine2Label');
      if (label) label.textContent = $rootScope.labels["L0015"];
      label = document.getElementById('cityLabel');
      if (label) label.textContent = $rootScope.labels["L0016"];
      label = document.getElementById('postcodeLabel');
      if (label) label.textContent = $rootScope.labels["L0017"];
      label = document.getElementById('countryLabel');
      if (label) label.textContent = $rootScope.labels["L0018"];
      label = document.getElementById('additionalContactsLabel');
      if (label) label.textContent = $rootScope.labels["L0046"];
      label = document.getElementById('spouseNameLabel');
      if (label) label.textContent = $rootScope.labels["L0047"];
      label = document.getElementById('spouseDOBLabel');
      if (label) label.textContent = $rootScope.labels["L0049"];
      label = document.getElementById('spouseEmailLabel');
      if (label) label.textContent = $rootScope.labels["L0048"];
      label = document.getElementById('spouseContactLabel');
      if (label) label.textContent = $rootScope.labels["L0050"];
  }


  // console.log($rootScope.list);
  // console.log($scope.clientSlug);
  $scope.addingClass = function(){
    if ($rootScope.client_id > 0)
    //if(window.location.hash == '#/clients_details')
    {
      $('button').prev('label').addClass('active');
      $scope.edit_class = 'edit_input';
      $scope.selectedClient = {'title':''};
    }
    else
      $scope.edit_class = 'edit_label';
  }

   $scope.incompleteClose = function(){
     $('.incomplete').addClass('hide');
   }

     //  if (oldAmt > $scope.cashPositionPopup.amount) {
      // alert("Insufficient funds available");
   //   $('.insufficient').removeClass('hide');
   // }
   // console.log(window.location.hash);

  $scope.addClient = function(client) {
    console.log("in addClient new");
    if (!client) 
    {
      $('.incomplete').removeClass('hide');
    }
    else
    {
      console.log(client);

      if (client.slug) {
        console.log("editing existing record");
        //client.invest_amount = client.networth;
        if ($rootScope.clientIndex) {
          $rootScope.list[$rootScope.clientIndex]=client;
        }
      }
      else {
        console.log("adding new record");
        client.id = 0;
        //client.invest_amount = client.networth;
        if (client.firstName && client.lastName)
          client.slug=client.firstName.replace(' ','-') + client.lastName.replace(' ','-');
        else if (client.firstName)
          client.slug=client.firstName.replace(' ','-');
        else if (client.lastName)
          client.lastName.replace(' ','-');

      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10) {
        dd='0'+dd
      }
      if(mm<10) {
        mm='0'+mm
      }
      client.member_since = yyyy.toString() + '-' + mm.toString() +'-' + dd.toString();
      client.review_date = (yyyy+1).toString() + '-' + mm.toString() + '-' + dd.toString();
      client.last_review = client.member_since;
      console.log(client.member_since);
      console.log(client.review_date);

      if ($rootScope.list) {
        $rootScope.list.push(client);
      } else {
        $rootScope.list = [];
        $rootScope.list.push(client);
      }
      var len=$rootScope.list.length;
      $rootScope.clientIndex = len;
      $scope.slug = client.slug;
    }
    $scope.clientSlug = client.slug;
    console.log($rootScope.list);
    console.log($rootScope.clientIndex);
    console.log($scope.clientSlug);
    console.log('hello');

    data_query = {
              'id': client.id,
              'advisor_id': $rootScope.current_advisor,
              'client_id': client.clientId,
              'first_name': client.firstName,
              'middle_name': client.middleName,
              'last_name': client.lastName,
              'name': client.firstName + " " + client.lastName,
              'title': client.title,
              'gender': client.gender,
              'date_of_birth': client.dateOfBirth,
              'retirement': client.expectedRetirementAge,
              'nationality': client.nationality,
              'residence': client.countryResidence,
              'marital_status': client.maritalStatus,
              'num_children': client.numberOfChildren,
              'email': client.emailAddress,
              'address1': client.address1,
              'address2': client.address2,
              'phone': client.bestNumberToContact,
              'phone_alt': client.additionalPhoneNumber,
              'city': client.city,
              'postcode': client.postCode,
              'country': client.country,
              'spouse_name': client.spouseName,
              'spouse_dob': client.spouseDateBirth,
              'spouse_email': client.spouseEmail,
              'spouse_contact': client.spouseContactNumber,
              'invest_amount': client.invest_amount,
              'networth': client.networth,
              'member_since': client.member_since,
              'review_date': client.review_date,
              'last_review': client.last_review,
              'netWorth': client.netWorth,
              'sourceincome': client.sourceincome,
              'client_income': client.income,
              'client_income_growth': client.incomeGrowth,
              'spouse_retirement': client.spouseRetirement,
              'spouse_income': client.spouseIncome,
              'spouse_income_growth': client.spouseIncomeGrowth,
              'slug': client.slug
            };

    console.log(data_query);
    var ClientPromise = Client.updateClient(client.id, data_query);
    ClientPromise.then(function(result) {
      console.log("in client promise");
      console.log(result);

      if (result) {
        $rootScope.client_id = result.client_id;
        $rootScope.clientId = result.client_id;
      }
      // Update the CASH balance - assume USD by default
      //var cashBalance = result.invest_amount * 1000000;
      //console.log("Create CASH Balance "+result.client_id+" "+cashBalance);
      //data_query = { 'client_id': results.client_id,
      //            'currency': 'USD',
      //            'amount': cashBalance };
      //res = CashHoldings.updateCash(0, data_query);

    //  console.log("AAA");
    //  console.log($scope.list);
      if ($rootScope.list) $rootScope.list.push(result);
      if ($scope.list) $scope.list.push(result);
      //$scope.renderPage('clients_list');
      $scope.renderPage('/asset-input'); 
    });
  }
  }

  $scope.mainList = $rootScope.list;
  $scope.cancel = function(){
    $rootScope.list = $scope.mainList;
    $scope.renderPage('clients_list');
  }

  console.log("AAA "+$rootScope.clientSlug);
  console.log($rootScope.list);
  if ($rootScope.clientSlug) {
    $scope.selectedClient = _.findWhere($rootScope.list, {'slug': $rootScope.clientSlug});
    console.log($scope.selectedClient);
  } else {
    var advisor_id = $rootScope.current_advisor;
    console.log("advisor "+advisor_id+" client "+$rootScope.client_id);
    var ClientPromise = Client.getOne(advisor_id, $rootScope.client_id);
    ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      console.log(result);
      if (result) {
        var c = { 'id': result.id,
              'advisor_id': result.advisor_id,
              'clientId': result.client_id,
              'firstName': result.first_name,
              'middleName': result.middle_name,
              'lastName': result.last_name,
              'name': result.first_name + " " + result.last_name,
              'title': result.title,
              'gender': result.gender,
              'dateOfBirth': result.date_of_birth,
              'expectedRetirementAge': result.retirement,
              'nationality': result.nationality,
              'countryResidence': result.residence,
              'maritalStatus': result.marital_status,
              'numberOfChildren': result.num_children,
              'emailAddress': result.email,
              'address1': result.address1,
              'address2': result.address2,
              'bestNumberToContact': result.phone,
              'additionalPhoneNumber': result.phone_alt,
              'city': result.city,
              'postCode': result.postcode,
              'country': result.country,
              'spouseName': result.spouse_name,
              'spouseDateBirth': result.spouse_dob,
              'spouseEmail': result.spouse_email,
              'spouseContactNumber': result.spouse_contact,
              'invest_amount': result.invest_amount,
              'networth': result.networth,
              'member_since': result.member_since,
              'review_date': result.review_date,
              'last_review': result.last_review,
              'netWorth': result.netWorth,
              'sourceincome': result.sourceincome,
              'income': parseFloat(result.client_income),
              'incomeGrowth': result.client_income_growth,
              'spouseRetirement': result.spouse_retirement,
              'spouseIncome': parseFloat(result.spouse_income),
              'spouseIncomeGrowth': result.spouse_income_growth,
              'slug': result.slug
            };
        $scope.selectedClient = c;
        $rootScope.clientSlug = result.slug;
      }
    })
  }
//   $rootScope.sign = false;
//   $scope.city = 'Bangalore';
//   $scope.city_pre = false;
//   $scope.state = 'Karnataka';
//   $scope.state_pre = false;
//   $scope.setValues = function() {
//     $scope.cities = {'city' :'Select City'}
//     $scope.states = {'state' :'Select State'}
//     $scope.city_values= [{'name': 'Bangalore', 'value' :'bangalore'},
//                         {'name': 'Hyderabad', 'value' :'hyderabad'},
//                         {'name': 'Pune', 'value' : 'pune'}
//                        ];
//     $scope.state_values= [{'name': 'Karnataka', 'value' :'karnataka'},
//                         {'name': 'Andhrapradesh', 'value' :'andhrapradesh'},
//                         {'name': 'Maharastra', 'value' : 'maharastra'}
//                        ];

//     setTimeout(function(){
//         $('.dropdown-button').dropdown();
//       }, 300);

//     }
//     $scope.changeCity  = function(city_selected) {
//       $scope.cities.city= city_selected;
//       console.log(city_selected);
//       if(city_selected != 'Bangalore')
//         $scope.city_pre = true;
//       else
//         $scope.city_pre = false;
//     }
//     $scope.changeState  = function(state_selected) {
//       $scope.states.state = state_selected;
//       console.log(state_selected);
//       if(state_selected != 'Karnataka')
//         $scope.state_pre = true;
//       else
//         $scope.state_pre = false;
//     }
// $scope.setValues();
  $scope.title= [{'name': 'Mr', 'value' :'mr'},
                 {'name': 'Ms', 'value' :'ms'}
                ];
  $scope.title_pre = false;
  // $scope.selectedClient = {'title':'select Title', 'gender':'Select Gender', 'nationality':'Select Nationality',
  //                          'countryResidence':'Select Country','maritalStatus':'Select Marital Status',
  //                          'numberOfChildren':'Select number of children'}

  $scope.gender_pre = false;
  $scope.nationality_pre = false;
  $scope.countryResidence_pre = false;
  $scope.maritalStatus_pre = false;
  $scope.numberOfChildren_pre = false;
  // $scope.selectedClient = {'title':''};
  $scope.setValues = function() {
  $scope.title= [{'name': 'Mr', 'value' :'mr'},
                 {'name': 'Miss', 'value': 'miss'},
                 {'name': 'Mrs', 'value' :'mrs'}
                ];
  $scope.gender=[{'name': 'Male', 'value' :'male'},
                 {'name': 'Female', 'value' :'female'}
                ];
  $scope.nationality=[{'name': 'Australia', 'value': 'Australia'},
                           {'name': 'China', 'value': 'China'},
                           {'name': 'India', 'value': 'India'},
                           {'name': 'Indonesia', 'value': 'Indonesia'},
                           {'name': 'Japan', 'value': 'Japan'},
                           {'name': 'Malaysia', 'value': 'Malaysia'},
                           {'name': 'New Zealand', 'value': 'New Zealand'},
                           {'name': 'Philippines', 'value': 'Philippines'},
                           {'name': 'Singapore', 'value': 'Singapore'},
                           {'name': 'South Korea', 'value': 'South Korea'},
                           {'name': 'Thailand', 'value': 'Thailand'},
                           {'name': 'USA', 'value' :'USA'}
                     ];
  $scope.countryResidence=[{'name': 'Australia', 'value': 'Australia'},
                           {'name': 'China', 'value': 'China'},
                           {'name': 'India', 'value': 'India'},
                           {'name': 'Indonesia', 'value': 'Indonesia'},
                           {'name': 'Japan', 'value': 'Japan'},
                           {'name': 'Malaysia', 'value': 'Malaysia'},
                           {'name': 'New Zealand', 'value': 'New Zealand'},
                           {'name': 'Philippines', 'value': 'Philippines'},
                           {'name': 'Singapore', 'value': 'Singapore'},
                           {'name': 'South Korea', 'value': 'South Korea'},
                           {'name': 'Thailand', 'value': 'Thailand'},
                           {'name': 'USA', 'value' :'USA'}
                          ];
  $scope.maritalStatus=[{'name': 'Married', 'value' :'married'},
                        {'name': 'Single', 'value' :'single'},
                        {'name': 'Divorced', 'value' :'divorced'},
                        {'name': 'Widowed', 'value' :'widowed'}
                       ];
  $scope.numberOfChildren=[{'name': 1, 'value': 1},
                           {'name': 2, 'value': 2},
                           {'name': 3, 'value': 3},
                           {'name': 4, 'value': 4},
                           {'name': 5, 'value': 5},
                           {'name': 'More than 5', 'value': 6}
                          ];
  setTimeout(function(){
      $('.dropdown-button').dropdown();
       $scope.addActive=function(){
       $(".edit_label label").addClass('active');
      }
      $scope.addActive();
    }, 100);

  }
  $scope.changeTitles = false;
  $scope.changeGenders = false;
  $scope.changeNationalitys = false;
  $scope.changeResidencys = false;
  $scope.changeMaritalStatuss = false;
  $scope.changeChildrenNumbers = false;
  $scope.changeTitle = function(title_selected) {
        $scope.selectedClient.title = title_selected;
        $scope.changeTitles = true;
        if(title_selected != 'Mr')
          $scope.title_pre = true;
        else
          $scope.title_pre = false;
  }
  
  $scope.changeGender = function(gender_selected) {
        $scope.selectedClient.gender = gender_selected;
        $scope.changeGenders = true;
        if(gender_selected != 'Male')
          $scope.gender_pre = true;
        else
          $scope.gender_pre = false;
  }
  $scope.changeNationality = function(nationality_selected) {
        $scope.selectedClient.nationality = nationality_selected;
        $scope.changeNationalitys = true;
        if(nationality_selected != 'Indian')
          $scope.nationality_pre = true;
        else
          $scope.nationality_pre = false;
  }
  $scope.changeResidency = function(countryResidence_selected) {
        $scope.selectedClient.countryResidence = countryResidence_selected;
        $scope.changeResidencys = true;
        if(countryResidence_selected != 'India')
          $scope.countryResidence_pre = true;
        else
          $scope.countryResidence_pre = false;
  }
  $scope.changeMaritalStatus = function(maritalStatus_selected) {
        $scope.selectedClient.maritalStatus = maritalStatus_selected;
        $scope.changeMaritalStatuss = true;
        if(maritalStatus_selected != 'Married')
          $scope.maritalStatus_pre = true;
        else
          $scope.maritalStatus_pre = false;
  }
  $scope.changeChildrenNumber = function(numberOfChildren_selected) {
        $scope.selectedClient.numberOfChildren = numberOfChildren_selected;
        $scope.changeChildrenNumbers = true;
        if(numberOfChildren_selected != 1)
          $scope.numberOfChildren_pre = true;
        else
          $scope.numberOfChildren_pre = false;
  }
  $scope.setValues();
  $scope.addingClass();

})

finvese.controller('clientHistoryController', function($rootScope, $scope,  $routeParams, $route, $location, SavedRiskProfiles, SavedPortfolio, Client, Advisors, Translations) {
  $rootScope.home = false;
  $rootScope.register = false;
  $rootScope.login = false;
  //$scope.reviewHead = ['Review Date','AUM','Target Return', 'Target Risk', 'Target Loss'];
  //$scope.allocationsHead = ['Date','AUM','Expected Return', 'Expected Risk'];

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = cid.toString();
    $scope.clientId = $rootScope.client_id;
  }

  if ($rootScope.current_client) {
    console.log("current client defined");
    //$rootScope.client_id = $rootScope.current_client.clientId;
  } else {
    setCurrentClient($rootScope, $rootScope.client_id, Client);
  }

  console.log("in clientHistoryController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Reviews";
      }
    })


  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('reviewsLabel');
      if (label) label.textContent = $rootScope.labels["L0156"];
      label = document.getElementById('allocationsLabel');
      if (label) label.textContent = $rootScope.labels["L0189"];
      //label = document.getElementById('tradesLabel');
      //label.textContent = $rootScope.labels["L0138"];
      
      //$scope.reviewHead = ['Review Date','AUM','Target Return', 'Target Risk', 'Target Loss'];
      $scope.reviewHead = [ $rootScope.labels["L0179"], //'Review Date',
                            $rootScope.labels["L0060"], //'AUM',
                            $rootScope.labels["L0081"], //'Target Return', 
                            $rootScope.labels["L0082"], //'Target Risk', 
                            $rootScope.labels["L0083"] ]; //'Target Loss'];


      //$scope.allocationsHead = ['Date','AUM','Expected Return', 'Expected Risk'];
      $scope.allocationsHead = [ $rootScope.labels["L0190"], //'Date',
                                 $rootScope.labels["L0060"], //'AUM',
                                 $rootScope.labels["L0192"], //'Expected Return', 
                                 $rootScope.labels["L0193"] ]; //'Expected Risk'];
 
  }

  $scope.allocations = [];
  //$scope.allocations = [{'date':'15/09/2016','aum':'aum','expectedReturn':12, 'expectedRisk':0},
  //                      {'date':'15/09/2016','aum':'aum','expectedReturn':12, 'expectedRisk':0}];

  $scope.review = [];
  //$scope.review = [{'reviewDate':'12/07/2016','aum':'aum','targetReturn':10000, 'targetRisk':0, 'targetLoss':0},
  //                {'reviewDate':'12/07/2016','aum':'aum','targetReturn':10000, 'targetRisk':0, 'targetLoss':0}]

  var data_query = { 'advisor_id': 1, 'client_id': $rootScope.client_id };
  var SavedRiskProfilePromise = SavedRiskProfiles.getOne($rootScope.client_id, data_query);
  SavedRiskProfilePromise.then(function(result){
    console.log("in saved risk profile promise "+$rootScope.client_id);
    //$scope.asset_class = _.pluck(result[0], 'asset_class');
    //$scope.asset_description = _.pluck(result[0], 'asset_description');
    //$scope.asset_weight = _.pluck(result[0], 'asset_weight');
    console.log(result);
    console.log(result[0]);
    var len = result.length;
    $scope.review = [];
    for (var i=0; i<len; i++) {
      var tmp =  {'reviewDate': convertDateFormat(result[i].date_saved),
                  'aum': result[i].initial_invest,
                  'targetReturn': result[i].target_return,
                  'targetRisk': result[i].target_risk, 'targetLoss': result[i].target_loss };
      $scope.review.push(tmp);
    }

  });



  var SavedPortfolioPromise = SavedPortfolio.getOne($rootScope.client_id);
  SavedPortfolioPromise.then(function(result) {
    console.log("in saved portfolio promise "+$rootScope.client_id);
    console.log(result);
    var len = result.length;

    for (var i=0; i < len; i++) {
        //var fundid = result[i].fund_id;
          // Add to table
        var tmp = {'date': convertDateFormat(result[i].date_saved),
                   'aum': result[i].aum,
                   'expectedReturn': roundTwo(result[i].expected_return),
                   'expectedRisk': roundTwo(result[i].expected_risk)
                  };
        $scope.allocations.push(tmp);
    }

  })

})

finvese.controller('tradeHistoryController', function($rootScope, $scope,  $routeParams, $route, $location, ClientTrades, Client, Advisors, Translations) {
  $rootScope.home = false;
  $rootScope.register = false;
  $rootScope.login = false;
 
  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = cid.toString();
    $scope.clientId = $rootScope.client_id;
  }

  if ($rootScope.current_client) {
    console.log("current client defined");
    //$rootScope.client_id = $rootScope.current_client.clientId;
  } else {
    setCurrentClient($rootScope, $rootScope.client_id, Client);
  }

  console.log("in tradeHistoryController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + "- Trade History";
      }
    })


  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      var label = document.getElementById('tradesLabel');
      if (label) label.textContent = $rootScope.labels["L0138"];
      
      //$scope.tradeHead = ['Date','Fund', 'BBGID', 'Bought / Sold', 'Units Bought / Sold', 'Unit Price','Outstanding Units'];
      $scope.tradeHead = [$rootScope.labels["L0190"], //'Date',
                          $rootScope.labels["L0191"], //'Fund', 
                          $rootScope.labels["L0055"], //'BBGID', 
                          $rootScope.labels["L0147"], //'Bought / Sold', 
                          $rootScope.labels["L0148"], //'Units Bought / Sold', 
                          $rootScope.labels["L0146"], //'Unit Price',
                          $rootScope.labels["L0149"] ]; //'Outstanding Units'];

      var data_query = { 'client_id': $rootScope.client_id, 'rows': 0 };
      var ClientTradePromise = ClientTrades.getOne($rootScope.client_id, data_query);
      ClientTradePromise.then(function(result) {
        console.log("inside client trade promise "+$rootScope.client_id);
        console.log(result);
        var len = result.length;

        for (var i=0; i < len; i++) {

          // Translate trade type
          if (result[i].trade_type === "Buy")
            result[i].trade_type = $rootScope.labels["L0152"];
          else if (result[i].trade_type === "Sell")
            result[i].trade_type = $rootScope.labels["L0153"];

          // Add to table
          var tmp = {'date': convertDateFormat(result[i].trade_date),
                     'ticker': result[i].fund_symbol,
                     'fund': result[i].fund_name,
                     'bbgid': result[i].bbgid,
                     'boughtOrSold': result[i].trade_type,
                     'unitsBoughtOrSold': result[i].trade_quantity,
                     'unitPrice': result[i].trade_price,
                     'outstanding': result[i].after_trade,
                     };
        $scope.trade.push(tmp);

        }
      }); // ClientTradePromise

  }

  //$scope.tradeHead = ['Date','Fund', 'BBGID', 'Bought / Sold', 'Units Bought / Sold', 'Unit Price','Outstanding Units'];
  
  $scope.trade = [];
  //$scope.trade = [{'date':'12/07/2016','fund':20000, 'boughtOrSold':0,'unitsBoughtOrSold':'0','unitPrice':20,'outstanding':10},
  //                {'date':'12/07/2016','fund':20000, 'boughtOrSold':0,'unitsBoughtOrSold':'0','unitPrice':20,'outstanding':10}]

})



finvese.controller('clientListController', function($rootScope, $scope,  $routeParams, $route, Client, Advisors, Translations) {

  $scope.allocations = [{'date':'15/09/2016','aum':'aum','expectedReturn':12, 'expectedRisk':0}];
  $scope.saveClient = $route.current.params.saveClient;
  $rootScope.expenses = false;
  $rootScope.clients_list = true;
  $rootScope.register = false;
  $rootScope.overview = false;
  $rootScope.risks_calibrations = false;
  $rootScope.assets_allocation = false;
  $rootScope.income  = false;
  $rootScope.savings = false;
  $rootScope.investments = false;
  $rootScope.home = false;
  $rootScope.login = false;
  $rootScope.nodashboard = false;
  $rootScope.clientListMenu = false;
  $rootScope.client_name = "";
  $rootScope.current_client = "";
  $scope.reverse = false;
  $scope.propertyName = 'name';

  setAdvisor($rootScope);
  document.title = "Clients";

  
  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    //console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      //setMenuLabels($rootScope);
      setAdvisorMenuLabels($rootScope);

      //console.log($rootScope.labels);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('clientListLabel');
      if (label) label.textContent = $rootScope.labels["L0021"];
      label = document.getElementById('newClientLabel');
      if (label) label.textContent = "+ "+$rootScope.labels["L0027"];
      label = document.getElementById('nameLabel');
      if (label) label.textContent = $rootScope.labels["L0022"];
      label = document.getElementById('aumLabel');
      if (label) label.textContent = $rootScope.labels["L0023"];
      label = document.getElementById('lastReviewLabel');
      if (label) label.textContent = $rootScope.labels["L0024"];
      label = document.getElementById('nextReviewLabel');
      if (label) label.textContent = $rootScope.labels["L0025"];
      label = document.getElementById('actionLabel');
      if (label) label.textContent = $rootScope.labels["L0026"];

      //$scope.list_head = ['Name','Assets Under Management (M)','Last Review Date','Next Review Date','Action'];
      $scope.list_head = [$rootScope.labels["L0022"], //'Name',
                          $rootScope.labels["L0023"], //'Assets Under Management (M)',
                          $rootScope.labels["L0024"], //'Last Review Date',
                          $rootScope.labels["L0025"], //'Next Review Date',
                          $rootScope.labels["L0026"] ]; //'Action'];

  }


  $scope.intValues = function() {
    $scope.list_head = ['Name','Assets Under Management (M)','Last Review Date','Next Review Date','Action'];

    $scope.propertyName = 'name';

    $scope.sortBy = function(propertyName) {
      //console.log("in sortBy "+propertyName+" "+$scope.reverse);
      //console.log(client_list);
      $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;

      if (propertyName == 'invest_amount') {
        //console.log("in invest_amount part "+$scope.reverse);
        if ($scope.reverse)
          $rootScope.list.sort(function(a,b) {return b.invest_amount - a.invest_amount;});
        else
          $rootScope.list.sort(function(a,b) {return a.invest_amount - b.invest_amount;});
      }
      if (propertyName == 'name') {
        //console.log("in name part "+$scope.reverse);
        if ($scope.reverse) {
          //client_list.sort(function(string1,string2) {return ((string1 > string2) - (string1 < string2));});
          $rootScope.list.sort(function(a, b) {
              if(a.name.toLowerCase() < b.name.toLowerCase()) return 1;
              if(a.name.toLowerCase() > b.name.toLowerCase()) return -1;
              return 0;
          });
        }
        else {
          //client_list.sort(function(string1,string2) {return ((string2 > string1) - (string2 < string1));});
          $rootScope.list.sort(function(a, b) {
              if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
              if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
              return 0;
          });
        }
      } 
      //console.log(client_list);
    }; // end sortBy

      $scope.goDashboard = function(index) {
        console.log("in goDashboard "+index + " " + $rootScope.list[index].id);
        console.log($rootScope.list[index]);
        $rootScope.client_id = $rootScope.list[index].id;
        $rootScope.current_client = $rootScope.list[index];
        $rootScope.client_name = $rootScope.list[index].name;
        $scope.renderPage("/client-dashboard");
      }
      $rootScope.goSigninDashboard = function(index) {
        console.log("in goDashboard "+index+ " " + $rootScope.list[index].id);
        $rootScope.clientListMenu = true;
        console.log($rootScope.list[index]);
        $rootScope.client_id = $rootScope.list[index].id;
        $rootScope.current_client = $rootScope.list[index];
        $rootScope.client_name = $rootScope.list[index].name;
        $scope.renderPage("/client-dashboard");
      }

      $scope.newClient = function() {
        console.log("new client was pressed");
        $rootScope.client_id = null;
        $rootScope.clientId = null;
        $scope.renderPage("/clients_details");
      }

      var advisorId = $rootScope.current_advisor;
      getClientList($rootScope, advisorId, Client);

      setTimeout(function(){
          $('.dropdown-button').dropdown();
          $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 100 // Creates a dropdown of 15 years to control year
          });
        }, 300);
      }
      $scope.confirmDelete =function(index){
        console.log("in confirmDelete "+index);
        $rootScope.client_index_to_delete = index;
       $("#delete_row").modal();
       $('this').parent().css("display","none");
       };
      $scope.close =function(){
        $('.close').trigger('click');
      };
      $scope.removeRow = function(index){
        index = $rootScope.client_index_to_delete;
        console.log("removing a row "+index);
        console.log($scope.list[index]);
        var cid = $scope.list[index].clientId;
        console.log("client_id is "+cid);
        res = Client.deleteClient($rootScope.current_advisor, cid);

        $scope.list.splice( index, 1 );
        $('.close').trigger('click');
       };
     $scope.intValues();
})

function productDisplay(products, total, chart, charttitle) {
  var unallocated = total;
  var allocations = [];
  angular.forEach(products, function(value, key){
    //console.log(value);
    if(value.portfolio_weight > 0){
            allocations.push([value.fund_name, value.portfolio_weight]);
            unallocated = unallocated - value.portfolio_weight;
    }
    if(key+1 == products.length && unallocated > 0){
      allocations.push(['Unallocated', unallocated]);
    }
    //console.log(allocations);
    });
  //console.log(unallocated);
  //console.log(allocations);
  $(chart).highcharts({
        chart: {
            type: 'pie',
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false,
            marginRight:'-20',
            marginTop:'25',
           },
        exporting: { enabled: false },
        title: {
            text: charttitle,
            align: 'center',
            verticalAlign: 'top',
            y: 5,
            x: 15,
            style:{ color:'#4D7FBA',
                    fontWeight:'bold',
                    fontSize: '11px !important',
                  }
            },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
            enabled:true
            },
        plotOptions: {
            series: {
                   allowPointSelect: true
                  }

        },
        credits:false,
        plotOptions: {
            pie: {
                  borderColor: 'null',
                  dataLabels: {
                        enabled: true,
                        distance:-10,
                        y:0,
                        style: {
                                fontWeight: 'bold',
                                color: 'white',
                                textShadow: '0px 1px 2px black',
                                letterSpacing:0,
                                fontSize:'11px',
                               }
                      },
                      startAngle: -240,
                      endAngle: 120,
                      center: ['50%', '52%']
                  }
        },
        series: [{
            name: 'EU-EQ',
            innerSize: '50%',
            size: '120%',
            data: allocations,
            states: {
                      hover: {
                      enabled: true,
                      halo: {
                          size: 3
                        }
                      }
                }

            }]
        });

    }


finvese.controller('tradesController', function($rootScope, $scope, $location, Product, ClientHoldings, ClientTrades, ProductPrice, CashBonds, CashHoldings, Client, ExchangeRate, Advisors, Translations) {
  $rootScope.product = false;
  $rootScope.investments = false;
  $rootScope.login = false;
  $rootScope.savings = false;
  $rootScope.home = false;
  $rootScope.nodashboard = false;
  $rootScope.clients_list = false;
  $rootScope.trades = true;
  $rootScope.register = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  setAdvisor($rootScope);
  //$rootScope.default_language = "en";


  if ($rootScope.current_client) {
    console.log("current client defined");
    $rootScope.clientCurrencyValue = $rootScope.current_client.invest_amount*1000000;
    //$rootScope.client_id = $rootScope.current_client.clientId;
  } else {
    setCurrentClient($rootScope, $rootScope.client_id, Client);
    //console.log("using default value");
    //$rootScope.clientCurrencyValue = 20000;
  }
  console.log("in tradesController "+$rootScope.client_id);

  console.log("in tradesController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + " - Trades";
      }
    })


  //$rootScope.bondsHead = ['Security', 'BBGID', 'Currency', 'Maturity', 'Coupon', 'Yield', 'Rating', 'Price', 'Quantity', 'Value', 'Target Units', 'Action'];

  //$scope.cashBonds = [ {'security':'T 0 3/4 09/30/18','maturity':1.9, 'coupon':0.750,'yield':0.830,'price':99.89,'quantity':100.0,'value':9989.00, 'action':'Trade'},
  //           {'security':'T 1 3/8 08/31/20','maturity':3.8,'coupon':1.375,'yield':1.142,'price':101.08,'quantity':100.0, 'value':10108.00,'action':'Trade'} ];
  $scope.cashBonds = [];

  //$rootScope.tradesHead = ['Ticker', 'Name', 'BBGID', 'Currency', 'Type','Class','Unit Price','Quantity','Value','Target Units','Action'];
  $scope.cashBondsPopup={'bbgid':null,'targetValue':null,'rating':null,'security':null,'maturity':null, 'coupon':null,'yield':null,'price':null,'quantity':null,'value':null, 'currency': null};
  //$scope.cashPositionHead = ['Currency','Amount','Action'];
  $scope.cashPosition = [];
  //$scope.cashPosition = [{'currency':'USD','amount':2000,'action':'Trade','newCurrency':null,'exchangeRate':3000,'amtToBuy':20000},
  //                       {'currency':'EUR','amount':2033,'action':'Trade','newCurrency':null,'exchangeRate':2000,'amtToBuy':10000}];
  $scope.cashPositionPopup = [{'currency':null, 'amount':null,'newCurrency':null, 'action':'Trade', 'exchangeRate':null,'amtToBuy':null, 'amountold': null, 'amountnew': null}];
  $scope.holdings = [];
  //$scope.holdings =[{'client_id':1,'ticker':'ABC','name':'ABC Investement','type':'Fund','class':'Equity','unitPrice':12, 'quantity':12, 'value':123, 'targetValue':23, 'action':'Trade'},
  //    {'client_id':2,'ticker':'ABC2','name':'ABC2 Investement','type':'Fund','class':'Equity','unitPrice':12, 'quantity':12, 'value':123, 'targetValue':23, 'action':'Trade'},
  //    {'client_id':3,'ticker':'ABC2','name':'ABC2 Investement','type':'Fund','class':'Equity','unitPrice':12, 'quantity':12, 'value':123, 'targetValue':23, 'action':'Trade'}]

  $scope.tradingPopup=[{'ticker':null,'name':null,'type':'Fund','class':'Equity','unitPrice':null, 'quantity':null, 'value':null, 'targetValue':null, 'currency': null, 'action':'Trade'}]
  $scope.product_list = [];


  //$scope.tradeHead = ['Date','Ticker','Fund', 'BBGID', 'Currency', 'Bought / Sold', 'Units Bought / Sold', 'Unit Price','Outstanding Units'];

  $scope.trade = [];
  //$scope.trade = [{'date':'12/07/2016','fund':20000, 'boughtOrSold':0,'unitsBoughtOrSold':'0','unitPrice':20,'outstanding':10},
  //              {'date':'12/07/2016','fund':20000, 'boughtOrSold':0,'unitsBoughtOrSold':'0','unitPrice':20,'outstanding':10}]
  $scope.currency= [{'name': 'USD', 'value' :'usd'},
                           {'name': 'SGD', 'value' :'sgd'},
                           {'name': 'EUR', 'value' :'eur'},
                          ];
  $scope.newCurrency= [{'name': 'USD', 'value' :'usd'},
                        {'name': 'SGD', 'value' :'sgd'},
                        {'name': 'EUR', 'value' :'eur'},
                      ];


    $scope.client_name = $rootScope.client_name;
    $scope.cashBond_list = [];
    $scope.exchange_rates = [];
    //$scope.selecSecurity.security = [];
              //[{'name': 'Security', 'value' :'Security'},
              //   {'name': 'Security2', 'value' :'Security2'}
              //  ];

  $scope.selectBuySell = {'title':'Buy'};
  $scope.title= [{'name': 'Buy', 'value' :'buy'},
                 {'name': 'Sell', 'value' :'sell'}
                ];

  var ProductPromise = Product.getAll();
  ProductPromise.then(function(result) {
    console.log("in product promise");
    //console.log(result.length);
    $scope.product_list = result;
  });

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      var label = document.getElementById('cashPositionLabel');
      if (label) label.textContent = $rootScope.labels["L0140"];
      label = document.getElementById('cashPositionLabel2');
      if (label) label.textContent = $rootScope.labels["L0140"];

      //$scope.cashPositionHead = ['Currency','Amount','Action'];
      $scope.cashPositionHead = [ $rootScope.labels["L0053"], //'Currency',
                                  $rootScope.labels["L0054"], //'Amount',
                                  $rootScope.labels["L0026"] ]; //'Action'];

      label = document.getElementById('cashBondsLabel');
      if (label) label.textContent = $rootScope.labels["L0141"];
      label = document.getElementById('cashBondsLabel2');
      if (label) label.textContent = $rootScope.labels["L0141"];
      label = document.getElementById('newCashBondLabel');
      if (label) label.textContent = "+ " + $rootScope.labels["L0142"];
      label = document.getElementById('newtrade');
      if (label) label.textContent = "+ " + $rootScope.labels["L0143"];

      //$rootScope.bondsHead = ['Security', 'BBGID', 'Currency', 'Maturity', 'Coupon', 'Yield', 'Rating', 'Price', 'Quantity', 'Value', 'Target Units', 'Action'];
      $rootScope.bondsHead = [$rootScope.labels["L0204"], //'Security', 
                              $rootScope.labels["L0055"], //'BBGID', 
                              $rootScope.labels["L0053"], //'Currency', 
                              $rootScope.labels["L0119"], //'Maturity', 
                              $rootScope.labels["L0120"], //'Coupon', 
                              $rootScope.labels["L0121"], //'Yield', 
                              $rootScope.labels["L0123"], //'Rating', 
                              $rootScope.labels["L0122"], //'Price', 
                              $rootScope.labels["L0150"], //'Quantity', 
                              $rootScope.labels["L0205"], //'Value', 
                              $rootScope.labels["L0144"], //'Target Units', 
                              $rootScope.labels["L0026"] ]; //'Action'];

      label = document.getElementById('currentCurrencyLabel');
      if (label) label.textContent = $rootScope.labels["L0206"];
      label = document.getElementById('newCurrencyLabel');
      if (label) label.textContent = $rootScope.labels["L0207"];
      label = document.getElementById('exchangeRateLabel');
      if (label) label.textContent = $rootScope.labels["L0208"];
      label = document.getElementById('amountNewLabel');
      if (label) label.textContent = $rootScope.labels["L0209"];
      label = document.getElementById('amountBuyLabel');
      if (label) label.textContent = $rootScope.labels["L0210"];
      label = document.getElementById('amountCurrentLabel');
      if (label) label.textContent = $rootScope.labels["L0211"];

      label = document.getElementById('holdingsLabel');
      if (label) label.textContent = $rootScope.labels["L0069"];
      //label = document.getElementById('tradesLabel');
      //label.textContent = $rootScope.labels["L0138"];
      label = document.getElementById('holdingsLabel2');
      if (label) label.textContent = $rootScope.labels["L0069"];

      //$rootScope.tradesHead = ['Ticker', 'Name', 'BBGID', 'Currency', 'Type','Class','Unit Price','Quantity','Value','Target Units','Action'];
      $rootScope.tradesHead = [ $rootScope.labels["L0145"], //'Ticker', 
                                $rootScope.labels["L0117"], //'Name', 
                                $rootScope.labels["L0055"], //'BBGID', 
                                $rootScope.labels["L0053"], //'Currency', 
                                $rootScope.labels["L0127"], //'Type',
                                $rootScope.labels["L0221"], //'Class',
                                $rootScope.labels["L0146"], //'Unit Price',
                                $rootScope.labels["L0150"], //'Quantity',
                                $rootScope.labels["L0205"], //'Value',
                                $rootScope.labels["L0144"], //'Target Units',
                                $rootScope.labels["L0026"] ]; //'Action'];
      
      //$scope.tradeHead = ['Date','Ticker','Fund', 'BBGID', 'Currency', 'Bought / Sold', 'Units Bought / Sold', 'Unit Price','Outstanding Units'];
      $scope.tradeHead = [$rootScope.labels["L0190"], //'Date',
                          $rootScope.labels["L0145"], //'Ticker',
                          $rootScope.labels["L0117"], //'Fund', 
                          $rootScope.labels["L0055"], //'BBGID', 
                          $rootScope.labels["L0053"], //'Currency', 
                          $rootScope.labels["L0147"], //'Bought / Sold', 
                          $rootScope.labels["L0148"], //'Units Bought / Sold', 
                          $rootScope.labels["L0146"], //'Unit Price',
                          $rootScope.labels["L0149"] ]; //'Outstanding Units'];

      label = document.getElementById('amountLabel');
      if (label) label.textContent = $rootScope.labels["L0054"];
      label = document.getElementById('cancelLabel1');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('submitLabel1');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('bbgidLabel');
      if (label) label.textContent = $rootScope.labels["L0055"];
      label = document.getElementById('securityLabel');
      if (label) label.textContent = $rootScope.labels["L0204"];
      label = document.getElementById('maturityLabel');
      if (label) label.textContent = $rootScope.labels["L0119"];
      label = document.getElementById('couponLabel');
      if (label) label.textContent = $rootScope.labels["L0120"];
      label = document.getElementById('yieldLabel');
      if (label) label.textContent = $rootScope.labels["L0121"];
      label = document.getElementById('ratingLabel');
      if (label) label.textContent = $rootScope.labels["L0123"];
      label = document.getElementById('priceLabel');
      if (label) label.textContent = $rootScope.labels["L0122"];
      label = document.getElementById('currencyLabel');
      if (label) label.textContent = $rootScope.labels["L0053"];
      label = document.getElementById('buySellLabel');
      if (label) label.textContent = $rootScope.labels["L0154"];
      label = document.getElementById('quantityLabel');
      if (label) label.textContent = $rootScope.labels["L0150"];
      label = document.getElementById('cancelLabel2');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('submitLabel2');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('currentQuantityLabel');
      if (label) label.textContent = $rootScope.labels["L0212"];
      label = document.getElementById('targetQuantityLabel');
      if (label) label.textContent = $rootScope.labels["L0213"];

      label = document.getElementById('tickerLabel2');
      if (label) label.textContent = $rootScope.labels["L0145"];
      label = document.getElementById('bbgidLabel2');
      if (label) label.textContent = $rootScope.labels["L0055"];
      label = document.getElementById('nameLabel2');
      if (label) label.textContent = $rootScope.labels["L0117"];
      label = document.getElementById('typeLabel2');
      if (label) label.textContent = $rootScope.labels["L0127"];
      label = document.getElementById('currencyLabel2');
      if (label) label.textContent = $rootScope.labels["L0117"];
      label = document.getElementById('unitPriceLabel2');
      if (label) label.textContent = $rootScope.labels["L0127"];
      label = document.getElementById('buySellLabel2');
      if (label) label.textContent = $rootScope.labels["L0154"];
      label = document.getElementById('classLabel2');
      if (label) label.textContent = $rootScope.labels["L0221"];
      label = document.getElementById('currentQtyLabel');
      if (label) label.textContent = $rootScope.labels["L0212"];
      label = document.getElementById('targetQtyLabel');
      if (label) label.textContent = $rootScope.labels["L0213"];
      label = document.getElementById('qtyTradeLabel');
      if (label) label.textContent = $rootScope.labels["L0222"];


      label = document.getElementById('cancelLabel3');
      if (label) label.textContent = $rootScope.labels["L0019"];
      label = document.getElementById('submitLabel3');
      if (label) label.textContent = $rootScope.labels["L0020"];

      var buy1 = $rootScope.labels["L0152"];
      var sell1 = $rootScope.labels["L0153"];
      //$scope.selectBuySell = {'title':'Buy'};
      //$scope.title= [{'name': 'Buy', 'value' :'buy'}, {'name': 'Sell', 'value' :'sell'} ];
      $scope.title = [{'name': buy1, 'value': buy1}, {'name': sell1, 'value': sell1}];
      $scope.selectBuySell = {'title': buy1};
      console.log("$scope.title "+$scope.title);

      var CashHoldingsPromise = CashHoldings.getOne($rootScope.client_id);
      CashHoldingsPromise.then(function(result) {
        console.log("inside cash holdings promise");
        console.log(result);

        for(var i=0; i<result.length; i++) {
          var tmp = { 'currency': result[i].currency,
                  'amount': result[i].amount,
                  'action': $rootScope.labels["L0139"] }; //'Trade'
          console.log(tmp);
          $scope.cashPosition.push(tmp);
        }
      }); // CashHoldingsPromise


      var clientid = $rootScope.client_id;
      var advisorid = $rootScope.current_advisor;

      var CashBondPromise = CashBonds.getAll();
      CashBondPromise.then(function(res) {
        //console.log("inside cash bond promise");
        //console.log(res);

        if (res) {
          $scope.cashBond_list = res;
        }

        var data_query = {'advisor_id': advisorid, 'client_id': clientid, 'asset_class': -1 };
        var ClientHoldingPromise = ClientHoldings.getOne(clientid, data_query);
        ClientHoldingPromise.then(function(result) {
          //console.log("inside client holding promise");
          //console.log(result);

          var len = result.length;
          for (var i=0; i < len; i++) {
            var fundid = result[i].fund_id;
            if (result[i].fund_symbol === "CASH") 
            {
              // Note: This case is obsolete. Should never happen
              console.log("setting cash");
              $rootScope.clientCurrencyValue = roundTwo(result[i].num_units);
              $scope.clientCurrencyValue = roundTwo(result[i].num_units);
            }
            else 
            {
              var fundid = result[i].fund_id;
              var bondid = result[i].bond_id;
              var unitPrice = result[i].unit_price;
              console.log("in bond section");

              if (bondid) {
                // Cash Bond
                // Look up the details of this bond

                var maturity;
                var bbgid;
                var bond_coupon;
                var bond_yield;
                var bond_rating;
                var bond_price;

                //  Note: The target_value stored in the database is in terms of the number of dollars,
                // but the value displayed on the screen is in terms of the number of units
                var targetValue = result[i].target_value;
                console.log("target value="+targetValue);

                for(var j=0; j < $scope.cashBond_list.length; j++) {
                  if ($scope.cashBond_list[j].bond_id == bondid) {
                    console.log("found a bond");
                    maturity = $scope.cashBond_list[j].maturity;
                    bbgid = $scope.cashBond_list[j].bbgid;
                    bond_coupon = $scope.cashBond_list[j].bond_coupon;
                    bond_yield = $scope.cashBond_list[j].bond_yield;
                    bond_rating = $scope.cashBond_list[j].rating;
                    bond_price= $scope.cashBond_list[j].bond_price;

                    if ($scope.cashBond_list[j].bond_price > 0)
                      targetValue = roundTwo(targetValue/($scope.cashBond_list[j].bond_price*10.0));

                  }
                } // for

                var act = "Trade";
                if ($rootScope.labels)
                  act = $rootScope.labels["L0139"];

                var tmp = {'advisor_id': result[i].advisor_id,
                     'client_id': result[i].client_id,
                     'security': result[i].fund_name,
                     'currency': result[i].currency,
                     'maturity': maturity,
                     'bbgid': bbgid,
                     'coupon': bond_coupon,
                     'yield': bond_yield,
                     'rating': bond_rating,
                     'price': bond_price,
                     'quantity': result[i].num_units,
                     'value': result[i].holding_value,
                     'bond_id': result[i].bond_id,
                     'targetValue': targetValue,
                     'action': act }; //'Trade'};
                console.log(tmp);
                $scope.cashBonds.push(tmp);

              } 
              else 
              {
                // Funds
                // Add to table
            
                if (result[i].num_units > 0.0)
                  unitPrice = result[i].holding_value/result[i].num_units;

                // Note: The target_value stored in the database is in terms of the number of dollars,
                // but the value displayed on the screen is in terms of the number of units

                if (result[i].target_value > 0.0 && unitPrice > 0.0)
                  result[i].target_value = roundTwo(result[i].target_value/unitPrice);

                var fundid = result[i].fund_id;
                console.log("in fund section");

                console.log("length "+$scope.product_list.length+" fund "+fundid);
                for(var j=0; j < $scope.product_list.length; j++) {
                  if ($scope.product_list[j].fund_id == fundid) {
                    console.log("found a fund");
                    result[i].bbgid = $scope.product_list[j].bbgid;
                  }
                }

                var act = "Trade";
                if ($rootScope.labels)
                  act = $rootScope.labels["L0139"];

                var tmp = {'advisor_id': result[i].advisor_id,
                     'client_id': result[i].client_id,
                     'ticker': result[i].fund_symbol,
                     'name': result[i].fund_name,
                     'bbgid': result[i].bbgid,
                     'currency': result[i].currency,
                     'type': result[i].fund_type,
                     'class': result[i].fund_class,
                     'unitPrice': unitPrice,
                     'currentQuantity': result[i].num_units,
                     'quantity': result[i].num_units,
                     'num_units': result[i].num_units,
                     'value': result[i].holding_value,
                     'fund_id': result[i].fund_id,
                     'asset_class': result[i].asset_class,
                     'targetValue': result[i].target_value,
                     'action': act }; //'Trade'};
                $scope.holdings.push(tmp);

                if (result[i].num_units == 0.0) {
                  // Unit price was 0. Look up unit price
                  var data_query = { 'fund_id': fundid, 'latest': 1};
                  var ProductPricePromise = ProductPrice.getOne($scope.current_fund, data_query);
                  ProductPricePromise.then(function(result) {
                    console.log("in product price promise "+fundid);
                    if (result) {
                      console.log(result.fund_price);
                      var tmpf = result.fund_id;
                      for (var k=0; k < $scope.holdings.length; k++) {
                        if ($scope.holdings[k].fund_id == tmpf) {
                          console.log("found it "+k+" "+tmpf+" "+result.fund_price);
                          $scope.holdings[k].unitPrice = result.fund_price;

                          if ($scope.holdings[k].targetValue > 0.0)
                            $scope.holdings[k].targetValue = roundTwo($scope.holdings[k].targetValue/result.fund_price);
                        }
                      } // for
                    }
                  }); // ProductPricePromise
                }
              }
            }
          }

        }); // ClientHoldingsPromise

      }); // CashBondPromise

      var data_query = { 'client_id': clientid, 'rows': 5 };
      var ClientTradePromise = ClientTrades.getOne(clientid, data_query);
      ClientTradePromise.then(function(result) {
        console.log("inside client trade promise "+clientid);
        //console.log(result);
        var len = result.length;

        //$scope.trade = [{'date':'12/07/2016','fund':20000, 'boughtOrSold':0,'unitsBoughtOrSold':'0','unitPrice':20,'outstanding':10},
        //            {'date':'12/07/2016','fund':20000, 'boughtOrSold':0,'unitsBoughtOrSold':'0','unitPrice':20,'outstanding':10}]

        for (var i=0; i < len; i++) {
          var fundid = result[i].fund_id;

          // Translate the trade type
          if (result[i].trade_type === "Buy")
            result[i].trade_type = buy1;
          else if (result[i].trade_type === "Sell")
            result[i].trade_type = sell1;

          // Add to table
          var tmp = {'date': convertDateFormat(result[i].trade_date),
                     'ticker': result[i].fund_symbol,
                     'fund': result[i].fund_name,
                     'bbgid': result[i].bbgid,
                     'currency': result[i].currency,
                     'boughtOrSold': result[i].trade_type,
                     'unitsBoughtOrSold': result[i].trade_quantity,
                     'unitPrice': roundTwo(result[i].trade_price),
                     'outstanding': result[i].after_trade,
                     };
          $scope.trade.push(tmp);
        }

      }); // ClientTradePromise
   
  }


    var clientid = $rootScope.client_id;
    var advisorid = $rootScope.current_advisor;

    var ExchangeRatePromise = ExchangeRate.getOne(1);
    ExchangeRatePromise.then(function(result){
      console.log("inside exchange rate promise");
      console.log(result);

      $scope.exchange_rates = result;
      $scope.currency = ['USD'];
      $scope.newCurrency = ['USD'];
      for(var i=0; i<result.length; i++) {
        var tmp = {'name': result[i].currency, 'value': result[i].currency};
        $scope.currency.push(tmp);
        $scope.newCurrency.push(tmp);
      }
    });

    //$scope.lookupTicker = function(ticker) {
    //  console.log("in lookupTicker "+ticker+ " " + $scope.product_list.length);
    //  var len = $scope.product_list.length;
    //  for (var i=0; i < len; i++) {
    //    if ($scope.product_list[i].ticker_name == ticker) {
    //      console.log("found it");
    //      $scope.tradingPopup.name = $scope.product_list[i].fund_name;
    //      $scope.tradingPopup.fund_id = $scope.product_list[i].fund_id;
    //      $scope.current_fund = $scope.product_list[i].fund_id;

    //      var data_query = { 'fund_id': $scope.current_fund, 'latest': 1};
    //      var ProductPricePromise = ProductPrice.getOne($scope.current_fund, data_query);
    //      ProductPricePromise.then(function(result) {
    //          console.log("in product price promise");
    //          console.log(result.fund_price);
    //      })
    //    }
    //  }
    //}

    $scope.validateTrade = function() {
      console.log("in validateTrade "+$scope.tradingPopup.quantity);
      console.log("in validateTrade currency "+$scope.tradingPopup.currency);
      console.log("in validateTrade available "+$scope.availBalance);
      console.log("in validateTrade unitprice "+$scope.tradingPopup.unitPrice);
      console.log("in validateTrade action "+$scope.selectBuySell.title);
      if ($scope.availBalance < $scope.tradingPopup.quantity*$scope.tradingPopup.unitPrice
        && ($scope.selectBuySell.title == "Buy")) {
        console.log("Insufficient balance available");
        //alert('Insufficient Balance available');
        $('.insufficient2').removeClass('hide');
      }
    }

    $scope.editTrade = function(array_index){
      $("#tradePopup").modal();
      $scope.edit_index = array_index;
      $scope.tradingPopup  = $scope.holdings[array_index];
      $scope.editable_form = true;

      console.log("Currency "+$scope.holdings[array_index].currency);
      var availBalance = 0.0;
      for(var i=0; i<$scope.cashPosition.length; i++) {
        if ($scope.cashPosition[i].currency == $scope.holdings[array_index].currency) {
          availBalance = $scope.cashPosition[i].amount;
        }
      }
      $scope.availBalance = availBalance;
      console.log("avail balance="+availBalance);

      $('.input-field input').each(function(){
        $(this).siblings().addClass('active');
      });
      $("#ticker").prop('disabled', true);
      $("#buyorsell").prop('disabled', false);

      // Check available funds
      if ($scope.availaBalance <= 0 && $scope.holdings[array_index].quantity<=0) {
        console.log("Insufficient balance available in currency "+$scope.holdings[array_index].currency);
        alert('Insufficient Balance available in currency '+$scope.holdings[array_index].currency);
        $('.insufficient2').removeClass('hide');
      }
    }

    $scope.newCashBonds = function(){
      $("#newCashBondsPopup").modal();
      $scope.cashBondsPopup={'bbgid':null,'rating':null,'targetQuantity':null,'security':null,'maturity':null, 'coupon':null,'yield':null,'price':null,'quantity':null,'value':null, 'action':'Trade'};
      $('#security, #bbgid').prop('disabled', false);
    }

    $scope.editCashBonds = function(array_index){
      $("#newCashBondsPopup").modal();
      $scope.edit_index = array_index;
      console.log("edit index "+array_index);
      console.log($scope.cashBonds[array_index]);
      $scope.cashBondsPopup  = $scope.cashBonds[array_index];
      $scope.cashBondsPopup.CurrentQuantity = $scope.cashBondsPopup.quantity;
      $scope.editable_form = true;
      $('#security, #bbgid').prop('disabled', true);
      $('.input-field input').each(function(){
        $(this).siblings().addClass('active');
      });

    }

    $scope.editCashPosition = function(array_index){
      $("#cashPositionPopup").modal();
      $scope.edit_index = array_index;
      console.log("edit index "+array_index);
      console.log($scope.cashPosition[array_index]);
      console.log($scope.cashPositionPopup);
      $scope.cashPositionPopup  = $scope.cashPosition[array_index];
      console.log($scope.cashPositionPopup);
      $scope.editable_form = true;
      $('.input-field .ng-empty').each(function(){
        $(this).siblings().removeClass('active');
      });

    }
    $scope.newTrade = function(){
      $scope.showAlert = true;
      $("#tradePopup").modal();
      $scope.tradingPopup={'ticker':null,'name':null,'type':'Fund','class':'Equity','unitPrice':null, 'quantity':null, 'value':null,
                           'targetValue':null, 'action':'Trade','currentQuantity':0};
      $("#ticker").prop('disabled', false);
      $("#buyorsell").prop('disabled', true);

      $('.input-field input').each(function(){
        $(this).siblings().addClass('active');
      });
    }
   $scope.insufficientClose = function(){
     $('.insufficient').addClass('hide');
   }
   $scope.insufficientClose2 = function(){
     $('.insufficient2').addClass('hide');
   }
    // $scope.title= [{'name': 'Mr', 'value' :'mr'},
    //              {'name': 'Ms', 'value' :'ms'}
    //             ];
  $scope.title_pre = false;
  $scope.security_pre = false;
  $scope.currentCurrency_pre = false;

  $scope.selectCurrency = {'currency':'USD'};
  $scope.selectNewCurrency = {'newCurrency':'USD'};
  $scope.selectSecurity = {'security':'Security'};
  $scope.setValues = function() {

 
  setTimeout(function(){
      $('.dropdown-button').dropdown();
       $scope.addActive=function(){
       $(".edit_label label").addClass('active');
      }
      $scope.addActive();
    }, 100);

  }
  $scope.changebuyOrSell = false;
  $scope.changeCashSecurity = false;
  $scope.changeCurrency = false;
  $scope.changenewCurrency = false;

  $scope.changeCurrency = function(currency_selected) {
    $scope.selectCurrency.currency = currency_selected;
    // $scope.changeCurrentCurrency = true;
    if(currency_selected != 'USD')
      $scope.currency_pre = true;
    else
      $scope.currency_pre = false;
  }
  
  $scope.convertAmount = function() {
    console.log("in convertAmount " + $scope.cashPositionPopup.amtToBuy);
    var oldAmt = $scope.cashPositionPopup.amtToBuy/$scope.cashPositionPopup.exchangeRate;
    console.log("old amount "+ oldAmt);
    $scope.cashPositionPopup.amountold = roundTwo(oldAmt);
    $scope.cashPositionPopup.amountnew = $scope.cashPositionPopup.amtToBuy;
    if (oldAmt > $scope.cashPositionPopup.amount) {
      // alert("Insufficient funds available");
      $('.insufficient').removeClass('hide');
    }
  }

  $scope.changeNew = function(newCurrency_selected) {
    // A new currency has been selected for the Cash popup
    var oldCurrency = $scope.cashPositionPopup.currency;
    console.log("in changeNew "+oldCurrency+" to "+newCurrency_selected);
    if ($scope.exchange_rates) {
      if (newCurrency_selected == "USD") {
        // New Currency is USD
        // Lookup old currency and use reciprical of exchange rate
        for(var i=0; i<$scope.exchange_rates.length; i++) {
          if ($scope.exchange_rates[i].currency == oldCurrency) {
            console.log("found it "+$scope.exchange_rates[i].rate);
            $scope.cashPositionPopup.exchangeRate = 1.0/$scope.exchange_rates[i].rate;
            $scope.cashPositionPopup.newCurrency = newCurrency_selected;
            $scope.cashPositionPopup.amountNewCurrency = $scope.cashPostionPopup.amount * $scope.cashPositionPopup.exchangeRate;
          }
        }
      } 
      else {
        // New currency is not USD
        if (oldCurrency == "USD") {
          for(var i=0; i<$scope.exchange_rates.length; i++) {
            if ($scope.exchange_rates[i].currency == newCurrency_selected) {
              console.log("found it "+$scope.exchange_rates[i].rate);
              $scope.cashPositionPopup.exchangeRate = $scope.exchange_rates[i].rate;
              $scope.cashPositionPopup.newCurrency = newCurrency_selected;
              $scope.cashPositionPopup.amountNewCurrency = $scope.cashPositionPopup.amount * $scope.cashPositionPopup.exchangeRate;
            }
          }
        } else {
          // Old Currency is not USD and new currency is not USD
          var rate1 = 0.0;
          var rate2 = 0.0;
          $scope.cashPositionPopup.newCurrency = newCurrency_selected;
          for(var i=0; i<$scope.exchange_rates.length; i++) {
            if ($scope.exchange_rates[i].currency == newCurrency_selected) {
              console.log("found it "+$scope.exchange_rates[i].rate);
              rate2 = $scope.exchange_rates[i].rate;
            }
            if ($scope.exchange_rates[i].currency == oldCurrency) {
              console.log("found it "+$scope.exchange_rates[i].rate);
              rate1 = $scope.exchange_rates[i].rate;
            }
          }
          console.log("rate1 "+rate1+" rate2 "+rate2+" factor "+rate2/rate1);
          $scope.cashPositionPopup.exchangeRate = rate2/rate1;
          $scope.cashPositionPopup.amountNewCurrency = $scope.cashPositionPopup.amount * $scope.cashPositionPopup.exchangeRate;
        }
      }
    }
    $scope.selectNewCurrency.newCurrency = newCurrency_selected;
    $scope.changenewCurrency = true;
    if(newCurrency_selected != 'USD')
      $scope.newCurrency_pre = true;
    else
      $scope.newCurrency_pre = false;
  }
  $scope.saving_plan_form = {'currency':null, 'amount':null,'newCurrency':null, 'action':'Trade', 'exchangeRate':null,'amtToBuy':null};
  // $scope.addPosition = function(saveings_plan_form){

  //     if($scope.editable_form) {
  //       $scope.cashPosition[$scope.edit_index] = $scope.cashPositionPopup;
  //     }
  //     else {
  //       $scope.cashPosition.push($scope.$scope.cashPositionPopup);
  //     }
  // }

  $scope.addPosition = function(cashPositionPopup){
    // Pressed submit on cash position popup
    if ($scope.cashPositionPopup.amountold > $scope.cashPositionPopup.amount)
      return false;

    var oldCurrency = $scope.cashPositionPopup.currency;
    var newCurrency = $scope.cashPositionPopup.newCurrency;
    var rate = $scope.cashPositionPopup.exchangeRate;
    var orgAmount = $scope.cashPositionPopup.amount;
    var newAmt = roundTwo($scope.cashPositionPopup.amountnew);
    var oldAmt = roundTwo($scope.cashPositionPopup.amountold);
    console.log("new currency "+newCurrency+" "+newAmt+" "+oldAmt+" "+rate);
    //newAmt = roundTwo(oldAmt * rate);
    // Deduct oldAmt from oldCurrency
    if ($scope.cashPosition) {
      var found = false;
      for(var i=0; i<$scope.cashPosition.length; i++) {
        if ($scope.cashPosition[i].currency == oldCurrency) {
          found = true;
          console.log("found old at position "+i+ " "+oldAmt);
          orgAmount = $scope.cashPosition[i].amount;
          $scope.cashPosition[i].amount = roundTwo($scope.cashPosition[i].amount - oldAmt);
          var data_query = { 'client_id': $rootScope.client_id,
                  'currency': oldCurrency,
                  'amount': $scope.cashPosition[i].amount,
                  'advisor_id': $rootScope.current_advisor };
          res = CashHoldings.updateCash(0, data_query);
        }
      }

    }

    // Add newAmt to newCurrency
    if ($scope.cashPosition) {
      var found = false;
      for(var i=0; i<$scope.cashPosition.length; i++) {
        if ($scope.cashPosition[i].currency == newCurrency) { 
          found = true;
          console.log("found new at position "+i+" "+newAmt);
          $scope.cashPosition[i].amount = roundTwo($scope.cashPosition[i].amount + newAmt);
          var data_query = { 'client_id': $rootScope.client_id,
                  'currency': newCurrency,
                  'amount': $scope.cashPosition[i].amount,
                  'advisor_id': $rootScope.current_advisor };
          res = CashHoldings.updateCash(0, data_query);
        }
      }

      if (!found) {
        // Create a new entry
        console.log("adding new line "+newAmt);
        var tmp = { 'currency': newCurrency,
                  'amount': roundTwo(newAmt),
                  'action': 'Trade'};
        console.log(tmp);
        $scope.cashPosition.push(tmp);
        var data_query = { 'client_id': $rootScope.client_id,
                  'currency': newCurrency,
                  'amount': newAmt,
                  'advisor_id': $rootScope.current_advisor };
        res = CashHoldings.updateCash(0, data_query);
      }
    }
  
    // Add a record to the Trades list
    // Update the trade in database
    data_query = { 'advisor_id': $rootScope.current_advisor,
                   'client_id':$rootScope.client_id,
                   'fund_symbol': oldCurrency,
                   'fund_name': newCurrency,
                   'bbgid': '',
                   'fund_type': 'FX',
                   'trade_type': 'FX',
                   'trade_quantity': newAmt,
                   'trade_price': rate,
                   'before_trade': orgAmount,
                   'after_trade': roundTwo(orgAmount - oldAmt),
                   'bond_id': 0,
                  'fund_id': 0 };
    console.log(data_query);
    res = ClientTrades.updateTrade(0, data_query);

    // Update the table on the screen
      var tmp = {'date': getTodaysDate(),
                 'ticker': oldCurrency,
                 'fund': newCurrency,
                 'bbgid': '',
                 'boughtOrSold': 'FX',
                 'unitsBoughtOrSold': newAmt,
                 'unitPrice': rate,
                 'outstanding': roundTwo(orgAmount - oldAmt),
                 };
      $scope.trade.push(tmp);

    console.log('asdsadas');
    var edt = $scope.editable_form;
    if($scope.editable_form)
      $scope.cashPosition[$scope.edit_index] = cashPositionPopup;
    else
      $scope.cashPosition.push(cashPositionPopup);

      $scope.editable_form = false;
      $scope.cashPositionPopup = {'currency':null, 'amount':null,'newCurrency':null, 'action':'Trade', 'exchangeRate':null,'amtToBuy':null};

      $('.close').trigger('click');
      setTimeout(function(){
        $('.dropdown-button').dropdown();
      }, 300);
      console.log("at end of addRow "+edt+" "+$scope.edit_index);
       

  };

  $scope.changeTrades = function(title_selected) {
    console.log("in changeTrades "+title_selected);
    console.log("scope.title "+$scope.title[0].value);
    $scope.selectBuySell.title = title_selected;
    $scope.changebuyOrSell = true;
    
    //if(title_selected != 'Buy')
    if(title_selected != $scope.title[0].value)
      $scope.title_pre = true;
    else
      $scope.title_pre = false;
  }

    $scope.changeSecurity = function(security_selected) {
      $scope.selectSecurity.security = security_selected;
      $scope.changeCashSecurity = true;
      if(security_selected != 'Security')
        $scope.security_pre = true;
      else
        $scope.security_pre = false;
    }

    $scope.checkCashBonds = function(cashBondsPopup) {
      console.log("in checkCashBonds "+$scope.selectBuySell.title);
      var valid = true;
      var curqty = $scope.cashBondsPopup.CurrentQuantity;
      var tradeType = "Buy";
      
      //if ($scope.selectBuySell.title == "Sell") {
      if ($scope.selectBuySell.title != $scope.title[0].value) {
        tradeType = "Sell";
      }
      console.log("trade type="+tradeType);
      console.log("current quantity = "+curqty);
      if (tradeType == "Sell" && curqty < $scope.cashBondsPopup.quantity) {
        valid = false;
        console.log("Selling more than available");
        //cashBondsPopup.quantity.$setValidity("invqty",false);
        cashBondsPopup.quantity_error = true;
        //ngModel.$setValidity("invqty",false);
        //cashBondsPopup.quantity.innerHTML = "Invalid quantity";
      }
      if (tradeType == "Buy") {
        var cur = cashBondsPopup.currency;
        var avail = 0.0;
        var found = false;
        var cost = $scope.cashBondsPopup.quantity * $scope.cashBondsPopup.price;
        for (var i=0; i<$scope.cashPosition.length; i++) {
          if ($scope.cashPosition[i].currency == cur) {
            avail = $scope.cashPosition[i].amount;
          }
        }
        console.log("currency "+cur);
        console.log("avail "+avail);
        if (cost > avail) {
          valid = false;
          $('.insufficient2').removeClass('hide');
          console.log("Insufficient balance available in currency");
        }
      }
      return valid;
    }

   $scope.addNewCash= function(saveings_plan_form){
      // A Cash Bond trade has been entered
      // if($scope.tradingPopup.ticker === null || $scope.tradingPopup.quantity === null ||
      //     $scope.tradingPopup.unitPrice === null)
      //     return false;
      console.log('addNewCash');
      console.log($scope.cashBondsPopup);
      var edt = $scope.editable_form;


      if($scope.editable_form) {
        console.log("in if part "+$scope.edit_index);
        //$scope.cashBonds[$scope.edit_index] = $scope.cashBondsPopup;
      }
      else {
        console.log("in addNewCash "+$scope.edit_index);
      }

      $scope.cashBondsPopup.value = $scope.cashBondsPopup.price * $scope.cashBondsPopup.quantity * 10.0;
      console.log($scope.cashBondsPopup.value);

      var currency = $scope.cashBondsPopup.currency;
      console.log("Currency "+ currency);
      var tradeType = "Buy";
      var newQty = $scope.cashBondsPopup.quantity; 

      //if ($scope.selectBuySell.title == "Sell") {
      if ($scope.selectBuySell.title != $scope.title[0].value) {
        tradeType = "Sell";
        //newQty = newQty - $scope.holdings[$scope.edit_index].quantity;
      } else {
        //newQty = newQty + $scope.holdings[$scope.edit_index].quantity;
      }
      console.log("trade type "+tradeType);

      var bondid = $scope.cashBondsPopup.bond_id;
      console.log("bond_id "+bondid);
      var oldUnits = $scope.cashBondsPopup.CurrentQuantity;
      console.log("old units "+oldUnits);
      console.log("new units "+newQty);
      console.log("edit index "+$scope.edit_index);

      if ($scope.edit_index >= 0) {
        // Editing an existing row

        if (tradeType == "Sell") {
          newQty = $scope.cashBondsPopup.CurrentQuantity - newQty;
        } else {
          newQty = newQty + $scope.cashBondsPopup.CurrentQuantity;
        }
        console.log($scope.cashBonds[$scope.edit_index]);
        $scope.cashBonds[$scope.edit_index].value = newQty * $scope.cashBondsPopup.price * 10.0;
        $scope.cashBonds[$scope.edit_index].quantity = newQty;
      }
      else {
        // Add a new holding
        $scope.cashBonds.push($scope.cashBondsPopup);
      }

      var oldValue = $scope.cashBondsPopup.CurrentQuantity * $scope.cashBondsPopup.price * 10.0;
      var newValue = newQty * $scope.cashBondsPopup.price * 10.0;
      //var thisAmt = ($scope.cashBondsPopup.quantity-oldUnits) * $scope.cashBondsPopup.price * 10.0;
      var thisAmt = Math.abs(newValue - oldValue);
      console.log("thisAmt "+thisAmt);
      console.log("num_units "+$scope.cashBondsPopup.CurrentQuantity+" new Quantity "+newQty+" new value "+newValue);
 
      // Update the Holdings table in database
      data_query = { 'advisor_id': $rootScope.current_advisor,
                         'client_id': $rootScope.client_id,
                         'fund_symbol': $scope.cashBondsPopup.security,
                         'fund_name': $scope.cashBondsPopup.security,
                         'fund_type': 'CASH BOND',
                         'bbgid': $scope.cashBondsPopup.bbgid,
                         'currency': currency,
                         //'fund_class': $scope.holdings[$scope.edit_index].class,
                         'num_units': newQty,
                         'unit_price': $scope.cashBondsPopup.price,
                         'holding_value': newQty * $scope.cashBondsPopup.price * 10.0,
                         'bond_id': bondid,
                         'fund_id': 0,
                         'asset_class': 0
                      };
      console.log(data_query);
      //$scope.holdings[$scope.edit_index].currentQuantity = newQty;
      //$scope.holdings[$scope.edit_index].quantity = newQty;
      //$scope.holdings[$scope.edit_index].num_units = newQty;
      //$scope.holdings[$scope.edit_index].value = newValue;
      res = ClientHoldings.updateHolding(0, data_query);

      // Update the trade
      data_query = { 'advisor_id': $rootScope.current_advisor,
                         'client_id':$rootScope.client_id,
                         'fund_symbol': $scope.cashBondsPopup.security,
                         'fund_name': $scope.cashBondsPopup.security,
                         'bbgid': $scope.cashBondsPopup.bbgid,
                         'currency': currency,
                         'fund_type': 'CASH BOND',
                         'trade_type': tradeType,
                         'trade_quantity': $scope.cashBondsPopup.quantity,
                         'trade_price': $scope.cashBondsPopup.price,
                         'before_trade': oldUnits,
                         'after_trade': newQty,
                         'bond_id': bondid,
                         'fund_id': 0 };
      console.log(data_query);
      res = ClientTrades.updateTrade(0, data_query);

      // Update the table on the screen
      var tmp = {'date': getTodaysDate(),
                     'ticker': $scope.cashBondsPopup.security,
                     'fund': $scope.cashBondsPopup.security,
                     'bbgid': $scope.cashBondsPopup.bbgid,
                     'currency': currency,
                     'boughtOrSold': tradeType,
                     'unitsBoughtOrSold': newQty - oldUnits,
                     'unitPrice': roundTwo($scope.cashBondsPopup.price),
                     'outstanding': newQty,
                     };
      $scope.trade.push(tmp);
      //}

      // Adjust cash balance (based on the currency of the cash bond)
      // Look for currency
      var found = false;
      var oldBalance = 0.0;
      var newBalance = 0.0;
      console.log("currency="+currency+ " "+$scope.cashPosition.length);
      for (var i=0; i<$scope.cashPosition.length; i++) {
        if ($scope.cashPosition[i].currency == currency) {
          // found it
          console.log("found it "+$scope.cashPosition[i].amount);
          oldBalance = $scope.cashPosition[i].amount;
        }
      }

      if (tradeType == "Sell")
        newBalance = roundTwo(oldBalance + thisAmt);
      else
        newBalance = roundTwo(oldBalance - thisAmt);
      console.log("old balance="+oldBalance+" new balance="+newBalance+" value="+thisAmt);

      var data_query = { 'client_id': $rootScope.client_id,
                  'currency': currency,
                  'amount': newBalance,
                  'advisor_id': $rootScope.current_advisor };
      console.log(data_query);
      var res = CashHoldings.updateCash(0, data_query);

      //var oldCashBalance = $rootScope.clientCurrencyValue;
      //var newCashBalance;
      //if (tradeType == "Sell")
      //  newCashBalance = roundTwo(oldCashBalance + thisAmt);
      //else
      //  newCashBalance = roundTwo(oldCashBalance - thisAmt);
      //console.log("old balance="+oldCashBalance+" new balance="+newCashBalance+" value="+thisAmt);

      //data_query = { 'advisor_id': $rootScope.current_advisor,
      //               'client_id': $rootScope.client_id,
      //               'fund_symbol': 'CASH',
      //               'fund_name': 'CASH',
      //               'num_units': newCashBalance,
      //               'unit_price': 1.0,
      //               'holding_value': newCashBalance,
      //               'fund_id': 0
      //             };
      //console.log(data_query);
      //res = ClientHoldings.updateHolding(0, data_query);
      //$rootScope.clientCurrencyValue = newCashBalance;
      //$scope.clientCurrencyValue = newCashBalance;

      // Update the new cash balance on the screen
      for (var i=0; i<$scope.cashPosition.length; i++) {
        if ($scope.cashPosition[i].currency == currency) {
          // found it
          console.log("found it while updating screen");
          $scope.cashPosition[i].amount = roundTwo(newBalance);
        }
      }

      $scope.editable_form = false;
      $scope.cashBondsPopup={'bbgid':null,'security':null,'maturity':null, 'coupon':null,'yield':null,'price':null,'quantity':null,'targetquantity':null,'rating':null,'value':null, 'Action':'Trade'};
      $('.close').trigger('click');
      setTimeout(function(){
          $('.dropdown-button').dropdown();
      }, 300);
    }
 
    $scope.setValues();




    $scope.addRow = function(saveings_plan_form){
      if($scope.tradingPopup.ticker === null || $scope.tradingPopup.quantity === null ||
          $scope.tradingPopup.unitPrice === null)
          return false;
      console.log('in addRow '+$scope.editable_form);
      console.log($scope.tradingPopup);
      var edt = $scope.editable_form;
      var tradeType = "Buy";


      if($scope.editable_form) {
        //$scope.holdings[$scope.edit_index] = $scope.tradingPopup;
      }
      else {
        $scope.tradingPopup.value = $scope.tradingPopup.unitPrice * $scope.tradingPopup.quantity;
        $scope.holdings.push($scope.tradingPopup);
      }

      $scope.editable_form = false;
      $scope.tradingPopup={'ticker':null,'name':null,'type':null,'class':null,'unitPrice':null,
        'quantity':null, 'value':null, 'targetValue':null, 'action':'Trade'};
      $('.close').trigger('click');
      setTimeout(function(){
          $('.dropdown-button').dropdown();
      }, 300);

      console.log("at end of addRow "+edt+" "+$scope.edit_index);

      if (edt) {
      //  if ($scope.edit_index >= 0) {

          console.log("in editing a holding "+$scope.selectBuySell.title);
          console.log($scope.holdings[$scope.edit_index]);
          var newQty = $scope.holdings[$scope.edit_index].num_units;
          if ($scope.selectBuySell.title == "Sell") {
            tradeType = "Sell";
            newQty = newQty - $scope.holdings[$scope.edit_index].quantity;
          } else {
            newQty = newQty + $scope.holdings[$scope.edit_index].quantity;
          }
          var thisQty = $scope.holdings[$scope.edit_index].quantity;
          var thisAmt = thisQty * $scope.holdings[$scope.edit_index].unitPrice;
          var newValue = newQty * $scope.holdings[$scope.edit_index].unitPrice;
          console.log("num_units "+$scope.holdings[$scope.edit_index].num_units+" new Quantity "+newQty+" new value "+newValue);


          //$scope.holdings[$scope.edit_index].value = $scope.holdings[$scope.edit_index].unitPrice * $scope.holdings[$scope.edit_index].quantity;
          //data_query = $scope.holdings[$scope.edit_index];
          var id = $scope.holdings[$scope.edit_index].id;
          var fundid = $scope.holdings[$scope.edit_index].fund_id;
          var orgunits = $scope.holdings[$scope.edit_index].num_units;
          var advisorid = $rootScope.current_advisor;
          if (!fundid) {
            fundid = $scope.current_fund;
          }
          console.log("advisor "+advisorid+" fund "+fundid);

          // Update the trade
          data_query = { 'advisor_id': $rootScope.current_advisor,
                         'client_id': $scope.holdings[$scope.edit_index].client_id,
                         'fund_symbol': $scope.holdings[$scope.edit_index].ticker,
                         'fund_name': $scope.holdings[$scope.edit_index].name,
                         'bbgid': $scope.holdings[$scope.edit_index].bbgid,
                         'fund_type': $scope.holdings[$scope.edit_index].type,
                         'trade_type': tradeType,
                         'trade_quantity': $scope.holdings[$scope.edit_index].quantity,
                         'trade_price': $scope.holdings[$scope.edit_index].unitPrice,
                         'before_trade': $scope.holdings[$scope.edit_index].currentQuantity,
                         'after_trade': newQty,
                         'currency': $scope.holdings[$scope.edit_index].currency,
                         'fund_id': fundid };
          res = ClientTrades.updateTrade(0, data_query);

          // Update the table on the screen
          var tmp = {'date': getTodaysDate(),
                     'ticker': $scope.holdings[$scope.edit_index].ticker,
                     'fund': $scope.holdings[$scope.edit_index].name,
                     'bbgid': $scope.holdings[$scope.edit_index].bbgid,
                     'currency': $scope.holdings[$scope.edit_index].currency,
                     'boughtOrSold': tradeType,
                     'unitsBoughtOrSold': $scope.holdings[$scope.edit_index].quantity,
                     'unitPrice': roundTwo($scope.holdings[$scope.edit_index].unitPrice),
                     'outstanding': newQty,
                     };
          $scope.trade.push(tmp);

          data_query = { 'advisor_id': $rootScope.current_advisor,
                         'client_id': $scope.holdings[$scope.edit_index].client_id,
                         'fund_symbol': $scope.holdings[$scope.edit_index].ticker,
                         'fund_name': $scope.holdings[$scope.edit_index].name,
                         'bbgid': $scope.holdings[$scope.edit_index].bbgid,
                         'fund_type': $scope.holdings[$scope.edit_index].type,
                         'fund_class': $scope.holdings[$scope.edit_index].class,
                         'currency': $scope.holdings[$scope.edit_index].currency,
                         'num_units': newQty,
                         'unit_price': $scope.holdings[$scope.edit_index].unitPrice,
                         'holding_value': newValue,
                         'fund_id': fundid,
                         'asset_class': $scope.holdings[$scope.edit_index].asset_class
                      };
          console.log(data_query);
          $scope.holdings[$scope.edit_index].currentQuantity = newQty;
          $scope.holdings[$scope.edit_index].quantity = newQty;
          $scope.holdings[$scope.edit_index].num_units = newQty;
          $scope.holdings[$scope.edit_index].value = newValue;
          res = ClientHoldings.updateHolding(0, data_query);

          // Update cash balance (based on the currency of the fund)
          // Look for currency
          var found = false;
          var oldBalance = 0.0;
          var newBalance = 0.0;
          var currency = $scope.holdings[$scope.edit_index].currency;
          console.log("currency="+currency+ " "+$scope.cashPosition.length);
          for (var i=0; i<$scope.cashPosition.length; i++) {
            if ($scope.cashPosition[i].currency == currency) {
              // found it
              console.log("found it "+$scope.cashPosition[i].amount);
              oldBalance = $scope.cashPosition[i].amount;
            }
          }

          if (tradeType == "Sell")
            newBalance = roundTwo(oldBalance + thisAmt);
          else
            newBalance = roundTwo(oldBalance - thisAmt);
          console.log("old balance="+oldBalance+" new balance="+newBalance+" value="+thisAmt);

          var data_query = { 'client_id': $rootScope.client_id,
                  'currency': currency,
                  'amount': newBalance,
                  'advisor_id': $rootScope.current_advisor };
          console.log(data_query);
          var res = CashHoldings.updateCash(0, data_query);

          // Update the new cash balance on the screen
          for (var i=0; i<$scope.cashPosition.length; i++) {
            if ($scope.cashPosition[i].currency == currency) {
              // found it
              console.log("found it while updating screen");
              $scope.cashPosition[i].amount = roundTwo(newBalance);
            }
          }


          //var oldCashBalance = $rootScope.clientCurrencyValue;
          //var newCashBalance;
          //if (tradeType == "Sell")
          //  newCashBalance = oldCashBalance + thisAmt;
          //else
          //  newCashBalance = oldCashBalance - thisAmt;
          //console.log("old balance="+oldCashBalance+" new balance="+newCashBalance+" value="+thisAmt);

          //data_query = { 'advisor_id': $rootScope.current_advisor,
          //               'client_id': $scope.holdings[$scope.edit_index].client_id,
          //               'fund_symbol': 'CASH',
          //               'fund_name': 'CASH',
          //               'num_units': newCashBalance,
          //               'unit_price': 1.0,
          //               'holding_value': newCashBalance,
          //               'fund_id': 0
          //            };
          //console.log(data_query);
          //res = ClientHoldings.updateHolding(0, data_query);
          //$rootScope.clientCurrencyValue = newCashBalance;
          //$scope.clientCurrencyValue = newCashBalance;

       } else {

          len = $scope.holdings.length;
          console.log("in adding a new holding "+$rootScope.current_advisor);
          console.log($scope.holdings[len-1]);
          var fundid = $scope.holdings[len-1].fund_id;
          $scope.holdings[len-1].client_id = $rootScope.client_id;
          $scope.holdings[len-1].advisor_id = $rootScope.current_advisor;
          $scope.holdings[len-1].id = 0;
          $scope.holdings[len-1].value = $scope.holdings[len-1].unitPrice * $scope.holdings[len-1].quantity;
          $scope.holdings[len-1].currentQuantity = $scope.holdings[len-1].quantity;
          $scope.holdings[len-1].num_units = $scope.holdings[len-1].quantity;

          //data_query = $scope.holdings[len-1];
          data_query = { 'advisor_id': $scope.holdings[len-1].advisor_id,
                         'client_id': $scope.holdings[len-1].client_id,
                         'fund_symbol': $scope.holdings[len-1].ticker,
                         'fund_name': $scope.holdings[len-1].name,
                         'bbgid': $scope.holdings[len-1].bbgid,
                         'currency': $scope.holdings[len-1].currency,
                         'fund_type': $scope.holdings[len-1].type,
                         'fund_class': $scope.holdings[len-1].class,
                         'num_units': $scope.holdings[len-1].quantity,
                         'unit_price': $scope.holdings[len-1].unitPrice,
                         'holding_value': $scope.holdings[len-1].value,
                         'asset_class': $scope.holdings[len-1].asset_class,
                         'fund_id': fundid
                      };
          console.log(data_query);
          res = ClientHoldings.updateHolding(0, data_query);

          data_query = { 'advisor_id': $scope.holdings[len-1].advisor_id,
                         'client_id': $scope.holdings[len-1].client_id,
                         'fund_symbol': $scope.holdings[len-1].ticker,
                         'fund_name': $scope.holdings[len-1].name,
                         'fund_type': $scope.holdings[len-1].type,
                         'bbgid': $scope.holdings[len-1].bbgid,
                         'trade_type': "Buy",
                         'trade_quantity': $scope.holdings[len-1].quantity,
                         'trade_price': $scope.holdings[len-1].unitPrice,
                         'before_trade': 0,
                         'after_trade': $scope.holdings[len-1].quantity,
                         'currency': $scope.holdings[len-1].currency,
                         'fund_id': fundid };
          res = ClientTrades.updateTrade(0, data_query);

          // Update the table on the screen
          var tmp = {'date': getTodaysDate(),
                     'ticker': $scope.holdings[len-1].ticker,
                     'fund': $scope.holdings[len-1].name,
                     'bbgid': $scope.holdings[len-1].bbgid,
                     'currency': $scope.holdings[len-1].currency,
                     'boughtOrSold': "Buy",
                     'unitsBoughtOrSold': $scope.holdings[len-1].quantity,
                     'unitPrice': roundTwo($scope.holdings[len-1].unitPrice),
                     'outstanding': $scope.holdings[len-1].quantity,
                     };
          $scope.trade.push(tmp);

   
          // Update cash balance (based on the currency of the fund)
          // Look for currency
          var thisAmt = $scope.holdings[len-1].quantity * $scope.holdings[len-1].unitPrice;
          var found = false;
          var oldBalance = 0.0;
          var newBalance = 0.0;
          var currency = $scope.holdings[len-1].currency;
          console.log("currency="+currency+ " "+$scope.cashPosition.length);
          for (var i=0; i<$scope.cashPosition.length; i++) {
            if ($scope.cashPosition[i].currency == currency) {
              // found it
              console.log("found it "+$scope.cashPosition[i].amount);
              oldBalance = $scope.cashPosition[i].amount;
            }
          }

          if (tradeType == "Sell")
            newBalance = roundTwo(oldBalance + thisAmt);
          else
            newBalance = roundTwo(oldBalance - thisAmt);
          console.log("old balance="+oldBalance+" new balance="+newBalance+" value="+thisAmt);

          var data_query = { 'client_id': $rootScope.client_id,
                  'currency': currency,
                  'amount': newBalance,
                  'advisor_id': $rootScope.current_advisor };
          console.log(data_query);
          var res = CashHoldings.updateCash(0, data_query);

          // Update the new cash balance on the screen
          for (var i=0; i<$scope.cashPosition.length; i++) {
            if ($scope.cashPosition[i].currency == currency) {
              // found it
              console.log("found it while updating screen");
              $scope.cashPosition[i].amount = roundTwo(newBalance);
            }
          }

          // Update the cash balance
          //var oldCashBalance = $rootScope.clientCurrencyValue;
          //var newCashBalance = oldCashBalance - $scope.holdings[len-1].value;
          //console.log("old balance="+oldCashBalance+" new balance="+newCashBalance+" value="+$scope.holdings[len-1].value);
          //data_query = { 'advisor_id': advisorid,
          //               'client_id': $scope.holdings[len-1].client_id,
          //               'fund_symbol': 'CASH',
          //               'fund_name': 'CASH',
          //               'num_units': newCashBalance,
          //               'unit_price': 1.0,
          //               'holding_value': newCashBalance,
          //               'fund_id': 0
          //            };
          //console.log(data_query);
          //res = ClientHoldings.updateHolding(0, data_query);
          //$rootScope.clientCurrencyValue = newCashBalance;
          //$scope.clientCurrencyValue = newCashBalance;

        }

     };

     $scope.lookupbbgid = function() {
      // User enter bbgid in Cash Bonds popup
       var bbgid = $scope.cashBondsPopup.bbgid;
       var len = $scope.cashBond_list.length;
       console.log("in lookupbbgid "+bbgid);
       for (var i=0; i < len; i++) {
        //console.log($scope.cashBond_list[i].security_name);
        if ($scope.cashBond_list[i].bbgid === bbgid) {
          console.log("found it");
          $scope.cashBondsPopup.security = $scope.cashBond_list[i].security_name;
          $scope.cashBondsPopup.maturity = $scope.cashBond_list[i].maturity;
          $scope.cashBondsPopup.coupon = $scope.cashBond_list[i].bond_coupon;
          $scope.cashBondsPopup.bbgid = $scope.cashBond_list[i].bbgid;
          $scope.cashBondsPopup.yield = $scope.cashBond_list[i].bond_yield;
          $scope.cashBondsPopup.price = $scope.cashBond_list[i].bond_price;
          $scope.cashBondsPopup.rating = $scope.cashBond_list[i].rating;
          $scope.cashBondsPopup.currency = $scope.cashBond_list[i].currency;
          $scope.cashBondsPopup.CurrentQuantity = 0;
          $scope.cashBondsPopup.bond_id = $scope.cashBond_list[i].bond_id;
        }
      }

     }

     $scope.lookupSecurity = function() {
       var sec = $scope.cashBondsPopup.security;
       var len = $scope.cashBond_list.length;
       console.log("in lookupSecurity "+sec);
       for (var i=0; i < len; i++) {
        //console.log($scope.cashBond_list[i].security_name);
        if ($scope.cashBond_list[i].security_name === sec) {
          console.log("found it");
          $scope.cashBondsPopup.maturity = $scope.cashBond_list[i].maturity;
          $scope.cashBondsPopup.coupon = $scope.cashBond_list[i].bond_coupon;
          $scope.cashBondsPopup.bbgid = $scope.cashBond_list[i].bbgid;
          $scope.cashBondsPopup.yield = $scope.cashBond_list[i].bond_yield;
          $scope.cashBondsPopup.price = $scope.cashBond_list[i].bond_price;
          $scope.cashBondsPopup.rating = $scope.cashBond_list[i].rating;
          $scope.cashBondsPopup.currency = $scope.cashBond_list[i].currency;
          $scope.cashBondsPopup.CurrentQuantity = 0;
          $scope.cashBondsPopup.bond_id = $scope.cashBond_list[i].bond_id;
        }
      }
     }

     $scope.lookupTicker = function(){
       $scope.showAlert = true;
       var tk = $scope.tradingPopup.ticker;
       var len = $scope.product_list.length;
       console.log("in lookupTicker "+tk+ " " + len);
       for (var i=0; i < len; i++) {
         if ($scope.product_list[i].ticker_name == tk) {
           console.log("found it");
           var curr = $scope.product_list[i].currency;
           $scope.tradingPopup.name = $scope.product_list[i].fund_name;
           $scope.tradingPopup.fund_id = $scope.product_list[i].fund_id;
           $scope.tradingPopup.currency = curr;
           $scope.tradingPopup.bbgid = $scope.product_list[i].bbgid;
           $scope.current_fund = $scope.product_list[i].fund_id;
           console.log($scope.tradingPopup.name);

           // Check for available balance
           var availamt = 0;
           for (var k=0; k<$scope.cashPosition.length; k++) {
             if ($scope.cashPosition[k].currency == curr) {
               availamt = $scope.cashPosition[k].amount;
             }
           }
           console.log("available amount="+availamt);
           if (availamt <= 0) {
            console.log("Insufficient balance available in currency "+curr);
            alert("Insufficient balance available in currency "+curr);
            $scope.showAlert = false;
            $('.insufficient2').removeClass('hide');
           }

           // Look up unit price
           var data_query = { 'fund_id': $scope.current_fund, 'latest': 1};
           var ProductPricePromise = ProductPrice.getOne($scope.current_fund, data_query);
           ProductPricePromise.then(function(result) {
              console.log("in product price promise");
              console.log(result.fund_price);
              if (result)
                $scope.tradingPopup.unitPrice = result.fund_price;

           })
         }
       }

       var availBalance = 0.0;
       for(var i=0; i<$scope.cashPosition.length; i++) {
         if ($scope.cashPosition[i].currency == $scope.tradingPopup.currency) {
           availBalance = $scope.cashPosition[i].amount;
         }
       }
       $scope.availBalance = availBalance;
       console.log("avail balance="+availBalance);
    }

    $scope.lookupBbgid2 = function(){
       var bbgid = $scope.tradingPopup.bbgid;
       var len = $scope.product_list.length;
       console.log("in lookupBbgid2 "+bbgid+ " " + len);
       for (var i=0; i < len; i++) {
         if ($scope.product_list[i].bbgid == bbgid) {
           console.log("found it");
           var curr = $scope.product_list[i].currency;
           $scope.tradingPopup.ticker = $scope.product_list[i].ticker_name;
           $scope.tradingPopup.name = $scope.product_list[i].fund_name;
           $scope.tradingPopup.fund_id = $scope.product_list[i].fund_id;
           $scope.tradingPopup.currency = curr;
           $scope.current_fund = $scope.product_list[i].fund_id;
           console.log($scope.tradingPopup.name);

           // Check for available balance
           var availamt = 0;
           for (var k=0; k<$scope.cashPosition.length; k++) {
             if ($scope.cashPosition[k].currency == curr) {
               availamt = $scope.cashPosition[k].amount;
             }
           }
           console.log("available amount="+availamt);
           if (availamt <= 0) {
            console.log("Insufficient balance available in currency "+curr);
            if ($scope.showAlert)
              alert("Insufficient balance available in currency "+curr);
            $('.insufficient2').removeClass('hide');
           }

           // Look up the unit price
           var data_query = { 'fund_id': $scope.current_fund, 'latest': 1};
           var ProductPricePromise = ProductPrice.getOne($scope.current_fund, data_query);
           ProductPricePromise.then(function(result) {
              console.log("in product price promise");
              console.log(result.fund_price);
              if (result)
                $scope.tradingPopup.unitPrice = result.fund_price;

           })
         }
       }

       var availBalance = 0.0;
       for(var i=0; i<$scope.cashPosition.length; i++) {
         if ($scope.cashPosition[i].currency == $scope.tradingPopup.currency) {
           availBalance = $scope.cashPosition[i].amount;
         }
       }
       $scope.availBalance = availBalance;
       console.log("avail balance="+availBalance);

    }

})

finvese.controller('productSelectionController', function($rootScope, $scope, $location, Product, ClientPortfolio, ProductAllocation, ClientHoldings, CashBonds, Client, SavedAllocation, ExchangeRate, $window, Advisors, Translations) {
  $rootScope.productSelection = true;
  $rootScope.product = false;
  $rootScope.investments = false;
  $rootScope.login = false;
  $rootScope.savings = false;
  $rootScope.home = false;
  $rootScope.nodashboard = false;
  $rootScope.spiderSeries1 = [];
  $rootScope.spiderSeries2 = [];
  $rootScope.spiderSeries3 = [];
  $rootScope.clients_list = false;
  $scope.product_allocations = [];
  $rootScope.register = false;

  console.log($location.$$path);
  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  console.log("CURRENT CLIENT");
  console.log($rootScope.current_client);
  if ($rootScope.current_client) {
    console.log("current client defined");
    //$rootScope.client_id = $rootScope.current_client.clientId;
  } else {
    setCurrentClient($rootScope, $rootScope.client_id, Client);
  }

  setAdvisor($rootScope);
  $scope.client_name = $rootScope.client_name;
  
  console.log("in productSelectionController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + " - Products";
      }
    });

  $scope.colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];
  //$scope.colors = ['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C', '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049'];
  console.log("in productSelectionController "+$rootScope.client_id);
 
  $scope.current_table = 0;
  $scope.product_tables = [];
  $scope.asset_titles = [];
  $scope.asset_description = [];
  $scope.asset_class = [];
  $scope.asset_weight = [];
  $scope.products_number = 0;
  $scope.value = 0;
  $scope.asset_total = [];
  $scope.spiderSeries = [{
            name: 'Benchmark',
            data: [6.914328, 6.56659, 0.27, 0.420824,16.8149],
            pointPlacement: 'on',
            color: $scope.colors[0]
      }];
  $scope.propertyName = 'fund_name';

  $scope.sortBy = function(propertyName) {
      $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
  };


  //$scope.product_head = ['Fund Name','NAV (m)','Ret 1Y','Ret 3Y','Ret 5Y','Sharpe 1Y','Sharpe 3Y','Sharpe 5Y',
  //                         'Volatility 1Y','Sortino','MaxDW','Selection','Portfolio Weight'];
  $scope.product_head = $scope.header1;

  //$scope.product_head_us_eo = ['Fund Name','NAV (m)','Ret 1Y','Ret 3Y','Ret 5Y','Sharpe 1Y','Sharpe 3Y','Sharpe 5Y',
  //                               'Volatility 1Y','Sortino','MaxDW','Selection','Portfolio Weight'];

  $scope.exchange_rates = [];

  $scope.gotCashBonds = function() {
    //console.log("in gotCashBonds "+$scope.asset_class[0]==0);
    return ($scope.asset_class[0]==0);
  }


  $scope.setHeaders = function() {
      console.log("in setHeaders");
      var label = document.getElementById('fundNameLabel');
      if (label) label.textContent = $rootScope.labels["L0117"];
      label = document.getElementById('bbgidLabel');
      if (label) label.textContent = $rootScope.labels["L0055"];
      label = document.getElementById('currencyLabel');
      if (label) label.textContent = $rootScope.labels["L0053"];
      label = document.getElementById('issuerLabel');
      if (label)
        label.textContent = $rootScope.labels["L0118"];
      label = document.getElementById('maturityLabel');
      if (label)
        label.textContent = $rootScope.labels["L0119"];
      label = document.getElementById('couponLabel');
      if (label)
        label.textContent = $rootScope.labels["L0120"];
      label = document.getElementById('yieldLabel');
      if (label)
        label.textContent = $rootScope.labels["L0121"];
      label = document.getElementById('priceLabel');
      if (label)
        label.textContent = $rootScope.labels["L0122"];
      label = document.getElementById('ratingLabel');
      if (label)
        label.textContent = $rootScope.labels["L0123"];
      label = document.getElementById('navLabel');
      if (label)
        label.textContent = $rootScope.labels["L0126"];
      label = document.getElementById('typeLabel');
      if (label)
        label.textContent = $rootScope.labels["L0127"];
      label = document.getElementById('ret1yLabel');
      if (label)
        label.textContent = $rootScope.labels["L0128"];
      label = document.getElementById('ret3yLabel');
      if (label)
        label.textContent = $rootScope.labels["L0129"];
      label = document.getElementById('ret5yLabel');
      if (label)
        label.textContent = $rootScope.labels["L0130"];
      label = document.getElementById('sharpe1yLabel');
      if (label)
        label.textContent = $rootScope.labels["L0131"];
      label = document.getElementById('sharpe3yLabel');
      if (label)
        label.textContent = $rootScope.labels["L0132"];
      label = document.getElementById('sharpe5yLabel');
      if (label)
        label.textContent = $rootScope.labels["L0133"];
      label = document.getElementById('volatilityLabel');
      if (label)
        label.textContent = $rootScope.labels["L0135"];
      label = document.getElementById('sortinoLabel');
      if (label)
        label.textContent = $rootScope.labels["L0134"];
      label = document.getElementById('maxDWLabel');
      if (label)
        label.textContent = $rootScope.labels["L0136"];
      //label = document.getElementById('feeLabel');
      //if (label)
      //  label.textContent = $rootScope.labels["L0137"];
      label = document.getElementById('selectionLabel');
      if (label) label.textContent = $rootScope.labels["L0124"];
      label = document.getElementById('portfolioLabel');
      if (label) label.textContent = "Allocation"; //$rootScope.labels["L0125"];
  }


  $scope.nextChange = function(){
      if ($scope.current_table < 0) $scope.current_table=0;
      if ($scope.current_table >= $scope.product_tables.length)
        $scope.current_table = $scope.product_tables.length-1;
      console.log("current_table "+$scope.current_table+" "+$rootScope.previous+" "+$rootScope.next);
      if ($scope.current_table==0)
        $rootScope.previous = false;
      else
        $rootScope.previous = true;

      if ($scope.current_table == $scope.product_tables.length-1)
        $rootScope.next = false;
      else
        $rootScope.next = true;

      //$rootScope.next = true;
      //$rootScope.previous = false;
      $scope.setHeaders();
  }

  $scope.previousChange = function(){
      if ($scope.current_table < 0) $scope.current_table=0;
      if ($scope.current_table >= $scope.product_tables.length)
        $scope.current_table = $scope.product_tables.length-1;
      console.log("current_table "+$scope.current_table+" "+$rootScope.previous+" "+$rootScope.next);
      if ($scope.current_table==0)
        $rootScope.previous = false;
      else
        $rootScope.previous = true;

      if ($scope.current_table == $scope.product_tables.length-1)
        $rootScope.next = false;
      else
        $rootScope.next = true;
      //$rootScope.previous = true;
      //$rootScope.next = false;
      $scope.setHeaders();
  }


  $scope.checkOrUnchecked = function(item){
        console.log(item);
        fundname = item.fund_name;
        console.log("in checkOrUnchecked "+fundname);
        console.log(item);
        if (item.bond_id) {
          // Skip cash bonds
        } 
        else {
          if (item.selected == false) {
            item.selected = true;
            console.log("item selected");
            console.log(item);
          
            var i1 = $rootScope.spiderSeries1.length;
            var newitem1 = {
                name: item.fund_name,
                data: [negZero(item.fund_return1y), negZero(item.fund_return3y), negZero(item.fund_return5y)],
                pointPlacement: 'on',
                color: $scope.colors[i1] };
            $rootScope.spiderSeries1.push(newitem1);

            var i2 = $rootScope.spiderSeries2.length;
            var newitem2 = {
                name: item.fund_name,
                data: [negZero(item.fund_volatility1y), negZero(item.fund_volatility3y), negZero(item.fund_volatility5y), negZero(item.fund_downrisk), negZero(item.fund_maxdw)],
                pointPlacement: 'on',
                color: $scope.colors[i2] };
            $rootScope.spiderSeries2.push(newitem2);

            var i3 = $rootScope.spiderSeries3.length;
            var newitem3 = {
              name: item.fund_name,
              data: [negZero(item.fund_sharpe1y), negZero(item.fund_sharpe3y), negZero(item.fund_sharpe5y), negZero(item.fund_sortino)],
              pointPlacement: 'on',
              color: $scope.colors[i3] };
            $rootScope.spiderSeries3.push(newitem3);

            var i = $scope.spiderSeries.length;
            newitem = {name: item.fund_name, data: [item.fund_nav, negZero(item.fund_return1y), negZero(item.fund_sharpe1y), negZero(item.fund_sortino), negZero(item.fund_volatility)],
                      pointPlacement: 'on', color: $scope.colors[i] };
            //console.log(newitem);
            //color: $scope.colors[0]
            $scope.spiderSeries.push(newitem);

            //$scope.productSelection();
          } else {
            item.selected = false;
            len = $scope.spiderSeries.length;
            console.log("item unselected "+len);
            for (var i=0; i < len; i++) {
              if (fundname === $scope.spiderSeries[i].name) {
                console.log("found it at position "+i);
                $scope.spiderSeries.splice(i,1);
                break;
                //console.log($scope.spiderSeries[0].name);
              }
            }

            // Remove from first spider chart
            len = $scope.spiderSeries1.length;
            //console.log("item unselected "+len);
            for (var i=0; i < len; i++) {
              if (fundname === $rootScope.spiderSeries1[i].name) {
                console.log("found it at position "+i);
                $rootScope.spiderSeries1.splice(i,1);
                break;
                //console.log($scope.spiderSeries[0].name);
              }
            }

            // Remove from second spider chart
            len = $scope.spiderSeries2.length;
            //console.log("item unselected "+len);
            for (var i=0; i < len; i++) {
              if (fundname === $rootScope.spiderSeries2[i].name) {
                console.log("found it at position "+i);
                $rootScope.spiderSeries2.splice(i,1);
                break;
                //console.log($scope.spiderSeries[0].name);
              }
            }

            // Remove from third spider chart
            len = $scope.spiderSeries3.length;
            //console.log("item unselected "+len);
            for (var i=0; i < len; i++) {
              if (fundname === $rootScope.spiderSeries3[i].name) {
                console.log("found it at position "+i);
                $rootScope.spiderSeries3.splice(i,1);
                break;
                //console.log($scope.spiderSeries[0].name);
              }
            }

          }
          //console.log($scope.spiderSeries3);
          console.log("after");
          console.log(item);
          $scope.excessReturns();
          $scope.risks();
          $scope.adjustedReturns();
          $scope.productSelection();
        }
  }

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    var ExchangeRatePromise = ExchangeRate.getOne(1);
    ExchangeRatePromise.then(function(result){
      //console.log("inside exchange rate promise");
      //console.log(result.length);

      $scope.exchange_rates = result;

      if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
      {
        $scope.setScreenLabels();
      }
      else
      {
        $rootScope.labels = {};
        var TranslationPromise = Translations.getAll($rootScope.default_language);
        TranslationPromise.then(function(result) {
          //console.log("in translation promise");
          //console.log(result);
          //$rootScope.labels = result;
          for(var i=0; i<result.length; i++) {
            $rootScope.labels[result[i].label_id] = result[i].label_string;
          }
    
          $scope.setScreenLabels();
        }); // TranslationsPromise
      }
    }); // ExchangeRatePromise
  }); // AdvisorPromise

  $scope.setScreenLabels = function()
  {
        setMenuLabels($rootScope);

        //header:['Fund Name','NAV (m)','Ret 1Y','Ret 3Y',
        //            'Ret 5Y','Sharpe 1Y','Sharpe 3Y','Sharpe 5Y','Volatility 1Y',
        //            'Sortino','MaxDW','Selection','Portfolio Weight']
        $scope.header1 = [$rootScope.labels["L0117"], //'Fund Name',
                        $rootScope.labels["L0126"], //'NAV (m)',
                        $rootScope.labels["L0128"], //'Ret 1Y',
                        $rootScope.labels["L0129"], //'Ret 3Y',
                        $rootScope.labels["L0130"], //'Ret 5Y',
                        $rootScope.labels["L0131"], //'Sharpe 1Y',
                        $rootScope.labels["L0132"], //'Sharpe 3Y',
                        $rootScope.labels["L0133"], //'Sharpe 5Y',
                        $rootScope.labels["L0135"], //'Volatility 1Y',
                        $rootScope.labels["L0134"], //'Sortino',
                        $rootScope.labels["L0136"], //'MaxDW',
                        $rootScope.labels["L0124"], //'Selection',
                        "Allocation" //$rootScope.labels["L0125"] //'Portfolio Weight'
                        ];

        //console.log("getting client portfolio");
        var SavedAllocationPromise = SavedAllocation.getOne($rootScope.client_id);
        SavedAllocationPromise.then(function(result){

          //console.log("in client porfolio promise");
          //console.log(result);
          //console.log(result[0]);
      
          var arrlen = result.length;
          j = 0;
          for (var i=0; i < arrlen; i++) {

            // ignore asset classes that have a weight of 0
            if (result[i].asset_weight > 0.005) {
              $scope.asset_class[j] = result[i].asset_class;
              $scope.asset_description[j] = result[i].asset_description;
              $scope.asset_weight[j] = result[i].asset_weight;
              $scope.asset_total[j] = 0;
              //$scope.old_weight[i] = result[i].asset_weight;
              var w = Math.round(result[i].asset_weight*100)/100;
              //$scope.asset_titles[i] = result[i].asset_description + " " + result[i].asset_weight.toString() + "%";
              $scope.asset_titles[j] = result[i].asset_description + " " + w.toString() + "%";
              j = j + 1;
            }
          }
          $scope.asset_length = j;
          console.log("found "+j);

          for (var jk=0; jk<j; jk++) {
            $scope.product_allocations.push([]);
            $scope.product_tables.push([]);
          }
      
          // Adjust the titles to remove the cash one
          //for (var jk=1; jk<j; jk++)
          //  $scope.asset_titles[jk-1] = $scope.asset_titles[jk];

          // Adjust the titles to remove the cash one
          //console.log($scope.asset_titles);
          //console.log($scope.asset_titles[0]);
          //$scope.product_allocations.push([]);
   
          //console.log($scope.asset_class);
          //console.log($scope.asset_description);
          //console.log("ASSET WEIGHT");
          //console.log($scope.asset_weight);
          //console.log($scope.asset_length);
          //console.log("TITLES");
          //console.log($scope.asset_titles);
          //console.log($scope.asset_titles.length);

          $scope.productGauge();

          //$scope.asset_length = arrlen;
          console.log($scope.asset_length);

          var clientid = $rootScope.client_id;

          // Handle Cash Bonds
          var nextTable = 0;
          if ($scope.asset_class[0]==0) {
            // There are some cash bonds
            var cashWeight = $scope.asset_weight[0];
            console.log($scope.asset_weight[1]);
            console.log("cashWeight="+cashWeight+" first asset "+$scope.asset_class[0]);
            if (cashWeight > 0.005 && $scope.asset_class[0]==0) {
              nextTable = 1;
              var CashBondPromise = CashBonds.getAll();
              CashBondPromise.then(function(result) {
                console.log("inside cash bond promise");
                console.log(result);

                if (result) {
                  for (var i=0; i < result.length; i++) {
                    result[i].fund_id = result[i].bond_id;
                    result[i].fund_name = result[i].security_name;
                    result[i].asset_class = 0;
                    result[i].portfolio_weight = 0;
                    result[i].old_weight = 0;
                    result[i].selected = false;
                  }
                  $scope.cashBond_list = result;
                }
                $scope.product_allocations[0] = $scope.cashBond_list;

                var aum = 0;
                if ($rootScope.current_client)
                  aum = $rootScope.current_client.invest_amount;
 
                var dq = { 'advisor_id': $rootScope.current_advisor, 'client_id': $rootScope.client_id, 'asset_class': 0 };
                var ClientHoldingsPromise0 = ClientHoldings.getOne(clientid, dq);
                ClientHoldingsPromise0.then(function(res) {
                  console.log("in client holdings promise "+res.length);
                  if (res.length > 0) {
                    var arrlen = $scope.product_allocations[0].length;
                    for (var j=0; j < res.length; j++) {
                      var fundid = res[j].bond_id;
                      if (!res[j].fund_allocation && res[j].holding_value > 0) 
                      {
                          console.log("found null");
                          res[j].fund_allocation = roundOne(res[j].holding_value/(aum*10000.0));
                      }
                      if (res[j].fund_allocation > 0) {
                        for (var k=0; k < arrlen; k++) {
                          if ($scope.product_allocations[0][k].fund_id == fundid) {
                            console.log(res[j].fund_allocation);
                            $scope.product_allocations[0][k].portfolio_weight = res[j].fund_allocation;
                            $scope.product_allocations[0][k].old_weight = res[j].fund_allocation;
                            $scope.asset_total[0] += res[j].fund_allocation;
                          }
                        }
                      }
                    }
                  } // res.length > 0

                  $scope.product_tables[0] = {title: $scope.asset_titles[0], header: $scope.header1,
                     body:$scope.product_allocations[0]};
                });

 //               var ProductAllocationPromise0 = ProductAllocation.getOne(clientid,0);
 //               ProductAllocationPromise0.then(function(res) {
 //               if (res.length > 0) {
 //                   var arrlen = $scope.product_allocations[0].length;
 //                   for (var j=0; j < res.length; j++) {
 //                     var fundid = res[j].fund_id;
 //                     if (res[j].fund_allocation > 0) {
 //                       for (var k=0; k < arrlen; k++) {
 //                         if ($scope.product_allocations[0][k].fund_id == fundid) {
 //                           console.log(res[j].fund_allocation);
 //                           $scope.product_allocations[0][k].portfolio_weight = res[j].fund_allocation;
//                            $scope.product_allocations[0][k].old_weight = res[j].fund_allocation;
//                            $scope.asset_total[0] += res[j].fund_allocation;
//                          }
//                        }
//                      }
//                    }
 //                 } // res.length > 0
//
 //                  $scope.product_tables[0] = {title: $scope.asset_titles[0], header: $scope.header1,
 //                    body:$scope.product_allocations[0]};

//                }); // ProductAllocationPromise

              }); // CashBondPromise
            }
          }

          if ($scope.asset_class[0]==0)
            nextTable=1;
          else
            nextTable=0;

          // Note: asset_class[0] is for "Cash" and should be ignored
          for (var kk=nextTable; kk < $scope.asset_length; kk++) {
            console.log("kk="+kk+" asset weight="+$scope.asset_weight[kk]);

            if ($scope.asset_weight[kk] > 0.005) {
              var c1 = $scope.asset_class[kk];
              console.log("in productSelectionController 1 kk="+kk+" c1 "+c1);
              var data_query = {'asset_class': c1, 'fund_id': 0 };

              var ProductPromise = Product.getOne(c1, data_query);
              ProductPromise.then(function(result) {
                //console.log("inside productpromise");
                //console.log(result);
                var arrlen = result.length;

                // RAB: don't rely on kk
                var kk = 0;
                for (var jj=1; jj<$scope.asset_length; jj++)
                  if (result[0].asset_class == $scope.asset_class[jj])
                    kk = jj;
                var c1 = $scope.asset_class[kk];
                console.log("BBB kk="+kk+" c1="+c1+" client="+$rootScope.client_id);

                for (var i=0; i < arrlen; i++) {
                  result[i].portfolio_weight = 0;
                  result[i].old_weight = 0;
                  result[i].selected = false;
                  result[i].fund_nav = roundOne(result[i].fund_nav);
                }
                //$scope.product_allocations.push(result);
                $scope.product_allocations[kk] = result;

                //$scope.product_tables.push({title: $scope.asset_titles[1], header:['Fund Name','NAV (m)','Ret 1Y','Ret 3Y',
                //  'Ret 5Y','Sharpe 1Y','Sharpe 3Y','Sharpe 5Y','Return Volatility',
                //  'Sortino','MaxDW','Portfolio Weight'], body:result});
                var dq = { 'advisor_id': $rootScope.current_advisor, 'client_id': $rootScope.client_id, 'asset_class': c1 };
                var ClientHoldingsPromise1 = ClientHoldings.getOne($rootScope.client_id,dq);
                ClientHoldingsPromise1.then(function(res) {
                //var ProductAllocationPromise1 = ProductAllocation.getOne($rootScope.client_id,c1);
                //ProductAllocationPromise1.then(function(res) {
                  console.log("inside client holdings promise1 ");
                  console.log(res);

                  if (res.length > 0) {

                    // Don't rely on kk
                    var mm = 0;
                    for (var jj=0; jj<$scope.asset_length; jj++) {
                      console.log("comparing "+res[0].asset_class+" with "+$scope.asset_class[jj]);
                      if (res[0].asset_class == $scope.asset_class[jj]) {
                        console.log("match at jj="+jj);
                        mm = jj;
                      }
                    }

                    console.log("ZZZ mm="+mm);
                    var aum = 0;
                    if ($rootScope.current_client)
                      aum = $rootScope.current_client.invest_amount;
                    console.log("aum = "+aum);
                    //console.log($scope.product_allocations);


                    var alen = res.length;
                    var arrlen = $scope.product_allocations[mm].length;
                    for (var j=0; j < alen; j++) {
                      var fundid = res[j].fund_id;
                      if (!res[j].fund_allocation && res[j].holding_value > 0) 
                      {
                          console.log("found null");
                          res[j].fund_allocation = roundOne(res[j].holding_value/(aum*10000.0));
                      }
                      if (res[j].fund_allocation > 0) {
                        for (var k=0; k < arrlen; k++) {
                          if ($scope.product_allocations[mm][k].fund_id == fundid) {
                            console.log(res[j].fund_allocation);

                            $scope.product_allocations[mm][k].portfolio_weight = res[j].fund_allocation;
                            $scope.product_allocations[mm][k].old_weight = res[j].fund_allocation;
                            $scope.asset_total[mm] += res[j].fund_allocation;

                            //$scope.product_allocations[mm][k].selected = false;
                            //if (mm == 0) {
                            //  $scope.checkOrUnchecked($scope.product_allocations[mm][k]);
                            //  $scope.product_allocations[mm][k].selected = true;
                            //}
                          }
                        }
                      }
                    }
                  } // res.length > 0

                  $scope.product_tables[kk] = {title: $scope.asset_titles[kk], header: $scope.header1, 
                    body:$scope.product_allocations[kk]};


                  if ($scope.product_tables.length > 1)
                    $rootScope.next = true;
                  $scope.productGauge();

                }); // ProductAllocationPromise

              }); // ProductPromise

            } // if asset weight > 0.005
            console.log("after getting products");

          } // for

        var label = document.getElementById('allocationLabel');
        if (label) label.textContent = $rootScope.labels["L0067"];
        label = document.getElementById('excessReturnsLabel');
        if (label) label.textContent = $rootScope.labels["L0114"];
        label = document.getElementById('risksLabel');
        if (label) label.textContent = $rootScope.labels["L0115"];
        label = document.getElementById('riskAdjustedReturnsLabel');
        if (label) label.textContent = $rootScope.labels["L0116"];
        label = document.getElementById('save_products');
        if (label) label.textContent = $rootScope.labels["L0180"];

        label = document.getElementById('fundNameLabel');
        if (label) label.textContent = $rootScope.labels["L0117"];
        label = document.getElementById('bbgidLabel');
        if (label) label.textContent = $rootScope.labels["L0055"];
        label = document.getElementById('currencyLabel');
        if (label) label.textContent = $rootScope.labels["L0053"];
        label = document.getElementById('issuerLabel');
        if (label) {
          console.log("found issuerLabel");
          label.textContent = $rootScope.labels["L0118"];
        } else {
          console.log("no issuerLabel");
        }
        label = document.getElementById('maturityLabel');
        if (label)
          label.textContent = $rootScope.labels["L0119"];
        label = document.getElementById('couponLabel');
        if (label)
          label.textContent = $rootScope.labels["L0120"];
        label = document.getElementById('yieldLabel');
        if (label)
          label.textContent = $rootScope.labels["L0121"];
        label = document.getElementById('priceLabel');
        if (label)
          label.textContent = $rootScope.labels["L0122"];
        label = document.getElementById('ratingLabel');
        if (label)
          label.textContent = $rootScope.labels["L0123"];
        label = document.getElementById('navLabel');
        if (label)
          label.textContent = $rootScope.labels["L0126"];
        label = document.getElementById('typeLabel');
        if (label)
          label.textContent = $rootScope.labels["L0127"];
        label = document.getElementById('ret1yLabel');
        if (label)
          label.textContent = $rootScope.labels["L0128"];
        label = document.getElementById('ret3yLabel');
        if (label)
          label.textContent = $rootScope.labels["L0129"];
        label = document.getElementById('ret5yLabel');
        if (label)
          label.textContent = $rootScope.labels["L0130"];
        label = document.getElementById('sharpe1yLabel');
        if (label)
          label.textContent = $rootScope.labels["L0131"];
        label = document.getElementById('sharpe3yLabel');
        if (label)
          label.textContent = $rootScope.labels["L0132"];
        label = document.getElementById('sharpe5yLabel');
        if (label)
          label.textContent = $rootScope.labels["L0133"];
        label = document.getElementById('volatilityLabel');
        if (label)
          label.textContent = $rootScope.labels["L0135"];
        label = document.getElementById('sortinoLabel');
        if (label)
          label.textContent = $rootScope.labels["L0134"];
        label = document.getElementById('maxDWLabel');
        if (label)
          label.textContent = $rootScope.labels["L0136"];
        label = document.getElementById('feeLabel');
        if (label)
          label.textContent = $rootScope.labels["L0137"];
        label = document.getElementById('selectionLabel');
        if (label) label.textContent = $rootScope.labels["L0124"];
        label = document.getElementById('portfolioLabel');
        if (label) label.textContent = $rootScope.labels["L0125"];

        }); // SavedAllocationPromise
  }


    $scope.successMsg = function(){
      $('.success_msg').css("display","block");
    }
    
    $scope.productValue  = function () {

      if (value.portfolio_weight  > 0) {
        console.log('working');
        $scope.value++;
      }
      $("#products-number").val($scope.value);
    }

    $scope.saveProductAllocations = function() {
          console.log("saving product allocations "+$rootScope.client_id);
          console.log($scope.asset_length);
          console.log($scope.asset_total);

          //console.log("saving first table");
          var clientid = $rootScope.client_id;

          /***** CASH BONDS ****/
          if ($scope.gotCashBonds())
          {
            // Save any cash bonds selections
            var cb = $scope.product_allocations[0];
            var cba = 0; // asset class

            // Delete any previously existing cash bond allocation
            data_query = {id: -1,
                    client_id: clientid,
                    asset_class: 0,
                    fund_id: -1,
                    fund_allocation: 0,
                    delete_flag: 1 };
            console.log(data_query);
            res = ProductAllocation.updateProductAllocations(data_query);

            for (var i=0; i < cb.length; i++) 
            {
              if (cb[i].portfolio_weight > 0) 
              {
                data_query = {id: cb[i].id,
                    client_id: clientid,
                    asset_class: cb[i].asset_class,
                    fund_id: cb[i].fund_id,
                    fund_allocation: cb[i].portfolio_weight};
                console.log(data_query);
                console.log("CURRENCY="+cb[i].currency);
                res = ProductAllocation.updateProductAllocations(data_query);

                var fundclass = 'Bonds';
                var wt = cb[i].portfolio_weight;
                var aum = 0;
                if ($rootScope.current_client)
                  aum = $rootScope.current_client.invest_amount;
                var targetval = aum * wt * 10000.0;
                console.log("aum="+aum+ " wt="+wt+" targetval="+targetval);
                if (cb[i].currency != "USD") 
                {
                  targetval = convertCurrency2(cb[i].currency,targetval,$scope.exchange_rates);
                }

                //console.log("CURRENT CLIENT");
                //console.log($rootScope.current_client);
                data_query = {'advisor_id': $rootScope.current_advisor,
                         'client_id': clientid,
                         'fund_symbol': cb[i].fund_name,
                         'fund_name': cb[i].fund_name,
                         'fund_type': 'Bond',
                         'fund_class': fundclass,
                         'bbgid': cb[i].bbgid,
                         'num_units': 0,
                         'unit_price': cb[i].unit_price,
                         'holding_value': 0,
                         'target_value': targetval,
                         'currency': cb[i].currency,
                         'fund_id': 0,
                         'asset_class': 0,
                         'fund_allocation': cb[i].portfolio_weight,
                         'bond_id': cb[i].bond_id
                      };
                console.log(data_query);
                res = ClientHoldings.updateHolding(0, data_query);
              }
            }
          }

          /****** FUNDS *****/
          var startidx = 1;
          if ($scope.gotCashBonds())
            startidx = 1;
          else
            startidx = 0;
          //gotCashBondsreturn ($scope.asset_class[0]==0);
          //console.log()
          //if ($scope.product_allocations[0][0].asset_class == 0)
          //  startidx = 0;
          console.log("**** STARTIDX "+startidx);

          for (var kkk=startidx; kkk < $scope.asset_length; kkk++) {

            var kk = kkk;
            console.log("kk="+kk);
            //console.log($scope.product_allocations);

            // Remove any existing records for this asset class and client
            var arrlen = $scope.product_allocations[kk].length;
            if (arrlen > 0) 
            {
              data_query = {id: -1,
                    client_id: clientid,
                    asset_class: $scope.product_allocations[kk][0].asset_class,
                    fund_id: -1,
                    fund_allocation: 0,
                    delete_flag: 1};
              console.log(data_query);
              res = ProductAllocation.updateProductAllocations(data_query);
            }

            for (var i=0; i < arrlen; i++) 
            {
              if ($scope.product_allocations[kk][i].portfolio_weight > 0) 
              {
                var fundid = $scope.product_allocations[kk][i].fund_id;
                data_query = {id: $scope.product_allocations[kk][i].id,
                    client_id: clientid,
                    asset_class: $scope.product_allocations[kk][i].asset_class,
                    fund_id: $scope.product_allocations[kk][i].fund_id,
                    fund_allocation: $scope.product_allocations[kk][i].portfolio_weight};
                console.log(data_query);
                console.log("CURRENCY="+$scope.product_allocations[kk][i].currency);
                res = ProductAllocation.updateProductAllocations(data_query);

                var fundclass = 'Equity';
                if ($scope.product_allocations[kk][i].asset_class >= 4000 && $scope.product_allocations[kk][i].asset_class < 5000)
                  fundclass = 'Fixed Income';
                var wt = $scope.product_allocations[kk][i].portfolio_weight;
                var aum = 0;
                if ($rootScope.current_client)
                  aum = $rootScope.current_client.invest_amount;
                var targetval = aum * wt * 10000.0;
                if ($scope.product_allocations[kk][i].currency != "USD") {
                  targetval = convertCurrency2($scope.product_allocations[kk][i].currency,targetval,$scope.exchange_rates);
                }
                
                console.log("FUNDID "+fundid);
                console.log("BBGID "+ $scope.product_allocations[kk][i].bbgid);
                //console.log($rootScope.current_client);
                data_query = {'advisor_id': $rootScope.current_advisor,
                         'client_id': clientid,
                         'fund_symbol': $scope.product_allocations[kk][i].ticker_name,
                         'fund_name': $scope.product_allocations[kk][i].fund_name,
                         'bbgid': $scope.product_allocations[kk][i].bbgid,
                         'fund_type': 'Fund',
                         'fund_class': fundclass,
                         'num_units': 0,
                         'unit_price': $scope.product_allocations[kk][i].unit_price,
                         'holding_value': 0,
                         'target_value': targetval,
                         'currency': $scope.product_allocations[kk][i].currency,
                         'asset_class': $scope.product_allocations[kk][i].asset_class,
                         'fund_allocation': $scope.product_allocations[k][i].portfolio_weight,
                         'fund_id': fundid
                      };
                res = ClientHoldings.updateHolding(0, data_query);
              }
            }

          }

          //data_query = $scope.product_allocations;
          //console.log(data_query);
          //res = Product.updateProducts(data_query);

          console.log("at end");

          $scope.successMsg();

    }


    $scope.updateProduct = function(item) {
      console.log("in updateProduct "+item.portfolio_weight+" "+item.asset_class);
      console.log(item);
      updvalue = item.portfolio_weight - item.old_weight;
      console.log("updvalue ="+updvalue);
      //console.log($scope.products_number);
      item.old_weight = item.portfolio_weight;

      for (var i=0; i < $scope.asset_length; i++) {
        if ($scope.asset_class[i] == item.asset_class) {
          console.log("updating "+i+" "+$scope.asset_total[i]+" "+updvalue);
          $scope.asset_total[i] += updvalue;
        }
      }
      console.log("after "+$scope.asset_total);
      //$scope.productStackChart();
      $scope.productGauge();

      // Update the database
        var clientid = $rootScope.client_id;
        var aum = 0;
        if ($rootScope.current_client)
          aum = $rootScope.current_client.invest_amount;

        var classweight = 0;
        for (var k=0; k<$scope.asset_length; k++) {
          if ($scope.asset_class[k] == item.asset_class)
            classweight = $scope.asset_weight[k]/100.0;
        }
        console.log('classweight='+classweight);
        console.log('aum='+aum);

        var fundclass = 'Equity';
        if (item.asset_class == 0) fundclass = 'Bonds';
        if (item.asset_class >= 4000 && item.asset_class < 5000) fundclass = 'Fixed Income';
        var fundtype = 'Fund';
        var fundid = item.fund_id;
        if (item.asset_class == 0) { fundtype = 'Bond'; fundid = 0; }

        //var targetval = aum * item.portfolio_weight * 10000.0;
        var targetval = aum * classweight * item.portfolio_weight * 10000.0;
        console.log("before targetval = "+targetval);
        if (item.currency != "USD") 
        {
            targetval = convertCurrency2(item.currency, targetval, $scope.exchange_rates);
        }
        console.log("after targetval = "+targetval);

        data_query = {id: item.id,
            client_id: clientid,
            asset_class: item.asset_class,
            fund_id: item.fund_id,
            fund_allocation: item.portfolio_weight,
            currency: item.currency, 
            aum: aum,
            targetval: targetval
          };
        console.log(data_query);
        res = ProductAllocation.updateProductAllocations(data_query);
  
        //console.log("CURRENT CLIENT");
        data_query = {'advisor_id': $rootScope.current_advisor,
                         'client_id': clientid,
                         'fund_symbol': item.fund_name,
                         'fund_name': item.fund_name,
                         'fund_type': fundtype,
                         'fund_class': fundclass,
                         'bbgid': item.bbgid,
                         'num_units': 0,
                         'unit_price': item.unit_price,
                         'holding_value': 0,
                         'target_value': targetval,
                         'currency': item.currency,
                         'fund_id': fundid,
                         'asset_class': item.asset_class,
                         'fund_allocation': item.portfolio_weight,
                         'bond_id': item.bond_id
                      };
        console.log(data_query);
        res = ClientHoldings.updateHolding(0, data_query);
    }


   $scope.productSelection = function(){
    Highcharts.setOptions({
           //colors: ['#3E6697', '#EB4932']
           colors: ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267']
        });

    $('#product_selection').highcharts({

        chart: {
            polar: true,
            type: 'line',
            marginLeft:20,
            x:10
        },
        exporting: { enabled:false },
        title: {
            text: '',
            x:0
        },

        pane: {
            size: '60%'
        },

        xAxis: {
            categories: ['NAV','Return 1Y','Sharpe 1Y','Sortino','Volatility'],
            tickmarkPlacement: 'on',
            lineWidth: 0,
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            y: 0,
            layout: 'vertical',
            marginTop:10,
        },
        series: $scope.spiderSeries
        //series: [{
        //    name: 'UNALLOCATED',
        //    data: [43000, 19000, 60000, 35000],
        //    pointPlacement: 'on'
        //}, {
        //    name: 'ALLOCATED',
        //    data: [50000, 39000, 42000, 31000],
        //    pointPlacement: 'on'
        //}]
         // series: [{
         //        name: 'Unallocated',
         //        //data: [5, 3, 4, 7, 2],
         //        data: series_unallocated,
         //        color: '#347DCF'
         //    }, {
         //        name: 'Allocated',
         //        //data: [1, 2, 3, 4, 5],
         //        data: series_allocated,
         //        color: '#EB4932'
         //    }]

      });
    }
    $scope.productSelection();

  $scope.productGauge = function () {

    // Uncomment to style it like Apple Watch
    /*
    if (!Highcharts.theme) {
        Highcharts.setOptions({
            chart: {
                backgroundColor: 'black'
            },
            colors: ['#F62366', '#9DFF02', '#0CCDD6'],
            title: {
                style: {
                    color: 'silver'
                }
            },
            tooltip: {
                style: {
                    color: 'silver'
                }
            }
        });
    }
    // */
    //console.log("** In Gauge");
    //console.log($scope.asset_description);
    //console.log($scope.asset_weight);
    //console.log($scope.asset_length);
    //console.log($scope.asset_total);
    //$scope.asset_total[0] = $scope.asset_weight[0];

    Highcharts.setOptions({
        colors: ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267']
        //colors: ['#347DCF', '#EB4932','#D00049','#8085E9','#64D0AE']
    });


    // The y values should be set to the proportion allocated

    numlines =$scope.asset_length;
    width = 72/numlines;
    //console.log("Width is "+width);
    start = 38;
    y_start = 28;
    background_inner = [];
    background_outer = [];
    foreground_center = [];
    y_vals = [];
    name_vals = [];
    for (i=0; i<numlines; i++) {
      background_inner.push(start.toString()+"%");
      background_outer.push((start+width).toString()+"%");
      foreground_center.push((start+width/2).toString()+"%");

      y_vals.push($scope.asset_total[i]);
      //$scope.asset_weight[i] = Math.round($scope.asset_weight[i]*100.0)/100.0;
      //if ($scope.asset_weight[i] > 0.005)
      //  y_vals.push(Math.round(($scope.asset_total[i]*100.0/$scope.asset_weight[i])*100.0)/100.0);
      //else
      //  y_vals.push(0);

      //y_vals.push(y_start);
      start = start + width + 1;
      //y_start = y_start + width;

      name_vals.push($scope.asset_description[i]);
    }
    //y_vals[0] = 100;
    //console.log(background_inner);
    //console.log(background_outer);
    //console.log(foreground_center);
    //console.log(y_vals);
    //console.log(name_vals);
    pane_background = [];
    series_array = [];
    for (i=0; i<numlines; i++) {
      new1 = { outerRadius: background_outer[i],
               innerRadius: background_inner[i],
               backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[i]).setOpacity(0.3).get(),
               borderWidth: 0 };
      pane_background.push(new1);

      new2 = { //showInLegend: true,
               name: $scope.asset_description[i],
               borderColor: Highcharts.getOptions().colors[i],
               data: [{ color: Highcharts.getOptions().colors[i],
                        radius: foreground_center[i],
                        total: $scope.asset_weight[i],
                        allocated: $scope.asset_total[i],
                        innerRadius: foreground_center[i],
                        y: y_vals[i] }] };
      series_array.push(new2);
    }
    //console.log(pane_background);
    //console.log(series_array);

 //           series: [{
 //           name: $scope.asset_description[0],
 //           borderColor: Highcharts.getOptions().colors[0],
 //           data: [{
 //               color: Highcharts.getOptions().colors[0],
//                radius: '100%',
 //               innerRadius: '100%',
 //               y: 80
 //           }]
 //       }, {
 //           name: $scope.asset_description[1],
 //           borderColor: Highcharts.getOptions().colors[1],
 //           data: [{
 //               color: Highcharts.getOptions().colors[1],
//                radius: '75%',
//                innerRadius: '75%',
//                y: 65
//            }]
//        }, {
//            name: 'Cash',
//            borderColor: Highcharts.getOptions().colors[2],
//            data: [{
//                color: Highcharts.getOptions().colors[2],
//                radius: '50%',
//                innerRadius: '50%',
//                y: 50
//            }]


//              background: [{ // Track for Move
//                outerRadius: '112%',
//                innerRadius: '88%',
//                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
//                borderWidth: 0
//            }, { // Track for Exercise
//                outerRadius: '87%',
//                innerRadius: '63%',
//                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.3).get(),
//                borderWidth: 0
//            }, { // Track for Stand
//                outerRadius: '62%',
//                innerRadius: '38%',
//                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2]).setOpacity(0.3).get(),
//                borderWidth: 0
//            }]

//    radius_series = ['100%', '75%', '50%', '25%'];
//    inner_radius_series = ['100%', '75%', '50%', '25%'];

    Highcharts.chart('product_gauge', {

        chart: {
            type: 'solidgauge',
            marginTop:0
        },
        exporting:{
          enabled:false,
        },
        title: {
            text: 'ASSET',
            align: 'center',
            verticalAlign: 'middle',
            y:0,
            style: {
                fontSize: '24px'
            }
        },

        tooltip: {
            borderWidth: 0,
            backgroundColor: 'none',
            shadow: false,
            verticalAlign: 'middle',
            style: {
                fontSize: '16px'
            },
            //pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
            //pointFormat: '{series.name}<br><span style="font-size:1.5em; color: {point.color}; font-weight: bold">{point.allocated} / {point.total} %</span>',
            pointFormat: '{series.name}<br><span style="font-size:1.5em; color: {point.color}; font-weight: bold">{point.allocated} %</span>',
            positioner: function (labelWidth, labelHeight) {
                return {
                    x: 150 - labelWidth / 2, //200 - labelWidth / 2,
                    y: 320 //180
                };
            }
        },

        pane: {
            startAngle: 0,
            endAngle: 360,
            background: pane_background
//            background: [{ // Track for Move
//                outerRadius: '112%',
//                innerRadius: '88%',
//                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.3).get(),
//                borderWidth: 0
//            }, { // Track for Exercise
//                outerRadius: '87%',
//                innerRadius: '63%',
//                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.3).get(),
//                borderWidth: 0
//            }, { // Track for Stand
//                outerRadius: '62%',
//                innerRadius: '38%',
//                backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[2]).setOpacity(0.3).get(),
//                borderWidth: 0
//            }]
        },

        yAxis: {
            min: 0,
            max: 100,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
//                borderWidth: '34px',
                borderWidth: width.toString()+"px",
                //borderWidth: '18px',
                dataLabels: {
                    enabled: false
                },
                linecap: 'round',
                stickyTracking: false
            }
        },

        series: series_array
        //[{
        //    name: $scope.asset_description[0],
        //    borderColor: Highcharts.getOptions().colors[0],
        //    data: [{
        //        color: Highcharts.getOptions().colors[0],
        //        radius: '100%',
        //        innerRadius: '100%',
        //        y: 80
        //    }]
        //}, {
        //    name: $scope.asset_description[1],
        //    borderColor: Highcharts.getOptions().colors[1],
        //    data: [{
        //        color: Highcharts.getOptions().colors[1],
        //        radius: '75%',
        //        innerRadius: '75%',
        //        y: 65
        //    }]
        //}, {
        //    name: 'Cash',
        //    borderColor: Highcharts.getOptions().colors[2],
        //    data: [{
        //        color: Highcharts.getOptions().colors[2],
        //        radius: '50%',
        //        innerRadius: '50%',
        //        y: 50
        //   }]
        //}]
    },

    /**
     * In the chart load callback, add icons on top of the circular shapes
     */
    function callback() {

        // Move icon
   //     this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
   //         .attr({
   //             'stroke': '#303030',
   //             'stroke-linecap': 'round',
   //             'stroke-linejoin': 'round',
   //             'stroke-width': 2,
   //             'zIndex': 10
   //         })
   //         .translate(185, 79)
   //         .add(this.series[2].group);

        // Exercise icon
  //      this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8, 'M', 8, -8, 'L', 16, 0, 8, 8])
  //          .attr({
  //              'stroke': '#303030',
 //               'stroke-linecap': 'round',
 //               'stroke-linejoin': 'round',
  //              'stroke-width': 2,
  //              'zIndex': 10
  //          })
   //         .translate(185, 115)
   //         .add(this.series[2].group);

   //     // Stand icon
    //    this.renderer.path(['M', 0, 8, 'L', 0, -8, 'M', -8, 0, 'L', 0, -8, 8, 0])
   //         .attr({
   //             'stroke': '#303030',
   //             'stroke-linecap': 'round',
  //              'stroke-linejoin': 'round',
  //              'stroke-width': 2,
  //              'zIndex': 10
  //          })
  //          .translate(185, 155)
  //          .add(this.series[2].group);
    })
  }

  //$scope.productGauge();

  $scope.excessReturns= function(){
    Highcharts.setOptions({
          colors: ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267']
          //colors: ['#347DCF', '#EB4932','#D00049','#8085E9','#64D0AE']
        });
    console.log("in spider excessReturns");
    console.log($rootScope.spiderSeries1);
    var dat = $rootScope.spiderSeries1;
    //var dat = [{
    //       name: 'TRBCX US',
    //       data: [43000, 19000, 60000],
    //       pointPlacement: 'on'
    //    }, {
    //       name: 'HEFT LN',
    //       data: [50000, 39000, 42000],
    //       pointPlacement: 'on'
    //    },{
    //       name: 'SCAMSMA HK',
    //       data: [58000, 59000, 22000],
    //       pointPlacement: 'on'
    //    }];

  $('#excessReturns').highcharts({

        chart: {
            polar: true,
            type: 'line',
            marginLeft:10,
        },
        exporting:{
          enabled:false,
        },
        title: {
            text: null,
            x: -80
        },

        pane: {
            size: '90%'
        },

        xAxis: {
            categories: ['Ex Ret 1y', 'Ex Ret 5y', 'Ex Ret 5y'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'center',
            verticalAlign: 'top',
            y: 0,
            layout: 'horizontal'
        },
        series: dat
        //series: [{
        //   name: 'TRBCX US',
        //   data: [43000, 19000, 60000],
        //   pointPlacement: 'on'
        //}, {
        //   name: 'HEFT LN',
        //   data: [50000, 39000, 42000],
        //   pointPlacement: 'on'
        //},{
        //   name: 'SCAMSMA HK',
        //   data: [58000, 59000, 22000],
        //   pointPlacement: 'on'
        //}]

    });

    }

    $scope.excessReturns();

    $scope.risks = function(){
      Highcharts.setOptions({
          colors: ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267']
          //colors: ['#347DCF', '#EB4932','#D00049','#8085E9','#64D0AE']
        });
      console.log("in spider risks");
      console.log($rootScope.spiderSeries2);
      var dat = $rootScope.spiderSeries2;
      //var dat = [{
      //     name: 'TRBCX US',
      //     data: [19000, 60000, 35000, 17000, 10000],
      //     pointPlacement: 'on'
      //  }, {
      //     name: 'HEFT LN',
      //     data: [39000, 42000, 31000, 26000, 14000],
      //     pointPlacement: 'on'
      //  },{
      //     name: 'SCAMSMA HK',
      //     data: [59000, 22000, 26000, 18000, 18000],
      //     pointPlacement: 'on'
      //  }];

      $('#risks').highcharts({

        chart: {
            polar: true,
            type: 'line',
            marginLeft:10,
        },
        exporting:{
          enabled:false,
        },
        title: {
            text: null,
            x: -80
        },

        pane: {
            size: '85%'
        },

        xAxis: {
            categories: ['Vol 1y', 'Vol 3y', 'Vol 5y','DW %', 'Down Risk'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'center',
            verticalAlign: 'top',
            y: 0,
            layout: 'horizontal'
        },
        series: dat
        //series: [{
        //   name: 'TRBCX US',
        //   data: [43000, 19000, 60000, 35000, 17000, 10000],
        //   pointPlacement: 'on'
        //}, {
        //   name: 'HEFT LN',
        //   data: [50000, 39000, 42000, 31000, 26000, 14000],
        //   pointPlacement: 'on'
        //},{
        //   name: 'SCAMSMA HK',
        //   data: [58000, 59000, 22000, 26000, 18000, 18000],
        //   pointPlacement: 'on'
        //}]

    });

    }

    $scope.risks();

    $scope.adjustedReturns = function(){
      console.log("in spider adjustedReturns");
      console.log($rootScope.spiderSeries3);
      Highcharts.setOptions({
          colors: ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267']
          //colors: ['#347DCF', '#EB4932','#D00049','#8085E9','#64D0AE']
        });
      var dat = $rootScope.spiderSeries3;
      //var dat = [{
      //     name: 'TRBCX US',
      //     data: [43000, 19000, 60000],
      //     pointPlacement: 'on'
      //  }, {
      //     name: 'HEFT LN',
      //     data: [50000, 39000, 42000],
      //     pointPlacement: 'on'
      //  },{
      //     name: 'SCAMSMA HK',
      //     data: [58000, 59000, 22000],
      //     pointPlacement: 'on'
      //  }];
      $('#adjustedReturns').highcharts({

        chart: {
            polar: true,
            type: 'line',
            marginLeft:10,
        },
        exporting:{
          enabled:false,
        },

        title: {
            text: null,
            x: -80
        },

        pane: {
            size: '90%'
        },

        xAxis: {
            categories: ['Sharpe 1y', 'Sharpe 3y', 'Sharpe 5y', 'Sortino'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'center',
            verticalAlign: 'top',
            y: 0,
            layout: 'horizontal'
        },
        series: dat
        //series: [{
        //   name: 'TRBCX US',
        //   data: [43000, 19000, 60000],
        //   pointPlacement: 'on'
        //}, {
        //   name: 'HEFT LN',
        //   data: [50000, 39000, 42000],
        //   pointPlacement: 'on'
        //},{
        //   name: 'SCAMSMA HK',
        //   data: [58000, 59000, 22000],
        //   pointPlacement: 'on'
        //}]

    });

    }

    $scope.adjustedReturns();

})


finvese.controller('advisorDetailsController', function($rootScope, $scope, Advisors, Translations) {
  console.log("in advisorDetailsController");
  $rootScope.overview = true;
  $rootScope.product = false;
  $rootScope.investments = false;
  $rootScope.savings = false;
  $rootScope.home = false;
  $rootScope.login = false;
  $rootScope.nodashboard = false;
  $rootScope.clients_list = false;
  $rootScope.register = false;
  $scope.advisor = [];

  $scope.language_values=[{'name': 'English', 'value' :'English'},
                 {'name': 'Chinese', 'value' :'Chinese'}
                ];
  $scope.language = 'English';
  document.title = "Profile";
  $scope.changeLanguage = false;
  $scope.language_pre = false;

  //$rootScope.default_language = "en";

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    $scope.language_values=[{'name': 'English', 'value' :'English'},
                 {'name': 'Chinese', 'value' :'Chinese'}
                ];
    $scope.language = 'English';
    $scope.changeLanguage = true;
    $scope.language_pre = false;

    console.log("LL ");
    console.log($scope.language_values);

    if (result) {
      $rootScope.default_language = result.default_language;
      $scope.advisor.advisorName = result.advisor_name;
      $scope.advisor.advisorFirm = result.advisor_firm;
      $scope.advisor.emailAddress = result.email_address;
      $scope.advisor.address1 = result.address1;
      $scope.advisor.address2 = result.address2;
      $scope.advisor.city = result.city;
      $scope.advisor.postCode = result.postcode;
      $scope.advisor.country = result.country;
      $scope.advisor.teleNumber = result.phone;
      $scope.advisor.additionalPhoneNumber = result.additional_phone;
      $scope.advisor.default_language = result.default_language;
      if (result.default_language == "en")
        $scope.advisor.language = "English";
      else
        $scope.advisor.language = "Chinese";
    }


    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }

    console.log("LL ");
    console.log($scope.language_values);
  });

  $scope.setScreenLabels = function()
  {
      setAdvisorMenuLabels($rootScope);

      console.log("Advisor Name: "+$rootScope.labels["L0008"]);
      $scope.advisor_name_label = $rootScope.labels["L0008"];
      console.log($scope.advisor_name_label);
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('advisorNameLabel');
      if (label) label.textContent = $rootScope.labels["L0008"];
      label = document.getElementById('advisorFirmLabel');
      if (label) label.textContent = $rootScope.labels["L0009"];
      label = document.getElementById('advisorInformationLabel');
      if (label) label.textContent = $rootScope.labels["L0007"];
      label = document.getElementById('contactInformationLabel');
      if (label) label.textContent = $rootScope.labels["L0010"];
      label = document.getElementById('emailAddressLabel');
      if (label) label.textContent = $rootScope.labels["L0011"];
      label = document.getElementById('addressLine1Label');
      if (label) label.textContent = $rootScope.labels["L0014"];
      label = document.getElementById('addressLine2Label');
      if (label) label.textContent = $rootScope.labels["L0015"];
      label = document.getElementById('telephoneNumberLabel');
      if (label) label.textContent = $rootScope.labels["L0012"];
      label = document.getElementById('additionalPhoneLabel');
      if (label) label.textContent = $rootScope.labels["L0013"];
      label = document.getElementById('cityLabel');
      if (label) label.textContent = $rootScope.labels["L0016"];
      label = document.getElementById('postCodeLabel');
      if (label) label.textContent = $rootScope.labels["L0017"];
      label = document.getElementById('countryLabel');
      if (label) label.textContent = $rootScope.labels["L0018"];
      label = document.getElementById('submitLabel');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
  }



  $scope.changeLanguage = function(language_selected) {
    console.log("in changeLanguage "+language_selected);
        $scope.advisor.language = language_selected;
        //$scope.changeLanguages = true;
        //if(language_selected != 'English')
        //  $scope.language_pre = true;
        //else
        //  $scope.language_pre = false;
  }

  $scope.changePassword = function() {
    console.log("in changePassword");
    $scope.renderPage('/change_password');
  }

  $scope.saveAdvisorDetails = function(){
    if ($scope.advisor.advisorName == null || $scope.advisor.emailAddress == null ) {
      return false;
    }
    if ($scope.advisor.language == "English")
      $scope.advisor.default_language = 'en';
    else
      $scope.advisor.default_language = 'ch';
    var data_query = { 'id': advisor_id,
                    'advisor_id': advisor_id,
                    'advisor_name': $scope.advisor.advisorName,
                    'advisor_firm': $scope.advisor.advisorFirm,
                    'email_address': $scope.advisor.emailAddress,
                    'address1': $scope.advisor.address1,
                    'address2': $scope.advisor.address2,
                    'city': $scope.advisor.city,
                    'postcode': $scope.advisor.postCode,
                    'country': $scope.advisor.country,
                    'phone': $scope.advisor.teleNumber,
                    'additional_phone': $scope.advisor.additionalPhoneNumber,
                    'default_language': $scope.advisor.default_language };
    var res = Advisors.updateAdvisor(advisor_id, data_query)
    $scope.renderPage('/overview');
  }

})


finvese.controller('cashflowSettingsController', function($rootScope, $scope,  $location, Advisors, Translations, CashflowDefaults) {
  console.log("in cashflowSettingsController");
  $rootScope.overview = true;
  $rootScope.product = false;
  $rootScope.investments = false;
  $rootScope.savings = false;
  $rootScope.home = false;
  $rootScope.login = false;
  $rootScope.nodashboard = false;
  $rootScope.clients_list = false;
  $rootScope.register = false;
  $scope.advisor = [];

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
    //console.log("client "+$rootScope.client_id);
  }

  setAdvisor($rootScope);

  console.log("in cashflowSettingsController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + " - Cashflow Settings";
      }
    })
  //$rootScope.default_language = "en";

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }
    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

    
  console.log("client_id "+$rootScope.client_id);

  var CashflowDefaultsPromise = CashflowDefaults.getOne($rootScope.client_id);
  CashflowDefaultsPromise.then(function(result) {
    console.log("in cashflow defaults promise");
    console.log(result);
    //console.log(result[0]);
    if (result)
    {
      $scope.advisor.ordinaryTax1 = result.ordinary_tax1;
      $scope.advisor.ordinaryTax2 = result.ordinary_tax2;
      $scope.advisor.ordinaryTax3 = result.ordinary_tax3;
      $scope.advisor.capitalTax1 = result.capital_tax1;
      $scope.advisor.capitalTax2 = result.capital_tax2;
      $scope.advisor.capitalTax3 = result.capital_tax3;
      $scope.advisor.taxBracket1 = result.taxbracket1;
      $scope.advisor.taxBracket2 = result.taxbracket2;
      $scope.advisor.taxBracket3 = result.taxbracket3;
      $scope.advisor.defaultYield = result.default_yield;
      $scope.advisor.defaultReturn = result.investment_return;
      $scope.advisor.inflationExpectation = result.inflation_expectation;
      $scope.advisor.avgMortalityAge = result.avg_mortality_age;
      $scope.advisor.feeaum1 = result.feeaum1;
      $scope.advisor.feeaum2 = result.feeaum2;
      $scope.advisor.feeaum3 = result.feeaum3;
      $scope.advisor.feepercent1 = result.feepercent1;
      $scope.advisor.feepercent2 = result.feepercent2;
      $scope.advisor.feepercent3 = result.feepercent3;
      $scope.advisor.feefixed = result.feefixed;

      $scope.advisor.client_birthyear = result.client_birthyear;
      $scope.advisor.client_retirement = result.client_retirement;
      $scope.advisor.client_income = result.client_income;
      $scope.advisor.client_income_growth = result.client_income_growth;
      $scope.advisor.spouse_birthyear = result.spouse_birthyear;
      $scope.advisor.spouse_retirement = result.spouse_retirement;
      $scope.advisor.spouse_income = result.spouse_income;
      $scope.advisor.spouse_income_growth = result.spouse_income_growth;
    }

  }); // CashflowDefaultsPromise


  $scope.setScreenLabels = function()
  {
      //setAdvisorMenuLabels($rootScope);
      setMenuLabels($rootScope);

      //console.log("Advisor Name: "+$rootScope.labels["L0008"]);
      //$scope.advisor_name_label = $rootScope.labels["L0008"];
      //console.log($scope.advisor_name_label);
      
      //var cb = document.getElementById('advisorName');
      var label = document.getElementById('avgMortalityAgeLabel');
      if (label) label.textContent = "Avg Mortality Age"; // $rootScope.labels["L0008"];
      label = document.getElementById('inflationExpectationLabel');
      if (label) label.textContent = "Expected Rate of Inflation (%)"; //$rootScope.labels["L0009"];
      label = document.getElementById('defaultYieldLabel');
      if (label) label.textContent = "Default Yield (%)"; //$rootScope.labels["L0007"];
      label = document.getElementById('defaultReturnLabel');
      if (label) label.textContent = "Default Investment Return (%)"; //$rootScope.labels["L0010"];

      label = document.getElementById('taxRateInformationLabel');
      if (label) label.textContent = "Tax Rate Information"; //$rootScope.labels["L0011"];
      label = document.getElementById('advisorFeeInformationLabel');
      if (label) label.textContent = "Advisor Fee Information"; //$rootScope.labels["L0014"];
      label = document.getElementById('taxBracket1Label');
      if (label) label.textContent = "Tax Bracket 1 ($)"; //$rootScope.labels["L0015"];
      label = document.getElementById('taxBracket2Label');
      if (label) label.textContent = "Tax Bracket 2 ($)"; //$rootScope.labels["L0012"];
      label = document.getElementById('taxBracket3Label');
      if (label) label.textContent = "Tax Bracket 3 ($)"; //$rootScope.labels["L0013"];
      label = document.getElementById('ordinaryTax1Label');
      if (label) label.textContent = "Ordinary Tax Rate 1 (%)"; //$rootScope.labels["L0016"];
      label = document.getElementById('ordinaryTax2Label');
      if (label) label.textContent = "Ordinary Tax Rate 2 (%)"; //$rootScope.labels["L0017"];
      label = document.getElementById('ordinaryTax3Label');
      if (label) label.textContent = "Ordinary Tax Rate 3 (%)"; //$rootScope.labels["L0018"];
      label = document.getElementById('capitalTax1Label');
      if (label) label.textContent = "Capital Gains Tax Rate 1 (%)"; //$rootScope.labels["L0016"];
      label = document.getElementById('capitalTax2Label');
      if (label) label.textContent = "Capital Gains Tax Rate 2 (%)"; //$rootScope.labels["L0017"];
      label = document.getElementById('capitalTax3Label');
      if (label) label.textContent = "Capital Gains Tax Rate 3 (%)"; //$rootScope.labels["L0018"];
      label = document.getElementById('feeaum1Label');
      if (label) label.textContent = "Advisor Fee AUM 1 ($)"; //$rootScope.labels["L0015"];
      label = document.getElementById('feeaum2Label');
      if (label) label.textContent = "Advisor Fee AUM 2 ($)"; //$rootScope.labels["L0015"];
      label = document.getElementById('feeaum3Label');
      if (label) label.textContent = "Advisor Fee AUM 3 ($)"; //$rootScope.labels["L0015"];
      label = document.getElementById('feepercent1Label');
      if (label) label.textContent = "Advisor Fee Percentage 1 (%)"; //$rootScope.labels["L0015"];
      label = document.getElementById('feepercent2Label');
      if (label) label.textContent = "Advisor Fee Percentage 2 (%)"; //$rootScope.labels["L0015"];
      label = document.getElementById('feepercent3Label');
      if (label) label.textContent = "Advisor Fee Percentage 3 (%)"; //$rootScope.labels["L0015"];
      label = document.getElementById('feeFixedLabel');
      if (label) label.textContent = "Fixed Advisor Fee ($)"; //$rootScope.labels["L0015"];
      label = document.getElementById('submitLabel');
      if (label) label.textContent = $rootScope.labels["L0020"];
      label = document.getElementById('cancelLabel');
      if (label) label.textContent = $rootScope.labels["L0019"];
  }

  $scope.changeLanguages = false;
  $scope.language_pre = false;
  $scope.language=[{'name': 'English', 'value' :'English'},
                 {'name': 'Chinese', 'value' :'Chinese'}
                ];

  $scope.saveCashflowDetails = function(){
    //if ($scope.advisor.advisorName == null || $scope.advisor.emailAddress == null ) {
    //  return false;
    //}
    var data_query = { 
                    'client_id': $rootScope.client_id,
                    'avg_mortality_age': $scope.advisor.avgMortalityAge,
                    'inflation_expectation': $scope.advisor.inflationExpectation,
                    'default_yield': $scope.advisor.defaultYield,
                    'investment_return': $scope.advisor.defaultReturn,
                    'ordinary_tax1': $scope.advisor.ordinaryTax1,
                    'ordinary_tax2': $scope.advisor.ordinaryTax2,
                    'ordinary_tax3': $scope.advisor.ordinaryTax3,
                    'capital_tax1': $scope.advisor.capitalTax1,
                    'capital_tax2': $scope.advisor.capitalTax2,
                    'capital_tax3': $scope.advisor.capitalTax3,
                    'taxbracket1': $scope.advisor.taxBracket1,
                    'taxbracket2': $scope.advisor.taxBracket2,
                    'taxbracket3': $scope.advisor.taxBracket3,
                    'feeaum1': $scope.advisor.feeaum1,
                    'feeaum2': $scope.advisor.feeaum2,
                    'feeaum3': $scope.advisor.feeaum3,
                    'feepercent1': $scope.advisor.feepercent1,
                    'feepercent2': $scope.advisor.feepercent2,
                    'feepercent3': $scope.advisor.feepercent3,
                    'feefixed': $scope.advisor.feefixed,
                    'client_birthyear': $scope.advisor.client_birthyear,
                    'client_retirement': $scope.advisor.client_retirement,
                    'client_income': $scope.advisor.client_income,
                    'client_income_growth': $scope.advisor.client_income_growth,
                    'spouse_birthyear': $scope.advisor.spouse_birthyear,
                    'spouse_retirement': $scope.advisor.spouse_retirement,
                    'spouse_income': $scope.advisor.spouse_income,
                    'spouse_income_growth': $scope.advisor.spouse_income_growth
                     };
     var res = CashflowDefaults.updateValues($rootScope.client_id, data_query)
     //$scope.renderPage('/overview');
  }

})



finvese.controller('cashflowReportController', function($rootScope, $scope, $location, Product, Cashflows, ClientTrades, CashHoldings, Client, ExchangeRate, Advisors, Translations, CashflowDefaults, SavingsPlan, Investment, Expense, Income, Client) {
  $rootScope.product = false;
  $rootScope.investments = false;
  $rootScope.login = false;
  $rootScope.savings = false;
  $rootScope.home = false;
  $rootScope.nodashboard = false;
  $rootScope.clients_list = false;
  $rootScope.trades = true;
  $rootScope.register = false;

  var s = $location.$$path;
  var pos = s.indexOf("/",2);
  //console.log("pos="+pos);
  if (pos >= 0) {
    var cid = s.slice(pos+1);
    //console.log(s.slice(pos+1));
    //$rootScope.current_client = cid.toString();
    $rootScope.client_id = parseInt(cid);
    $scope.clientId = $rootScope.client_id;
  }

  setAdvisor($rootScope);
  //$rootScope.default_language = "en";
  console.log("in cashflowReportController advisor " + $rootScope.current_advisor + " client "+$scope.clientId);
  var ClientPromise = Client.getOne($rootScope.current_advisor, $rootScope.client_id);
  ClientPromise.then(function(result) {
      console.log("in client promise getOne");
      //console.log(result);
      if (result) {
        $scope.client_name = result.first_name + " " + result.last_name;
        $scope.client_shortName = result.first_name[0] + "." + result.last_name;
        console.log("client " + $scope.client_name + " // " + $scope.client_shortName);
        document.title = $scope.client_shortName + " - Cashflow Reports";
      }
    })

  if ($rootScope.current_client) {
    console.log("current client defined");
    $rootScope.clientCurrencyValue = $rootScope.current_client.invest_amount*1000000;
    //$rootScope.client_id = $rootScope.current_client.clientId;
  } else {
    setCurrentClient($rootScope, $rootScope.client_id, Client);
    //console.log("using default value");
    //$rootScope.clientCurrencyValue = 20000;
  }
  console.log("in cashflowReportController "+$rootScope.client_id);
  $scope.client_name = $rootScope.client_name;


  $scope.cashBonds = [];

  $scope.cashPosition = [];
  //$scope.cashPositionPopup = [{'currency':null, 'amount':null,'newCurrency':null, 'action':'Trade', 'exchangeRate':null,'amtToBuy':null, 'amountold': null, 'amountnew': null}];
  $scope.holdings = [];
  $scope.product_list = [];

  $scope.trade = [];
  $scope.currency= [{'name': 'USD', 'value' :'usd'},
                           {'name': 'SGD', 'value' :'sgd'},
                           {'name': 'EUR', 'value' :'eur'},
                          ];
  $scope.newCurrency= [{'name': 'USD', 'value' :'usd'},
                        {'name': 'SGD', 'value' :'sgd'},
                        {'name': 'EUR', 'value' :'eur'},
                      ];

    
    $scope.cashBond_list = [];
    $scope.exchange_rates = [];

  var advisor_id = $rootScope.current_advisor;
  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise");
    console.log(result);

    if (result) {
      $rootScope.default_language = result.default_language;
    }

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }
    
        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  {
      setMenuLabels($rootScope);

      //var label = document.getElementById('cashPositionLabel');
      //label.textContent = $rootScope.labels["L0140"];


      //$scope.cashPositionHead = ['Currency','Amount','Action'];
      //$scope.cashPositionHead = [ $rootScope.labels["L0053"], //'Currency',
      //                            $rootScope.labels["L0054"], //'Amount',
      //                            $rootScope.labels["L0026"] ]; //'Action'];

      //label = document.getElementById('holdingsLabel');
      //label.textContent = $rootScope.labels["L0069"];
      //label = document.getElementById('tradesLabel');
      //label.textContent = $rootScope.labels["L0138"];
      //label = document.getElementById('holdingsLabel2');
      //label.textContent = $rootScope.labels["L0069"];
    
      //$scope.tradeHead = ['Date','Ticker','Fund', 'BBGID', 'Currency', 'Bought / Sold', 'Units Bought / Sold', 'Unit Price','Outstanding Units'];
      $scope.cashflowHead = [ 'Year', //$rootScope.labels["L0190"], //'Date',
                          'Client Age', //$rootScope.labels["L0145"], //'Ticker',
                          'Spouse Age', //$rootScope.labels["L0117"], //'Fund', 
                          'Beginning Balance', //$rootScope.labels["L0055"], //'BBGID', 
                          'Net Cash Flows', //$rootScope.labels["L0053"], //'Currency', 
                          'Net Investment Return', //$rootScope.labels["L0147"], //'Bought / Sold', 
                          //$rootScope.labels["L0148"], //'Units Bought / Sold', 
                          //$rootScope.labels["L0146"], //'Unit Price',
                          'Ending Balance' ]; //$rootScope.labels["L0149"] ]; //'Outstanding Units'];

      $scope.cashflow = [];
      var clientid = $rootScope.client_id;
      var advisorid = $rootScope.current_advisor;

      var CashflowDefaultsPromise = CashflowDefaults.getOne($rootScope.client_id);
      CashflowDefaultsPromise.then(function(result) {
        console.log("inside cashflow defaults promise");
        console.log(result);

        //for(var i=0; i<result.length; i++) {
        //  var tmp = { 'currency': result[i].currency,
        //          'amount': result[i].amount,
        //          'action': $rootScope.labels["L0139"] }; //'Trade'
        //  console.log(tmp);
        //  $scope.cashPosition.push(tmp);
        //}
        var settings = result;

        var SavingsPlanPromise = SavingsPlan.getOne($rootScope.client_id);
        SavingsPlanPromise.then(function(result){

          var saving_plans = result;
          for(var j=0; j<saving_plans.length; j++)
          {
            saving_plans[j].amount = parseFloat(result[j].amount);
          }

          var InvestmentPromise = Investment.getOne($rootScope.client_id);
          InvestmentPromise.then(function(result){

            var investments = result;
            for(var j=0; j<investments.length; j++)
            {
              investments[j].account_value = parseFloat(result[j].account_value);
              if (result[j].cost_basis)
                investments[j].cost_basis = parseFloat(result[j].cost_basis);
            }
  
            var ExpensePromise = Expense.getOne($rootScope.client_id);
            ExpensePromise.then(function(result){

              var expenses = result;
              for(var j=0; j<expenses.length; j++)
              {
                expenses[j].annual_need = parseFloat(result[j].annual_need);
              }

              var IncomePromise = Income.getOne($rootScope.client_id);
              IncomePromise.then(function(result){

                var other_income = result;
                for(var j=0; j<other_income.length; j++)
                {
                  other_income[j].annual_flow = parseFloat(result[j].annual_flow);
                }

                var ClientPromise = Client.getOne(advisor_id, $rootScope.client_id);
                ClientPromise.then(function(result) {

                  var s = result.date_of_birth;
                  if (s) settings.client_birthyear = parseInt(s.slice(s.length-4, s.length));
                  console.log(settings.client_birthyear);
                  settings.client_retirement = result.retirement;
                  settings.client_income = result.client_income;
                  settings.client_income_growth = result.client_income_growth;
                  s = result.spouse_dob;
                  if (s) settings.spouse_birthyear = parseInt(s.slice(s.length-4, s.length));
                  settings.spouse_retirement = result.spouse_retirement;
                  settings.spouse_income = result.spouse_income;
                  settings.spouse_income_growth = result.spouse_income_growth;


                  $scope.cashflow = compute_cashflows(settings, saving_plans, investments, expenses, other_income, false);

                  $scope.xlabels = [];
                  $scope.dataseries = [];
                  for (var i=0; i < $scope.cashflow.length; i++)
                  {
                    $scope.xlabels.push($scope.cashflow[i].year.toString());
                    $scope.dataseries.push($scope.cashflow[i].endbalance);
                  }
                  console.log($scope.xlabels);
                  console.log($scope.dataseries);
                  $scope.displayCashChart($scope.xlabels, $scope.dataseries);

                }); // ClientPromise

              }); // IncomePromise

            }); // ExpensePromise

          }); // InvestmentPromise

        }); // SavingsPlanPromise
      
      }); // CashflowDefaultsPromise

      // Not used at the moment
      //var CashflowPromise = Cashflows.getOne(clientid);
      //CashflowPromise.then(function(result) {
      //  console.log("inside cashflow promise "+clientid);
      //  var len = result.length;

      //   for (var i=0; i < len; i++) {
      //    var fundid = result[i].fund_id;

          // Add to table
      //    var tmp = {'year': result[i].year,
      //               'clientage': result[i].client_age,
      //               'spouseage': result[i].spouse_age,
      //               'beginbalance': result[i].begin_balance,
      //               'netcashflow': result[i].net_cashflow,
      //               'netinvreturn': result[i].net_invreturn,
      //               'endbalance': result[i].end_balance,
      //               'totaltaxes': result[i].total_taxes,
      //               'totalfees': result[i].total_fees
      //               };
      //    $scope.cashflow.push(tmp);
      //  }

      //}); // CashflowPromise
   
  }


    //var clientid = $rootScope.client_id;
    //var advisorid = $rootScope.current_advisor;

    //var ExchangeRatePromise = ExchangeRate.getOne(1);
    //ExchangeRatePromise.then(function(result){
    //  console.log("inside exchange rate promise");
    //  console.log(result);

    //  $scope.exchange_rates = result;
    //  $scope.currency = ['USD'];
    //  $scope.newCurrency = ['USD'];
    //  for(var i=0; i<result.length; i++) {
    //    var tmp = {'name': result[i].currency, 'value': result[i].currency};
    //    $scope.currency.push(tmp);
    //    $scope.newCurrency.push(tmp);
    //  }
    //});

  $scope.setValues = function() {
    setTimeout(function(){
      $('.dropdown-button').dropdown();
       $scope.addActive=function(){
       $(".edit_label label").addClass('active');
      }
      $scope.addActive();
    }, 100);
  }
  
  $scope.setValues();

  $scope.displayCashChart = function(xLabels, dataseries) {
    console.log("in displayCashChart");
    //console.log(xLabels);
    //console.log(dataseries);
    //Highcharts.setOptions({
    //  colors: ['#EB4932', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
    //           '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    //});
    $('#cashflowgraph').highcharts({
      exporting: { enabled: false },
        title: {
            text: 'Projected Account Values',
            enabled:true,
            x: -20,
            style:{
                  color:'#ffffff',
                  fontWeight:'bold',
                  fontSize: '13px !important',
                 }
        },
        //xAxis: {
        //    categories: xLabels,
        //},
        yAxis: {
            title: {
                text: 'Projected Account Values (USD)',
                style:{
                  fontSize: '16px !important',
                 }
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' USD'
        },
        legend: {
            layout: 'vertical',
            align: 'middle',
            verticalAlign: 'top',
            enabled: false,
            borderWidth: 0,
            style:{
                  color:'#4D7FBA',
                  fontWeight:'bold',
                  fontSize: '13px !important',
                 }
        },
        plotOptions: {
            series: {
                marker: { enabled: false },
                pointStart: 2017
            },
            line: {
                dataLabels: {
                    align: 'top',
                  enabled: false
                },
                enableMouseTracking: true
            }
        },
        //series: dataseries
        series: [{
            name: 'Balance',
            data: dataseries //[43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
          }]
    });
  }

});

finvese.value('reloadAfterLogin', false);


finvese.controller('overviewController', function($rootScope, $scope, localStorageService, $window, $document, AdvisorAllocation, SavedPortfolio, ClientHoldings, Client, Advisors, CashHoldings, ExchangeRate, SwapRates, CalculatedRisks, Translations, reloadAfterLogin) {

if(localStorageService.previous === 'http://www.finvese.co/#/'){
  $window.location.reload();
}


/*$scope.$on("$locationChangeStart", function(e, currentLocation, previousLocation){
    console.error( previousLocation);
    if(previousLocation === 'http://www.finvese.co/#/'){
      $window.location.reload();
    }
})*/

/*
reloadAfterLogin = true;
console.error(reloadAfterLogin);*/
  /*if(reloadAfterLogin === false){
    console.error(reloadAfterLogin);
    reloadAfterLogin = true;
    $window.location.reload();
  }*/

  $rootScope.overview = true;
  $rootScope.product = false;
  $rootScope.investments = false;Translations
  $rootScope.savings = false;
  $rootScope.home = false;
  $rootScope.login = false;
  $rootScope.register = false;
  $rootScope.nodashboard = false;
  $rootScope.clients_list = false;
  $scope.advisor_allocation = [];

  setAdvisor($rootScope);
  document.title = "Dashboard";

  //$rootScope.default_language = "en";

  var advisor_id = $rootScope.current_advisor;

  var AdvisorPromise = Advisors.getOne(advisor_id);
  AdvisorPromise.then(function(result) {
    console.log("in advisor promise "+advisor_id);

    //console.log(result);

    if (result) {
      console.log(result.advisor_name);
      $rootScope.advisorname = result.advisor_name;
      $rootScope.default_language = result.default_language;
    }
    console.log("Default Language="+$rootScope.default_language);

    if ($rootScope.labels && Object.keys($rootScope.labels).length > 0)
    {
      $scope.setScreenLabels();
    }
    else
    {
      $rootScope.labels = {};
      var TranslationPromise = Translations.getAll($rootScope.default_language);
      TranslationPromise.then(function(result) {
        //console.log("in translation promise");
        //console.log(result);
        //$rootScope.labels = result;
        for(var i=0; i<result.length; i++) {
          $rootScope.labels[result[i].label_id] = result[i].label_string;
        }

        $scope.setScreenLabels();
      });
    }
  });

  $scope.setScreenLabels = function()
  { 
      setAdvisorMenuLabels($rootScope);

      //console.log($rootScope.labels);
      console.log("Advisor Name: "+$rootScope.labels["L0008"]);
      $scope.advisor_name_label = $rootScope.labels["L0008"];
      console.log($scope.advisor_name_label);
      
      var label = document.getElementById('clientPositioningLabel');
      if (label) label.textContent = $rootScope.labels["L0175"];
      label = document.getElementById('advisoryModelPortfolioLabel');
      if (label) label.textContent = $rootScope.labels["L0098"];
      label = document.getElementById('clientPortfolioAllocationLabel');
      if (label) label.textContent = $rootScope.labels["L0176"];
      label = document.getElementById('clientPerformanceLabel');
      if (label) label.textContent = $rootScope.labels["L0177"];
      label = document.getElementById('clientPerformanceTargetLabel');
      if (label) label.textContent = $rootScope.labels["L0178"];
  }

  if (!$rootScope.list) {
    getClientList($rootScope, advisor_id, Client);
  }

  // Retrieve the exchange rates
  $scope.exchange_rates = [];
  var ExchangeRatePromise = ExchangeRate.getOne(1);
  ExchangeRatePromise.then(function(result){
    console.log("inside exchange rate promise");
    console.log(result);
    if (result) $scope.exchange_rates = result;
  });

  // Retrieve the swap rates - stored in $rootScope.swapRates
  getSwapRate($rootScope, SwapRates);

      // Retrieve the advisor portfolio
      var AdvisorAllocationPromise = AdvisorAllocation.getOne(advisor_id);
      AdvisorAllocationPromise.then(function(result) {
        console.log("inside advisor allocation promise "+advisor_id);
        //console.log(result);
        var arrlen = result.length;
        //console.log(arrlen);
        $scope.advisor_allocation = result;
        //    for (var i=0; i < arrlen; i++) {
        //      result[i].portfolio_weight = 0;
        //      result[i].old_weight = 0;
        //      result[i].selected = false;
        //      //result[i].total_value = 0;
        //    }
        $scope.displayChart2();
      });

      // Get data for Client Positioning Bubble Chart
      $scope.client_portfolios = [];
      var CalculatedRisksPromise = CalculatedRisks.getAll(advisor_id);
      CalculatedRisksPromise.then(function(result) {
        console.log("inside calculated risks promise "+advisor_id);
        var arrlen = result.length;
        console.log(arrlen);

        for (var i=0; i < arrlen; i++) {
          var tmpName = result[i].client_id.toString();
          var tmpCountry = 'Client '+result[i].client_id.toString();

          if ($rootScope.list) {
            var len = $rootScope.list.length;
            for (var k=0; k < len; k++) {
              if ($rootScope.list[k].clientId == result[i].client_id) {
                var nme = $rootScope.list[k].firstName;
                var initials = nme.slice(0,1);
                if ($rootScope.list[k].middleName)
                  nme = nme + " " + $rootScope.list[k].middleName;
                tmpCountry = nme + " " + $rootScope.list[k].lastName;
                if ($rootScope.list[k].lastName)
                  tmpName = initials + $rootScope.list[k].lastName.slice(0,1);
              }
            }
          }

          var tmp = { x: roundTwo(result[i].calc_risk),
                      y: roundTwo(result[i].calc_return),
                      z: result[i].aum,
                      name: tmpName,
                      client_id: result[i].client_id,
                      country: tmpCountry
                    };
          $scope.client_portfolios.push(tmp);
        }
        $scope.displayChart1();
      })
      /*
      var dquery = { 'client_id': 0, 'advisor_id': advisor_id };
      var SavedPortfolioPromise = SavedPortfolio.getAll(advisor_id, dquery);
      SavedPortfolioPromise.then(function(result) {
        console.log("inside saved portfolio promise "+advisor_id);
        var arrlen = result.length;
        console.log(arrlen);

        for (var i=0; i < arrlen; i++) {
          var tmpName = result[i].client_id.toString();
          var tmpCountry = 'Client '+result[i].client_id.toString();

          if ($rootScope.list) {
            var len = $rootScope.list.length;
            for (var k=0; k < len; k++) {
              if ($rootScope.list[k].clientId == result[i].client_id) {
                var nme = $rootScope.list[k].firstName;
                var initials = nme.slice(0,1);
                if ($rootScope.list[k].middleName)
                  nme = nme + " " + $rootScope.list[k].middleName;
                tmpCountry = nme + " " + $rootScope.list[k].lastName;
                tmpName = initials + $rootScope.list[k].lastName.slice(0,1);
              }
            }
          }

          var tmp = { x: roundTwo(result[i].expected_risk),
                      y: result[i].expected_return,
                      z: result[i].aum,
                      name: tmpName,
                      client_id: result[i].client_id,
                      country: tmpCountry
                    };
          $scope.client_portfolios.push(tmp);
        }
        $scope.displayChart1();
      })
      */
  
      // Look up the cash holdings
      $scope.cash_holdings = [];
      $scope.totals = { 'cash': 0.0, 'stocks': 0.0, 'bonds': 0.0, 'alternatives': 0.0 };
      $scope.portfolio = [];

      var CashHoldingsPromise = CashHoldings.getAll($rootScope.current_advisor);
      CashHoldingsPromise.then(function(result) {
        console.log("inside cash holdings promise");
        console.log(result.length);
        for(var i=0; i<result.length; i++) {
          console.log(result[i]);
          if (result[i].currency == 'USD') {
            console.log("found AAA "+result[i].amount);
            $scope.totals.cash = $scope.totals.cash + result[i].amount;
          } else {
            var amt = convertCurrency(result[i].currency, result[i].amount);
          }

          // Look for this client in the portfolio
          var found = false;
          for (var j=0; j<$scope.portfolio.length; j++) {
            if ($scope.portfolio[j].client_id == result[i].client_id) {
              console.log("found the client");
              found = true;
              $scope.portfolio[j].cash = $scope.portfolio[j].cash + result[i].amount;
            }
          }
          if (!found) {
            console.log("adding client "+result[i].client_id);
            var tmp = { 'client_id': result[i].client_id, 'cash': result[i].amount, 'cashbonds': 0 }
            $scope.portfolio[j] = tmp;
          }
        }

        // Get the client holdings
        //var data_query = { 'advisor_id': advisor_id, 'client_id': 0 };
        //console.log(data_query);
        var ClientHoldingsPromise = ClientHoldings.getAll(advisor_id);
        ClientHoldingsPromise.then(function(result) {
          console.log("inside client holdings promise "+advisor_id);
          console.log(result);
          var arrlen = result.length;
          console.log(arrlen);

          for (var i=0; i < arrlen; i++) {
            //console.log(result[i].fund_name);
            if (result[i].fund_name == "CASH") {
              $scope.totals.cash = $scope.totals.cash + result[i].num_units;
             //console.log(totals.cash);
            } else {
              if (result[i].fund_class == "Equity")
                $scope.totals.stocks = $scope.totals.stocks + result[i].holding_value;
              else
                $scope.totals.bonds = $scope.totals.bonds + result[i].holding_value;
            }
          }

          $scope.totals.cash = $scope.totals.cash / 1000000.0;
          $scope.totals.stocks = $scope.totals.stocks / 1000000.0;
          $scope.totals.bonds = $scope.totals.bonds / 1000000.0;
          $scope.totals.alternatives = $scope.totals.alternatives / 1000000.0;
          console.log($scope.totals);

          // Client Portfolio Allocation
          $scope.displayChart3();
        });
      });

    /* chart 1 start here */
    $scope.displayChart1 = function () {
      // Client Positioning Bubble Chart
      var dat = $scope.client_portfolios;
                //[
                //    { x: 14, y: 6.5,    z:6,  name: '4', country: 'Client 4' },
                //    { x: 8,  y: 5,    z: 11, name: '2', country: 'Client 2' },
                //    { x: 30, y: 13.5, z: 20, name: '14', country: 'Client 14' },
                //    { x: 16, y: 7.5,  z: 10, name: '11', country: 'Client 11' },
                //    { x: 20, y: 11,   z: 31, name: '16', country: 'Client 16' }
                //];

        Highcharts.setOptions({
           colors: ['#4374AE', '#3E6697', '#335680', '#92A5D0', '#B7C6E0']
        });

        Highcharts.chart('client_positioning', {

            chart: {
                type: 'bubble',
                plotBorderWidth:0,
                zoomType: 'xy',
                marginLeft:50,
            },
            exporting: { enabled: false },
            legend: {
                enabled: false
            },

            title: {
                text: 'Client Positioning',
                 style:{
                         color:'#333 !important',
                         fontWeight:'bold !important',
                         fontSize: '18px !important',
                       },
            },
            xAxis: {
                gridLineWidth: 0,
                enabled:true,
                title: {
                    text: 'RISK',
                     style:{
                             color:'#333 !important',
                             fontWeight:'bold !important',
                             fontSize: '12px !important',
                          },
                     y:-50,
                     x:110
                },
                labels: {
                    format: '{value}%',
                    enabled:true,

                }
            },

            yAxis: {
                lineWidth: 1,
                min: 0,
                offset: -1,
                tickWidth: 1,
                title: {
                    text: 'RETURNS',
                    style:{
                             color:'#333 !important',
                             fontWeight:'bold !important',
                             fontSize: '12px !important',
                          },
                    x:85,
                    y:-40,
                },

                labels: {
                    format: '{value}%'
                },
                gridLineWidth: 0,
                maxPadding: 0.2
            },

            tooltip: {
                useHTML: true,
                headerFormat: '<table>',
                pointFormat: '<tr><th colspan="2"><h5>{point.country}</h5></th></tr>' +
                    '<tr><th>Risk:</th><td>{point.x}%</td></tr>' +
                    '<tr><th>Return:</th><td>{point.y}%</td></tr>' +
                    '<tr><th>AUM (m):</th><td>{point.z}</td></tr>',
                footerFormat: '</table>',
                followPointer: true
            },

            plotOptions: {
                series: {
                    events: {
                      click: function bubleRenderPage(event) {
                           $scope.bubleSeriesClient(event);
                        },
                      },
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                         style:{
                             color:'#333 !important',
                             fontSize: '14px !important',
                          },

                    }
                }
            },

            series: [{
                data: dat,
                

            }]

        });
  }/* chart 1 end here */
  $scope.bubleSeriesClient = function(event){
    console.log("on click");
    //console.log(event);
    console.log(event.point); 
    var str = "";
    if (event.point.client_id) {
      str = "/" + event.point.client_id.toString();
    }
    $scope.goToSiteUrl('/#/client-dashboard'+str);
  }
  /* chart 4 start here */
  $scope.displayChart4 = function() {

        var dat = [
                    { x: 8.30, y: 8.66,   z: 5,  name: '4', country: 'Client 4' },
                    { x: 18.76, y: 11.27, z: 11, name: '2', country: 'Client 2' },
                    { x: 11.68, y: 10.92, z: 10, name: '1', country: 'Client 1' },
                    { x: 9.36, y: 9.77,   z: 50, name: '3', country: 'Client 3' },
                    { x: 10.27, y: 8.58, z: 8,  name: '5', country: 'Client 5' },
                    { x: 14.66, y: 8.08,  z: 4,  name: '6', country: 'Client 6' },
                    { x: 6.82, y: 4.25,  z: 15, name: '12', country: 'Client 12' },
                    { x: 7.24, y: 7.15,  z: 10, name: '7', country: 'Client 7' },
                    { x: 5.26, y: 4.66,  z: 35, name: '13', country: 'Client 13' },
                    { x: 9.18, y: 6.68,  z: 8,  name: '8', country: 'Client 8' },
                    { x: 7.14, y: 6.45,  z: 8,  name: '10', country: 'Client 10' },
                    { x: 11.73, y: 6.38,  z: 5,  name: '9', country: 'Client 9' },
                    { x: 5.10, y: 4.23, z: 25, name: '15', country: 'Client 15' },
                    { x: 9.62, y: 4.09, z: 20, name: '14', country: 'Client 14' },
                    { x: 8.69, y: 5.08, z: 10, name: '11', country: 'Client 11' },
                    { x: 6.62, y: 4.05, z: 31, name: '16', country: 'Client 16' }
                ];

        Highcharts.chart('client_performance', {

            chart: {
                type: 'bubble',
                plotBorderWidth:0,
                zoomType: 'xy',
                marginLeft:50,
            },
            exporting: { enabled: false },
            legend: {
                enabled: false
            },

            title: {
                text: 'Client Performance (5 Years)',
                 style:{
                         color:'#4D7FBA !important',
                         fontWeight:'bold !important',
                         fontSize: '18px !important',
                       },
            },

            xAxis: {
                gridLineWidth: 0,
                title: {
                    text: 'RISK',
                     style:{
                             color:'#333 !important',
                             fontWeight:'bold !important',
                             fontSize: '12px !important',
                           },
                    y:-50,
                    x:110,
                },

                labels: {
                    format: '{value}%',
                    enabled:true,
                }
            },

            yAxis: {
                lineWidth: 1,
                min: 0,
                offset: -1,
                tickWidth: 1,
                title: {
                     text: 'RETURNS',
                     style:{
                             color:'#333 !important',
                             fontWeight:'bold !important',
                             fontSize: '12px !important',
                          },
                    x:85,
                    y:-60,

                },

                labels: {
                    format: '{value}%'
                },
                gridLineWidth: 0,
                maxPadding: 0.2
            },

            tooltip: {
                useHTML: true,
                headerFormat: '<table>',
                pointFormat: '<tr><th colspan="2"><h5>{point.country}</h5></th></tr>' +
                    '<tr><th>Risk:</th><td>{point.x}%</td></tr>' +
                    '<tr><th>Return:</th><td>{point.y}%</td></tr>' +
                    '<tr><th>AUM (m):</th><td>{point.z}</td></tr>',
                footerFormat: '</table>',
                followPointer: true
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                        style:{
                             color:'#333 !important',
                             fontSize: '14px !important',
                          },
                    }
                }
            },

            series: [{
                data: dat
                  //[
                  //  { x: 8.30, y: 8.66,   z: 5,  name: '4', country: 'Client 4' },
                  //  { x: 18.76, y: 11.27, z: 11, name: '2', country: 'Client 2' },
                  //  { x: 11.68, y: 10.92, z: 10, name: '1', country: 'Client 1' },
                  //  { x: 9.36, y: 9.77,   z: 50, name: '3', country: 'Client 3' },
                  //  { x: 10.27, y: 8.58, z: 8,  name: '5', country: 'Client 5' },
                  //  { x: 14.66, y: 8.08,  z: 4,  name: '6', country: 'Client 6' },
                  //  { x: 6.82, y: 4.25,  z: 15, name: '12', country: 'Client 12' },
                  //  { x: 7.24, y: 7.15,  z: 10, name: '7', country: 'Client 7' },
                  //  { x: 5.26, y: 4.66,  z: 35, name: '13', country: 'Client 13' },
                  //  { x: 9.18, y: 6.68,  z: 8,  name: '8', country: 'Client 8' },
                  //  { x: 7.14, y: 6.45,  z: 8,  name: '10', country: 'Client 10' },
                  //  { x: 11.73, y: 6.38,  z: 5,  name: '9', country: 'Client 9' },
                  //  { x: 5.10, y: 4.23, z: 25, name: '15', country: 'Client 15' },
                  //  { x: 9.62, y: 4.09, z: 20, name: '14', country: 'Client 14' },
                  //  { x: 8.69, y: 5.08, z: 10, name: '11', country: 'Client 11' },
                  //  { x: 6.62, y: 4.05, z: 31, name: '16', country: 'Client 16' }
                  //]
            }]

        });
  }/* chart4 end here */

  /* chart2 start here */
   $scope.displayChart2 = function() {
      var len = $scope.advisor_allocation.length;
      console.log("in displayChart2 "+len);
      //var colors = ['#D00049', '#EB4932', '#8085E9', '#347DCF', '#64D0AE'];
      var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];

      var dat = [];
      for (var i=0; i < len; i++) {
        var tmp = { name: $scope.advisor_allocation[i].asset_description,
                    y: $scope.advisor_allocation[i].asset_weight,
                    color: colors[i] };
        dat.push(tmp);
               //= [{
               //     name: ' CASH ',
               //     y: 10.38,
               //     color: colors[0]
               // }, {
               //     name: 'US-EQ',
               //     y: 56.33,
               //     color: colors[1]
               // }, {
               //     name: 'EU-EQ',
               //     y: 24.03,
               //     color: colors[2]
               // }, {
               //     name: 'GL-IB',
               //     y: 4.77,
               //     color: colors[3]
               // }, {
               //     name: 'AS_EQ',
               //     y: 0.91,
               //     color: colors[4]
               // }];
      }
        Highcharts.chart('advisory_model', {
            chart: {
              type: 'pie',
              renderTo: 'advisory_model',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'15',
            },
            legend: {
              align: 'center',
              verticalAlign: 'bottom',
              itemStyle: {
                fontWeight: 'normal'
              }
            },
            exporting: { enabled: false },
            title: {
                text: '',
                //text: '$100M',
                 style:{
                         color:'#333 !important',
                         fontWeight:'bold !important',
                         fontSize: '21px !important',
                       },
                  align: 'center',
                  verticalAlign: 'middle',
                align: 'center',
                verticalAlign: 'middle',
                y: -20
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
              enabled:true,
              animation:false,
              borderColor: 'null',
          },

 
            plotOptions: {
              // pie: {
              //     dataLabels: {
              //         enabled: true,
              //         distance: -40,
              //         rotate: 'left',
              //         style: {
              //             fontWeight: 'bold',
              //             color: 'white',
              //             textShadow: '0px 1px 2px black',
              //             fontSize:'10px',
              //             letterSpacing:'0px',
              //         },
              //         y: 0,
              //         format: '{point.name} , {point.y:.1f}%',
              //     },
              //     startAngle: -240,
              //     endAngle: 120,
              //     center: ['50%', '52%']
              // }
             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                    },
                    showInLegend: true,
                    center: ['50%', '130px'],
                }
           },
            // series: [{
            //     type: 'pie',
            //     name: 'Model Portfolio',
            //     innerSize: '50%',
            //     size:'103%',
            //     data: [
            //         ['Cash',   10.38],
            //         ['US-EQ',  56.33],
            //         ['EU-EQ',  24.03],
            //         ['GL-IB',   4.77],
            //         ['AS_EQ',   0.91]
            //     ]
            // }]
             series: [{
                name: 'Advisory Model Portfolio',
                colorByPoint: true,
                innerSize: '60%',
                data: dat
                  //[{
                  //  name: ' CASH ',
                  //  y: 10.38,
                  //  color: '#D00049',
                //}, {
                  //  name: 'US-EQ',
                  //  y: 56.33,
                  //  color: '#EB4932'
                //}, {
                 //   name: 'EU-EQ',
                 //   y: 24.03,
                 //   color: '#8085E9'
                //}, {
                //    name: 'GL-IB',
                //    y: 4.77,
                //    color: '#347DCF'
                //}, {
                //    name: 'AS_EQ',
                //    y: 0.91,
                //    color: '#64D0AE'
                //}]
            }]
        });
  }/* chart2 end here */
  // $(dom_id).highcharts({
  //         chart: {
  //             plotBackgroundColor: null,
  //             plotBorderWidth: 0,
  //             plotShadow: false,
  //             marginTop:40,
  //             type: 'pie'
  //         },
  //         exporting: { enabled: false },
  //         title: {
  //             text: title_text,
  //             align: halign,
  //             verticalAlign: valign,
  //             y: yangle,
  //             style: style_text_object
  //         },
  //         tooltip: {
  //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
  //             enabled:true,
  //             animation:false,
  //             borderColor: 'null',
  //         },
  //         plotOptions: {
  //             // pie: {
  //             //     dataLabels: {
  //             //         enabled: true,
  //             //         distance: -40,
  //             //         rotate: 'left',
  //             //         style: {
  //             //             fontWeight: 'bold',
  //             //             color: 'white',
  //             //             textShadow: '0px 1px 2px black',
  //             //             fontSize:'10px',
  //             //             letterSpacing:'0px',
  //             //         },
  //             //         y: 0,
  //             //         format: '{point.name} , {point.y:.1f}%',
  //             //     },
  //             //     startAngle: -240,
  //             //     endAngle: 120,
  //             //     center: ['50%', '52%']
  //             // }
  //            pie: {
  //                   allowPointSelect: true,
  //                   cursor: 'pointer',
  //                   dataLabels: {
  //                       enabled: false
  //                   },
  //                   showInLegend: true
  //               }
  //         },
  //         // series: [{
  //         //     type: 'pie',
  //         //     name: 'Advisor Model Portfolio',
  //         //     innerSize: '40%',
  //         //     data: chart_data,
  //         //     states: {
  //         //             hover: {
  //         //               enabled: false,
  //         //               halo: {
  //         //                 size: 0
  //         //               }
  //         //             }
  //         //           }
  //         // }]
  //          series: [{
  //               name: 'Advisor Model Portfolio',
  //               colorByPoint: true,
  //               data: [{
  //                   name: 'Cash',
  //                   y: 56.33,
  //                   color: '#FF5252',
  //               }, {
  //                   name: 'Europe',
  //                   y: 24.03,
  //                   color: '#8085E9',
  //                   sliced: true,
  //                   selected: true
  //               }, {
  //                   name: 'Asia',
  //                   y: 10.38,
  //                   color: '#8D4654'
  //               }, {
  //                   name: 'N_America',
  //                   y: 4.77,
  //                   color: '#7898BF'
  //               }, {
  //                   name: 'GL-IG',
  //                   y: 0.91,
  //                   color: '#ACEDEF'
  //               }]
  //           }]
  //     });
  //   }

  /* chart3 start here */
    $scope.displayChart3 = function() {
      // Client Portfolio Allocation

        //var totals = { 'cash': 0.0, 'stocks': 0.0, 'bonds': 0.0, 'alternatives': 0.0 };
        //totals = $scope.computeTotals();
        var grandTotal = roundTwo($scope.totals.cash + $scope.totals.stocks + $scope.totals.bonds + $scope.totals.alternatives);
        var totalStr = "$" + grandTotal.toString() + "M";
        var colors = ['#66c2a5', '#00b5d9', '#6666ff', '#8250e6', '#71acbc', '#bfb286', '#4fc452', '#9ecc29', '#fcca00', '#ff9419', '#ff4733', '#e62267'];

        var dat = [{
                    name: ' CASH',
                    y: $scope.totals.cash,
                    color: colors[0] //'#D00049',
                }, {
                    name: 'EQUITY',
                    y: $scope.totals.stocks,
                    color: colors[1] //'#EB4932'
                }, {
                    name: 'FIXED INCOME',
                    y: $scope.totals.bonds,
                    color: colors[2] // #8085E9'
                }, {
                    name: 'ALTERNATIVES',
                    y: $scope.totals.alternatives,
                    color: colors[3] // '#347DCF'
                }];

        Highcharts.chart('client_portfolio', {
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'15',
                // marginTop:'-28',
            },
            exporting: { enabled: false },
            legend: {
              align: 'center',
              verticalAlign: 'bottom',
              //layout: 'vertical',
              //x: 0,
              //y: 0,
              //floating: true,
              itemStyle: {
                fontWeight: 'normal'
              }
            },
            title: {
                text: totalStr, //'$100M',
                 style:{
                         color:'#333 !important',
                         fontWeight:'bold !important',
                         fontSize: '21px !important',
                      },
                  y:-26,
                  align: 'center',
                  verticalAlign: 'middle',
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            // plotOptions: {
            //     pie: {
            //         borderColor: 'transparent',
            //         shadow: false,
            //         startAngle: -220,
            //         endAngle: 140,
            //         center: ['50%', '45%'],
            //         colors: [
            //                  '#ee7d31',
            //                  '#a5a5a5',
            //                  '#ffc000',
            //                  '#5a9bd5',
            //                 ],
            //         dataLabels: {
            //             enabled: true,
            //             distance: -50,
            //             style: {
            //                 fontWeight: 'bold',
            //                 color: 'white',
            //                 textShadow: '0px 1px 2px black',
            //                 letterSpacing:'0px',
            //             },
            //             format: '{point.name} , {point.y:.1f}%',
            //         }
            //     }
            // },
            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true,
                    center: ['50%', '130px']
                }
           },
            // series: [{
            //     type: 'pie',
            //     name: 'All Client Portfolio',
            //     innerSize: '50%',
            //     size:'85%',
            //     data: [
            //         ['Cash',   11.0],
            //         ['Stocks', 35.0],
            //         ['Bonds',  50.0],
            //         ['Alternatives', 4.0]
            //     ]
            // }]
            series: [{
                name: 'All Client Portfolio Allocation',
                colorByPoint: true,
                innerSize: '60%',
                data: dat,
                //data: [{
                //    name: ' CASH',
                //    y: 11.0,
                //    color: '#D00049',
                //}, {
                //    name: 'STOCKS',
                //    y: 35.0,
                //    color: '#EB4932'
                //}, {
                //    name: 'BONDS',
                //    y: 50.0,
                //    color: '#8085E9'
                //}, {
                //    name: 'ALTERNATIVES',
                //    y: 4.0,
                //    color: '#347DCF'
                //}]
            }]
        });
   }

  /* chart3 end here */

  /*chart5 start here */
   $scope.displayChart5 = function () {
      Highcharts.chart('client_target', {
            chart: {
                type: 'column'
            },
            exporting: { enabled: false },
            title: {
                text: 'Client Performance vs Target',
                 style:{
                         color:'#4D7FBA !important',
                         fontWeight:'bold !important',
                         fontSize: '18px !important',
                      },
            },
            /*subtitle: {
                text: 'Source: WorldClimate.com'
            },*/
            xAxis: {
                categories: [
                    '4','2','1','3','5','6','12','7','13','8','10','9','15','14','11','16'
                ],
                offset:-156,
                crosshair: true,
            },
            yAxis: {
                /*min: 0,*/
                labels: {
                  format: '{value} %'
                },
                gridLineWidth: 0,
                title: {
                    text: 'Performance (%)',
                    enabled:false,
                }
            },
            legend: {
                    layout: 'vertical',
                    backgroundColor: '#FFFFFF',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 300,
                    y: 50,
                    floating: true,
                },
            tooltip: {
                headerFormat: '<span style="font-size:12px">Client {point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:2">{series.name}: </td>' +
                    '<td style="padding:2"><b>{point.y:.1f}%</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                     colors: [
                             '#ee7d31',
                             '#a5a5a5',
                            ],
                },

            },
            series: [{
                name: 'X RETURN',
                color:'#347DCF',
                data: [-0.82, 4.72, -2.61, -2.87, -0.99, 3.57, -0.20, -3.92, -1.85, -2.15, -3.14, 1.55, -1.93, 2.69, -0.40, -0.50]

            }, {
                name: 'X RISK',
                color:'#EB4932',
                data: [2.27, 2.04, 0.88, 1.26, 0.82, 0.84, -0.55, -0.43, -0.64, -1.41, -1.41, -1.19, -1.25, -2.65, -1.24, -1.40]

            }]
        });
   }
  /*chart5 end here */

  $scope.runSimulation = function(){
      $scope.displayChart1();
      $scope.displayChart2();
      $scope.displayChart3();
      $scope.displayChart4();
      $scope.displayChart5();
  }
  $scope.runSimulation();
})

finvese.controller('whyController', function($rootScope, $scope) {
      $rootScope.why = false;
      $rootScope.service  = false;
      $rootScope.advisor = false;
      $rootScope.contact = false;
      $rootScope.signup = false;
      $rootScope.home = false;
      $rootScope.nodashboard = false;
      $rootScope.register = false;
      $rootScope.login = false;

})
finvese.controller('serviceController', function($rootScope, $scope) {
      $rootScope.why  = false;
      $rootScope.service  = false;
      $rootScope.login = false;
      $rootScope.contact = false;
      $rootScope.signup = false;
      $rootScope.home = false;


})

finvese.controller('contactController', function($rootScope, $scope) {
      $rootScope.why  = false;
      $rootScope.service  = false;
      $rootScope.login = false;
      $rootScope.contact = false;
      $rootScope.signup = false;
      $rootScope.home = false;
})

finvese.controller('homeController', function($rootScope, $scope, $window, $location, $route, $routeParams, $filter, $timeout, Countries, localStorageService, UserAuthenticate) {
      $rootScope.home = true;
      $rootScope.why  = false;
      $rootScope.register = false;
      $rootScope.service  = false;
      $rootScope.login = false;
      $rootScope.contact = false;
      $rootScope.nodashboard = false;

  console.log('in homeController'); 

          //$('#content-body').css({'top': '0', 'height':'100%'}); // removing "top block" on home page when loading page

   
    $rootScope.home = true; 
    $rootScope.why = false; 
    $rootScope.register = false; 
    $rootScope.service = false; 
    $rootScope.login = false; 
    $rootScope.contact = false; 
    $rootScope.nodashboard = false; 

    $scope.userLoginEmail = ''; 
    $scope.userRegisterEmail = '';
    $scope.userResetEmail = ''; 

    $rootScope.userLoginEmailCheck; 
    $rootScope.userLoginPasswordCheck; 

    $rootScope.userRegisterUsernameCheck; 
    $rootScope.userRegisterPasswordCheck; 
    $rootScope.userRegisterEmailCheck; 

    $rootScope.userResetEmailCheck; 
    $rootScope.currentUser = {'email': '', 'password': ''};

    $scope.emailErrorMessage  = "Please insert corrent email address"; 

    $scope.usernameErrorMessage = "Please enter your name"; 

    $scope.passwordErrorMessage = "Please insert corrent password"; 

// —-------— signIn && signUp methods start —-----------------— 

    var formModal = $('.CDForm-user-modal'), 
    formLogin = formModal.find('#CDForm-login'), 
    formSignup = formModal.find('#CDForm-signup'), 
    formForgotPassword = formModal.find('#CDForm-reset-password'), 
    formModalTab = $('.CDForm-switcher'), 
    tabLogin = formModalTab.children('li').eq(0).children('a'), 
    tabSignup = formModalTab.children('li').eq(1).children('a'), 
    forgotPasswordLink = formLogin.find('.CDForm-form-bottom-message a'), 
    backToLoginLink = formForgotPassword.find('.CDForm-form-bottom-message a'), 
    hideShowPassLogin = $('#hide-password-login'), 
    hideShowPassRegister = $('#hide-password-register'), 
    mainNav = $('.main-nav'); 

    $(document).mouseup(function (e){ // close modal window when user clicked outside the modal window
      var modalContainer = $('.CDForm-user-modal-container');
      if (!modalContainer.is(e.target)
          && modalContainer.has(e.target).length === 0) {
        $scope.closeModal();
      }
    });

    $(document).keydown(function(e) { // close modal window when "escape" press
      if( e.keyCode === 27 ) {
            $scope.closeModal();
      }
  });
  

  $scope.login_selected = function(){ // render login form
    console.log('login_selected'); 
    mainNav.children('ul').removeClass('is-visible'); 
    formModal.addClass('is-visible'); 
    formLogin.addClass('is-selected'); 
    formSignup.removeClass('is-selected'); 
    formForgotPassword.removeClass('is-selected'); 
    tabLogin.addClass('selected'); 
    tabSignup.removeClass('selected');
    
    $scope.resetDefault();
  } 
  $scope.signup_selected = function(){ // render registration form
    console.log('signup_selected'); 
    mainNav.children('ul').removeClass('is-visible'); 
    formModal.addClass('is-visible'); 
    formLogin.removeClass('is-selected'); 
    formSignup.addClass('is-selected'); 
    formForgotPassword.removeClass('is-selected'); 
    tabLogin.removeClass('selected'); 
    tabSignup.addClass('selected');
    
    $scope.resetDefault();
  } 

  $scope.forgot_password_selected = function(){ // render reset password form
    console.log('forgot_password_selected'); 
    formLogin.removeClass('is-selected'); 
    formSignup.removeClass('is-selected'); 
    formForgotPassword.addClass('is-selected'); 
    
    $scope.resetDefault();
  } 

  $scope.resetDefault = function(){ // set dafault values when rendering a pages

    $rootScope.currentUser.email = '';
    $rootScope.currentUser.password = ''; 
    $rootScope.currentUser.username = '';

    $('#signup_password').css({'border-color':'#d2d8d8'}); 
    $('#signup_email').css({'border-color':'#d2d8d8'}); 
    $('#signup_username').css({'border-color':'#d2d8d8'});
    $('#reset_email').css({'border-color':'#d2d8d8'});
    $('#signin_password').css({'border-color':'#d2d8d8'});
    $('#signin_email').css({'border-color':'#d2d8d8'}); 
    
    $('#register_username_error').css({'visibility':'hidden', 'opacity': '0'}); 
    $('#register_email_error').css({'visibility':'hidden', 'opacity': '0'}); 
    $('#register_password_error').css({'visibility':'hidden', 'opacity': '0'}); 
    $('#reset_email_error').css({'visibility':'hidden', 'opacity': '0'}); 
    $('#login_email_error').css({'visibility':'hidden', 'opacity': '0'}); 
    $('#login_password_error').css({'visibility':'hidden', 'opacity': '0'}); 
  }

  $scope.closeModal = function(){ // closing modal window method
    formModal.removeClass('is-visible'); 
  } 


  $scope.checkEmail = function(currentUser, login, register, forgot){ // validation of entered email

    var correctEmail = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i; 

    if(login) {
      $rootScope.userLoginEmailCheck = correctEmail.test(currentUser.email);
    } else if(register) {
      $rootScope.userRegisterEmailCheck  = correctEmail.test(currentUser.email);
    } else if(forgot) {
      $rootScope.userResetEmailCheck  = correctEmail.test(currentUser.email);
    }

  } 

  $scope.validateLogin = function(){ // verification of entered data before sending data to authorization method

    console.log('in validateLogin');

    var correctEmail = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;

    $rootScope.userLoginEmailCheck = correctEmail.test($rootScope.currentUser.email);

    if(!$('#signin_email').val()){ 
      $('#login_email_error').css({'visibility':'visible', 'opacity': '1'});
      $('#signin_email').css({'border-color':'#d76666'});
      $rootScope.userLoginEmailCheck = false;
    } 

    if($rootScope.userLoginEmailCheck == true){ 
      $('#login_email_error').css({'visibility':'hidden', 'opacity': '0'});
      $('#signin_email').css({'border-color':'#d2d8d8'}); 
    } else { 
      $('#login_email_error').css({'visibility':'visible', 'opacity': '1'}); 
      $('#signin_email').css({'border-color':'#d76666'});
    } 

    if( !$('#signin_password').val()){ 
      $('#login_password_error').css({'visibility':'visible', 'opacity': '1'}); 
      $('#signin_password').css({'border-color':'#d76666'});
      $rootScope.userLoginPasswordCheck  = false; 
    } else { 
      $('#login_password_error').css({'visibility':'hidden', 'opacity': '0'}); 
      $('#signin_password').css({'border-color':'#d2d8d8'});
      $rootScope.userLoginPasswordCheck  = true;
    } 

    if(($rootScope.userLoginEmailCheck == true) && ($rootScope.userLoginPasswordCheck == true)){ 
      $scope.updateLoginReset($rootScope.currentUser,false);
    } 

  } 

  $scope.validateReset = function(){ // verification of entered data before sending data to "reset password" method

    console.log('in validateReset'); 


    if($rootScope.userResetEmailCheck == true){ 
      $('#reset_email_error').css({'visibility':'hidden', 'opacity': '0'});
      $('#reset_email').css({'border-color':'#d2d8d8'});
      $scope.updateLoginReset($rootScope.currentUser,true); 
    } else { 
      $('#reset_email_error').css({'visibility':'visible', 'opacity': '1'}); 
      $('#reset_email').css({'border-color':'#d76666'});
    } 

  } 

  $scope.validateRegister = function(){ // verification of entered data before sending data to registration method

    console.log('in validateRegister'); 
 
    var correctEmail = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;

    $rootScope.userRegisterEmailCheck = correctEmail.test($rootScope.currentUser.email);

    if( !$('#signup_username').val()){ 
      $('#register_username_error').css({'visibility':'visible', 'opacity': '1'});
      $('#signup_username').css({'border-color':'#d76666'}); 
      $rootScope.userRegisterUsernameCheck  = false; 
    } else {
      $('#register_username_error').css({'visibility':'hidden', 'opacity': '0'}); 
      $('#signup_username').css({'border-color':'#d2d8d8'});
      $rootScope.userRegisterUsernameCheck  = true; 
    } 

    if(!$('#signup_email').val()){ 
      $rootScope.userRegisterEmailCheck = false;
    } 

    if($rootScope.userRegisterEmailCheck  == true){ 
      $('#register_email_error').css({'visibility':'hidden', 'opacity': '0'});
      $('#signup_email').css({'border-color':'#d2d8d8'}); 
    } else { 
      $('#register_email_error').css({'visibility':'visible', 'opacity': '1'}); 
      $('#signup_email').css({'border-color':'#d76666'}); 
    } 

    if( !$('#signup_password').val()){ 
      $('#register_password_error').css({'visibility':'visible', 'opacity': '1'}); 
      $('#signup_password').css({'border-color':'#d76666'});
      $rootScope.userRegisterPasswordCheck  = false; 
    } else { 
      $('#register_password_error').css({'visibility':'hidden', 'opacity': '0'}); 
      $('#signup_password').css({'border-color':'#d2d8d8'}); 
      $rootScope.userRegisterPasswordCheck  = true; 
    } 


    if($rootScope.userRegisterUsernameCheck && $rootScope.userRegisterEmailCheck && $rootScope.userRegisterPasswordCheck){
      $scope.updateRegister($rootScope.currentUser);
    }


  } 

  $scope.HideShowPasswordLogin = function(){ // hide and show entered password on login form
    var togglePass = hideShowPassLogin, 
    passwordField = togglePass.prev('input'); 

    ( 'password' == passwordField.attr('type') ) ? passwordField.attr('type', 'text') : passwordField.attr('type', 'password'); 
    ( 'Hide' == togglePass.text() ) ? togglePass.text('Show') : togglePass.text('Hide'); 

    passwordField.focus(); 
    passwordField.val(passwordField.val()); 
  } 

  $scope.HideShowPasswordRegister = function(){ // hide and show entered password on registration form
    var togglePass = hideShowPassRegister, 
    passwordField = togglePass.prev('input'); 

    ( 'password' == passwordField.attr('type') ) ? passwordField.attr('type', 'text') : passwordField.attr('type', 'password'); 
    ( 'Hide' == togglePass.text() ) ? togglePass.text('Show') : togglePass.text('Hide'); 

    passwordField.focus(); 
    passwordField.val(passwordField.val()); 
  }

  $scope.checkUniq = function(){ // checking the entered email for uniqueness
    
    if($rootScope.user.email){
      var myAuthenticatePromise = UserAuthenticate.uniqPhone({email: $rootScope.user.email});
      myAuthenticatePromise.then(function(result) {  // this is only run after $http completes
        $scope.already_taken = false;
      }, 
      function (error) {
        $scope.already_taken = true;
        console.log("$scope.user.phone$scope.user.phone");
        //console.log($rootScope.user);
        //console.log($rootScope.user.phone);
        //console.log($rootScope.user.phone.toString().length);
        //$scope.user_phone = $rootScope.user.phone.toString().length;
        $scope.success_message = null;
      });
    }else{
      console.log("empty message");
    }
  }

  $scope.updateRegister = function(user){ // registrarion method(we used method of previous developer)

    console.log('terms ', $('#accept_terms').prop('checked'));

    if($('#accept_terms').prop('checked')){
      // $rootScope.user.region = $rootScope.regionSelected;
    console.log('$scope.user.region111');
    console.log(user);  
    // user.region = $scope.user.region;
    if((user.email === undefined && user.phone === undefined && user.region === undefined) || user.username === undefined ||  user.password === undefined){      
        return false;
      }
/*    if(user.region && user.region.phone_code && user.phone)
      $rootScope.phone = user.region.phone_code+user.phone;
    
    if(user.region)
      user.region = _.findWhere($rootScope.countries, {'iso':'US'});
*/
    $scope.showLoader = true;
    // else
      // $rootScope.ema = user.email;
   // console.log(user.region);
    //user_params = {contact_type: 'sms', first_name: user.first_name, country_id: user.region.id, phone: $rootScope.phone, password: user.password, 


      user_params = {contact_type: 'sms', first_name: user.username, country_id: 1, phone: $rootScope.phone, password: user.password, 
          profile_image_id: user.profile_image_id, preferred_messenger_name: user.preferred_messenger_name, email: user.email, last_name: '' };
      
      console.log(user_params);
      $scope.signup_completed = false;
      var myAuthenticatePromise = UserAuthenticate.userSignup(user_params);
      myAuthenticatePromise.then(function(result) {  // this is only run after $http completes      
        $scope.signup_completed = true;
        localStorageService.add('verify_current_user', result);
        // $rootScope.current_user = result;
        $scope.error_message = null;
        $scope.showLoader = false;
        $scope.success_message = 'Signed up successfully! please verify code';
        if(user.email && user.email.length < 0)
          $location.path('/verify');
        else
          //$location.path('/verify_email');
          $location.path('/signin');
        }, 
        function (error) {
          $scope.showLoader = false;
          $scope.signup_completed = true;
          localStorageService.remove('user_token');
          $scope.error_message = 'Invalid username and password';
          $scope.success_message = null;
        }

      );
    }
  }

  $scope.updateLoginReset = function(user, forgot) { // login and reset password method(we used method of previous developer)
    
    $rootScope.phone_errors = true;
    $rootScope.password_errors = true;
    console.log(user);
    user_phone = user.phone;
    // user.phone = user.region.phone_code+user.phone+"";      
    
    if((user.email === undefined ) || user.password === undefined){      
      return false;
    }
    console.log('user login');
    
    if(user.region && user.region.phone_code)
      data_query = {email: user.email+"", password: user.password}
    else
      data_query = {email: user.email, password: user.password}
    
    console.log(data_query);

    $scope.showLoader = true;
    $scope.signin_completed = false;
    var myAuthenticatePromise = UserAuthenticate.userAuthenticate(data_query);
    myAuthenticatePromise.then(function(result) {  // this is only run after $http completes
        $scope.signin_completed = true;
        $scope.showLoader = false;
        $rootScope.signinAdvisor = true;

        localStorageService.add('current_user', result);


        var d = new Date();
        var m = d.getMonth()+1;
        var ms = m.toString();
        if (m < 10) ms = "0" + ms;
        var s = d.getFullYear().toString() + ms + d.getDate().toString();
        localStorageService.add('current_login', s);

        localStorageService.add('user_token', result.token);
        $rootScope.password_errors = false;
        if(forgot)
          $scope.renderPage('/forget');
        else{
          localStorageService.previous = 'http://www.finvese.co/#/';
          $location.path('/overview');
        }
          //$scope.renderPage('overview');
          //$location.path('/overview');
          //
          
        
        
      }, 
      function (error) {
        $scope.showLoader = false;
        $scope.errorSignin = true;
        $scope.signin_completed = true;
        localStorageService.remove('user_token');
        user.phone = user_phone;   
        $scope.error_message = true;
        $scope.success_message = null;
      }
    );
    }

  finvese.factory('UserAuthenticate', function($http, $q, $rootScope, localStorageService, myConfig) {
  var login = function(data_query) {
    console.log('login iN');
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/sessions", data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var logout = function(data_query) {
    var deferred = $q.defer();
    token = localStorageService.get('user_token');
    $http({method:"GET", url:myConfig.api+"/api/v1/sessions/destroy", data:data_query, headers:{"Authorization":token, "android":$rootScope.android, "ios":$rootScope.ios, "LuxeCorp": $rootScope.app_version}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var phoneUniq = function(data_query){
    var deferred = $q.defer();    
    //console.log("in signup");
    //console.log(myConfig.api+"/api/v1/users/uniq_phone?email");
    $http({method:"GET", url:myConfig.api+"/api/v1/users/uniq_phone?email="+data_query.email}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var signup = function(data_query) {
    var deferred = $q.defer();
    //console.log("in signup");
    //console.log(myConfig.api+"/users/1");
    //$http({method:"POST", url:myConfig.api+"/users.json", data:{user: data_query}}).success(function(result){
    $http({method:"POST", url:myConfig.api+"/api/v1/users/1", data:{user: data_query}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  var updateProfile = function(data_query) {
      token = localStorageService.get('user_token');
      var deferred = $q.defer();

      $http({method:"POST", url:myConfig.api+"/users/update_user", headers:{"Authorization":token}, data:{user: data_query}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });
      return deferred.promise;
    };

  var updatePassword = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/users/update_password", headers:{"Authorization":token}, data:{user: data_query}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  var verify = function(data_query) {
    var deferred = $q.defer();
    token = localStorageService.get('user_token');
    $http({method:"POST", url:myConfig.api+"/sms_verifications/verify.json", data:{user: data_query}, headers:{"Authorization":token}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var verifyEmail = function(data_query) {
    var deferred = $q.defer();
    token = localStorageService.get('user_token');
    $http({method:"GET", url:myConfig.api+"/users/confirmation", params:data_query, headers:{"Authorization":token}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  var getUser = function(token) {
    var deferred = $q.defer();
    $http({method:"GET", url:myConfig.api+"/api/v1/sessions", params:{}, headers:{"Authorization":token}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };


  return { userAuthenticate: login,
  userSignup: signup,
  verifySignup: verify,
  verifySignupEmail: verifyEmail,
  uniqPhone: phoneUniq,
  logout: logout,
  updateProfile: updateProfile,
  updatePassword: updatePassword,
  getUser: getUser };
  
})

finvese.factory('Countries', function($http, $q, localStorageService, myConfig) {
  var getData = function() {
    var results = {};
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
      $http({method:"GET", url:myConfig.api+"/api/v1/countries", headers:{"Authorization":token}}).success(function(result){
        console.log("in factory");
        console.log(result);
        //results.data = result.countries;
        //deferred.resolve(results);
        deferred.resolve(result);
      });
      return deferred.promise;
    };
  return { all: getData };  
})

finvese.factory('ForgetPassword', function($http, $q, localStorageService, myConfig) {
  var sendCode = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
      $http({method:"POST", url:myConfig.api+"/api/v1/forget_passwords/send_code", data:data_query, headers:{"Authorization":token}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });;
      return deferred.promise;
    };

  var updatePassword = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
      $http({method:"POST", url:myConfig.api+"/api/v1/forget_passwords/update_password", data:data_query, headers:{"Authorization":token}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });
      return deferred.promise;
    };

  var verifyToken = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();
    console.log(data_query)
      $http({method:"GET", url:myConfig.api+"/users/password/edit", params:data_query, headers:{"Authorization":token}}).success(function(result){
        deferred.resolve(result);
      }).error(function(result){
        deferred.reject(result);
      });
      return deferred.promise;
    };

  var updateProfile = function(data_query) {
    token = localStorageService.get('user_token');
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/users/update_user", headers:{"Authorization":token}, data:{user: data_query}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { sendCode: sendCode,
  updatePassword: updatePassword,
  verifyPasswordToken: verifyToken,
  updateProfile: updateProfile };  
})

  $scope.empoweringInvestors= function() {
  Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#empoweringInvestors').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>68<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>32<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                }]
            }]
        });
   }

   $scope.empoweringInvestors();

   $scope.enablingAdvisor= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enablingAdvisor').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>68<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>32<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                }]
            }]
        });
   }

   $scope.enablingAdvisor();

   $scope.enhancingPerformance= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancingPerformance').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>68<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>32<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                }]
            }]
        });
   }

   $scope.enhancingPerformance();

   $scope.enhancingPerformance2= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancingPerformance2').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>70<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>35<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }]
            }]
        });
   }

   $scope.enhancingPerformance2();

   $scope.enhancingPerformance3= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancingPerformance3').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>72<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>38<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                },{
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }]
            }]
        });
   }

   $scope.enhancingPerformance3();

   $scope.enhancingPerformance4= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancingPerformance4').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>74<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>40<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ]
            }]
        });
   }

   $scope.enhancingPerformance4();

   $scope.enhancingPerformance5= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancingPerformance5').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>76<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>42<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [ {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                }, {
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }]
            }]
        });
   }

   $scope.enhancingPerformance5();

   $scope.enablingAdvisors= function() {
  Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enablingAdvisors').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'-35',
            },
            exporting: { enabled: false },
            title: {
                text:' ',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },

            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                }]
            }]
        });
   }

   $scope.enablingAdvisors();

   $scope.enablingadvertisers2= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enablingadvertisers2').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>70<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>35<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }]
            }]
        });
   }

   $scope.enablingadvertisers2();

   
   $scope.enablingadvertisers3= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enablingadvertisers3').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>72<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>38<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                },{
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }]
            }]
        });
   }

   $scope.enablingadvertisers3();

   $scope.enablingadvertisers4= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enablingadvertisers4').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>74<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>40<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ]
            }]
        });
   }

   $scope.enablingadvertisers4();

   $scope.enablingadvertisers5= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enablingadvertisers5').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>76<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>42<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [ {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                }, {
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }]
            }]
        });
   }
   $scope.enablingadvertisers5();

   $scope.enhancing2= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancing2').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>70<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>35<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }]
            }]
        });
   }

   $scope.enhancing2();

   
   $scope.enhancing3= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancing3').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>72<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>38<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ,{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                },{
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }]
            }]
        });
   }

   $scope.enhancing3();

   $scope.enhancing4= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancing4').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>74<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>40<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [{
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }, {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                } ]
            }]
        });
   }

   $scope.enhancing4();

   $scope.enhancing5= function() {
    Highcharts.setOptions({
      colors: ['#1b6845', '#8085E9', '#D00049', '#347DCF', '#64D0AE', '#ED7A2A','#656FEB','#2C8D5C',
               '#2C57BE','#FF0000','#333333','#F2AE33','#60C379','#D00049']
    });
        $('#enhancing5').highcharts({
            chart: {
              type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft:'0',
            },
            exporting: { enabled: false },
            title: {
                text: '<div><h2>76<br><p>STOCKS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                x:-29,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            subtitle: {
                text: '<div><h2>42<br><p>BONDS</p></h2></div>',
                align: 'center',
                verticalAlign: 'middle',
                y: -1,
                x:20,
                style:{
                  fontWeight:'normal',
                  fontSize: '18px !important',
                  color:'#6f7e8d',
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                enabled: false,
            },

            plotOptions: {

             pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false
                }
           },

            series: [{
                name: 'Investments',
                colorByPoint: true,
                innerSize: '70%',
                data: [ {
                    name: 'STDCHART',
                    y: 11.03,
                    color: '#70aee0'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#86b47b'
                },{
                    name: 'CS',
                    y: 10.91,
                    color: '#73b27a'
                },{
                    name: 'CS',
                    y: 5.51,
                    color: '#5ea157'
                },{
                    name: ' TROWE',
                    y: 5.58,
                    color: '#478553',
                }, {
                    name: 'HENDERSON',
                    y: 29.33,
                    color: '#1b6845'
                }, {
                    name: 'STDCHART',
                    y: 6.73,
                    color: '#0f4034'
                }, {
                    name: 'PIMCO',
                    y: 4.97,
                    color: '#0a376b'
                }, {
                    name: 'CS',
                    y: 12.11,
                    color: '#0a54a2'
                }, {
                    name: 'HENDERSON',
                    y: 6.03,
                    color: '#1981ce'
                }]
            }]
        });
   }
   $scope.enhancing5();

  $scope.advisorBar = function () {
        Highcharts.setOptions({
           colors: ['#4374AE', '#3E6697', '#335680', '#92A5D0', '#B7C6E0']
        });
        $('#advisore-bar').highcharts({

            chart: {
                type: 'bubble',
                plotBorderWidth:0,
                zoomType: 'xy',
                marginLeft:50,
            },
            exporting: { enabled: false },
            legend: {
                enabled: false
            },

            title: {
                text: ' ',
                 style:{
                         color:'#333 !important',
                         fontWeight:'bold !important',
                         fontSize: '18px !important',
                       },
            },
            xAxis: {
                gridLineWidth: 0,
                enabled:true,
                title: {
                    text: ' ',
                     style:{
                             color:'#333 !important',
                             fontWeight:'bold !important',
                             fontSize: '12px !important',
                          },
                     y:-50,
                     x:110
                },
                labels: {
                    format: '{value}%',
                    enabled:true,

                }
            },

            yAxis: {
                lineWidth: 1,
                min: 0,
                offset: -1,
                tickWidth: 1,
                title: {
                    text: ' ',
                    style:{
                             color:'#333 !important',
                             fontWeight:'bold !important',
                             fontSize: '12px !important',
                          },
                    x:70,
                    y:-60,
                },

                labels: {
                    format: '{value}%'
                },
                gridLineWidth: 0,
                maxPadding: 0.2
            },

            tooltip: {
                useHTML: true,
                headerFormat: '<table>',
                pointFormat: '<tr><th colspan="2"><h5>{point.country}</h5></th></tr>' +
                    '<tr><th>Risk:</th><td>{point.x}%</td></tr>' +
                    '<tr><th>Return:</th><td>{point.y}%</td></tr>' +
                    '<tr><th>AUM (m):</th><td>{point.z}</td></tr>',
                footerFormat: '</table>',
                followPointer: true,
                enabled:false,
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}',
                         style:{
                             color:'#333 !important',
                             fontSize: '8px !important',
                          },
                    }
                }
            },

            series: [{
                data: [
                    { x: 14, y: 6.5,    z:6,  name: '4', country: 'Client 4' },
                    { x: 8,  y: 5,    z: 11, name: '2', country: 'Client 2' },
                    { x: 19, y: 10,   z: 10, name: '1', country: 'Client 1' },
                    { x: 28, y: 15,   z: 50, name: '3', country: 'Client 3' },
                    { x: 10, y: 6,    z: 8,  name: '5', country: 'Client 5' },
                    { x: 18, y: 7.5,    z: 6,  name: '6', country: 'Client 6' },
                    { x: 14, y: 9,  z: 15, name: '12', country: 'Client 12' },
                    { x: 23, y: 9,    z: 10, name: '7', country: 'Client 7' },
                    { x: 26, y: 12,   z: 35, name: '13', country: 'Client 13' },
                    { x: 16, y: 7.5,  z: 8,  name: '8', country: 'Client 8' },
                    { x: 18, y: 9.8,  z: 8,  name: '10', country: 'Client 10' },
                    { x: 20, y: 9.4,  z: 4,  name: '9', country: 'Client 9' },
                    { x: 16, y: 7.5,  z: 10, name: '11', country: 'Client 11' },
                ]
            }]

        });
  }
  $scope.advisorBar();
})

finvese.controller('finveseController', function($rootScope, $scope, $window, $location, localStorageService, Advisors) {
      $rootScope.home = true;
      $rootScope.why  = false;
      $rootScope.service  = false;
      $rootScope.login = false;
      $rootScope.contact = false;
      $rootScope.signup = false;
      $rootScope.advisorname = "Advisor Name";
      $rootScope.current_advisor = null;

      //console.log("in finveseController");
      //localSt orageService.remove('current_user');

      //$scope.getUser = function(){
      //  $rootScope.current_user = localStorageService.get('current_user');
      //  if($rootScope.current_user)
      //    return true;
      //  else
      //    return false;
      //}

      $scope.getUser = function(){
        $rootScope.current_user = localStorageService.get('current_user');
        if($rootScope.current_user) {
          $rootScope.current_login = localStorageService.get('current_login');
          var d = new Date();
          var m = d.getMonth()+1;
          var ms = m.toString();
          if (m < 10) ms = "0" + ms;
          var td = d.getFullYear().toString() + ms + d.getDate().toString();

          if ($rootScope.current_login) {
            // Login date defined
            if (td === $rootScope.current_login) {
              // Valid login date
              return true;
            } else {
              // Invalid login date
              localStorageService.remove('current_user');
              localStorageService.remove('current_login');
              return false;
            }
          } else {
            // Login date not defined
            $rootScope.current_login = td;
            localStorageService.add('current_login', td);
            return true;
          }
        }
        else {
          // Not logged in
          return false;
        }
      }

      // Loop up the advisor name
      //$rootScope.current_uid = 1;
      if ($scope.getUser()) {
        console.log("calling setAdvisor");
        setAdvisor($rootScope);
  
        if ($rootScope.current_user) {
          console.log("setting advisorId");
          var advisorId = $rootScope.current_user.user.id;
      
          var AdvisorPromise = Advisors.getOne(advisorId);
          AdvisorPromise.then(function(result) {
            console.log("in advisor promise "+advisorId);
            console.log(result);

            if (result) {
              console.log(result.advisor_name);
              $rootScope.advisorname = result.advisor_name;
              $rootScope.default_language = result.default_language;
            }
          });
        }
      }

      // $rootScope.goSigninDashboard = function(index) {
      //   $rootScope.clientListMenu = true;
      //   console.log(client_list[index]);
      //   $rootScope.client_id = client_list[index].id;
      //   $scope.renderPage("/client-dashboard");
      // }
      $rootScope.hideHeader = function(){
        $('.navbar-fixed-top').addClass('ng-hide');
      }

      $rootScope.color = {
        red: Math.floor(Math.random() * 255),
        green: Math.floor(Math.random() * 255),
        blue: Math.floor(Math.random() * 255)
      };

      $scope.updateGoogle =function(){
        return true;
      }
      // $rootScope.dropdown = function(is_last){
      //   if(is_last){
      //     $('select').material_select();
      //   }
      // }



	$scope.isCordova = function(){
    return (typeof(cordova) !== 'undefined');
  }

  $scope.reloadPage = function(){
    console.log($location.url());
    if($location.url().match('/search/false'))
      $scope.renderPage('/search')
    else
      if($location.url().match('/search'))
        $scope.renderPage('/search/false')
    else
      $route.reload();
  }

  $scope.goToSiteUrl = function(site_url){
    $window.location.href = site_url;
  }




  if (typeof(cordova) === 'undefined') {
    $scope.goToSite = function(site_url){
    console.log(site_url);
      $window.location.href = site_url;
    }
  }
  else{
    if(navigator.userAgent.match(/Android/i)){

      $scope.goToSite = function(site_url){
        window.plugins.webintent.startActivity({
          action: window.plugins.webintent.ACTION_VIEW,
          url: site_url },
          function() {},
          function() {alert('Please install the app')}
        );
      }
    }else{
      if(navigator.userAgent.match(/iPhone/i)){
        $scope.goToSite = function(site_url){
          $window.location.href = site_url;
        }
      }

    }
  }



  // $scope.goHome = function(){
  //   $scope.renderPage('/brands');
  // }


  $scope.goBack = function(){
    $scope.renderPage($window.history.back());
  }


  $rootScope.$on("$routeChangeStart", function(){
    $rootScope.loading = true;
    $scope.current_location_loading = true;
    $rootScope.current_page = $location.path().replace('/', '');
    $rootScope.pageTitle = false;
    console.log($location.path().replace('/', ''));
  });

  $rootScope.$on("$routeChangeSuccess", function(){
    $rootScope.loading = false;
  });

  $rootScope.$on("$stateChangeSuccess", function(){
    console.log('loaded');
  });

  $scope.renderPage = function(path, query_params){
    console.log('pathpathpath '+path+" "+$rootScope.client_id);
    //if (path == '/signout') 
    //{
    //  console.log("in signout");
    //  localStorageService.remove('current_user');
    //}
    var c = $rootScope.clientId;
    if (!c) { c = $rootScope.client_id; $rootScope.clientId = $rootScope.client_id; }

	  if (c) 
    {
      console.log("inside if "+c);

      if (path == "/product_selection" || path == "/trades" || path == "/assets_class" ||
            path == "/assets_allocation" || path == "/risks_calibrations" || path == "/client-dashboard" ||
            path == "/client_history" || path == "/clients_details" || path == "/savings" ||
            path == "/investments" || path == "/income" || path == "/expenses" || path == "/asset-input" ||
            path == "/cashflow_report" || path == "/cashflow_settings" || path == "/trade_history") {
		    //console.log("xxx "+$rootScope.client_id);
		    path = path + "/" + c.toString();
      }
	  }

    if(query_params)
      path = path+query_params
    console.log(path);
    if(path == '/'){
      $rootScope.home = true;
      $rootScope.logo = true;
      $rootScope.scoop = true;
    }else{
      $rootScope.logo = false;
      console.log($rootScope.logo);
    }

    $rootScope.show_menu = false;
    $location.path(path);
    // $location.replace();
  };


})

finvese.config(function($routeProvider, $locationProvider, $provide) {
    $routeProvider
      .when('/', {
        templateUrl: 'templates/home.html',
        controller: 'homeController'
      }).
       when('/clients_details', {
        templateUrl: 'templates/edit_details.html',
        controller: 'clientDetailsController'
      }).
      // when('/signout', {        
      //templateUrl: 'templates/home.html',
      //controller: 'SignoutController'
      //}).
      //  when('/user-log', {
      //   templateUrl: 'templates/users-log.html',
      //   controller: 'createUserController'
      // }).
       when('/clients_details/:current_client', {
        templateUrl: 'templates/edit_details.html',
        controller: 'clientDetailsController'
      }).
      when('/client-dashboard', {
        templateUrl: 'templates/client-dashboard.html',
        controller: 'clientDashboardController'
      }).
      when('/asset-input/:current_client', {
        templateUrl: 'templates/asset-input.html',
        controller: 'assetInputController'
      }).
      when('/client-dashboard/:current_client', {
        templateUrl: 'templates/client-dashboard.html',
        controller: 'clientDashboardController'
      }).
       when('/edit_details/:slug/:clientIndex', {
        templateUrl: 'templates/edit_details.html',
        controller: 'clientDetailsController'
      }).
       when('/risks_calibrations', {
        templateUrl: 'templates/risks_calibrations.html',
        controller: 'riskCalibrationsController'
      }).
      when('/risks_calibrations/:current_client', {
        templateUrl: 'templates/risks_calibrations.html',
        controller: 'riskCalibrationsController'
      }).
       when('/assets_class', {
        templateUrl: 'templates/assets_class.html',
        controller: 'assetsClassController'
      }).
      when('/assets_class/:current_client', {
        templateUrl: 'templates/assets_class.html',
        controller: 'assetsClassController'
      }).
       when('/assets_allocation', {
        templateUrl: 'templates/assets_allocation.html',
        controller: 'assetsAllocationController'
      }).
      when('/assets_allocation/:current_client', {
        templateUrl: 'templates/assets_allocation.html',
        controller: 'assetsAllocationController'
      }).
       when('/advisor_allocation/clientId/0', {
        templateUrl: 'templates/advisor-assetclass.html',
        controller: 'assetsClassController'
      }).
        when('/advisor_assetAllocation', {
        templateUrl: 'templates/advisor-assetallocation.html',
        controller: 'assetsAllocationController'
      }).
       when('/investments', {
        templateUrl: 'templates/investments.html',
        controller: 'investmentsController'
      }).
      when('/investments/:current_client', {
        templateUrl: 'templates/investments.html',
        controller: 'investmentsController'
      }).
       when('/savings', {
        templateUrl: 'templates/savings_plan.html',
        controller: 'savingsController'
      }).
        when('/savings/:current_client', {
        templateUrl: 'templates/savings_plan.html',
        controller: 'savingsController'
      }).
       when('/income', {
        templateUrl: 'templates/other_income.html',
        controller: 'incomeController'
      }).
        when('/income/:current_client', {
        templateUrl: 'templates/other_income.html',
        controller: 'incomeController'
      }).
       when('/expenses', {
        templateUrl: 'templates/expenses.html',
        controller: 'expensesController'
      }).
       when('/expenses/:current_client', {
        templateUrl: 'templates/expenses.html',
        controller: 'expensesController'
      }).
       when('/clients_list',{
        templateUrl:'templates/clients_list.html',
        controller:'clientListController'
      }).
       when('/client_history',{
        templateUrl:'templates/client_history.html',
        controller:'clientHistoryController'
      }).
      when('/client_history/:current_client',{
        templateUrl:'templates/client_history.html',
        controller:'clientHistoryController'
      }).
      when('/trade_history',{
        templateUrl:'templates/trade_history.html',
        controller:'tradeHistoryController'
      }).
      when('/trade_history/:current_client',{
        templateUrl:'templates/trade_history.html',
        controller:'tradeHistoryController'
      }).
       when('/clients_list/:saveClient',{
        templateUrl:'templates/clients_list.html',
        controller:'clientListController'
      }).
       when('/product_selection', {
        templateUrl: 'templates/product-selection.html',
        controller: 'productSelectionController'
      }).
       when('/product_selection/:current_client', {
        templateUrl: 'templates/product-selection.html',
        controller: 'productSelectionController'
      }).
      when('/product_selection_new/:current_client', {
        templateUrl: 'app/products-selection/product-selection.html',
        controller: 'productSelectionControllerNew'
      }).
       when('/trades', {
        templateUrl: 'templates/trades.html',
        controller: 'tradesController'
      }).
      when('/trades/:current_client', {
        templateUrl: 'templates/trades.html',
        controller: 'tradesController'
      }).
       when('/overview', {
        templateUrl: 'templates/overview.html',
        controller: 'overviewController'
      }).
        when('/advisorDetails', {
        templateUrl: 'templates/advisor-details.html',
        controller: 'advisorDetailsController'
      }).
       when('/sidemenu', {
        templateUrl: 'templates/side_menu.html'
      }).
        when('/why', {
        templateUrl: 'templates/overview.html',
        controller: 'overviewController'
      }).
        when('/service', {
        templateUrl: 'templates/overview.html',
        controller: 'overviewController'
      }).
        when('/advisor', {
        templateUrl: 'templates/advisor.html',
        controller: 'advisorController'
      }).
        when('/cashflow_settings', {
        templateUrl: 'templates/cashflow_settings.html',
        controller: 'cashflowSettingsController'
      }).
        when('/cashflow_settings/:current_client', {
        templateUrl: 'templates/cashflow_settings.html',
        controller: 'cashflowSettingsController'
      }).
        when('/cashflow_report', {
        templateUrl: 'templates/cashflow_report.html',
        controller: 'cashflowReportController'
      }).
        when('/cashflow_report/:current_client', {
        templateUrl: 'templates/cashflow_report.html',
        controller: 'cashflowReportController'
      }).
        when('/contact', {
        templateUrl: 'templates/overview.html',
        controller: 'overviewController'
      })
})

finvese.factory('SwapRates', function($http, $q, $rootScope, myConfig) {
  var getLatest = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/swap_rates"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getLatest: getLatest};
})

finvese.factory('ExpectedReturns', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/expected_returns"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll};
})

finvese.factory('Advisor', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var deferred = $q.defer();
    strp = client_id.toString();
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateAdvisor = function(id, data_query) {
    console.log("inside updateAdvisor");
    console.log(data_query);
    strp = id.toString();

    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/advisor/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateAdvisor: updateAdvisor };
})


finvese.factory('Client', function($http, $q, $rootScope, myConfig) {
  var getAll = function(advisor_id) {
    var aid = 1;
    if (advisor_id)
      aid = advisor_id;
    var data_query = { 'advisor_id': aid };
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client", headers:{}, params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(advisor_id, client_id) {
    var deferred = $q.defer();
    var strp = "1";
    if (client_id)
      strp = client_id.toString();
    console.log(strp);
    var dq = {'advisor_id': advisor_id, 'client_id': client_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/client/"+strp, headers:{}, params: dq}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var createClient = function(data_query) {
    console.log("inside createclient");
    console.log(data_query);
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client/", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var updateClient = function(id, data_query) {
    console.log("inside updateClient");
    console.log(data_query);
    strp = id.toString();

    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var deleteClient = function(advisor_id, client_id) {
    console.log("inside deleteClient");
    var strp = "1";
    if (client_id)
      strp = client_id.toString();
    var data_query = { 'advisor_id': advisor_id, 'client_id': client_id, 'delete_flag': 1  };
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, createClient: createClient, updateClient: updateClient, deleteClient: deleteClient };
})


finvese.factory('SavingsPlan', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/savings_plan"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var deferred = $q.defer();
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/savings_plan/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var createSavings = function(data_query) {
    console.log("inside createSavings");
    console.log(data_query);
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/savings_plan/", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var updateSavings = function(id, data_query) {
    console.log("inside updateSavings");
    console.log(data_query);
    var strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/savings_plan/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var deleteSavings = function(id, data_query) {
    console.log("inside deleteSavings");
    console.log(data_query);
    var deferred = $q.defer();
    var strp = "-" + id.toString();
    $http({method:"POST", url:myConfig.api+"/api/v1/savings_plan/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, createSavings: createSavings, updateSavings: updateSavings, deleteSavings: deleteSavings };
})

finvese.factory('Investment', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/investment"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var deferred = $q.defer();
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/investment/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var createInvestment = function(data_query) {
    console.log("inside createInvestment");
    console.log(data_query);
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/investment/", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var updateInvestment = function(id, data_query) {
    console.log("inside updateInvestment");
    console.log(data_query);
    var strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/investment/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var deleteInvestment = function(id, data_query) {
    console.log("inside deleteInvestment "+id);
    console.log(data_query);
    var deferred = $q.defer();
    var strp = "-" + id.toString();
    $http({method:"POST", url:myConfig.api+"/api/v1/investment/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, createInvestment: createInvestment, updateInvestment: updateInvestment, deleteInvestment: deleteInvestment };
})

finvese.factory('Income', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/other_income"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    //console.log("in income getOne "+client_id);
    var deferred = $q.defer();
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/other_income/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var createIncome = function(data_query) {
    console.log("inside createIncome");
    console.log(data_query);
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/other_income/", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var updateIncome = function(id, data_query) {
    console.log("inside updateIncome");
    console.log(data_query);
    var strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/other_income/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var deleteIncome = function(id, ata_query) {
    console.log("inside deleteIncome");
    console.log(data_query);
    var deferred = $q.defer();
    var strp = "-" + id.toString();

    $http({method:"POST", url:myConfig.api+"/api/v1/other_income/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, createIncome: createIncome, updateIncome: updateIncome, deleteIncome: deleteIncome };
})

finvese.factory('Expense', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/expense"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var deferred = $q.defer();
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/expense/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateExpense = function(id, data_query) {
    console.log("inside updateExpense");
    console.log(data_query);
    var strp = id.toString();

    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/expense/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var createExpense = function(data_query) {
    console.log("inside createExpense");
    console.log(data_query);
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/expense/", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var deleteExpense = function(id, data_query) {
    console.log("inside deleteExpense id="+id);
    console.log(data_query);
    strp = "-"+id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/expense/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateExpense: updateExpense, createExpense: createExpense, deleteExpense: deleteExpense };
})


finvese.factory('Montecarlos', function($http, $q, $rootScope, myConfig) {
  var getAll = function(client_id) {
    console.log("in getAll");
    data_query = {client_id: client_id};
    console.log(data_query);
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/montecarlos", params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id, data_query) {
    var deferred = $q.defer();
    var strp;
    //console.log("in Montecarlos.getOne "+client_id);
    if (client_id)
      strp = client_id.toString();
    else
      strp = "1";
    $http({method:"GET", url: myConfig.api+"/api/v1/montecarlos/"+strp, params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateOne = function(data_query) {
    var deferred = $q.defer();
    console.log("in Montecarlos.updateOne");
    $http({method:"PUT", url: myConfig.api+"/api/v1/montecarlos/1", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, updateOne: updateOne};
})



finvese.factory('AssetClass', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/asset_class"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll};
})

finvese.factory('RiskProfiles', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/riskprofile"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var deferred = $q.defer();
    var strp;
    if (client_id)
      strp = client_id.toString();
    else
      strp = "1";
    $http({method:"GET", url: myConfig.api+"/api/v1/riskprofile/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateRiskProfile = function(data_query) {
    console.log("inside updateRiskProfile");
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/riskprofile/1", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateRiskProfile: updateRiskProfile };
})

finvese.factory('ModelPortfolio', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/model_portfolio"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
      strp = client_id.toString();
    else
      strp = "1";

    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/model_portfolio/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne };
})

finvese.factory('ClientPortfolio', function($http, $q, $rootScope, myConfig) {
  var getAll = function(client_id) {
    var deferred = $q.defer();
    var data_query;
    if (client_id)
      data_query = {'client_id': client_id};
    else
      data_query = {'client_id': 0};

    $http({method:"GET", url: myConfig.api+"/api/v1/client_portfolio", params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var deferred = $q.defer();
    var strp;
    if (client_id)
      strp = client_id.toString();
    else
      strp = "0";
    $http({method:"GET", url: myConfig.api+"/api/v1/client_portfolio/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateClientPortfolio = function(data_query) {
    console.log("inside updateClientPortfolio");
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client_portfolio/1", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  var saveClientPortfolio = function(id, data_query) {
    console.log("inside saveClientPortfolio");
    console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client_portfolio/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateClientPortfolio: updateClientPortfolio, saveClientPortfolio: saveClientPortfolio };
})

finvese.factory('Product', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/product"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(asset_class, data_query) {
    var deferred = $q.defer();
    if (asset_class)
      strp = asset_class.toString();
    else
      strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/product/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateProducts = function(data_query) {
    console.log("inside updateProducts");
    console.log(data_query);

    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/product/1", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateProducts: updateProducts };
})

finvese.factory('ProductAllocation', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/product_allocation"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id, asset_class) {
    data_query = {'client_id': client_id, 'asset_class': asset_class};
    console.log("inside ProductAllocation getOne");
    console.log(data_query);

    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/product_allocation/1", params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
   };
   var updateProductAllocations = function(data_query) {
    //console.log("inside ProductAllocation updateProductAllocation");
    //console.log(data_query);

    var deferred = $q.defer();
    $http({method:"POST", url: myConfig.api+"/api/v1/product_allocation/1", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
   };
   return { getAll: getAll, getOne: getOne, updateProductAllocations: updateProductAllocations };
})

finvese.factory('ProductPrice', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/product_price"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(fund_id, data_query) {
    //console.log("in ProductPrice getOne "+fund_id);
    //console.log(data_query);
    var deferred = $q.defer();
    if (fund_id)
        strp = fund_id.toString();
    else
        strp = "1";
    //console.log(strp);

    $http({method:"GET", url: myConfig.api+"/api/v1/product_price/"+strp, headers:{}, params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})


finvese.factory('MarketPrice', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/market_price"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(asset_id) {
    var deferred = $q.defer();
    if (asset_id)
        strp = asset_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/market_price/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})

finvese.factory('Index', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/index"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(asset_id) {
    var deferred = $q.defer();
    if (asset_id)
        strp = asset_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/index/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})

finvese.factory('IndexPrice', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/index_price"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(asset_id) {
    var deferred = $q.defer();
    if (asset_id)
        strp = asset_id.toString();
    else
        strp = "1";
    console.log(strp);
    $http({method:"GET", url: myConfig.api+"/api/v1/index_price/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})


finvese.factory('ClientSelection', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client_selection"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client_selection/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateSelection = function(data_query) {
    console.log("inside updateSelection function");
    console.log(data_query);
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client_selection/1", headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, updateSelection: updateSelection };
})

finvese.factory('ProductHolding', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/product_holding"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/product_holding/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})


finvese.factory('AdvisorPortfolio', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor_portfolio"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(advisor_id) {
    var strp;
    if (advisor_id)
        strp = advisor_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor_portfolio/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var savePortfolio = function(id, data_query) {
    console.log("inside savePortfolio");
    //console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/advisor_portfolio/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, savePortfolio: savePortfolio };
})

finvese.factory('AdvisorAllocation', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor_allocation"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(advisor_id) {
    var strp;
    if (advisor_id)
        strp = advisor_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor_allocation/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var saveAdvisorAllocation = function(id, data_query) {
    console.log("inside saveAdvisorAllocation");
    console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/advisor_allocation/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, saveAdvisorAllocation: saveAdvisorAllocation };
})

finvese.factory('SavedPortfolio', function($http, $q, $rootScope, myConfig) {
  var getAll = function(advisor_id, data_query) {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/saved_portfolio", headers:{}, params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/saved_portfolio/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var savePortfolio = function(client_id, data_query) {
    console.log("inside savePortfolio");
    //console.log(id);
    var strp = "1";
    if (client_id)
      strp = client_id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/saved_portfolio/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, savePortfolio: savePortfolio };
})

finvese.factory('SavedAllocation', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/saved_allocation"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/saved_allocation/"+strp}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var saveAllocation = function(id, data_query) {
    console.log("inside saveAdvisorAllocation");
    console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/saved_allocation/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne, saveAllocation: saveAllocation };
})

finvese.factory('SavedRiskProfiles', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/saved_riskprofile"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id, data_query) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/saved_riskprofile/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var saveProfile = function(id, data_query) {
    console.log("inside savePortfolio");
    //console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/saved_riskprofile/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, saveProfile: saveProfile };
})

finvese.factory('ClientHoldings', function($http, $q, $rootScope, myConfig) {
  var getAll = function(advisor_id) {
    var aid = 1;
    if (advisor_id)
        aid = advisor_id;
    var data_query = { 'advisor_id': aid};
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client_holding", headers:{}, params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id, data_query) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client_holding/"+strp, headers:{}, params: data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateHolding = function(id, data_query) {
    console.log("inside updateHolding");
    //console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client_holding/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateHolding: updateHolding };
})

finvese.factory('ClientTrades', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client_trade"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id, data_query) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/client_trade/"+strp, headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateTrade = function(id, data_query) {
    console.log("inside updateTrade");
    //console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/client_trade/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateTrade: updateTrade };
})


finvese.factory('Advisors', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(advisor_id) {
    var strp;
    if (advisor_id)
        strp = advisor_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    var data_query = { 'advisor_id': advisor_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/advisor/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateAdvisor = function(id, data_query) {
    console.log("inside updateTrade");
    //console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/advisor/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateAdvisor: updateAdvisor };
})


finvese.factory('CashBonds', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/cash_bond"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(bond_id) {
    var strp;
    if (bond_id)
        strp = bond_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    var data_query = { 'bond_id': bond_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/cash_bond/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})


finvese.factory('AumValues', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/aum_value"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    var data_query = { 'client_id': client_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/aum_value/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})


finvese.factory('CashHoldings', function($http, $q, $rootScope, myConfig) {
  var getAll = function(advisor_id) {
    var advisorid = 1;
    if (advisor_id)
      advisorid = advisor_id;
    console.log("advisor_id="+ advisorid);
    var data_query = { 'client_id': 0, 'advisor_id': advisorid };
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/cash_holding", headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    var data_query = { 'client_id': client_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/cash_holding/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateCash = function(id, data_query) {
    console.log("inside updateCash");
    //console.log(id);
    strp = id.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/cash_holding/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getAll: getAll, getOne: getOne, updateCash: updateCash };
})


finvese.factory('ExchangeRate', function($http, $q, $rootScope, myConfig) {
  var getAll = function() {
    var deferred = $q.defer();
    $http({method:"GET", url: myConfig.api+"/api/v1/exchange_rate"}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(currency) {
    var strp = "USD";
    if (currency)
        strp = currency;
    var deferred = $q.defer();
    //var data_query = { 'client_id': client_id };
      $http({method:"GET", url: myConfig.api+"/api/v1/exchange_rate/"+strp, headers:{}}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})

finvese.factory('CalculatedRisks', function($http, $q, $rootScope, myConfig) {
  var getAll = function(advisor_id) {
    //var strp;
    //if (advisor_id)
    //    strp = advisor_id.toString();
    //else
    //    strp = "1";
    var deferred = $q.defer();
    var data_query = { 'advisor_id': advisor_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/calculated_risk", headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var getOne = function(client_id) {
    var strp;
    if (client_id)
        strp = client_id.toString();
    else
        strp = "1";
    var deferred = $q.defer();
    var data_query = { 'client_id': client_id };
    $http({method:"GET", url: myConfig.api+"/api/v1/calculated_risk/"+strp, headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll, getOne: getOne };
})

finvese.factory('Translations', function($http, $q, $rootScope, myConfig) {
  var getAll = function(locale) {
    var deferred = $q.defer();
    var data_query = { 'locale': locale };
    $http({method:"GET", url: myConfig.api+"/api/v1/translation", headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getAll: getAll };
})


finvese.factory('Cashflows', function($http, $q, $rootScope, myConfig) {
  var getOne = function(clientid) {
    var deferred = $q.defer();
    var data_query = { 'client_id': clientid };
    $http({method:"GET", url: myConfig.api+"/api/v1/cashflow", headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };

  return { getOne: getOne };
})

finvese.factory('CashflowDefaults', function($http, $q, $rootScope, myConfig) {
  var getOne = function(clientid) {
    var strp = "1";
    if (clientid) 
      strp = clientid.toString();
    var deferred = $q.defer();
    var data_query = { 'client_id': clientid };
    $http({method:"GET", url: myConfig.api+"/api/v1/cashflow_default/"+strp, headers:{}, params:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });

    return deferred.promise;
  };
  var updateValues = function(clientid, data_query) {
    console.log("inside updateValues");
    //console.log(id);
    var strp = "1";
    if (clientid) 
      strp = clientid.toString();
    var deferred = $q.defer();

    $http({method:"POST", url:myConfig.api+"/api/v1/cashflow_default/"+strp, headers:{}, data:data_query}).success(function(result){
      deferred.resolve(result);
    }).error(function(result){
      deferred.reject(result);
    });
    return deferred.promise;
  };
  return { getOne: getOne, updateValues: updateValues };
})

compute_cashflows = function(settings, saving_plans, investments, expenses, other_income, debug)
{
  var today = new Date();
  var CurrentYear = today.getFullYear();
  console.log("CurrentYear "+ CurrentYear);
  //var debug = true;

  // Average Age of Mortality for the Country
  var AvgMortalityAge = settings.avg_mortality_age;

  // 10 Year Inflation Expectation
  var InflationExpectation = settings.inflation_expectation/100.0;

  // Default Yield for Taxable Account
  var Yield = settings.default_yield/100.0;

  var investmentreturn = settings.investment_return/100.0;

  // Fixed tax rates
  var FixedOrdinaryIncomeTaxRate = 1.0;
  var FixedCapitalGainsTaxRate = 1.0;
  var useTaxTables = false;
 
  // Tax tables
  var OrdinaryIncomeTaxRate1 = settings.ordinary_tax1/100.0;
  var OrdinaryIncomeTaxRate2 = settings.ordinary_tax2/100.0;
  var OrdinaryIncomeTaxRate3 = settings.ordinary_tax3/100.0;
  var CapitalGainsTaxRate1 = settings.capital_tax1/100.0;
  var CapitalGainsTaxRate2 = settings.capital_tax2/100.0;
  var CapitalGainsTaxRate3 = settings.capital_tax3/100.0;
  var TaxBracket1 = settings.taxbracket1;
  var TaxBracket2 = settings.taxbracket2;
  var TaxBracket3 = settings.taxbracket3;

  if (debug)
  {
    AvgMortalityAge = 85; 
    InflationExpectation = 0.03;
    Yield = 0.025;
    investmentreturn = 0.07;
    OrdinaryIncomeTaxRate1 = 0.15;
    OrdinaryIncomeTaxRate2 = 0.30;
    OrdinaryIncomeTaxRate3 = 0.50;
    CapitalGainsTaxRate1 = 0.0;
    CapitalGainsTaxRate2 = 0.10;
    CapitalGainsTaxRate3 = 0.20;
    TaxBracket1 = 100000;
    TaxBracket2 = 300000;
    TaxBracket3 = 1000000;
  }

  console.log("CurrentYear "+ CurrentYear);
  console.log("AvgMortalityAge "+AvgMortalityAge);
  console.log("InflationExpectation "+InflationExpectation);
  console.log("TaxBracket1 "+TaxBracket1);
  console.log("TaxBracket2 "+TaxBracket2);
  console.log("OrdinaryIncomeTaxRate1 "+OrdinaryIncomeTaxRate1);
  console.log("OrdinaryIncomeTaxRate2 "+OrdinaryIncomeTaxRate2);
  console.log("OrdinaryIncomeTaxRate3 "+OrdinaryIncomeTaxRate3);
  console.log("CapitalGainsTaxRate1 "+CapitalGainsTaxRate1);
  console.log("CapitalGainsTaxRate2 "+CapitalGainsTaxRate2);
  console.log("CapitalGainsTaxRate3 "+CapitalGainsTaxRate3);
  console.log("FixedOrdinaryIncomeTaxRate "+FixedOrdinaryIncomeTaxRate);
  console.log("FixedCapitalGainsTaxRate "+FixedCapitalGainsTaxRate);
  console.log("Yield "+ Yield);

  // Advisor Fees
  var AdvisorFeeAUM1  = settings.feeaum1;
  var AdvisorFeeAUM2  = settings.feeaum2;
  var AdvisorFeeAUM3  = settings.feeaum3;
  var AdvisorFeePercent1 = settings.feepercent1/100.0;
  var AdvisorFeePercent2 = settings.feepercent2/100.0;
  var AdvisorFeePercent3 = settings.feepercent3/100.0;
  var AdvisorFeeFixed = settings.feefixed;

  if (debug)
  {
    AdvisorFeeAUM1  = 1000000;
    AdvisorFeeAUM2  = 10000000;
    AdvisorFeeAUM3  = 20000000;
    AdvisorFeePercent1 = 0.03;
    AdvisorFeePercent2 = 0.02;
    AdvisorFeePercent3 = 0.01;
    AdvisorFeeFixed = -5000;
  }

  console.log("AdvisorFeeAUM1 "+AdvisorFeeAUM1);
  console.log("AdvisorFeeAUM2 "+AdvisorFeeAUM2);
  console.log("AdvisorFeeAUM3 "+AdvisorFeeAUM3);
  console.log("AdvisorFeePercent1 "+AdvisorFeePercent1);
  console.log("AdvisorFeePercent2 "+AdvisorFeePercent2);
  console.log("AdvisorFeePercent3 "+AdvisorFeePercent3);
  console.log("AdvisorFeeFixed "+AdvisorFeeFixed);

  // Time Horizon
  var TimeHorizonYears = 0; // End Year of analysis (if fixed)
  var TimeHorizonFixed = 0;     // 0 = Fixed horizon

  console.log("TimeHorizonFixed "+TimeHorizonFixed);
  console.log("TimeHorizonYears "+TimeHorizonYears);

  // Client Data
  var IndividualBirthYear = settings.client_birthyear;
  var IndividualRetirementAge = settings.client_retirement;
  var IndividualWageIncome = settings.client_income;
  var IndividualWageGrowth = settings.client_income_growth/100.0;
  var IndividualMortalityYear = 0;
  var IndividualRetirementYear = 0;

  var SpouseBirthYear = settings.spouse_birthyear;
  var SpouseRetirementAge = settings.spouse_retirement;
  var SpouseWageIncome = settings.spouse_income;
  var SpouseWageGrowth = settings.spouse_income_growth/100.0;
  var SpouseMortalityYear = 0;
  var SpouseRetirementYear = 0;
  var married = "Yes";

  if (debug)
  {
    IndividualBirthYear = 1964;
    IndividualRetirementAge = 65;
    IndividualWageIncome = 100000;
    IndividualWageGrowth = 0.01;

    SpouseBirthYear = 1963;
    SpouseRetirementAge = 65;
    SpouseWageIncome = 60000;
    SpouseWageGrowth = 0.03;
    married = "Yes";
  }

  IndividualMortalityYear = IndividualBirthYear + AvgMortalityAge;
  IndividualRetirementYear = IndividualBirthYear + IndividualRetirementAge;
  SpouseMortalityYear = SpouseBirthYear + AvgMortalityAge;
  SpouseRetirementYear = SpouseBirthYear + SpouseRetirementAge;

  console.log("IndividualBirthYear "+IndividualBirthYear);
  console.log("IndividualMortalityYear "+IndividualMortalityYear);
  console.log("IndividualWageIncome "+IndividualWageIncome);
  console.log("IndividualWageGrowth "+IndividualWageGrowth);
  console.log("IndividualRetirementAge "+IndividualRetirementAge);
  console.log("IndividualRetirementYear "+IndividualRetirementYear);
  console.log("SpouseBirthYear "+SpouseBirthYear);
  console.log("SpouseMortalityYear "+SpouseMortalityYear);
  console.log("SpouseWageIncome "+SpouseWageIncome);
  console.log("SpouseWageGrowth "+SpouseWageGrowth);
  console.log("SpouseRetirementAge "+SpouseRetirementAge);
  console.log("SpouseRetirementYear "+SpouseRetirementYear);

  var TimeHorizonYear = TimeHorizonYears;
  if (TimeHorizonFixed == 0)
    TimeHorizonYear = max(IndividualMortalityYear, SpouseMortalityYear)
  console.log("TimeHorizonYear "+ TimeHorizonYear);

  // - Page 7
  var SumSavingsTaxable = new Array(100);
  var SumSavingsTaxDeferred = new Array(100);
  var SumSavingsTaxFree = new Array(100);
  var SumGrossSavings = new Array(100);
  var PortfolioReturnPercent = new Array(100);

  //for(var t=CurrentYear; t<TimeHorizonYear+1; t++)
  for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  {
    SumSavingsTaxable[t] = 0;
    SumSavingsTaxDeferred[t] = 0;
    SumSavingsTaxFree[t] = 0;
    SumGrossSavings[t] = 0;
    PortfolioReturnPercent[t] = investmentreturn;
  }

  var SavingPlans = saving_plans;
  if (debug) 
  {
    SavingPlans = getSavingPlans(debug);
  }

  var SavingsSaver = [];
  var SavingsAccountType = [];
  var SavingsDescription = [];
  var SavingsRangeType = [];
  var SavingsBeginAge = [];
  var SavingsEndAge = [];
  var SavingsBeginYear = [];
  var SavingsEndYear = [];
  var SavingsEntryType = [];
  var SavingsDollars = [];
  var SavingsInflationAdjustment = [];
  var SavingsPerCent = [];
  // 2-D
  var SavingsProvisional = [];
  var SavingsTaxable = [];
  var SavingsTaxDeferred = [];
  var SavingsTaxFree = [];
  if (SavingPlans.length > 0)
  {
    SavingsSaver = new Array(SavingPlans.length);
    SavingsAccountType = new Array(SavingPlans.length);
    SavingsDescription = new Array(SavingPlans.length);
    SavingsRangeType = new Array(SavingPlans.length);
    SavingsBeginAge = new Array(SavingPlans.length);
    SavingsEndAge = new Array(SavingPlans.length);
    SavingsBeginYear = new Array(SavingPlans.length);
    SavingsEndYear = new Array(SavingPlans.length);
    SavingsEntryType = new Array(SavingPlans.length);
    SavingsDollars = new Array(SavingPlans.length);
    SavingsInflationAdjustment = new Array(SavingPlans.length);
    SavingsPerCent = new Array(SavingPlans.length);

    for (var j=0; j<SavingPlans.length; j++)
    {
      SavingsProvisional[j] = new Array(100);
      SavingsTaxable[j] = new Array(100);
      SavingsTaxDeferred[j] = new Array(100);
      SavingsTaxFree[j] = new Array(100); 
    }
  }

  for(var x=0; x<SavingPlans.length; x++)
  {
    SavingsSaver[x] = SavingPlans[x].saver_name;
    SavingsAccountType[x] = SavingPlans[x].taxable;
    SavingsDescription[x] = SavingPlans[x].description;
    SavingsRangeType[x] = SavingPlans[x].date_range_in;
    if (SavingsRangeType[x] == "Age") 
    {
      SavingsBeginAge[x] = SavingPlans[x].date_range_begin;
      SavingsEndAge[x] = SavingPlans[x].date_range_end;
      if (SavingsSaver[x] == "Client")
      {
        SavingsBeginYear[x] = IndividualBirthYear + SavingsBeginAge[x];
        SavingsEndYear[x] = IndividualBirthYear + SavingsEndAge[x];
      }
      else
      {
        SavingsBeginYear[x] = SpouseBirthYear + SavingsBeginAge[x]
        SavingsEndYear[x] = SpouseBirthYear + SavingsEndAge[x];
      }
    }
    else
    {
      SavingsBeginYear[x] = SavingPlans[x].date_range_begin;
      SavingsEndYear[x] = SavingPlans[x].date_range_end;
    }

    SavingsEntryType[x] = SavingPlans[x].measure;
    if (SavingsEntryType[x] == "$")
    {
      SavingsDollars[x] = SavingPlans[x].amount;
      if (SavingPlans[x].inflation == "Yes")
        SavingsInflationAdjustment[x] = InflationExpectation;
      else
        SavingsInflationAdjustment[x] = 0;
    }
    else
    {
      SavingsPerCent[x] = SavingPlans[x].amount / 100.0;
      if (SavingsSaver[x] == "Client")
      {
        SavingsDollars[x] = IndividualWageIncome * SavingsPerCent[x]; 
        SavingsInflationAdjustment[x] = InflationExpectation + IndividualWageGrowth;
      }
      else
      { 
        SavingsDollars[x] = SpouseWageIncome * SavingsPerCent[x]; 
        SavingsInflationAdjustment[x] = InflationExpectation + SpouseWageGrowth;
      }
    }

    // Page 9
    //for(var t=CurrentYear; t<TimeHorizonYear+1; t++)
    for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
    {
      //SavingsProvisional[x][t] = SavingsDollars[x]*(1+SavingsInflationAdjustment[x])**(t-CurrentYear);
      //SavingsProvisional[x][t] = SavingsDollars[x]*Math.pow(1+SavingsInflationAdjustment[x], t-CurrentYear);
      SavingsProvisional[x][t] = SavingsDollars[x]*Math.pow(1+SavingsInflationAdjustment[x], t);
      //console.log("SavingsProvisional "+SavingsProvisional[x][t]);

      if (t >= SavingsBeginYear[x]-CurrentYear && t <= SavingsEndYear[x]-CurrentYear) 
      {
        //console.log("SavingsAccountType "+SavingsAccountType[x]);
        if (SavingsAccountType[x] === "Taxable")
        {
          SavingsTaxable[x][t] = SavingsProvisional[x][t];
          SavingsTaxDeferred[x][t] = 0;
          SavingsTaxFree[x][t] = 0;
        }
        else
        {
          SavingsTaxable[x][t] = 0;
          if (SavingsAccountType[x] === "Tax-deferred")
          {
            SavingsTaxDeferred[x][t] = SavingsProvisional[x][t];
            SavingsTaxFree[x][t] = 0;
          }
          else
          {
            SavingsTaxDeferred[x][t] = 0;
            SavingsTaxFree[x][t] = SavingsProvisional[x][t];
          }
        }
      }
      else
      {
        SavingsTaxable[x][t] = 0;
        SavingsTaxDeferred[x][t] = 0;
        SavingsTaxFree[x][t] = 0;
      }
      //console.log("SavingsTaxFree "+SavingsTaxFree[x][t]);

    }

  } // end x

  // Page 10
  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  {
    for(var x=0; x<SavingPlans.length; x++)
    {
      SumSavingsTaxable[t] += SavingsTaxable[x][t];
      SumSavingsTaxDeferred[t] += SavingsTaxDeferred[x][t];
      SumSavingsTaxFree[t] += SavingsTaxFree[x][t];
    }
    SumGrossSavings[t] =  SumSavingsTaxable[t] + SumSavingsTaxDeferred[t] + SumSavingsTaxFree[t];

    if (debug) 
    {
      console.log("SumSavingsTaxable[t] "+ SumSavingsTaxable[t] + " SumSavingsTaxDeferred[t] " 
        + SumSavingsTaxDeferred[t] + " SumSavingsTaxFree[t] " + SumSavingsTaxFree[t] );
    }
  }

  // Page 12
  var TaxableAccountBeginValue = new Array(100);
  var TaxableAccountCostBasisBeginValue = new Array(100);
  var TaxableAccountTaxFreePercent = new Array(100);
  var TaxDeferredAccountBeginValue = new Array(100);
  var TaxFreeAccountBeginValue = new Array(100);
  var TotalInvestmentAccountBeginValue = new Array(100);
  var TaxableAccountTaxFreePercent0 = 1.0;
  //for(var t=0; t<100; t++)
  for(var t=0; t<1; t++)
  {
    TaxableAccountBeginValue[t] = 0;
    TaxableAccountCostBasisBeginValue[t] = 0;
    TaxableAccountTaxFreePercent[t] = 0;
    TaxDeferredAccountBeginValue[t] = 0;
    TaxFreeAccountBeginValue[t] = 0;
    TotalInvestmentAccountBeginValue[t] = 0;
  }

  //console.log("calling getInvestmentAccounts");
  var InvestmentAccounts = investments;
  if (debug)
  {
    InvestmentAccounts = getInvestmentAccounts(debug);
  }
  var InvestmentAccountOwner = [];
  var InvestmentAccountType = [];
  var InvestmentAccountDescription = [];
  var InvestmentAccountValue = [];
  var InvestmentAccountCostBasis = [];
  var TaxableAccountValue = [];
  var TaxDeferredAccountValue = [];
  var TaxFreeAccountValue = [];
  if (InvestmentAccounts.length > 0) 
  {
    InvestmentAccountOwner = new Array(InvestmentAccounts.length);
    InvestmentAccountType = new Array(InvestmentAccounts.length);
    InvestmentAccountDescription = new Array(InvestmentAccounts.length);
    InvestmentAccountValue = new Array(InvestmentAccounts.length);
    InvestmentAccountCostBasis = new Array(InvestmentAccounts.length);
    TaxableAccountValue = new Array(InvestmentAccounts.length);
    TaxDeferredAccountValue = new Array(InvestmentAccounts.length);
    TaxFreeAccountValue = new Array(InvestmentAccounts.length); 
  }

  for(var x=0; x<InvestmentAccounts.length; x++)
  {
    InvestmentAccountOwner[x] = InvestmentAccounts[x].account_owner;
    InvestmentAccountType[x] = InvestmentAccounts[x].taxable;
    InvestmentAccountDescription[x] = InvestmentAccounts[x].description;
    InvestmentAccountValue[x] = InvestmentAccounts[x].account_value;
    //InvestmentAccountCostBasis[x] = InvestmentAccounts[x].costbasis;

    if (InvestmentAccountType[x] === "Taxable")
    {
      TaxableAccountValue[x] = InvestmentAccountValue[x];
      InvestmentAccountCostBasis[x] = InvestmentAccounts[x].cost_basis;
      TaxDeferredAccountValue[x] = 0;
      TaxFreeAccountValue[x] = 0;
    }
    else
    {
      TaxableAccountValue[x] = 0;
      InvestmentAccountCostBasis[x] = 0;
      if (InvestmentAccountType[x] === "Tax-deferred")
      {
        TaxDeferredAccountValue[x] = InvestmentAccountValue[x];
        TaxFreeAccountValue[x] = 0;
      }
      else
      {
        TaxDeferredAccountValue[x] = 0;
        TaxFreeAccountValue[x] = InvestmentAccountValue[x];
      }
    }
  }

  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  //for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  for(var t=0; t<1; t++)
  {
    for(var x=0; x<InvestmentAccounts.length; x++)
    {
      TaxableAccountBeginValue[t] += TaxableAccountValue[x];
      TaxableAccountCostBasisBeginValue[t] += InvestmentAccountCostBasis[x];
      TaxDeferredAccountBeginValue[t] += TaxDeferredAccountValue[x];
      TaxFreeAccountBeginValue[t] += TaxFreeAccountValue[x];
    }
    //TaxableAccountTaxFreePercent[t-1] = TaxableAccountCostBasisBeginValue[t]/TaxableAccountBeginValue[t];
    if (t>0)
      TaxableAccountTaxFreePercent[t-1] = TaxableAccountCostBasisBeginValue[t]/TaxableAccountBeginValue[t];
    else 
      TaxableAccountTaxFreePercent0 = TaxableAccountCostBasisBeginValue[t]/TaxableAccountBeginValue[t];
    
    //console.log("TaxableAccountBeginValue[t] "+TaxableAccountBeginValue[t] 
    //  + " TaxDeferredAccountBeginValue[t] "+TaxDeferredAccountBeginValue[t]
    //  + " TaxFreeAccountBeginValue[t] "+TaxFreeAccountBeginValue[t]);
    TotalInvestmentAccountBeginValue[t] = TaxableAccountBeginValue[t] + TaxDeferredAccountBeginValue[t] + TaxFreeAccountBeginValue[t];
    //console.log("TotalInvestmentAccountBeginValue[t] "+ TotalInvestmentAccountBeginValue[t]);
  }

  // Page 14
  var SumIncomeNeeds = new Array(100);
  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  for(var t=0; t<100; t++)
  {
    SumIncomeNeeds[t] = 0;
  }

  var IncomeNeeds = expenses;
  if (debug)
  {
    IncomeNeeds = getIncomeNeeds(debug);
  }
  var IncomeNeedDescription = [];
  var IncomeNeedRangeType = [];
  var IncomeNeedName = [];
  var IncomeNeedBeginAge = [];
  var IncomeNeedEndAge = [];
  var IncomeNeedBeginYear = [];
  var IncomeNeedEndYear = [];
  var IncomeNeedDollars = [];
  var IncomeNeedInflationAdjust = [];
  var IncomeNeedInflationAdjustment = [];
  var IncomeNeedNominalDollars = [];
  if (IncomeNeeds.length > 0)
  {
    IncomeNeedDescription = new Array(IncomeNeeds.length);
    IncomeNeedRangeType = new Array(IncomeNeeds.length);
    IncomeNeedName = new Array(IncomeNeeds.length);
    IncomeNeedBeginAge = new Array(IncomeNeeds.length);
    IncomeNeedEndAge = new Array(IncomeNeeds.length);
    IncomeNeedBeginYear = new Array(IncomeNeeds.length);
    IncomeNeedEndYear = new Array(IncomeNeeds.length);
    IncomeNeedDollars = new Array(IncomeNeeds.length);
    IncomeNeedInflationAdjust = new Array(IncomeNeeds.length);
    IncomeNeedInflationAdjustment = new Array(IncomeNeeds.length);
    for(var x=0; x<IncomeNeeds.length; x++)
    {
      IncomeNeedNominalDollars[x] = new Array(100);
    }
  }

  for(var x=0; x<IncomeNeeds.length; x++)
  {
    IncomeNeedDescription[x] = IncomeNeeds[x].description;
    IncomeNeedRangeType[x] = IncomeNeeds[x].date_range_in;
    if (IncomeNeedRangeType[x] === "Age")
    {
      IncomeNeedName[x] = IncomeNeeds[x].expense_name;
      IncomeNeedBeginAge[x] = IncomeNeeds[x].date_range_begin;
      IncomeNeedEndAge[x] = IncomeNeeds[x].date_range_end;
      if (IncomeNeedName[x] === "Client")
      {
        IncomeNeedBeginYear[x] = IncomeNeedBeginAge[x] + IndividualBirthYear;
        IncomeNeedEndYear[x] = IncomeNeedEndAge[x] + IndividualBirthYear;
      }
      else
      {
        IncomeNeedBeginYear[x] = IncomeNeedBeginAge[x] + SpouseBirthYear;
        IncomeNeedEndYear[x] = IncomeNeedEndAge[x] + SpouseBirthYear;
      }
    }
    else
    {
      IncomeNeedBeginYear[x] = IncomeNeeds[x].date_range_begin;
      IncomeNeedEndYear[x] = IncomeNeeds[x].date_range_end;
    }

    IncomeNeedDollars[x] = IncomeNeeds[x].annual_need;
    IncomeNeedInflationAdjust[x] = IncomeNeeds[x].inflation;
    if (IncomeNeedInflationAdjust[x] === "Yes")
      IncomeNeedInflationAdjustment[x] = InflationExpectation;
    else
      IncomeNeedInflationAdjustment[x] = 0;

    //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
    for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
    {
      if (t >= IncomeNeedBeginYear[x]-CurrentYear && t <= IncomeNeedEndYear[x]-CurrentYear)
        IncomeNeedNominalDollars[x][t] = IncomeNeedDollars[x] * Math.pow(1+IncomeNeedInflationAdjustment[x], t);
      else
        IncomeNeedNominalDollars[x][t] = 0;
    }
  }

  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  {
    for(var x=0; x<IncomeNeeds.length; x++)
    {
      SumIncomeNeeds[t] += IncomeNeedNominalDollars[x][t];
    }

    console.log("SumIncomeNeeds[t] "+SumIncomeNeeds[t]);
  }

  // Page 16
  var SumOtherIncome = new Array(100);
  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  for(var t=0; t<100; t++)
  {
    SumOtherIncome[t] = 0;
  }

  var OtherIncome = other_income;
  if (debug)
  {
    OtherIncome = getOtherIncome(debug);
  }

  var OtherIncomeDescription = [];
  var OtherIncomeRangeType = [];
  var OtherIncomeName = [];
  var OtherIncomeBeginAge = [];
  var OtherIncomeEndAge = [];
  var OtherIncomeBeginYear = [];
  var OtherIncomeEndYear = [];
  var OtherIncomeDollars = [];
  var OtherIncomeInflationAdjust = [];
  var OtherIncomeInflationAdjustment = [];
  var OtherIncomeNominalDollars = [];
  if (OtherIncome.length > 0)
  {
    OtherIncomeDescription = new Array(OtherIncome.length);
    OtherIncomeRangeType = new Array(OtherIncome.length);
    OtherIncomeName = new Array(OtherIncome.length);
    OtherIncomeBeginAge = new Array(OtherIncome.length);
    OtherIncomeEndAge = new Array(OtherIncome.length);
    OtherIncomeBeginYear = new Array(OtherIncome.length);
    OtherIncomeEndYear = new Array(OtherIncome.length);
    OtherIncomeDollars = new Array(OtherIncome.length);
    OtherIncomeInflationAdjust = new Array(OtherIncome.length);
    OtherIncomeInflationAdjustment = new Array(OtherIncome.length);
    for(var x=0; x<OtherIncome.length; x++)
    {
      OtherIncomeNominalDollars[x] = new Array(100);
    }
  }
  for(var x=0; x<OtherIncome.length; x++)
  {
    OtherIncomeDescription[x] = OtherIncome[x].description;
    OtherIncomeRangeType[x] = OtherIncome[x].date_range_in;
    if (OtherIncomeRangeType[x] === "Age")
    {
      OtherIncomeName[x] = OtherIncome[x].income_name;
      OtherIncomeBeginAge[x] = OtherIncome[x].date_range_begin;
      OtherIncomeEndAge[x] = OtherIncome[x].date_range_end;
      if (OtherIncomeName[x] === "Client")
      {
        OtherIncomeBeginYear[x] = OtherIncomeBeginAge[x] + IndividualBirthYear;
        OtherIncomeEndYear[x] = OtherIncomeEndAge[x] + IndividualBirthYear;
      }
      else
      {
        OtherIncomeBeginYear[x] = OtherIncomeBeginAge[x] + SpouseBirthYear;
        OtherIncomeEndYear[x] = OtherIncomeEndAge[x] + SpouseBirthYear;
      }
    }
    else
    {
      OtherIncomeBeginYear[x] = OtherIncome[x].date_range_begin;
      OtherIncomeEndYear[x] = OtherIncome[x].date_range_end;
    }

    OtherIncomeDollars[x] = OtherIncome[x].annual_flow;
    OtherIncomeInflationAdjust[x] = OtherIncome[x].inflation;
    if (OtherIncomeInflationAdjust[x] === "Yes")
      OtherIncomeInflationAdjustment[x] = InflationExpectation;
    else
      OtherIncomeInflationAdjustment[x] = 0;

    //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
    for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
    {
      if (t >= OtherIncomeBeginYear[x]-CurrentYear && t <= OtherIncomeEndYear[x]-CurrentYear)
        OtherIncomeNominalDollars[x][t] = OtherIncomeDollars[x] * Math.pow(1+OtherIncomeInflationAdjustment[x], t);
      else
        OtherIncomeNominalDollars[x][t] = 0;
    }
  }

  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  {
    for(var x=0; x<OtherIncome.length; x++)
    {
      SumOtherIncome[t] += OtherIncomeNominalDollars[x][t];
    }

    console.log("SumOtherIncome[t] "+SumOtherIncome[t]);
  }

  // Page 19
  console.log("---------------- page 19 -----------------");
  var IndividualNominalWageIncome = new Array(100);
  var SpouseNominalWageIncome = new Array(100);
  var TaxableAccountYieldDollars = new Array(100);
  var NonInvestmentCashFlowIncome = new Array(100);
  var TaxableCapitalGains = new Array(100);
  var TaxableOrdinaryIncome = new Array(100);
  var NetCashFlow = new Array(100);
  var TotalFamilyIncome = new Array(100);
  var CapitalGainsTaxRate = new Array(100);
  var OrdinaryIncomeTaxRate = new Array(100);
  var CapitalGainsTaxDue = new Array(100);
  var OrdinaryIncomeTaxDue = new Array(100);
  var TotalTaxDue = new Array(100);

  var AdvisorFeeAUM = new Array(100);
  var AdvisorFee = new Array(100);
  var TotalAdvisorFee = new Array(100);
  var TaxDeferredAccountCashFlow = new Array(100);
  var TaxableAccountCashFlow = new Array(100);
  var TaxFreeAccountCashFlow = new Array(100);
  var TaxFreeAccountInvestableValue = new Array(100);
  var TaxFreeAccountAdvisorFee = new Array(100);
  var TaxFreeAccountNetInvestableValue = new Array(100);
  var TaxFreeAccountInvestmentReturn = new Array(100);
  var TaxFreeAccountEndValue = new Array(100);
  var TaxDeferredAccountInvestableValue = new Array(100);
  var TaxDeferredAccountAdvisorFee = new Array(100); 
  var TaxDeferredAccountNetInvestableValue = new Array(100); 
  var TaxDeferredAccountInvestmentReturn = new Array(100); 
  var TaxDeferredAccountEndValue = new Array(100);    
  var TaxableAccountPreTaxValue = new Array(100);
  var TaxableAccountPostTaxValue = new Array(100);
  var TaxableAccountInvestableValue = new Array(100);
  var TaxableAccountAdvisorFee = new Array(100);
  var TaxableAccountNetInvestableValue = new Array(100);
  var TaxableAccountInvestmentReturn = new Array(100);
  var TaxableAccountEndValue = new Array(100);
  var TaxableAccountCostBasisChange = new Array(100);
  var TaxableAccountCostBasisEndValue = new Array(100);
  //var TaxableAccountCostBasisBeginValue = new Array(100);
  var TaxDeferredAccountProvisionalTaxesDue = new Array(100);
  var TaxDeferredAccountProvisionalCashFlow = new Array(100);
  var TaxDeferredAccountPreTaxValue = new Array(100);
  var TotalInvestmentAccountNetInvestmentReturn = new Array(100);
  var TotalInvestmentAccountEndValue = new Array(100);
  var TotalInvestmentAccountCashFlows = new Array(100);
  var TotalInvestmentAccountPostTaxPercent = new Array(100);

  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  {
    console.log("***** "+t+" *****");
    //if (t < IndividualRetirementYear)
    if (t+CurrentYear < IndividualRetirementYear)
      IndividualNominalWageIncome[t] = IndividualWageIncome * Math.pow(1+InflationExpectation+IndividualWageGrowth, t);
    else
      IndividualNominalWageIncome[t] = 0;

    //if (t < SpouseRetirementYear)
    if (t+CurrentYear < SpouseRetirementYear)
      SpouseNominalWageIncome[t] = SpouseWageIncome * Math.pow(1+InflationExpectation+SpouseWageGrowth, t);
    else
      SpouseNominalWageIncome[t] = 0;

    TaxableAccountYieldDollars[t] = TaxableAccountBeginValue[t] * Yield;
    NonInvestmentCashFlowIncome[t] = IndividualNominalWageIncome[t] + SpouseNominalWageIncome[t]
              - SumSavingsTaxDeferred[t] + SumOtherIncome[t] + TaxableAccountYieldDollars[t];
  
    TaxableCapitalGains[t] = 0;
    TaxableOrdinaryIncome[t] = 0;
    NetCashFlow[t] = SumGrossSavings[t] - SumIncomeNeeds[t] + SumOtherIncome[t];

    console.log("IndividualNominalWageIncome "+IndividualNominalWageIncome[t]);
    console.log("SpouseNominalWageIncome "+ SpouseNominalWageIncome[t]);
    console.log("TaxableAccountBeginValue "+TaxableAccountBeginValue[t]);
    console.log("TaxableAccountYieldDollars "+TaxableAccountYieldDollars[t]);
    console.log("NonInvestmentCashFlowIncome "+NonInvestmentCashFlowIncome[t]);
    console.log("TaxDeferredAccountBeginValue "+TaxDeferredAccountBeginValue[t]);
    //console.log("TaxableAccountTaxFreePercent "+TaxableAccountTaxFreePercent[t]);
    console.log("NetCashFlow A "+NetCashFlow[t]);

    // Page 20
    if (NetCashFlow[t] < 0)
    {
      if (-NetCashFlow[t] >= TaxableAccountBeginValue[t])
      {
        console.log("tax case1");
        if (-NetCashFlow[t] >= TaxableAccountBeginValue[t]+TaxDeferredAccountBeginValue[t])
        {
          console.log("tax case1a");
          if (TaxableAccountBeginValue[t] == 0) 
          {
            TaxableCapitalGains[t] = 0;
            TaxableAccountTaxFreePercent[t-1] = 1.0;
          }
          else
          {
            //TaxableCapitalGains[t] = TaxableAccountBeginValue[t] * (1-TaxableAccountTaxFreePercent[t-1]);
            if (t>0)
              TaxableCapitalGains[t] = TaxableAccountBeginValue[t] * (1-TaxableAccountTaxFreePercent[t-1]);
            else
              TaxableCapitalGains[t] = TaxableAccountBeginValue[t] * (1-TaxableAccountTaxFreePercent0);
          }
          if (TaxableCapitalGains[t] > 0)
            TaxableOrdinaryIncome[t] = TaxDeferredAccountBeginValue[t];
          else
            TaxableOrdinaryIncome[t] = 0;
        }
        else
        {
          console.log("tax case1b");
          //TaxableCapitalGains[t] = TaxableAccountBeginValue[t] * (1-TaxableAccountTaxFreePercent[t-1]);
          if (t > 0)
            TaxableCapitalGains[t] = TaxableAccountBeginValue[t] * (1-TaxableAccountTaxFreePercent[t-1]);
          else
            TaxableCapitalGains[t] = TaxableAccountBeginValue[t] * (1-TaxableAccountTaxFreePercent0);
          
          if (TaxableCapitalGains[t] > 0)
            TaxableOrdinaryIncome[t] = -(NetCashFlow[t]) - TaxableAccountBeginValue[t];
          else
            TaxableOrdinaryIncome[t] = 0;
        }
      }
      else
      {
        console.log("tax case2 "+TaxableAccountTaxFreePercent0);
        //TaxableCapitalGains[t] = -NetCashFlow[t]*(1-TaxableAccountTaxFreePercent[t-1]);
        if (t > 0)
          TaxableCapitalGains[t] = -NetCashFlow[t]*(1-TaxableAccountTaxFreePercent[t-1]);
        else
          TaxableCapitalGains[t] = -NetCashFlow[t]*(1-TaxableAccountTaxFreePercent0);
      }
    }
    TotalFamilyIncome[t] = NonInvestmentCashFlowIncome[t] + TaxableCapitalGains[t] + TaxableOrdinaryIncome[t];

    console.log("TaxableOrdinaryIncome "+TaxableOrdinaryIncome[t]);
    //console.log("TaxableAccountTaxFreePercent "+TaxableAccountTaxFreePercent[t]);
    console.log("TaxableCapitalGains "+TaxableCapitalGains[t]);
    console.log("TotalFamilyIncome "+TotalFamilyIncome[t]);

    //if (FixedCapitalGainsTaxRate) 
    if (useTaxTables) 
    {
      CapitalGainsTaxRate[t] = FixedCapitalGainsTaxRate;
      OrdinaryIncomeTaxRate[t] = FixedOrdinaryIncomeTaxRate;
    }
    else
    {
      //if (TotalFamilyIncome[t] <= TaxBracket1 * Math.pow(1+InflationExpectation, t-CurrentYear))
      if (TotalFamilyIncome[t] <= TaxBracket1 * Math.pow(1+InflationExpectation, t))
      {
        CapitalGainsTaxRate[t] = CapitalGainsTaxRate1;
        OrdinaryIncomeTaxRate[t] = OrdinaryIncomeTaxRate1;
      }
      //else if (TotalFamilyIncome[t] <= TaxBracket2 * Math.pow(1+InflationExpectation, t-CurrentYear))
      else if (TotalFamilyIncome[t] <= TaxBracket2 * Math.pow(1+InflationExpectation, t))
      {
        CapitalGainsTaxRate[t] = CapitalGainsTaxRate2;
        OrdinaryIncomeTaxRate[t] = OrdinaryIncomeTaxRate2;
      }
      else
      {
        CapitalGainsTaxRate[t] = CapitalGainsTaxRate3;
        OrdinaryIncomeTaxRate[t] = OrdinaryIncomeTaxRate3;
      }
    }

    CapitalGainsTaxDue[t] = TaxableCapitalGains[t] * (-CapitalGainsTaxRate[t]);
    OrdinaryIncomeTaxDue[t] = (SumOtherIncome[t] + TaxableAccountYieldDollars[t] + TaxableOrdinaryIncome[t]) * (-OrdinaryIncomeTaxRate[t]);
    TotalTaxDue[t] = CapitalGainsTaxDue[t] + OrdinaryIncomeTaxDue[t];

    console.log("OrdinaryIncomeTaxRate[t] "+OrdinaryIncomeTaxRate[t]);
    console.log("CapitalGainsTaxRate "+CapitalGainsTaxRate[t]);
    console.log("TaxableAccountYieldDollars[t] "+TaxableAccountYieldDollars[t]);
    console.log("TaxableOrdinaryIncome[t] "+TaxableOrdinaryIncome[t]);
    console.log("SumOtherIncome[t] "+SumOtherIncome[t]);
    console.log("OrdinaryIncomeTaxDue[t] "+OrdinaryIncomeTaxDue[t]);
    console.log("CapitalGainsTaxDue[t] "+CapitalGainsTaxDue[t]);
    console.log("TotalTaxDue[t] "+TotalTaxDue[t]);
  //JAM}

  // Page 22
  console.log("page 22");

  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  //JAM for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  //JAM {
    AdvisorFeeAUM[t] = TotalInvestmentAccountBeginValue[t] + NetCashFlow[t] + TotalTaxDue[t];
    if (AdvisorFeeAUM[t] <= 0) 
    {
      AdvisorFeeAUM[t] = 0.0001;
      AdvisorFee[t] = 0.0001;
    }
    else if (AdvisorFeeAUM[t] <= AdvisorFeeAUM1)
      AdvisorFee[t] = -AdvisorFeeAUM[t] * AdvisorFeePercent1;
    else if (AdvisorFeeAUM[t] <= AdvisorFeeAUM2)
      AdvisorFee[t] = -AdvisorFeeAUM1*AdvisorFeePercent1 - (AdvisorFeeAUM[t]-AdvisorFeeAUM1)*AdvisorFeePercent2;
    else
      AdvisorFee[t] = -AdvisorFeeAUM1*AdvisorFeePercent1 - (AdvisorFeeAUM2-AdvisorFeeAUM1)*AdvisorFeePercent2
                -(AdvisorFeeAUM[t]-AdvisorFeeAUM2)*AdvisorFeePercent3;

    TotalAdvisorFee[t] = AdvisorFee[t] + AdvisorFeeFixed;
    

    console.log("TotalInvestmentAccountBeginValue "+TotalInvestmentAccountBeginValue[t]);
    console.log("TotalTaxDue "+TotalTaxDue[t]);
    console.log("AdvisorFeeAUM "+AdvisorFeeAUM[t]);
    console.log("AdvisorFee "+AdvisorFee[t])
    console.log("AdvisorFeeFixed "+AdvisorFeeFixed);
    console.log("TotalAdvisorFee "+TotalAdvisorFee[t]);
    console.log("NetCashFlow "+NetCashFlow[t]);
    
    if (NetCashFlow[t] >= 0)
    {
      // Track A - Page 23
      console.log("Track A");
      TaxDeferredAccountCashFlow[t] = 0;
      TaxableAccountCashFlow[t] = 0;
      if (NetCashFlow[t] >= SumSavingsTaxFree[t])
        TaxFreeAccountCashFlow[t] = SumSavingsTaxFree[t];
      else
      {
        TaxFreeAccountCashFlow[t] = NetCashFlow[t];
        TaxDeferredAccountCashFlow[t] = 0;
        TaxableAccountCashFlow[t] = 0;
      }

      TaxFreeAccountInvestableValue[t] = TaxFreeAccountBeginValue[t] + TaxFreeAccountCashFlow[t];
      TaxFreeAccountAdvisorFee[t] = TaxFreeAccountInvestableValue[t]/AdvisorFeeAUM[t] * TotalAdvisorFee[t];
      TaxFreeAccountNetInvestableValue[t] = TaxFreeAccountInvestableValue[t] + TaxFreeAccountAdvisorFee[t];
      TaxFreeAccountInvestmentReturn[t] = PortfolioReturnPercent[t] * TaxFreeAccountNetInvestableValue[t];
      TaxFreeAccountEndValue[t] = TaxFreeAccountNetInvestableValue[t] + TaxFreeAccountInvestmentReturn[t];
      TaxFreeAccountBeginValue[t+1] = TaxFreeAccountEndValue[t];

      console.log("AA TaxDeferredAccountCashFlow "+TaxDeferredAccountCashFlow[t]);

      // Page 24
      //RAB if (TaxDeferredAccountCashFlow[t] != 0)
      //RAB {
        if (NetCashFlow[t] - TaxFreeAccountAdvisorFee[t] >= SumSavingsTaxDeferred[t])
          TaxDeferredAccountCashFlow[t] = SumSavingsTaxDeferred[t];
        else
        {
          TaxDeferredAccountCashFlow[t] = NetCashFlow[t] - TaxFreeAccountCashFlow[t];
          TaxableAccountCashFlow[t] = 0;
        }
      //RAB}

      console.log("AA TaxDeferredAccountCashFlow "+TaxDeferredAccountCashFlow[t]);

      TaxDeferredAccountInvestableValue[t] = TaxDeferredAccountBeginValue[t] + TaxDeferredAccountCashFlow[t];
      TaxDeferredAccountAdvisorFee[t] = TaxDeferredAccountInvestableValue[t]/AdvisorFeeAUM[t] * TotalAdvisorFee[t];
      TaxDeferredAccountNetInvestableValue[t] = TaxDeferredAccountInvestableValue[t] + TaxDeferredAccountAdvisorFee[t];
      TaxDeferredAccountInvestmentReturn[t] = PortfolioReturnPercent[t] * TaxDeferredAccountNetInvestableValue[t];
      TaxDeferredAccountEndValue[t] = TaxDeferredAccountNetInvestableValue[t] + TaxDeferredAccountInvestmentReturn[t];      
      TaxDeferredAccountBeginValue[t+1] = TaxDeferredAccountEndValue[t];

      //RAB if (TaxableAccountCashFlow[t] == 0)
      //RAB TaxableAccountPreTaxValue[t] = TaxableAccountBeginValue[t];
      //RAB else
      //RAB {
        TaxableAccountCashFlow[t] = NetCashFlow[t] - TaxFreeAccountCashFlow[t] - TaxDeferredAccountCashFlow[t];
        TaxableAccountPreTaxValue[t] = TaxableAccountBeginValue[t] + TaxableAccountCashFlow[t];
      //RAB}

      console.log("AA TaxableAccountCashFlow "+TaxableAccountCashFlow[t]);

      TaxableAccountPostTaxValue[t] = TaxableAccountPreTaxValue[t];
      TaxableAccountInvestableValue[t] = TaxableAccountPostTaxValue[t];
      TaxableAccountAdvisorFee[t] = TaxableAccountInvestableValue[t] / AdvisorFeeAUM[t] * TotalAdvisorFee[t];
      TaxableAccountNetInvestableValue[t] = TaxableAccountInvestableValue[t] + TaxableAccountAdvisorFee[t];
      TaxableAccountInvestmentReturn[t] = PortfolioReturnPercent[t] * TaxableAccountNetInvestableValue[t];
      TaxableAccountEndValue[t] = TaxableAccountNetInvestableValue[t] + TaxableAccountInvestmentReturn[t];
      TaxableAccountBeginValue[t+1] = TaxableAccountEndValue[t];
      TaxableAccountCostBasisChange[t] = TaxableAccountCashFlow[t] + TaxableAccountYieldDollars[t];
      TaxableAccountCostBasisEndValue[t] = TaxableAccountCostBasisBeginValue[t] + TaxableAccountCostBasisChange[t];
      TaxableAccountCostBasisBeginValue[t+1] = TaxableAccountCostBasisEndValue[t];
      TaxableAccountTaxFreePercent[t] = TaxableAccountCostBasisEndValue[t] / TaxableAccountEndValue[t]; // RAB

      console.log("AA TaxableAccountCostBasisBeginValue "+TaxableAccountCostBasisBeginValue[t]);
      console.log("AA TaxableAccountCostBasisChange "+TaxableAccountCostBasisChange[t]);
      console.log("AA TaxableAccountCostBasisEndValue "+TaxableAccountCostBasisEndValue[t]);
      //console.log("AA TaxableAccountTaxFreePercent "+TaxableAccountTaxFreePercent[t]);
      console.log("AA TaxFreeAccountInvestableValue "+TaxFreeAccountInvestableValue[t]);
      console.log("AA TaxFreeAccountEndValue "+TaxFreeAccountEndValue[t]);
      console.log("AA TaxDeferredAccountInvestableValue "+TaxDeferredAccountInvestableValue[t]);
      console.log("AA TaxDeferredAccountEndValue "+TaxDeferredAccountEndValue[t]);
      console.log("AA TaxableAccountPreTaxValue "+TaxableAccountPreTaxValue[t]);
      console.log("AA TaxableAccountPostTaxValue "+TaxableAccountPostTaxValue[t]);
    }
    else
    {
      // Track B - Page 26
      console.log("Track B");
      if (-NetCashFlow[t] >= TaxableAccountBeginValue[t])
      {
        console.log("Track B case 1");
        TaxableAccountCashFlow[t] = -TaxableAccountBeginValue[t];
        TaxDeferredAccountProvisionalCashFlow[t] = NetCashFlow[t] - TaxableAccountCashFlow[t];
        TaxableAccountEndValue[t] = 0;
        TaxableAccountBeginValue[t+1] = 0;
        TaxDeferredAccountProvisionalTaxesDue[t] = TotalTaxDue[t];
        TaxableAccountCostBasisEndValue[t] = 0;
        TaxableAccountCostBasisBeginValue[t+1] = 0;

        TaxableAccountCostBasisChange[t] = -TaxableAccountCostBasisBeginValue[t]; // RAB
        //TaxFreeAccountInvestableValue[t] = 0; //RAB
        //TaxDeferredAccountEndValue[t] = 0; // RAB
        //TaxFreeAccountEndValue[t] = 0; // RAB

        TaxableAccountInvestmentReturn[t] = 0; // RAB
        TaxableAccountPreTaxValue[t] = 0; // RAB
        TaxableAccountPostTaxValue[t] = 0; // RAB
        TaxableAccountInvestableValue[t] = 0; // RAB
        TaxableAccountNetInvestableValue[t] = 0; // RAB
        TaxableAccountTaxFreePercent[t] = TaxableAccountCostBasisEndValue[t] / TaxableAccountEndValue[t];

        //TaxDeferredAccountInvestmentReturn[t] = 0; // RAB
        //TaxFreeAccountInvestmentReturn[t] = 0; // RAB
        TaxableAccountAdvisorFee[t] = 0; // RAB
        //TaxDeferredAccountAdvisorFee[t] = 0; //RAB
        //TaxFreeAccountAdvisorFee[t] = 0; // RAB
      }
      else
      {
        console.log("Track B case 2");
        TaxableAccountCashFlow[t] = NetCashFlow[t];
        TaxableAccountPreTaxValue[t] = TaxableAccountBeginValue[t] + TaxableAccountCashFlow[t];
        TaxDeferredAccountProvisionalCashFlow[t] = 0;

        console.log("BB Before TaxableAccountTaxFreePercent "+TaxableAccountTaxFreePercent[t]);

        
        if (TaxableAccountPreTaxValue[t] <= -TotalTaxDue[t])
        {
          console.log("Track B case2a");
          TaxDeferredAccountProvisionalTaxesDue[t] = TotalTaxDue[t] + TaxableAccountPreTaxValue[t];
          TaxableAccountEndValue[t] = 0;
          TaxableAccountBeginValue[t+1] = 0;
          TaxableAccountCostBasisEndValue[t] = 0;
          TaxableAccountCostBasisBeginValue[t+1] = 0;
          TaxableAccountPostTaxValue[t] = 0;
        }
        else
        {
          console.log("Track B case2b");

          TaxableAccountPostTaxValue[t] = TaxableAccountPreTaxValue[t] + TotalTaxDue[t];
          TaxableAccountInvestableValue[t] = TaxableAccountPostTaxValue[t];
          TaxableAccountAdvisorFee[t] = TaxableAccountInvestableValue[t]/AdvisorFeeAUM[t] * TotalAdvisorFee[t];
          TaxableAccountNetInvestableValue[t] = TaxableAccountInvestableValue[t] + TaxableAccountAdvisorFee[t];
          TaxableAccountInvestmentReturn[t] = PortfolioReturnPercent[t] * TaxableAccountNetInvestableValue[t];
          TaxableAccountEndValue[t] = TaxableAccountNetInvestableValue[t] + TaxableAccountInvestmentReturn[t];
          TaxableAccountBeginValue[t+1] = TaxableAccountEndValue[t];
          // Changed line below
          if (t > 0)
            TaxableAccountCostBasisChange[t] = TaxableAccountCashFlow[t]*TaxableAccountTaxFreePercent[t-1] + TaxableCapitalGains[t] + TaxableAccountYieldDollars[t];
          else
            TaxableAccountCostBasisChange[t] = TaxableAccountCashFlow[t]*TaxableAccountTaxFreePercent0 + TaxableCapitalGains[t] + TaxableAccountYieldDollars[t];

          TaxableAccountCostBasisEndValue[t] = TaxableAccountCostBasisBeginValue[t] + TaxableAccountCostBasisChange[t];
          TaxableAccountTaxFreePercent[t] = TaxableAccountCostBasisEndValue[t] / TaxableAccountEndValue[t];
          TaxableAccountCostBasisBeginValue[t+1] = TaxableAccountCostBasisEndValue[t];
          TaxDeferredAccountProvisionalTaxesDue[t] = 0; // RAB                          
        }
      }

      console.log("BB TaxableAccountCostBasisBeginValue "+TaxableAccountCostBasisBeginValue[t]);
      console.log("BB TaxableAccountCostBasisChange "+TaxableAccountCostBasisChange[t]);
      console.log("BB TaxableAccountCostBasisEndValue "+TaxableAccountCostBasisEndValue[t]);
      console.log("BB TaxableAccountTaxFreePercent "+TaxableAccountTaxFreePercent[t]);
      console.log("BB TaxableAccountPreTaxValue "+TaxableAccountPreTaxValue[t]);
      console.log("BB TotalTaxDue "+TotalTaxDue[t]);
      console.log("BB TotalAdvisorFee "+TotalAdvisorFee[t]);
      console.log("BB TaxableAccountPostTaxValue "+TaxableAccountPostTaxValue[t]);
      console.log("BB TaxableAccountCashFlow "+TaxableAccountCashFlow[t]);
      console.log("BB TaxableAccountInvestableValue "+TaxableAccountInvestableValue[t]);
      console.log("BB TaxableAccountAdvisorFee "+TaxableAccountAdvisorFee[t]);
      console.log("BB TaxableAccountNetInvestableValue "+TaxableAccountNetInvestableValue[t]);
      console.log("BB TaxDeferredAccountProvisionalCashFlow "+TaxDeferredAccountProvisionalCashFlow[t]);

        // Page 27
        if (-TaxDeferredAccountProvisionalCashFlow[t] >= TaxDeferredAccountBeginValue[t])
        {
          console.log("case 27a");
          TaxDeferredAccountCashFlow[t] = -(TaxDeferredAccountBeginValue[t]);
          TaxFreeAccountCashFlow[t] = TaxDeferredAccountProvisionalCashFlow[t] + TaxDeferredAccountProvisionalTaxesDue[t]
                          - TaxDeferredAccountCashFlow[t];
          TaxDeferredAccountEndValue[t] = 0;
          TaxDeferredAccountBeginValue[t+1] = 0;

          TaxDeferredAccountAdvisorFee[t] = 0; // RAB
          TaxDeferredAccountInvestableValue[t] = 0; // RAB
          TaxDeferredAccountNetInvestableValue[t] = 0; // RAB
          TaxDeferredAccountInvestmentReturn[t] = 0; // RAB
        }
        else
        {
          console.log("case 27b");
          console.log("BB TaxDeferredAccountProvisionalCashFlow "+TaxDeferredAccountProvisionalCashFlow[t]);
          TaxDeferredAccountCashFlow[t] = TaxDeferredAccountProvisionalCashFlow[t];
          TaxDeferredAccountPreTaxValue[t] = TaxDeferredAccountBeginValue[t] + TaxDeferredAccountCashFlow[t];
          if (-TaxDeferredAccountProvisionalTaxesDue[t] >= TaxDeferredAccountPreTaxValue[t])
          {
            console.log("case 27ba");
            TaxFreeAccountCashFlow[t] = TaxDeferredAccountProvisionalTaxesDue[t] + TaxDeferredAccountPreTaxValue[t];
            TaxDeferredAccountEndValue[t] = 0;
            TaxDeferredAccountBeginValue[t+1] = 0;
          }
          else
          {
            console.log("case 27bb");
            TaxFreeAccountCashFlow[t] = 0;
            TaxDeferredAccountInvestableValue[t] = TaxDeferredAccountBeginValue[t] + TaxDeferredAccountCashFlow[t]
                                + TaxDeferredAccountProvisionalTaxesDue[t];
            TaxDeferredAccountAdvisorFee[t] = TaxDeferredAccountInvestableValue[t]/AdvisorFeeAUM[t] * TotalAdvisorFee[t];
            TaxDeferredAccountNetInvestableValue[t] = TaxDeferredAccountInvestableValue[t] + TaxDeferredAccountAdvisorFee[t];
            TaxDeferredAccountInvestmentReturn[t] = PortfolioReturnPercent[t] * TaxDeferredAccountNetInvestableValue[t];
            TaxDeferredAccountEndValue[t] = TaxDeferredAccountNetInvestableValue[t] + TaxDeferredAccountInvestmentReturn[t];
            TaxDeferredAccountBeginValue[t+1] = TaxDeferredAccountEndValue[t];
          }
        }

        // Page 28
        TaxFreeAccountInvestableValue[t] = TaxFreeAccountBeginValue[t] + TaxFreeAccountCashFlow[t];
        if (TaxFreeAccountInvestableValue[t] <= 0)
        {
          TaxFreeAccountEndValue[t] = TaxFreeAccountInvestableValue[t];
          TaxFreeAccountBeginValue[t+1] = TaxFreeAccountEndValue[t];

          TaxFreeAccountInvestmentReturn[t] = 0; //RAB
          TaxFreeAccountAdvisorFee[t] = 0; // RAB
          TaxFreeAccountNetInvestableValue[t] = 0; //RAB
        }
        else
        {
          TaxFreeAccountAdvisorFee[t] = TaxFreeAccountInvestableValue[t]/AdvisorFeeAUM[t] * TotalAdvisorFee[t];
          TaxFreeAccountNetInvestableValue[t] = TaxFreeAccountInvestableValue[t] + TaxFreeAccountAdvisorFee[t];
          TaxFreeAccountInvestmentReturn[t] = PortfolioReturnPercent[t] * TaxFreeAccountNetInvestableValue[t];
          TaxFreeAccountEndValue[t] = TaxFreeAccountNetInvestableValue[t] + TaxFreeAccountInvestmentReturn[t];
          TaxFreeAccountBeginValue[t+1] = TaxFreeAccountEndValue[t];
        }


      

      console.log("BB TaxDeferredAccountProvisionalTaxesDue "+TaxDeferredAccountProvisionalTaxesDue[t]);
      console.log("BB TaxDeferredAccountInvestableValue "+TaxDeferredAccountInvestableValue[t]);
      console.log("BB TaxDeferredAccountNetInvestableValue "+TaxDeferredAccountNetInvestableValue[t]);
      console.log("BB TaxDeferredAccountEndValue "+TaxDeferredAccountEndValue[t]);
      console.log("BB TaxFreeAccountBeginValue "+TaxFreeAccountBeginValue[t]);
      console.log("BB TaxFreeAccountCashFlow "+TaxFreeAccountCashFlow[t]);
      console.log("BB TaxFreeAccountInvestableValue "+TaxFreeAccountInvestableValue[t]);

    }

    // Page 29
    console.log("TaxFreeAccountInvestableValue "+TaxFreeAccountInvestableValue[t]);
    console.log("TaxableAccountBeginValue[t] "+TaxableAccountBeginValue[t]);
    console.log("TaxDeferredAccountBeginValue[t] "+TaxDeferredAccountBeginValue[t]);
    console.log("TaxFreeAccountBeginValue[t] "+TaxFreeAccountBeginValue[t]);
    console.log("TaxableAccountEndValue[t] "+TaxableAccountEndValue[t]);
    console.log("TaxDeferredAccountEndValue[t] "+TaxDeferredAccountEndValue[t]);
    console.log("TaxFreeAccountEndValue[t] "+TaxFreeAccountEndValue[t]);
    console.log("TaxableAccountInvestmentReturn "+TaxableAccountInvestmentReturn[t]);
    console.log("TaxDeferredAccountInvestmentReturn "+TaxDeferredAccountInvestmentReturn[t]);
    console.log("TaxFreeAccountInvestmentReturn "+TaxFreeAccountInvestmentReturn[t]);
    TotalInvestmentAccountBeginValue[t] = TaxableAccountBeginValue[t] + TaxDeferredAccountBeginValue[t] + TaxFreeAccountBeginValue[t];
    TotalInvestmentAccountNetInvestmentReturn[t] = TaxableAccountInvestmentReturn[t] + TaxableAccountAdvisorFee[t]
                    + TaxDeferredAccountInvestmentReturn[t] + TaxDeferredAccountAdvisorFee[t]
                    + TaxFreeAccountInvestmentReturn[t] + TaxFreeAccountAdvisorFee[t];
    TotalInvestmentAccountEndValue[t] = TaxableAccountEndValue[t] + TaxDeferredAccountEndValue[t] + TaxFreeAccountEndValue[t];
    TotalInvestmentAccountCashFlows[t] = TotalInvestmentAccountEndValue[t] - TotalInvestmentAccountNetInvestmentReturn[t] - TotalInvestmentAccountBeginValue[t];
    TotalInvestmentAccountPostTaxPercent[t] = (TaxableAccountCostBasisEndValue[t]+TaxFreeAccountEndValue[t]) / TotalInvestmentAccountEndValue[t];

    console.log("TaxableAccountInvestmentReturn "+TaxableAccountInvestmentReturn[t]);
    console.log("TaxDeferredAccountInvestmentReturn "+TaxDeferredAccountInvestmentReturn[t]);
    console.log("TaxFreeAccountInvestmentReturn "+TaxFreeAccountInvestmentReturn[t]);
    console.log("TaxableAccountAdvisorFee "+TaxableAccountAdvisorFee[t]);
    console.log("TaxDeferredAccountAdvisorFee "+TaxDeferredAccountAdvisorFee[t]);
    console.log("TaxFreeAccountAdvisorFee "+TaxFreeAccountAdvisorFee[t]);
    console.log("TotalInvestmentAccountCashFlows "+TotalInvestmentAccountCashFlows[t]);
    console.log("TotalInvestmentAccountNetInvestmentReturn "+TotalInvestmentAccountNetInvestmentReturn[t]);

    //RAB
    TotalInvestmentAccountBeginValue[t+1] = TotalInvestmentAccountEndValue[t];
  }


  // Display the Results
  console.log("displaying");
  var tableRef = document.getElementById('cashflowtable');
  //for(var t=CurrentYear; t<TimeHorionYear+1; t++)
  //console.log("end "+TimeHorizonYear+1-CurrentYear);
  console.log("TimeHorizonYear "+TimeHorizonYear);
  console.log("CurrentYear "+CurrentYear);

  var cashflows = []; 

  for(var t=0; t<TimeHorizonYear+1-CurrentYear; t++)
  {
    //console.log("inserted row for "+t);

    // Insert a row in the table at the last row
    //var newRow = tableRef.insertRow(tableRef.rows.length);

    // Column 1 has Individual's age
    var tmp = { 'clientage': (t+CurrentYear-IndividualBirthYear),
                'spouseage': (t+CurrentYear-SpouseBirthYear),
                'year': (t+CurrentYear),
                'beginbalance': roundTwo(TotalInvestmentAccountBeginValue[t]),
                'netcashflow': roundTwo(TotalInvestmentAccountCashFlows[t]),
                'netinvreturn': roundTwo(TotalInvestmentAccountNetInvestmentReturn[t]),
                'endbalance': roundTwo(TotalInvestmentAccountEndValue[t]),
                'totaltaxes': roundTwo(TotalTaxDue[t]),
                'totalfees': roundTwo(TotalAdvisorFee[t]) };
    cashflows.push(tmp);
  }

  return cashflows;
}
